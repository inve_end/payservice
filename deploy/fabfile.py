# -*- coding: utf-8 -*-
import uuid
import getpass
from os import path, listdir
from distutils.util import strtobool

import yaml
from fabric.context_managers import lcd, cd
from fabric.contrib import console, files
from fabric.operations import local, put, sudo, run
from fabric.state import env
from fabric.colors import green
from fabric.api import task, parallel
from console import print_start, print_info, print_done, print_error
from fabric.contrib.files import exists, append


CUR_DIR = path.dirname(path.abspath(__file__))
PROJECT_ROOT = path.dirname(CUR_DIR)
SETTINGS = {}
# DIR name should endswith '/'
REMOTE_PROJECT_DIR = '/home/ubuntu/af-env/'
REMOTE_VIRENV = '/home/ubuntu/af-env/'
REMOTE_USER = 'ubuntu'
LOG_PATH = '/var/log/pay/'


@task
def dep(name='test'):
    """choose env name, for example: aliyun
    """
    global SETTINGS
    setting_file = path.join(CUR_DIR, '%s/deploy.yaml' % name)
    with open(setting_file) as f:
        SETTINGS = yaml.safe_load(f)

    env.warn_only = True
    env.colorize_errors = True
    env.key_filename = path.expanduser(
        SETTINGS['default'].get('key_filename', ''))
    env.need_confirm = SETTINGS['default'].get('need_confirm', True)
    if not env.key_filename and 'password' in SETTINGS['default']:
        env.password = SETTINGS['default']['password']


@task
def pro(name='payservice'):
    """choose project name, for example: payservice
    """
    if not SETTINGS:
        # load default env
        dep()
    login_names = SETTINGS.get('default', {})
    user = login_names.get(getpass.getuser()) if login_names.get(
        getpass.getuser()) else login_names.get('user')
    port = login_names.get('port') or 22
    env.project_name = name
    for host in SETTINGS['env'][name]:
        env.hosts.append(user + '@' + host + ':%s' % port)

def zsh_install():
    print_start("zsh_install start")
    sudo("apt-get install -y zsh curl git libgeoip1 libgeoip-dev geoip-bin")
    sudo("chsh ubuntu -s /usr/bin/zsh")
    if exists("~/.oh-my-zsh"):
        print_info("Oh-my-zsh exists, skip installing Oh-my-zsh.")
    else:
        run("git clone git://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh")
        sudo("cp -r  .oh-my-zsh /home/ubuntu/")
    put(CUR_DIR + "/zshrc.in", "/home/ubuntu/.zshrc", use_sudo=True)
    print_done('zsh_install end')


def install_nginx():
    print_start("Setting up Nginx")
    if not exists("/usr/sbin/nginx"):
        sudo("apt-get update")
        sudo("apt-get install -y nginx")
    print_info("Updating nginx config file")
    put(CUR_DIR + "/pay.nginx.conf", "/etc/nginx/conf.d/pay.nginx.conf", use_sudo=True)
    sudo("service nginx restart")
    print_done("Setting up nginx done.")


def install_mysql():
    print_start("Installing Mysql")
    if exists("/usr/sbin/mysqld"):
        print_info("Mysql exists, skip installing Mysql.")
    else:
        sudo("apt-get update")
        sudo("apt-get install -y mysql-server")
    print_done("Installing Mysql done.")


def install_redis():
    print_start("Installing Redis")
    if exists("/usr/local/bin/redis-server"):
        print_info("Redis exists, skip installing Redis.")
    else:
        run("curl -O http://download.redis.io/releases/redis-3.2.0.tar.gz")
        run("tar zxf redis-3.2.0.tar.gz")
        with cd("redis-3.2.0"):
            run("make")
            sudo("make install")
        put(CUR_DIR + "/redis.conf", "/etc/redis.conf", use_sudo=True)
        put(CUR_DIR + "/redis.service", "/etc/systemd/system/redis.service", use_sudo=True)
        sudo("mkdir -p /var/log/redis/")
        sudo("mkdir -p /var/lib/redis/")
        sudo("adduser --system --group --no-create-home redis")
        sudo("chown redis:redis -R /var/log/redis/")
        sudo("chown redis:redis -R /var/lib/redis/")
        sudo("systemctl start redis")
        sudo("systemctl status redis")
    print_done("Installing Redis done.")


def setup_supervisor():
    print_start("Setting up Supervisor")
    if exists("/usr/bin/supervisord"):
        print_info("Supervisor exists, skip installing Supervisor.")
    else:
        sudo("apt-get update")
        sudo("apt-get install -y supervisor")
        sudo("chmod 777 -R /var/log/supervisor/")
    print_start("Configuring Supervisor")
    print_info("update supervisor config file")
    put(CUR_DIR + "/pay.supervisor.conf", "/etc/supervisor/conf.d/pay.conf", use_sudo=True)
    sudo("supervisorctl update")
    print_done("Setting up Supervisor done.")


@task
def envinstall(is_restart=True):
    print_start("envinstall start.")
    if not confirm('install dependency'):
        return
    dependencies = SETTINGS['dependency'].get(env.project_name, [])
    for dependency in dependencies:
        if isinstance(dependency, dict):
            for k, v in dependency.iteritems():
                sudo('%s %s' % (k, ' '.join(v)))
        elif isinstance(dependency, basestring):
            sudo(dependency)

    if not files.exists('/home/ubuntu', use_sudo=True):
        sudo('useradd ubuntu -m -g admin')
    zsh_install()
    # install_mysql()
    install_nginx()
    setup_supervisor()
    install_redis()
    # if 'pip install virtualenv' in dependencies:
    #     sudo('chown -R %s /home/%s/.cache/pip' % (REMOTE_USER, REMOTE_USER))
    #     if not files.exists('%sbin/python' % REMOTE_VIRENV):
    #         sudo('virtualenv --no-site-packages %s' % REMOTE_VIRENV)
    #         sudo('chown -R %s %s' % (REMOTE_USER, REMOTE_VIRENV))
    #     put('%s/requirement.txt' % CUR_DIR, '/tmp/requirement.txt')
    #     sudo('source %s/bin/activate && pip install -r /tmp/requirement.txt' % (
    #         REMOTE_VIRENV,))
    #     run('rm /tmp/requirement.txt')

    sudo('mkdir -p %s' % LOG_PATH)
    sudo('chown -R %s %s' % (REMOTE_USER, LOG_PATH))
    # add uwsgi required system config
    sudo("sysctl -w net.core.somaxconn=4096")
    append("/etc/sysctl.conf", "net.core.somaxconn=4096", use_sudo=True)
    print_done("envinstall done.")


@task
#@parallel
def deploy(is_restart=True):
    if not confirm('deploy'):
        return

    is_restart = bool(strtobool(str(is_restart)))
    temp_folder = '/tmp/' + str(uuid.uuid4())
    r_temp_folder = '/tmp/' + str(uuid.uuid4())
    local('mkdir %s' % temp_folder)
    project_path = find_path(env.project_name)
    local('cp -r %s %s' % (project_path, temp_folder))

    package_dir = path.join(temp_folder, env.project_name)
    with lcd(package_dir):
        special_env_dct = SETTINGS['env'].get(env.project_name, {}).get(
            env.host)
        for binfile, commands in special_env_dct.iteritems():
            for command in commands:
                if binfile == 'bash' or binfile == 'shell':
                    local(command)
                else:
                    local('%s %s' %(binfile, command))

    with lcd(temp_folder):
        local('tar cf {0}.tar.gz --exclude "*.pyc" --exclude=".git" {0}'.format(
            env.project_name))
        run("mkdir -p %s" % r_temp_folder)
        put('%s.tar.gz' % package_dir, r_temp_folder)

    with cd(r_temp_folder):
        run("tar xf %s.tar.gz" % env.project_name)
        sudo('rm -r %s/%s-backup' % (REMOTE_PROJECT_DIR, env.project_name))
        sudo("mv {0}/{1} {0}/{1}-backup".format(
            REMOTE_PROJECT_DIR, env.project_name))
        sudo("mv %s %s" % (env.project_name, REMOTE_PROJECT_DIR))
        sudo('chown -R %s %s' % (REMOTE_USER, REMOTE_PROJECT_DIR))
    sudo('rm -r %s' % r_temp_folder)
    local('rm -rf %s' % temp_folder)
    restart(env.project_name, is_restart)
    print env.project_name
    print green('deploy %s@%s done' % (env.project_name, env.host))


@task
#@parallel
def restore():
    sudo('rm -r %s/%s' % (REMOTE_PROJECT_DIR, env.project_name))
    sudo('mv {0}/{1}-backup {0}/{1}'.format(
        REMOTE_PROJECT_DIR, env.project_name))
    restart(env.project_name)
    print green('restore done')


def confirm(task_name):
    if env.need_confirm:
        if not console.confirm(
                'Are you sure you want to %s' % task_name, default=True):
            return False
    return True


def find_path(project_name):
    current_dir = PROJECT_ROOT
    while current_dir != '/':
        for f in listdir(current_dir):
            if f == project_name and path.isdir(path.join(current_dir, f)):
                return path.join(current_dir, f)
        current_dir = path.dirname(current_dir)
    raise Exception('source folder not found!')


def restart(project_name, is_restart=True):
    if project_name == 'payservice':
        # pass
        sudo('''kill `ps -ef|grep payservice |grep -v grep|awk '{print $2}' `''')
