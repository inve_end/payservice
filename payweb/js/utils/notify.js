define(function (require, exports, module) {
    'use strict';
    var $ = require('jquery'),
        PNotify = require('pnotify');
    PNotify.prototype.options.styling = "bootstrap3";
    var Stacks = {
        stack_top_right: {
            "dir1": "down",
            "dir2": "left",
            "push": "top",
            "spacing1": 10,
            "spacing2": 10
        },
        stack_top_left: {
            "dir1": "down",
            "dir2": "right",
            "push": "top",
            "spacing1": 10,
            "spacing2": 10
        },
        stack_bottom_left: {
            "dir1": "right",
            "dir2": "up",
            "push": "top",
            "spacing1": 10,
            "spacing2": 10
        },
        stack_bottom_right: {
            "dir1": "left",
            "dir2": "up",
            "push": "top",
            "spacing1": 10,
            "spacing2": 10
        },
        stack_bar_top: {
            "dir1": "down",
            "dir2": "right",
            "push": "top",
            "spacing1": 0,
            "spacing2": 0
        },
        stack_bar_bottom: {
            "dir1": "up",
            "dir2": "right",
            "spacing1": 0,
            "spacing2": 0
        },
        stack_context: {
            "dir1": "down",
            "dir2": "left",
            "context": $("#stack-context")
        }
    };
    var findWidth = function (noteStack) {
        if (noteStack == "stack_bar_top") {
            return "100%";
        }
        if (noteStack == "stack_bar_bottom") {
            return "70%";
        } else {
            return "290px";
        }
    };
    var createNotify = function (title, text, noteStyle, noteStack) {
        return new PNotify({
            title: title,
            text: text,
            addclass: noteStack,
            type: noteStyle,
            stack: Stacks[noteStack],
            width: findWidth(noteStack),
            delay: 1400
        });
    };
    exports.notifyResp = function (data) {
        var message = "服务器错误！";
        data = data.responseJSON;
        if (data != null) {
            message = data.msg || ERROR_CODE[data.status] || message;
        }
        return createNotify("警告", "操作失败！" + message, "warning", "stack_bottom_right");
    };
    exports.success = function (title, text) {
        return createNotify(title || "提示", text || "操作成功！", 'success', 'stack_bottom_right');
    };
    exports.warning = function (title, text) {
        return createNotify(title || "错误", text || "操作失败！", 'warning', 'stack_bottom_right');
    };
    exports.error = function (title, text) {
        return createNotify(title || "警告", text || "出现错误", 'danger', 'stack_bottom_right');
    }
});