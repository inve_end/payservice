define(function (require, exports, module) {
    'use strict';
    var moment = require('moment');
    var DATETIME_FORMAT = "YYYY/MM/DD HH:mm:ss";
    exports.today = function(){
        return moment().format('YYYY-MM-DD');
    };
    exports.todayTS = function(){
        var today = exports.today();
        return moment(today, 'YYYY-MM-DD').unix();
    };
    exports.now = function (format) {
        if (format) {
            return moment().format(format);
        }
        return moment();
    };
    exports.yesterday = function () {
        return moment().add(-1, 'days').format('YYYY-MM-DD');
    };
    exports.tomorrow = function(){
        return moment().add(1, 'days').format('YYYY-MM-DD');
    };
    exports.toDate = function (unix_timestamp, format) {
        var unix_timestamp = parseInt(unix_timestamp, 10);
        format = format || this.DATETIME_FORMAT;
        return moment(unix_timestamp).format(format);
    };
    exports.fromDate = function (dtstring, format) {
        format = format || this.DATETIME_FORMAT;
        return moment(dtstring, format);
    };
});