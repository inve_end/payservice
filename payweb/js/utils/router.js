define(function (require, exports, module) {
    'use strict';
    var utils = require('js/utils/tools'),
        tz = require('js/utils/tz');
    var prepareSearch = function (key, reflush) {
        if (key == null) key = utils.getCurrentKey();
        var align = localStorage.getItem(key + '_align') || 'c';
        utils.setrs(align);

        if (reflush) {
            switch (key) {
                case "op/template":
                    utils.saveSearched(key, {
                        status: 1,
                        type: -1
                    });
                    break;
                case "order":
                    utils.saveSearched(key, {
                        status: 5,
                    });
                    break;
                case "goods":
                    utils.saveSearched(key, {
                        category_id: [],
                        source_type: [],
                        brand_id: [],
                    });
                    break;
                case "op/show":
                    utils.saveSearched(key, {
                        status: 2
                    });
                    break;
                case "order/ship":
                    utils.saveSearched(key, {
                        status: [0],
                        send_type: [],
                        category_id: [],
                        brand_id: [],
                        source_type: []
                    });
                    break;
                case "stats/activity":
                case "stats/order":
                case "stats/pay":
                default:
                    utils.saveSearched(key, {});
                    break;
            }
        }
    };
    exports.prepareSearch = prepareSearch;
    exports.parseQs = function (qs, defaultOrder, defaultSize) {
        var params = utils.parseQueryString(qs);
        params.$page = params.$page ? parseInt(params.$page) : 1;
        params.$size = defaultSize || PAGE_SIZE;
        if (!params.$orderby) params.$orderby = defaultOrder || 'id';
        return params;
    };
    exports.errorPage = function () {
        window.location.href = '/404.html';
    };
    exports.commonList = function (key, qs, path, config) {
        var conf = config[key];
        if (conf == undefined) return this.errorPage();
        var params = exports.parseQs(qs, conf.order, conf.size);
        prepareSearch(null, params._reflush);
        delete params._reflush;
        require([path + key], function (view) {
            utils.loadTemplate(view.List, function () {
                var page = new view.List(params);
                page.render();
            });
        });
    };
    exports.commonDetail = function (key, id, path, config) {
        var conf = config[key];
        if (conf == undefined || conf.noDetail) return this.errorPage();
        utils.setrs('c');
        var params = {};
        if (id == 'add') {
            if (conf.forbidNew) return this.errorPage();
        } else {
            params.id = id;
        }
        require([path + key], function (view) {
            utils.loadTemplate(view.Detail, function () {
                var page = new view.Detail(params);
                page.render();
            });
        });
    };
});
