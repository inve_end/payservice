define(function (require, exports, module) {
    'use strict';
    var Backbone = require('backbone');
    exports.Model = Backbone.Model.extend({
        defaults: {},
        parse: function (response) {
            if (response.data) {
                return response.data;
            }
            return response;
        }
    });
    exports.Collection = Backbone.Collection.extend({
        total: 0,
        overview: {},
        parse: function (response) {
            this.total = response.data.total_count;
            this.overview = response.data.overview;
            return response.data.list;
        }
    });

});