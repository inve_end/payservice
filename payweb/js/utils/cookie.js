define(function (require, exports, module) {
    'use strict';
    var $ = require('jquery');
    exports.addCookie = function (name, value, expires) {
        var d = new Date();
        d.setTime(d.getTime() + (expires * 24 * 60 * 60 * 1000));
        var expiresStr = d.toGMTString();
        document.cookie = name + "=" + value + ";expires=" + expiresStr;
    };
    exports.getCookie = function (name) {
        var cookieName = name + "=";
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var c = cookies[i];
            while (c.charAt(0) == ' ') c = c.substring(1);
            if (c.indexOf(cookieName) != -1) return c.substring(cookieName.length, c.length);
        }
        return "";
    };
    exports.deleteCookie = function (name) {
        // 默认为当前的host
        var domain = arguments[1] ? arguments[1] : window.location.host;
        // 默认为当前的URL
        var path = arguments[2] ? arguments[2] : "/";
        //document.cookie = name + "=;domain=" + domain + ";path=" + path + ";expires=Thu, 01 Jan 1970 00:00:00 GMT";
        document.cookie = name + "=;path=" + path + ";expires=Thu, 01 Jan 1970 00:00:00 GMT";
    };
});