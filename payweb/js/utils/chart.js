define(function (require, exports, module) {
    'use strict';
    require('highcharts');

    exports.getHighChartPieOptions = function () {
        return {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            tooltip: {
                pointFormat: '{point.name}: <b>{point.percentage: .2f}%</b>'
            },
            title: {
                text: null,
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.y}',
                    }
                }
            },
            series: [{
                name: '占比',
                colorByPoint: true,
                data: []
            }]
        };
    };
    exports.getHighChartColumnOptions = function (stack) {
        var option = {
            chart: {
                type: 'column'
            },
            xAxis: {
                categories: [],
                crosshair: true,
            },
            yAxis: {
                min: 0,
                title: {
                    text: '数值'
                }
            },
            title: {
                text: null,
            },
            tooltip: {
                pointFormat: '{point.name}: <b>{point.y}</b>'
            },
            series: []
        };
        if (stack) {
            option.plotOptions = {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    }
                }
            };
        }
        return option;
    };
    exports.getHighChartLineOptions = function () {
        return {
            chart: {
                type: 'line'
            },
            xAxis: {
                categories: []
            },
            yAxis: {
                min: 0,
                title: {
                    text: '值'
                }
            },
            title: {
                text: null,
            },
            tooltip: {
                pointFormat: '{point.name}: <b>{point.y}</b>'
            },
            series: []
        };
    };
});