define(function (require, exports, module) {
    'use strict';
    var Backbone = require('backbone'),
        cookie = require('js/utils/cookie'),
        $ = require('jquery'),
        notify = require('js/utils/notify'),
        utils = require('js/utils/tools'),
        _ = require('underscore');

    var MainRouter = Backbone.Router.extend({
        initialize: function () {
            var userId = cookie.getCookie('pay_user_id');
            if (!userId) {
                window.location.href = '/user.html';
                return;
            }
            if (userId && USER_INFO.info == null) {
                $.get('/pay/admin/user/' + userId + '/').done(function (resp) {
                    $('#user-full-name').text(resp.data.nickname);
                    var role = resp.data.role;
                    var perm_role = resp.data.perm;
                    if (perm_role) {
                        perm_role = perm_role.split('|');
                    } else {
                        perm_role = [];
                    }
                    USER_INFO.info = resp.data;
                    if (role <= 2) {
                        $('#statsMenu').hide();
                        $('#reportMenu').hide();
                    }
                    if (role <= 1) {
                        $('#consoleMenu').hide();
                    }
                    // permission role init
                    _.each($("li"), function (ele){
                        var menu_id = ele.id;
                        if (menu_id && menu_id.startsWith("menu_")) {
                            var menu_attrs = menu_id.split('_');
                            var menu_val = menu_attrs[2];
                            if (role == 5 || (perm_role && perm_role.indexOf(menu_val) != -1)) {
                                $('#'+menu_id).show();
                                var parent = $('#'+menu_id).parents("li").eq(0);
                                parent.show();
                            } else {
                                $('#'+menu_id).hide();
                            }
                        }
                    });
                }).fail(function (data) {
                    notify.notifyResp(data);
                    window.location.href = '/user.html';
                });
            }
            $('body').on('keypress', '#sidebar_right', function (e) {
                e.stopImmediatePropagation();
                if (e.which == 13) $('#search').click();
            });
            $.ajaxSetup({
                cache: false
            });
        },
    });

    exports.init = function () {
        var routes = [];
        _.each(['console','op'], function (k) {
            routes.push('js/apps/' + k + '/router');
        });
        require(routes, function () {
            _.each(arguments, function (r) {
                new r.Router();
            });
            new MainRouter();
            Backbone.history.start();
        });
    };
});
