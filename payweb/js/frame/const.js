define([], function () {
    'use strict';
    //全局常量
    window.USER_INFO = {};

    window.PAGE_SIZE = 30;

    window.PRICE_UNITS = {
        0: 'RMB',
        1: 'USD'
    };

    window.SHOW_STATUS = {
        "1": "待晒单",
        "2": "待审核",
        "4": "已通过",
        "8": "未通过",
        "16": "预审核",
    };

    window.ERROR_CODE = {
        2: '参数错误',
        3: '需要HTTPS',
        4: '数据错误',
        5: '数据库错误',
        6: '缓存错误',
        101: '用户不存在',
        102: '密码错误',
        103: '验证码错误',
        104: '账户重复',
        105: 'TOKEN过期',
        106: '用户未激活',
        107: '权限不足',
        201: '商品库存不足',
    };

    window.ACTIVITY_STATUS = {
        0: '准备开始',
        1: '进行中',
        2: '待开奖',
        4: '已开奖',
        8: '已过期',
        16: '已暂停',
    };

    window.COUNTRY_CODE = {
        'cn': '中国',
        'vn': '越南'
    };

    window.DEVICE_TYPE = {
        '1': 'Android',
        '2': 'iOS',
        '4': 'wap',
        // '8': 'web',
        '16': 'iOS Pro',
        '32': 'iOS HD',
    };

    window.IOS_CHN = ['ios_pro', 'ios_hd', 'ios']
    window.NOT_ANDROID_CHN = ['ios_pro', 'ios_hd', 'ios']

    window.USER_TYPE = {
        '1': '新用户',
        '2': '次日用户',
        '3': '周用户',
        '4': '老用户',
    };

    window.MCH_TYPE = {
        6001015: '幸运彩票',
        6001000: '万豪彩票',
        6001011: '电玩城-球球斗地主',
    }

    window.CHANNEL_TYPE = {
//        11: '爱贝wap',
//        13: '威付通支付宝wap',
//        18: '中信支付宝',
//        21: '明天云支付宝',
//        24: '布优云',
//        26: '中信微信',
//        29: 'OPENEPAY',
//        30: 'PAYTECH',
        31: 'YOUPAY',
//        32: 'TRPAY',
        34: '水滴支付',
        35: '畅付wap',
        36: '全球支付wap',
        40: '如意wap',
//        41: 'PAYTECH new',
        42: 'DR支付',
//        43: '集团支付',
//        44: '通和宝',
//        46: '麻袋支付',
//        48: '贝付宝',
        49: '维思支付',
//        50: '易爱支付',
//        51: '开联支付',
        52: '多通支付',
//        53: '口袋支付',
        54: '信付通支付',
        55: '君晟支付',
        56: 'BOPAY支付',
        57: 'KVPAY支付',
//        58: 'YHXPAY支付',
//        59: '沂尚支付',
//        60: '波音支付',
//        61: '富盈宝支付',
        62: '畅支付',
//        63: '自有支付宝支付',
        64: '艾尚支付',
//        65: '众宝通支付',
        66: '优聚合支付',
        67: 'E时代支付',
//        68: '新快钱支付',
//        69: '玖赢支付',
//        70: '汇合支付',
        71: '泽圣支付',
//        72: '创云微支付',
        73: '国福支付',
        74: '同乐支付',
//        75: '简单支付',
        76: '豆豆支付',
//        77: '零点支付',
//        78: '520E支付',
        79: '中付宝支付',
//        80: '西部支付wap',
        81: '迅联保支付wap',
//        82: '信付宝支付wap',
        83: '微派支付wap',
//        84: '摩宝支付wap',
        85: '永福通支付wap',
//        86: '铂富通支付wap',
        87: '亿付宝支付wap',
        88: '聚成支付wap',
        89: '汇天支付wap',
        90: '集团点卡支付',
        91: '乐天付',
//        92: '微宝付',
//        93: '汇宝通支付',
        94: '金阳支付',
//        95: '联赢支付',
        96: '水滴扫码支付宝',
        97: '天机支付',
        98: '侨首贸易支付',
        99: '拼多宝支付',
        100: '宜安通支付',
        104: '瑞云支付',
        105: '百盛支付',
        106: '水滴支付银联',
        107: '环讯支付',
        108: '鸿创支付',
        109: '海富支付',
        110: '汇银支付',
        111: '集团闪付2支付',
        112: '汽车贸易支付',
        113: '汇银通支付',
        114: '信捷支付',
        115: '瑞宝支付',
        116: '御投支付',
        117: '讯收宝支付',
        118: '随身智付',
        119: '3V支付',
        120: '贝富支付',
        121: '讯支付',
        122: '梨支付',
        123: '掌迅支付',
//        124: '讯支付1',
        125: '闪亿付',
//        126: '安付宝',
        127: 'E时代V2.0',
        128: '蛋蛋支付',
        129: '通达支付',
        130: '速付支付',
        131: '荔滔博支付',
        132: '新闪付2支付',
        133: '合乐支付',
        134: '凯瑞支付',
        135: '鑫支付',
        136: '闪付3',
        137: '信客云支付',
        138: '万利汇支付',
        139: '多多支付',
        140: '支付通支付',
        141: '乐智付',
        142: '国付通',
        143: '亿惠付',
        144: '广通付',
        145: '易智付',
        146: '365商城',
        147: '悍付',
        148: '新蛋蛋支付',
        149: '密商支付',
        150: '讯联支付',
        151: '博士支付',
        152: '智付平方支付',
        153: '直支付',
        154: '腾支付',
    };

    window.OPEN_TYPE = ['sdk', 'html', 'app_url', 'sys_url', 'sys_html']

    window.SERVICE_NAME = ['wechat', 'alipay', 'unionpay', 'qq', 'jd', 'recharge_card', 'test']

    window.CHANNEL_STATUS = {
        0: '不可用',
        1: '可用',
    }

    window.PAY_STATUS = {
        0: '未支付',
        1: '支付失败',
        2: '支付成功',
    }

    window.PAY_NOTIFY_STATUS = {
        0: '未回调',
        1: '回调失败',
        2: '回调成功',
    }

    window.WITHDRAW_STATUS = {
        0: '未兑换',
        1: '兑换失败',
        2: '兑换成功',
    }

    window.WITHDRAW_NOTIFY_STATUS = {
        0: '未回调',
        1: '回调失败',
        2: '回调成功',
    }

    window.USER_ROLE = {
        0: "未激活/黑名单用户",
        1: "一般用户",
        2: "运营人员",
        3: "高级运营",
        4: "运营主管",
        5: "开发者",
    };

    window.PERMISSION_ROLE = {
        101: "商户管理",
        201: "支付通道数据统计",
        202: "支付通道管理",
        203: "支付策略管理",
        204: "支付订单管理",
        205: "MG支付通道数据统计",
        301: "下分通道管理",
        302: "下分订单管理",
        1201: "用户管理",
        1202: "权限管理",
        1203: "操作记录"
    }

    window.TRANSACTION_TYPE = {
        1: "充值",
        2: "系统奖励",
        4: "余额购买",
        8: "退款"
    }

    window.TRANSACTION_STATUS = {
        0: "进行中",
        1: "已完成",
        2: "失败"
    }

    window.CALLBACK_STATUS = {
        0: "未召回",
        1: "已电话",
        2: "已短信",
        4: "已发红包",
        8: "已召回"
    }

    window.RED_ENVELOPE_STATUS = {
        0: "待兑换",
        1: "兑换失败",
        2: "兑换成功"
    }

    window.RESOURCE_DICT = {
        'login': '登陆',
        'single_mch': '商户管理',
        'signle_channel': '渠道管理',
    }

    window.PERM_DICT = {
        0: '无',
        1: '读',
        2: '写'
    }
    window.ACTION_DICT = {
        1: '创建',
        2: '更新',
        3: '删除'
    }

    window.MISSED_TYPE = {
        0: '大客户',
        1: '新增'
    }

    window.CALLBACK_STATUS = {
        0: '未召回',
        1: '已电话',
        2: '已短信',
        4: '已发红包',
        8: '已召回',
    }

    window.VIP_TYPE = {
        0: '周榜',
        1: '半月榜',
        2: '月榜'
    }

});
