define(function (require, exports, module) {
    'use strict';
    var $ = require('jquery'),
        _ = require('underscore'),
        Backbone = require('backbone'),
        notify = require('js/utils/notify'),
        models = require('js/apps/console/models'),
        opModelCls = require('js/apps/op/models'),
        utils = require('js/utils/tools'),
        app = Backbone.history;

    require('multiselect');
    require('confirm');

    var UserListView = Backbone.View.extend({
        tagName: "div",
        events: {
            'click #search': 'doSearch',
            'click a.onClickDelete': 'toDelete',
        },
        initialize: function (options) {
            this.options = options;
            this.collection = new models.UserCollection();
            this.collection.bind('change reset remove', this.renderWithData, this);
        },
        render: function () {
            this.$el.html("");
            $(window).scrollTop(0);
            return this;
        },
        toDelete: function (e) {
            utils.deleteItem(e, this.collection);
        },
        doSearch: function () {
            var options = {},
                searched = {};
            searched.email = $('#searchEmail').val();
            searched.role = parseInt($('#searchRole').val());
            if (searched.role != -1) {
                options.role = searched.role;
            }
            if (searched.email) {
                options.email = searched.email;
            }
            utils.saveSearched('console/user', searched);
            app.navigate(utils.composeQueryString('#console/user/', options), {
                trigger: true
            });
        },
        renderWithData: function () {
            this.$el.html(this.template({
                items: this.collection.toJSON(),
                searched: utils.getSearched('console/user'),
                info: USER_INFO.info,
            }));
            var that = this;
            utils.getPaginator(that.options, this.collection.total, '#console/user/');
            return this;
        },
        load: function () {
            this.collection.fetch({
                reset: true,
                data: this.options,
                error: function (c, r, o) {
                    notify.notifyResp(r);
                    $('#content').append('<h4 class="text-muted">无数据</h4>');
                },
            });
        }
    });
    exports.List = Backbone.View.extend({
        el: "#content_wrapper",
        template: "console/UserList.html",
        initialize: function (options) {
            this.options = options;
        },
        render: function () {
            var view = new UserListView(this.options);
            view.template = this.template;

            this.$el.empty();
            this.$el.append(view.render().el);
            view.load();

            return this;
        }
    });
    var UserDetailView = Backbone.View.extend({
        tagName: "div",
        className: "panel-body pn",
        events: {
            'click #userSave': 'toSave'
        },
        initialize: function (options) {
            this.model = new models.User();
            this.model.set('id', options.id);
            this.model.bind('change reset', this.renderWithData, this);
        },
        render: function () {
            this.$el.html("");
            $(window).scrollTop(0);
            return this;
        },
        toSave: function (e) {
            e.preventDefault();
            var permVal = $('#inputPerm').val().join('|');
            var attrs = {
                'nickname': $('#inputNickname').val(),
                'role': parseInt($('#inputRole').val()),
                'perm': permVal
            };
            this.model.save(attrs, {
                patch: true,
                success: function (model, response) {
                    notify.success('提示', '保存成功！');
                    setTimeout(function () {
                        history.back();
                    }, 500);
                },
                error: function (model, response) {
                    notify.notifyResp(response);
                }
            });

            return false;
        },
        renderWithData: function () {
            this.$el.html(this.template({
                info: this.model.toJSON(),
                role: USER_INFO.info.role,
                perm: USER_INFO.info.perm
            }));
            utils.multiselect('.select');
            return this;
        },
        load: function () {
            this.model.fetch({
                reset: true
            });
        }
    });
    exports.Detail = Backbone.View.extend({
        el: "#content_wrapper",
        template: "console/UserDetail.html",
        initialize: function (id) {
            this.id = id;
        },
        render: function () {
            var view = new UserDetailView(this.id);
            view.template = this.template;

            this.$el.empty();
            this.$el.append(view.render().el);
            view.load();

            return this;
        }
    });

});
