define(function (require, exports, module) {
    'use strict';
    var Backbone = require('backbone'),
        utils = require('js/utils/tools'),
        helper = require('js/utils/router'),
        path = 'js/apps/console/',
        config = {
            'user': {
                'forbidNew': true
            },
            'perm': {
                'forbidNew': true
            },
            'record': {
                'noDetail': true
            },
        };

    exports.Router = Backbone.Router.extend({
        routes: {
             "": "gotoIndex",
            "console/:key/(?*qs)": "consoleList",
            "console/:key/:id/": "consoleDetail"
        },
        gotoIndex: function(){
            this.consoleList('user');
        },
        consoleList: function (key, qs) {
            helper.commonList(key, qs, path, config);
        },
        consoleDetail: function(key, id){
            helper.commonDetail(key, id, path, config);
        },
    });
});