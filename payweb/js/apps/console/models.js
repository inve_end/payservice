define(function (require, exports, module) {
    'use strict';
    var base = require('js/utils/model'),
        _ = require('underscore');

    var User = base.Model.extend({
        defaults: {
            "nickname": "",
            "email": "",
            "role": 0,
            "created_at": "",
            "updated_at": ""
        },
        urlRoot: "/pay/admin/user/",
    });
    var UserCollection = base.Collection.extend({
        model: User,
        url: "/pay/admin/user/",
    });
    var Permission = base.Model.extend({
        defaults: {
            "url": "",
            "permission": "",
            "min_role": 0,
            "desc": "",
            "created_at": 1,
            "updated_at": ""
        },
        urlRoot: "/pay/admin/permission/",
    });
    var PermissionCollection = base.Collection.extend({
        model: Permission,
        url: "/pay/admin/permission/",
    });
    var Record = base.Model.extend({
        defaults: {
            "resource": "",
            "resource_id": "",
            "action": 0,
            "content": "",
            "created_at": 1,
        },
        urlRoot: "/pay/admin/record/",
    });
    var RecordCollection = base.Collection.extend({
        model: Record,
        url: "/pay/admin/record/",
    });

    module.exports = {
        User: User,
        UserCollection: UserCollection,
        Permission: Permission,
        PermissionCollection: PermissionCollection,
        Record: Record,
        RecordCollection: RecordCollection
    };

});
