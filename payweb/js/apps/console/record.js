define(function (require, exports, module) {
    'use strict';
    var $ = require('jquery'),
        _ = require('underscore'),
        Backbone = require('backbone'),
        notify = require('js/utils/notify'),
        models = require('js/apps/console/models'),
        utils = require('js/utils/tools'),
        app = Backbone.history;
    require('jquery-ui');
    require('datetimepicker');

    var RecordListView = Backbone.View.extend({
        tagName: "div",
        events: {
            'click #search': 'doSearch',
            'click .show-info': 'showInfo'
        },
        initialize: function (options) {
            this.options = options;
            this.collection = new models.RecordCollection();
            this.collection.bind('change reset remove', this.renderWithData, this);
        },
        render: function () {
            this.$el.html('');
            $(window).scrollTop(0);
            return this;
        },
        doSearch: function () {
            var options = utils.getAllInput('#sidebar_right');
            utils.saveSearched('console/record', options);
            app.navigate(utils.composeQueryString('#console/record/', options), {
                trigger: true
            });
        },
        showInfo: function (e) {
            e.preventDefault();
            var content = $(e.target).data('content'),
                rows = content.split(', '),
                $gen_html = $('<ul></ul>');
            _.each(rows, function (row) {
                if (row.length > 100) {
                    var rs = row.split(','),
                        row = rs.join('<br/>')
                }
                $gen_html.append($('<li></li>').append(row));
            });
            $.dialog({
                'title': '内容详情',
                'content': $gen_html.html(),
                'backgroundDismiss': true,
                'theme': 'bootstrap',
            })
            return false;
        },
        renderWithData: function () {
            this.$el.html(this.template({
                items: this.collection.toJSON(),
                searched: utils.getSearched('console/record'),
            }));
            var that = this;
            utils.renderTable('main-list', {
                $orderby: that.options.$orderby || 'id',
                sortCallback: function (field) {
                    that.options.$orderby = field;
                    that.options.$page = 1;
                    var newUrl = utils.composeQueryString('#console/record/', that.options);
                    app.navigate(newUrl, {
                        trigger: true
                    });
                }
            });
            utils.getPaginator(this.options, this.collection.total, '#console/record/');
            $('.date-input').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
            });
            return this;
        },
        load: function () {
            this.collection.fetch({
                reset: true,
                data: this.options,
                error: function (c, r, o) {
                    notify.notifyResp(r);
                },
            });
        }
    });
    exports.List = Backbone.View.extend({
        el: "#content_wrapper",
        template: "console/RecordList.html",
        initialize: function (options) {
            this.options = options;
        },
        render: function () {
            var view = new RecordListView(this.options);
            view.template = this.template;
            this.$el.empty().append(view.render().el);
            view.load();
            return this;
        }
    });

});