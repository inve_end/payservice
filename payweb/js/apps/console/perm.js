define(function (require, exports, module) {
    'use strict';
    var $ = require('jquery'),
        _ = require('underscore'),
        Backbone = require('backbone'),
        notify = require('js/utils/notify'),
        models = require('js/apps/console/models'),
        utils = require('js/utils/tools'),
        app = Backbone.history;

    var PermListView = Backbone.View.extend({
        tagName: "div",
        initialize: function (options) {
            this.options = options;
            this.collection = new models.PermissionCollection();
            this.collection.bind('change reset remove', this.renderWithData, this);
        },
        render: function () {
            this.$el.html("");
            $(window).scrollTop(0);
            return this;
        },
        renderWithData: function () {
            this.$el.html(this.template({
                items: this.collection.toJSON(),
            }));
            var that = this;
            utils.getPaginator(that.options, this.collection.total,
                '#console/perm/');
            return this;
        },
        load: function () {
            this.collection.fetch({
                reset: true,
                data: this.options,
                error: function (c, r, o) {
                    notify.notifyResp(r);
                    $('#content').append('<h4 class="text-muted">无数据</h4>');
                },
            });
        }
    });
    exports.List = Backbone.View.extend({
        el: "#content_wrapper",
        template: "console/PermissionList.html",
        initialize: function (options) {
            this.options = options;
        },
        render: function () {
            var view = new PermListView(this.options);
            view.template = this.template;

            this.$el.empty();
            this.$el.append(view.render().el);
            view.load();

            return this;
        }
    });
    var PermDetailView = Backbone.View.extend({
        tagName: "div",
        className: "panel-body pn",
        events: {
            'click #permSave': 'toSave'
        },
        initialize: function (options) {
            this.model = new models.Permission();
            this.model.set('id', options.id);
            this.model.bind('change reset', this.renderWithData, this);
        },
        render: function () {
            this.$el.html("");
            $(window).scrollTop(0);
            return this;
        },
        toSave: function (e) {
            e.preventDefault();
            var attrs = {
                'min_role': parseInt($('#inputMinRole').val()),
            };
            this.model.save(attrs, {
                patch: true,
                success: function (model, response) {
                    notify.success('提示', '保存成功！');
                    setTimeout(function () {
                        history.back();
                    }, 500);
                },
                error: function (model, response) {
                    notify.notifyResp(response);
                }
            });

            return false;
        },
        renderWithData: function () {
            this.$el.html(this.template({
                info: this.model.toJSON()
            }));
            return this;
        },
        load: function () {
            this.model.fetch({
                reset: true
            });
        }
    });
    exports.Detail = Backbone.View.extend({
        el: "#content_wrapper",
        template: "console/PermissionDetail.html",
        initialize: function (id) {
            this.id = id;
        },
        render: function () {
            var view = new PermDetailView(this.id);
            view.template = this.template;

            this.$el.empty();
            this.$el.append(view.render().el);
            view.load();

            return this;
        }
    });


});