define(function (require, exports, module) {
    'use strict';
    var Backbone = require('backbone'),
        utils = require('js/utils/tools'),
        helper = require('js/utils/router'),
        path = 'js/apps/op/',
        config = {
            'mch': {
                'order': '-updated_at'
            },
            'channel': {
                'order': '-created_at'
            },
            'strategy': {
                'order': '-created_at'
            },
            'pay': {
                'order': '-created_at'
            },
            'withdraw_channel': {
                'order': '-created_at'
            },
            'withdraw_order': {
                'order': '-created_at'
            },
            'channel_stat': {
            },
            'channel_mg_stat': {
                'order': '-channel_id'
            },
        };

    exports.Router = Backbone.Router.extend({
        routes: {
            "op/:key/(?*qs)": "opList",
            "op/:key/:id/": "opDetail",
        },
        opList: function (key, qs) {
            helper.commonList(key, qs, path, config);
        },
        opDetail: function(key, id){
            helper.commonDetail(key, id, path, config);
        },
    });
});
