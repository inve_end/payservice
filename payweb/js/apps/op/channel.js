define(function (require, exports, module) {
    'use strict';
    var $ = require('jquery'),
        _ = require('underscore'),
        Backbone = require('backbone'),
        notify = require('js/utils/notify'),
        utils = require('js/utils/tools'),
        models = require('js/apps/op/models'),
        moment = require('moment'),
        app = Backbone.history;
    require('datetimepicker');
    require('multiselect');

    var ChannelListView = Backbone.View.extend({
        tagName: "div",
        events: {
            'click a.onClickDelete': 'toDelete',
            'click #search': 'doSearch',
        },
        initialize: function (options) {
            this.options = options;
            this.collection = new models.ChannelCollection();
            this.collection.bind('change reset remove', this.renderWithData, this);
            this.mchCollection = new models.MchCollection();
            this.mchCollection.bind('change reset remove', this.renderWithData, this);
        },
        render: function () {
            this.$el.html("");
            $(window).scrollTop(0);
            return this;
        },
        toDelete: function (e) {
            utils.deleteItem(e, this.collection);
        },
        doSearch: function () {
            var options = {},
                searched = {};
            searched.id = $('#searchId').val();
            searched.mch_id = $('#searchMchId').val();
            searched.name = $('#searchName').val();
            searched.service_name = $('#searchServiceName').val();
            searched.chn_type = parseInt($('#searchChannelType').val());
            searched.status = parseInt($('#searchStatus').val());

            if (searched.id) {
                options.id = searched.id;
            }
            if (searched.mch_id != -1) {
                options.mch_id = searched.mch_id;
            }
            if (searched.name) {
                options.name = {};
                options.name.$like = searched.name;
            }
            if (searched.service_name != -1) {
                options.service_name = searched.service_name;
            }
            if (searched.chn_type != -1) {
                options.chn_type = searched.chn_type;
            }
            if (searched.status != -1) {
                options.status = searched.status;
            }
            utils.saveSearched('op/channel', searched);
            app.navigate(utils.composeQueryString('#op/channel/', options), {
                trigger: true
            });
        },
        renderWithData: function () {
            this.$el.html(this.template({
                searched: utils.getSearched('op/channel'),
                items: this.collection.toJSON(),
                mchs: this.mchCollection.toJSON()
            }));
            var that = this;
            utils.renderTable('main-list', {
                $orderby: that.options.$orderby || 'id',
                sortCallback: function (field) {
                    that.options.$orderby = field;
                    that.options.$page = 1;
                    var newUrl = utils.composeQueryString('#op/channel/', that.options);
                    app.navigate(newUrl, {
                        trigger: true
                    });
                }
            });
            utils.getPaginator(this.options, this.collection.total, '#op/channel/');
            return this;
        },
        load: function () {
            this.mchCollection.fetch({
                reset: true,
                data: {
                    "$size": -1
                },
                error: function (c, r, o) {
                    notify.notifyResp(r);
                },
            });
            this.collection.fetch({
                reset: true,
                data: this.options,
                error: function (c, r, o) {
                    notify.notifyResp(r);
                    $('#content').append('<h4 class="text-muted">无数据</h4>');
                },
            });
        }
    });
    exports.List = Backbone.View.extend({
        el: "#content_wrapper",
        template: "op/ChannelList.html",
        initialize: function (options) {
            this.options = options;
        },
        render: function () {
            var view = new ChannelListView(this.options);
            view.template = this.template;

            this.$el.empty();
            this.$el.append(view.render().el);
            view.load();

            return this;
        }
    });
    var ChannelDetailView = Backbone.View.extend({
        tagName: "div",
        className: "panel-body pn",
        events: {
            'click a.template-save': 'toSaveOrCreate',
        },
        initialize: function (options) {
            this.model = new models.Channel();
            if (options.id) {
                this.model.set('id', options.id);
            }
            this.model.bind('change reset', this.renderWithData, this);
            this.mchCollection = new models.MchCollection();
            this.mchCollection.bind('change reset remove', this.renderWithData, this);
        },
        render: function () {
            this.$el.html("");
            $(window).scrollTop(0);
            return this;
        },
        _getCurrentInputs: function () {
            var t = $.trim($('#inputInfo').val());
            var infoObject = eval('(' + t + ')');
            var inputHourMoney = parseInt($('#inputHourMoney').val())
            if(inputHourMoney && inputHourMoney != -1)
            {
                if(!infoObject.incr_risk){infoObject.incr_risk={};}
                if(!infoObject.incr_risk.hourly){infoObject.incr_risk.hourly={}}
                infoObject.incr_risk.hourly.amount=inputHourMoney;
            }
            var inputDailyMoney = parseInt($('#inputDailyMoney').val())
            if(inputDailyMoney && inputDailyMoney != -1)
            {
                if(!infoObject.incr_risk){infoObject.incr_risk={};}
                if(!infoObject.incr_risk.daily){infoObject.incr_risk.daily={}}
                infoObject.incr_risk.daily.amount=inputDailyMoney;
            }
            var inputDailyStart = parseInt($('#inputDailyStart').val())
            if(!isNaN(inputDailyStart) && inputDailyStart != -1)
            {
                if(!infoObject.daily_open_time){infoObject.daily_open_time={};}
                infoObject.daily_open_time.start = inputDailyStart;
            }
            var inputDailyEnd = parseInt($('#inputDailyEnd').val())
            if(!isNaN(inputDailyEnd) && inputDailyEnd != -1)
            {
                if(!infoObject.daily_open_time){infoObject.daily_open_time={};}
                infoObject.daily_open_time.end =inputDailyEnd;
            }
            var data = {
                'name': $('#inputName').val(),
                'mch_id': $('#inputMchId').val(),
                'chn_type': $('#inputChnType').val(),
                'open_type': $('#inputOpenType').val(),
                'service_name': $('#inputServiceName').val(),
                'info': JSON.stringify(infoObject),
                'status': $('#inputStatus').val(),
                'weight': $('#inputWeight').val(),
                'level': $('#inputLevel').val(),
            }
            return data;
        },
        toSaveOrCreate: function (e) {
            e.preventDefault();
            var isAllRight = utils.simpleCheck();
            if (!isAllRight) {
                notify.error('错误', '输入错误，请检验');
                return;
            }
            var attrs = this._getCurrentInputs();
            this.model.save(attrs, {
                patch: true,
                success: function (model, response) {
                    notify.success('提示', '保存成功！');
                    setTimeout(function () {
                        history.back();
                    }, 500);
                },
                error: function (model, response) {
                    notify.notifyResp(response);
                }
            });
            return false;
        },
        renderWithData: function () {
            this.$el.html(this.template({
                info: this.model.toJSON(),
                mchs: this.mchCollection.toJSON()
            }));
            $('.date-box').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        },
        load: function () {
            this.mchCollection.fetch({
                reset: true,
                data: {
                    "$size": -1
                },
                error: function (c, r, o) {
                    notify.notifyResp(r);
                },
            });
            if (this.model.id) {
                this.model.fetch({
                    reset: true
                });
            } else {
                this.renderWithData();
            }
        }
    });
    exports.Detail = Backbone.View.extend({
        el: "#content_wrapper",
        template: "op/ChannelDetail.html",
        initialize: function (id) {
            this.id = id;
        },
        render: function () {
            var view = new ChannelDetailView(this.id);
            view.template = this.template;

            this.$el.empty();
            this.$el.append(view.render().el);
            view.load();

            return this;
        }
    });
});
