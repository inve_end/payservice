define(function (require, exports, module) {
    'use strict';
    var base = require('js/utils/model'),
        _ = require('underscore');
    exports.Mch = base.Model.extend({
        defaults: {
            name: "",
            api_key: "",
            service_conf: {
                'wechat': {'chns':[], 'notify_prefix': window.DEFAULT_NOTIFY_PREFIX},
                'alipay': {'chns':[], 'notify_prefix': window.DEFAULT_NOTIFY_PREFIX},
                'unionpay': {'chns':[], 'notify_prefix': window.DEFAULT_NOTIFY_PREFIX},
            }
        },
        urlRoot: "/pay/admin/mch/",
    });
    exports.MchCollection = base.Collection.extend({
        model: exports.Mch,
        url: "/pay/admin/mch/",
    });

    exports.Channel = base.Model.extend({
        defaults: {
            id: '',
            name: '',
            service_name: '',
            chn_type: 0,
            open_type: 0,
            info: '',
            status: 0,
            weight: 10,
            level: 0,
        },
        urlRoot: "/pay/admin/channel/",
    });
    exports.ChannelCollection = base.Collection.extend({
        model: exports.Channel,
        url: "/pay/admin/channel/",
    });
    exports.WithdrawChannel = base.Model.extend({
        defaults: {
            id: '',
            name: '',
            mch_id: 0,
            app_id: 0,
            payer_name: 0,
            status: 0,
            weight: 10,
            balance: 0,
        },
        urlRoot: "/pay/admin/withdraw/channel/",
    });
    exports.WithdrawChannelCollection = base.Collection.extend({
        model: exports.Channel,
        url: "/pay/admin/withdraw/channel/",
    });
    exports.Strategy = base.Model.extend({
        defaults: {
            id: '',
            title: '',
            chns: '',
            content: '',
            status: 0,
        },
        urlRoot: "/pay/admin/strategy/",
    });
    exports.StrategyCollection = base.Collection.extend({
        model: exports.Strategy,
        url: "/pay/admin/strategy/",
    });
    exports.PayOrder = base.Model.extend({
        defaults: {
            id: '',
            out_trade_no: '',
            mch_id: 0,
            channel_id: 0,
            channel_type: 0,
            third_id: 0,
            status: 2,
            notify_status: 2,
            total_fee: 0,
            payed_at: '',
            created_at: '',
            updated_at: '',
        },
        urlRoot: "/pay/admin/order/pay/",
    });
    exports.PayOrderCollection = base.Collection.extend({
        model: exports.PayOrder,
        url: "/pay/admin/order/pay/",
    });
    exports.WithdrawOrder = base.Model.extend({
        defaults: {
            id: '',
            out_trade_no: '',
            mch_id: 0,
            channel_id: 0,
            third_id: 0,
            status: 2,
            notify_status: 2,
            total_fee: 0,
            withdraw_at: '',
            created_at: '',
            updated_at: '',
        },
        urlRoot: "/pay/admin/withdraw/order/",
    });
    exports.WithdrawOrderCollection = base.Collection.extend({
        model: exports.WithdrawOrder,
        url: "/pay/admin/withdraw/order/",
    });
    exports.ChannelData = base.Model.extend({
        defaults: {
            channel_type: '',
            channel_id: '',
            mch_id: 0,
            channel_name: '',
            service_name: '',
            total: 0,
            count: 0,
        },
        urlRoot: "/pay/admin/order/pay/",
    });
    exports.ChannelDataCollection = base.Collection.extend({
        model: exports.ChannelData,
        url: "/pay/admin/order/pay/",
    });

     exports.ChannelMgData = base.Model.extend({
        defaults: {
            count: 0,
            start: '',
            mch_id: 0,
            channel_id: 0,
            end: '',
            channel_name: '',
            service_name: '',
            total: 0,
            id: '',
        },
        urlRoot: "/pay/api/stat/",
    });
    exports.ChannelMgDataCollection = base.Collection.extend({
        model: exports.ChannelMgData,
        url: "/pay/api/stat/",
    });


});
