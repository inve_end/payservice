define(function (require, exports, module) {
    'use strict';
    var $ = require('jquery'),
        _ = require('underscore'),
        Backbone = require('backbone'),
        notify = require('js/utils/notify'),
        utils = require('js/utils/tools'),
        models = require('js/apps/op/models'),
        moment = require('moment'),
        app = Backbone.history;
    require('datetimepicker');
    require('multiselect');

    var WithdrawChannelListView = Backbone.View.extend({
        tagName: "div",
        events: {
            'click a.onClickDelete': 'toDelete',
            'click #search': 'doSearch',
        },
        initialize: function (options) {
            this.options = options;
            this.collection = new models.WithdrawChannelCollection();
            this.collection.bind('change reset remove', this.renderWithData, this);
            this.mchCollection = new models.MchCollection();
            this.mchCollection.bind('change reset remove', this.renderWithData, this);
        },
        render: function () {
            this.$el.html("");
            $(window).scrollTop(0);
            return this;
        },
        toDelete: function (e) {
            utils.deleteItem(e, this.collection);
        },
//        toCharge: function (e) {
//            alert("下分支付充值接口，调试中")
//            var money = prompt("请输入充值金额（元）","10000");
//            var itemId = $(e.currentTarget).closest('tr').data('id');
//            alert(itemId)
//            if (money != null) {
//                alert("您的充值金额为" + money +"元");
//            } else {
//                alert("你按了[取消]按钮");
//            }
//            $.post(url, {'id':itemId, 'money':money});
//        },
        doSearch: function () {
            var options = {},
                searched = {};
            searched.id = $('#searchId').val();
            searched.mch_id = $('#searchMchId').val();
            searched.name = $('#searchName').val();
            searched.status = parseInt($('#searchStatus').val());

            if (searched.id) {
                options.id = searched.id;
            }
            if (searched.mch_id != -1) {
                options.mch_id = searched.mch_id;
            }
            if (searched.name) {
                options.name = {};
                options.name.$like = searched.name;
            }
            if (searched.status != -1) {
                options.status = searched.status;
            }
            utils.saveSearched('op/withdraw_channel', searched);
            app.navigate(utils.composeQueryString('#op/withdraw_channel/', options), {
                trigger: true
            });
        },
        renderWithData: function () {
            this.$el.html(this.template({
                searched: utils.getSearched('op/withdraw_channel'),
                items: this.collection.toJSON(),
                mchs: this.mchCollection.toJSON()
            }));
            var that = this;
            $('.channelCharge').click(function() {
                var data_id = $(this).data('id');
                var money = prompt("请输入充值金额（元）","10000");
                if (money != null) {
                    alert("您的充值金额为" + money +"元");
                } else {
                    alert("你按了[取消]按钮");
                    return;
                }
                $.post('/pay/admin/withdraw/channel/'+data_id+'/charge/', {'money':money}).done(function (resp) {
                  var data = resp.data;
                  notify.success('提示', '刷新成功！');
                  that.load();
                }).fail(function (data) {
                  notify.notifyResp(data);
                });
            });
            utils.renderTable('main-list', {
                $orderby: that.options.$orderby || 'id',
                sortCallback: function (field) {
                    that.options.$orderby = field;
                    that.options.$page = 1;
                    var newUrl = utils.composeQueryString('#op/withdraw_channel/', that.options);
                    app.navigate(newUrl, {
                        trigger: true
                    });
                }
            });
            utils.getPaginator(this.options, this.collection.total, '#op/withdraw_channel/');
            return this;
        },
        load: function () {
            this.mchCollection.fetch({
                reset: true,
                data: {
                    "$size": -1
                },
                error: function (c, r, o) {
                    notify.notifyResp(r);
                },
            });
            this.collection.fetch({
                reset: true,
                data: this.options,
                error: function (c, r, o) {
                    notify.notifyResp(r);
                    $('#content').append('<h4 class="text-muted">无数据</h4>');
                },
            });
        }
    });
    exports.List = Backbone.View.extend({
        el: "#content_wrapper",
        template: "op/WithdrawChannelList.html",
        initialize: function (options) {
            this.options = options;
        },
        render: function () {
            var view = new WithdrawChannelListView(this.options);
            view.template = this.template;

            this.$el.empty();
            this.$el.append(view.render().el);
            view.load();

            return this;
        }
    });
    var WithdrawChannelDetailView = Backbone.View.extend({
        tagName: "div",
        className: "panel-body pn",
        events: {
            'click a.template-save': 'toSaveOrCreate',
        },
        initialize: function (options) {
            this.model = new models.WithdrawChannel();
            if (options.id) {
                this.model.set('id', options.id);
            }
            this.model.bind('change reset', this.renderWithData, this);
            this.mchCollection = new models.MchCollection();
            this.mchCollection.bind('change reset remove', this.renderWithData, this);
        },
        render: function () {
            this.$el.html("");
            $(window).scrollTop(0);
            return this;
        },
        _getCurrentInputs: function () {
            var data = {
                'name': $('#inputName').val(),
                'mch_id': $('#inputMchId').val(),
                'app_id': $('#inputAppId').val(),
                'payer_name': $.trim($('#inputPayerName').val()),
                'private_key': $.trim($('#inputPrivateKey').val()),
                'public_key': $.trim($('#inputPublicKey').val()),
                'status': $('#inputStatus').val(),
                'weight': $('#inputWeight').val(),
                'balance': $('#inputBalance').val(),
            }
            return data;
        },
        toSaveOrCreate: function (e) {
            e.preventDefault();
            var isAllRight = utils.simpleCheck();
            if (!isAllRight) {
                notify.error('错误', '输入错误，请检验');
                return;
            }
            var attrs = this._getCurrentInputs();
            this.model.save(attrs, {
                patch: true,
                success: function (model, response) {
                    notify.success('提示', '保存成功！');
                    setTimeout(function () {
                        history.back();
                    }, 500);
                },
                error: function (model, response) {
                    notify.notifyResp(response);
                }
            });
            return false;
        },
        renderWithData: function () {
            this.$el.html(this.template({
                info: this.model.toJSON(),
                mchs: this.mchCollection.toJSON()
            }));
            $('.date-box').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        },
        load: function () {
            this.mchCollection.fetch({
                reset: true,
                data: {
                    "$size": -1
                },
                error: function (c, r, o) {
                    notify.notifyResp(r);
                },
            });
            if (this.model.id) {
                this.model.fetch({
                    reset: true
                });
            } else {
                this.renderWithData();
            }
        }
    });
    exports.Detail = Backbone.View.extend({
        el: "#content_wrapper",
        template: "op/WithdrawChannelDetail.html",
        initialize: function (id) {
            this.id = id;
        },
        render: function () {
            var view = new WithdrawChannelDetailView(this.id);
            view.template = this.template;

            this.$el.empty();
            this.$el.append(view.render().el);
            view.load();

            return this;
        }
    });
});
