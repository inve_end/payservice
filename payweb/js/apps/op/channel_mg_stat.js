define(function (require, exports, module) {
    'use strict';
    var $ = require('jquery'),
        _ = require('underscore'),
        Backbone = require('backbone'),
        notify = require('js/utils/notify'),
        models = require('js/apps/op/models'),
        utils = require('js/utils/tools'),
        app = Backbone.history;
    require('datetimepicker');


    var ChannelMgDataListView = Backbone.View.extend({
        tagName: "div",
        events: {
            'click #search': 'doSearch',
        },

        initialize: function (options) {
            this.options = options;
            this.collection = new models.ChannelMgDataCollection();
            this.collection.bind('change remove reset', this.renderWithData, this);
        },
        render: function () {
            this.$el.html("");
            $(window).scrollTop(0);
            return this;
        },

        doSearch: function () {
            var options = {},
            searched = {};
            searched.mch_id = parseInt($("#searchMchId").val());
            searched.channel_id = parseInt($("#searchChannelId").val());
            searched.start_at_lower = $("#searchStartAtLower").val();
            searched.start_at_upper = $('#searchStartAtUpper').val();
            searched.channel_type = parseInt($('#searchChannelType').val());

            if (searched.mch_id != -1) {
                options.mch_id = searched.mch_id;
            }
            if (searched.channel_id) {
                options.channel_id = searched.channel_id;
            }
            if (searched.start_at_lower || searched.start_at_upper) {
                options.start = {};
                if (searched.start_at_lower) options.start.$gte = searched.start_at_lower;
                if (searched.start_at_upper) options.start.$lt = searched.start_at_upper;
            }
            if (searched.channel_type != -1) {
                options.channel_type = searched.channel_type;
            }

            utils.saveSearched('op/channel_mg_stat', searched);
            app.navigate(utils.composeQueryString('#op/channel_mg_stat/', options), {
                trigger: true
            });
        },
        renderWithData: function () {
            console.log(this.collection.toJSON());
            this.$el.html(this.template({
                searched: utils.getSearched('op/channel_mg_stat'),
                list: this.collection.toJSON()
            }));
            var that = this;
            utils.renderTable('main-list', {
                $orderby: that.options.$orderby || '-channel_id',
                sortCallback: function (field) {
                    that.options.$orderby = field;
                    that.options.$page = 1;
                    var newUrl = utils.composeQueryString('#op/channel_mg_stat/', that.options);
                    app.navigate(newUrl, {
                        trigger: true
                    });
                }
            });
            $('[data-type=date]').datetimepicker({
                defaultDate: false,
                format: 'YYYY-MM-DD HH:mm:ss'
            });
            utils.getPaginator(that.options, this.collection.total, '#op/channel_mg_stat/');
            return this;
        },
        load: function () {
            this.collection.fetch({
                reset: true,
                data: this.options,
                error: function (c, r, o) {
                    notify.notifyResp(r);
                    $('#content').append('<h4 class="text-muted">无数据</h4>');
                }
            });
        }
    });
    exports.List = Backbone.View.extend({
        el: "#content_wrapper",
        template: "op/ChannelMgStatistics.html",
        initialize: function (options) {
            this.options = options;
        },
        render: function () {
            var view = new ChannelMgDataListView(this.options);
            view.template = this.template;
            this.$el.empty();
            this.$el.append(view.render().el);
            view.load();

            return this;
        }
    });

});
