define(function (require, exports, module) {
    'use strict';
    var $ = require('jquery'),
        _ = require('underscore'),
        Backbone = require('backbone'),
        notify = require('js/utils/notify'),
        utils = require('js/utils/tools'),
        models = require('js/apps/op/models'),
        moment = require('moment'),
        app = Backbone.history;
    require('datetimepicker');
    require('multiselect');

    var StrategyListView = Backbone.View.extend({
        tagName: "div",
        events: {
            'click a.onClickDelete': 'toDelete',
            'click #search': 'doSearch',
        },
        initialize: function (options) {
            this.options = options;
            this.collection = new models.StrategyCollection();
            this.collection.bind('change reset remove', this.renderWithData, this);
            this.mchCollection = new models.MchCollection();
            this.mchCollection.bind('change reset remove', this.renderWithData, this);
        },
        render: function () {
            this.$el.html("");
            $(window).scrollTop(0);
            return this;
        },
        toDelete: function (e) {
            utils.deleteItem(e, this.collection);
        },
        doSearch: function () {
            var options = {},
                searched = {};
            searched.id = $('#searchId').val();
            searched.mch_id = $('#searchMchId').val();
            searched.title = $('#searchTitle').val();
            searched.status = parseInt($('#searchStatus').val());

            if (searched.id) {
                options.id = searched.id;
            }
            if (searched.mch_id != -1) {
                options.mch_id = searched.mch_id;
            }
            if (searched.title) {
                options.title = {};
                options.title.$like = searched.title;
            }
            if (searched.status != -1) {
                options.status = searched.status;
            }
            utils.saveSearched('op/strategy', searched);
            app.navigate(utils.composeQueryString('#op/strategy/', options), {
                trigger: true
            });
        },
        renderWithData: function () {
            this.$el.html(this.template({
                searched: utils.getSearched('op/strategy'),
                items: this.collection.toJSON(),
                mchs: this.mchCollection.toJSON()
            }));
            var that = this;
            utils.renderTable('main-list', {
                $orderby: that.options.$orderby || 'id',
                sortCallback: function (field) {
                    that.options.$orderby = field;
                    that.options.$page = 1;
                    var newUrl = utils.composeQueryString('#op/strategy/', that.options);
                    app.navigate(newUrl, {
                        trigger: true
                    });
                }
            });
            utils.getPaginator(this.options, this.collection.total, '#op/strategy/');
            return this;
        },
        load: function () {
            this.mchCollection.fetch({
                reset: true,
                data: {
                    "$size": -1
                },
                error: function (c, r, o) {
                    notify.notifyResp(r);
                },
            });
            this.collection.fetch({
                reset: true,
                data: this.options,
                error: function (c, r, o) {
                    notify.notifyResp(r);
                    $('#content').append('<h4 class="text-muted">无数据</h4>');
                },
            });
        }
    });
    exports.List = Backbone.View.extend({
        el: "#content_wrapper",
        template: "op/StrategyList.html",
        initialize: function (options) {
            this.options = options;
        },
        render: function () {
            var view = new StrategyListView(this.options);
            view.template = this.template;

            this.$el.empty();
            this.$el.append(view.render().el);
            view.load();

            return this;
        }
    });
    var StrategyDetailView = Backbone.View.extend({
        tagName: "div",
        className: "panel-body pn",
        events: {
            'click a.template-save': 'toSaveOrCreate',
        },
        initialize: function (options) {
            this.model = new models.Strategy();
            if (options.id) {
                this.model.set('id', options.id);
            }
            this.model.bind('change reset', this.renderWithData, this);
            this.mchCollection = new models.MchCollection();
            this.mchCollection.bind('change reset remove', this.renderWithData, this);
        },
        render: function () {
            this.$el.html("");
            $(window).scrollTop(0);
            return this;
        },
        _getCurrentInputs: function () {
            var data = {
                'title': $('#inputTitle').val(),
                'mch_id': $('#inputMchId').val(),
                'chns': $('#inputChns').val(),
                'content': $.trim($('#inputContent').val()),
                'status': $('#inputStatus').val(),
            }
            return data;
        },
        toSaveOrCreate: function (e) {
            e.preventDefault();
            var isAllRight = utils.simpleCheck();
            if (!isAllRight) {
                notify.error('错误', '输入错误，请检验');
                return;
            }
            var attrs = this._getCurrentInputs();
            this.model.save(attrs, {
                patch: true,
                success: function (model, response) {
                    notify.success('提示', '保存成功！');
                    setTimeout(function () {
                        history.back();
                    }, 500);
                },
                error: function (model, response) {
                    notify.notifyResp(response);
                }
            });
            return false;
        },
        renderWithData: function () {
            this.$el.html(this.template({
                info: this.model.toJSON(),
                mchs: this.mchCollection.toJSON()
            }));
            $('.date-box').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        },
        load: function () {
            this.mchCollection.fetch({
                reset: true,
                data: {
                    "$size": -1
                },
                error: function (c, r, o) {
                    notify.notifyResp(r);
                },
            });
            if (this.model.id) {
                this.model.fetch({
                    reset: true
                });
            } else {
                this.renderWithData();
            }
        }
    });
    exports.Detail = Backbone.View.extend({
        el: "#content_wrapper",
        template: "op/StrategyDetail.html",
        initialize: function (id) {
            this.id = id;
        },
        render: function () {
            var view = new StrategyDetailView(this.id);
            view.template = this.template;

            this.$el.empty();
            this.$el.append(view.render().el);
            view.load();

            return this;
        }
    });
});
