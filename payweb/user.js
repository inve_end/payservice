(function () {
    requirejs.config({
        baseUrl: '.',
        paths: {
            jquery: 'lib/jquery/jquery-1.11.1.min',
            'jquery-ui': 'lib/jquery/jquery_ui/jquery-ui.min',
            underscore: 'lib/underscore/underscore-min',
            backbone: 'lib/backbone/backbone-min',
            bootstrap: 'lib/bootstrap/bootstrap.min',
            multiselect: 'lib/plugins/multiselect/bootstrap-multiselect',
            pnotify: 'lib/plugins/pnotify/pnotify',
            fullscreen: 'lib/plugins/fullscreen/jquery.fullscreen',
            nanoscroller: 'lib/plugins/nanoscroller/jquery.nanoscroller.min',
            paginator: 'lib/plugins/pagination/bootstrap-paginator.min',
            confirm: 'lib/plugins/confirm/jquery-confirm.min',
        },
        shim: {
            underscore: {
                exports: '-'
            },
            'jquery-ui':{
                deps: ['jquery']
            },
            fullscreen: {
                deps: ['jquery']
            },
            paginator: {
                deps: ['jquery', 'bootstrap']
            },
            confirm: {
                deps: ['jquery']
            },
            bootstrap: {
                deps: ['jquery']
            },
            backbone: {
                deps: ['underscore', 'jquery'],
                exports: 'Backbone'
            },
            multiselect: {
                deps: ['jquery', 'bootstrap']
            },
            pnotify: {
                deps: ['jquery', 'bootstrap']
            }
        }
    });
    requirejs(['js/frame/core', 'js/frame/user', 'js/frame/const'], function(Core, User){
        Core.init();
        User.init();
    });
})();