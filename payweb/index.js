(function () {
    var config = {
        baseUrl: '.',
        paths: {
            jquery: 'lib/jquery/jquery-1.11.1.min',
            'jquery-ui': 'lib/jquery/jquery_ui/jquery-ui.min',
            underscore: 'lib/underscore/underscore-min',
            backbone: 'lib/backbone/backbone-min',
            bootstrap: 'lib/bootstrap/bootstrap.min',
            multiselect: 'lib/plugins/multiselect/bootstrap-multiselect',
            pnotify: 'lib/plugins/pnotify/pnotify',
            magnific: 'lib/plugins/magnific/jquery.magnific-popup.min',
            moment: 'lib/plugins/moment/moment.min',
            mOxie: 'lib/plugins/plupload/js/moxie',
            plupload: 'lib/plugins/plupload/js/plupload.full.min',
            plupload_i18n: 'lib/plugins/plupload/js/i18n/zh_CN',
            qiniu: 'lib/plugins/qiniu/qiniu.min',
            datetimepicker: 'lib/plugins/datetimepicker/js/bootstrap-datetimepicker.min',
            daterangepicker: 'lib/plugins/daterangepicker/daterangepicker',
            highcharts: 'lib/plugins/Highcharts/js/highcharts',
            fullscreen: 'lib/plugins/fullscreen/jquery.fullscreen',
            paginator: 'lib/plugins/pagination/bootstrap-paginator.min',
            confirm: 'lib/plugins/confirm/jquery-confirm.min',
            nanoscroller: 'lib/plugins/nanoscroller/jquery.nanoscroller.min',
        },
        shim: {
            underscore: {
                exports: '_'
            },
            paginator: {
                deps: ['bootstrap']
            },
            backbone: {
                deps: ['underscore', 'jquery'],
                exports: 'Backbone'
            },
            multiselect: {
                deps: ['jquery', 'bootstrap']
            },
            pnotify: {
                deps: ['jquery', 'bootstrap']
            },
            mOxie: {
                exports: 'mOxie'
            },
            plupload: {
                deps: ['mOxie'],
                exports: 'plupload'
            },
            plupload_i18n: {
                deps: ['plupload']
            },
            qiniu: {
                deps: ['plupload', 'plupload_i18n'],
                exports: 'Qiniu'
            },
        }
    };
    var jqueryPlugins = ['jquery-ui', 'bootstrap', 'fullscreen', 'confirm', 'magnific', 'highcharts'];
    for (var i = 0; i < jqueryPlugins.length; ++i){
        config.shim[jqueryPlugins[i]] = {
            deps: ['jquery']
        }
    }
    requirejs.config(config);
    requirejs(['js/frame/core', 'backbone', 'js/frame/router',
        'jquery', 'js/utils/cookie', 'js/utils/notify', 'js/utils/tz',
        'magnific', 'js/frame/const', 'fullscreen', 'js/frame/settings'
    ], function (Core, Backbone, router, $, cookie, notify, tz) {
        Core.init();
        router.init();

        var screenCheck = $.fullscreen.isNativelySupported();

        // Attach handler to navbar fullscreen button
        $('.request-fullscreen').click(function () {
            // Check for fullscreen browser support
            if (screenCheck) {
                if ($.fullscreen.isFullScreen()) {
                    $.fullscreen.exit();
                } else {
                    $('html').fullscreen({
                        overflow: 'visible'
                    });
                }
            } else {
                alert('Your browser does not support fullscreen mode.')
            }
        });
        $('#logout-link').click(function () {
            cookie.deleteCookie('bigbang_user_id');
            cookie.deleteCookie('bigbang_user_token');
            window.location.href = '/user.html';
        });
        $('#profile-link').magnificPopup({
            items: {
                src: '#editProfilePanel',
                type: 'inline'
            },
            midClick: true,
            closeOnBgClick: false,
            callbacks: {
                beforeOpen: function () {
                    $('#submitProfileChange').off('click').click(function () {
                        var nickname = $('#inputNickName').val(),
                            password = $('#inputPassword').val(),
                            confirmPassword = $('#confirmPassword').val(),
                            data = {};
                        if (nickname != '') data.nickname = nickname;
                        if (password != '' && password == confirmPassword) data.password = password;
                        if (password != confirmPassword) {
                            notify.failed('警告', '两次输入密码不一致');
                            return;
                        }
                        $('#submitProfileChange').attr('disabled', 'disabled');
                        $.ajax({
                            url: '/pay/admin/user/' + USER_INFO.info.id,
                            type: 'PUT',
                            data: JSON.stringify(data),
                            success: function () {
                                notify.success();
                                location.reload();
                                $.magnificPopup.close();
                                if (data.password) {
                                    $('#logout-link').click();
                                }
                            },
                            error: function (data) {
                                notify.notifyResp(data);
                                $('#submitProfileChange').removeAttr('disabled');
                            }
                        })
                    });
                },
            }
        });
        var today = tz.today(),
            todayTS = tz.todayTS(),
            qs = 'created_at={"$gt":"' + today + '"}&_reflush=1',
            qsTS = 'start_ts={"$gt":' + todayTS + '}&_reflush=1',
            href = window.location.href;
        $('[data-key]').each(function () {
            var qstr = $(this).hasClass("ts") ? qsTS : qs;
            $(this).attr("href", "#op/" + $(this).data('key') + '/?' + qstr);
        });

        if (href.indexOf('#goods/') != -1) {
            $('#opMenu').addClass('menu-open');
            $('#goodsMenu').addClass('menu-open');
        } else if (href.indexOf('#order/') != -1) {
            $('#opMenu').addClass('menu-open');
            $('#orderMenu').addClass('menu-open');
        } else if (href.indexOf('#stats') != -1) {
            $('#statsMenu').addClass('menu-open');
        } else if (href.indexOf('#vips/') != -1) {
            $('#statsMenu').addClass('menu-open');
            $('#vipsMenu').addClass("menu-open");
        } else if (href.indexOf('#console/') != -1) {
            $('#consoleMenu').addClass("menu-open");
        } else if (href.indexOf('#report/') != -1) {
            $('#reportMenu').addClass("menu-open");
        } else {
            $('#opMenu').addClass('menu-open');
        }
    });
})();
