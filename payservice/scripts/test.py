# -*- coding: utf-8 -*-
from __future__ import absolute_import
import os
import sys
import json
# import tablib
import logging
import time
import datetime

# add up one level dir into sys path
sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))
os.environ['DJANGO_SETTINGS_MODULE'] = 'base.settings'

import requests
# from common.mch import handler as mch_handler


CHARGE_URL = 'http://127.0.0.1:8989/api/charge/create/'


def test_charge(**kwargs):
    sdk_version = kwargs.get('sdk_version')
    service = kwargs.get('service')
    mch_id = kwargs.get('mch_id')
    out_trade_no = kwargs.get('out_trade_no')
    body = kwargs.get('body')
    total_fee = kwargs.get('total_fee')
    mch_create_ip = kwargs.get('mch_create_ip')
    notify_url = kwargs.get('notify_url')
    sign = mch_handler.generate_sign(mch_id, kwargs)
    print sign
    kwargs.update(sign=sign)
    res = requests.post(CHARGE_URL, data=kwargs, timeout=3)
    print res.text

d ={u'body': u'\u5546\u54c1', u'service': u'wxapp', u'extra': u'{"user_info": {"count_withdraw": 0, "user_id": 3022022545, "account_day": 1, "user_alicount": "18811136169", "total_withdraw": 0, "device_ip": "172.104.185.146", "bankcard": "", "chn": "ios_glowworm_chess", "total_recharge": 0, "active_day": 1, "device_type": "ios", "count_recharge": 0, "charge_success_count": 0, "tel": "", "total_bet": 0.0, "user_name": "18811136169", "device_id": "7F84BAB9-7AFE-4AF5-B657-DFBE0446C8EA"}}', u'sdk_version': u'android_v_1_1', u'sign': u'4F1C5FF5964AD68CB39F7CDAD0B1D53F', u'mch_id': u'6001011', u'out_trade_no': u'7236702579493943864', u'total_fee': u'300.0', u'notify_url': u'http://yinduxiaofei.com:8080/api/third/justpay/notify/', u'mch_create_ip': u'127.0.0.1'}

def generate_sign(parameter):
    s = ''
    for k in sorted(parameter.keys()):
        s += '%s=%s&' % (k, parameter[k])
    print s

if __name__ == "__main__":
    generate_sign(d)
    # test_charge(sdk_version='android_v_1_0', service='alipay', mch_id='6001000',
    #     out_trade_no=int(time.time()), body=u'实惠网盘', total_fee=11, mch_create_ip='127.0.0.1',
    #     notify_url='http://219.135.56.195/api/v1/justpay/notify/')
