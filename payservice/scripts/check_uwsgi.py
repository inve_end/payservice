# -*- coding: utf-8 -*-

import time
from datetime import datetime


def _get_str(s, start, end):
    p1 = s.index(start)
    p2 = s.index(end, p1 + len(start))
    if p2 > p1:
        return s[p1 + len(start): p2]
    else:
        return ''



if __name__ == "__main__":
    with open('/Users/kael/uwsgi.log.1', 'r') as f:
        lines = f.readlines()
        line_num = len(lines)
        # print lines
        print line_num
        count = 0
        total = 0
        max = 0
        cc = 0
        for line in lines:
            if 'charge/create/' not in line:
                continue
            count += 1
            print line
            spend_time = int(_get_str(line, 'bytes in ', ' msecs ('))
            print spend_time
            total += spend_time
            if spend_time > max:
                max = spend_time
            if spend_time >= 5000:
                cc += 1

        print 'total request'
        print count
        print 'total_spend time'
        print total
        print 'avg spend time'
        print total / count
        print max
        print cc