#! -*- coding:utf-8 -*-
import requests
import hashlib


def generate_sign(parameter, key):
    s = ''
    for k in sorted(parameter.keys()):
        s += '%s=%s&' % (k, parameter[k])
    s += 'key=%s' % key
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().upper()
    return sign

if __name__ == '__main__':
    parameter_dict = {
        'mch_id': '6001015',
        'out_trade_no': '1655356111943904256',
        'status': 2
    }
    parameter_dict['sign'] = generate_sign(parameter_dict, '080bf4115fe34105a08adb47100e4d2e')
    print requests.post('http://pay.xyz115.com:9999/pay/api/charge/notify_order_status/', data=parameter_dict, timeout=5).text