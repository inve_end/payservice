#! -*- coding:utf-8 -*-
import logging
from datetime import datetime, timedelta
from django.core.management.base import BaseCommand

from common.order.model import PAY_STATUS, PayOrder
from common.channel.model import CHANNEL_TYPE
from common.channel.channel_map import CHANNEL_HANDLER

_LOGGER = logging.getLogger(__name__)

_CHANNELS = {
    695: {'appid': 'KS', 'channel_type': CHANNEL_TYPE.KS3PAY},
    767: {'appid': 'ZSQP', 'channel_type': CHANNEL_TYPE.KS3PAY},
    1023: {'appid': 'KS', 'channel_type': CHANNEL_TYPE.KS7PAY},
    1083: {'appid': 'KS', 'channel_type': CHANNEL_TYPE.KS7PAY},
    1077: {'appid': '149', 'channel_type': CHANNEL_TYPE.FUTUREPAY},
    1054: {'appid': '147', 'channel_type': CHANNEL_TYPE.FUTUREPAY},
    1053: {'appid': '148', 'channel_type': CHANNEL_TYPE.FUTUREPAY},
    1052: {'appid': '147', 'channel_type': CHANNEL_TYPE.FUTUREPAY},
    1051: {'appid': '146', 'channel_type': CHANNEL_TYPE.FUTUREPAY},
    1048: {'appid': '144', 'channel_type': CHANNEL_TYPE.FUTUREPAY},
    1025: {'appid': '144', 'channel_type': CHANNEL_TYPE.FUTUREPAY},
    1017: {'appid': '145765', 'channel_type': CHANNEL_TYPE.XINPAY},
    1015: {'appid': '145764', 'channel_type': CHANNEL_TYPE.XINPAY},
    1014: {'appid': '145765', 'channel_type': CHANNEL_TYPE.XINPAY},
    1013: {'appid': '145763', 'channel_type': CHANNEL_TYPE.XINPAY},
    1012: {'appid': '145764', 'channel_type': CHANNEL_TYPE.XINPAY},
    953: {'appid': '145763', 'channel_type': CHANNEL_TYPE.XINPAY},
    952: {'appid': '145765', 'channel_type': CHANNEL_TYPE.XINPAY},
    954: {'appid': '145764', 'channel_type': CHANNEL_TYPE.XINPAY},
    1147: {'appid': '2018126', 'channel_type': CHANNEL_TYPE.MANAPAY},
    1148: {'appid': '2018126', 'channel_type': CHANNEL_TYPE.MANAPAY},
    1510: {'appid': 'KS', 'channel_type': CHANNEL_TYPE.KS3PAY},
    1511: {'appid': 'KS', 'channel_type': CHANNEL_TYPE.GROUPPAY},
    1586: {'appid': '1069', 'channel_type': CHANNEL_TYPE.XUNLIANPAY},
}


def _get_appid(channel_id):
    return _CHANNELS[channel_id]['appid']


def _get_channel_type(channel_id):
    return _CHANNELS[channel_id]['channel_type']


class Command(BaseCommand):

    def handle(self, **options):
        now = datetime.utcnow()
        start = now + timedelta(minutes=-20)
        print('channel_query start')
        # _LOGGER.info('channel_query start')
        for channel_id in _CHANNELS.keys():
            items = PayOrder.query.filter(PayOrder.channel_id == channel_id).filter(
                PayOrder.status == PAY_STATUS.READY). \
                filter(PayOrder.created_at >= start).all()
            print('channel_query channel[%s], count[%s]' % (channel_id, len(items)))
            # _LOGGER.info('channel_query channel[%s], count[%s]' , channel_id, len(items))
            for order in items:
                try:
                    CHANNEL_HANDLER[_get_channel_type(channel_id)].query_charge(order, _get_appid(channel_id))
                except:
                    print('channel_query except')
        print('channel_query end')
        # _LOGGER.info('channel_query end')
