#! -*- coding:utf-8 -*-
import json
import logging

from django.core.management.base import BaseCommand

from common.channel.model import Channel

_LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):


    def handle(self, mch_id, sdk, **options):
        print 'start'
        print mch_id
        print sdk
        items = Channel.query.filter(Channel.mch_id == int(mch_id)).all()
        print 'total : %s' % len(items)
        count = 0
        for item in items:
            j = json.loads(item.info)
            j['sdk_version']['include'].append(sdk)
            item.info = json.dumps(j, ensure_ascii=False)
            item.save()
            print count
            count = count + 1
        print 'end'
