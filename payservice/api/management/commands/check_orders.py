#! -*- coding:utf-8 -*-
import logging
from django.core.management.base import BaseCommand
from common.order import db as order_db
from datetime import datetime

import csv


_LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):

    def handle(self, checkfile_path, **kwargs):
        print '------begin------'
        with open(checkfile_path, 'r') as f:
            reader = csv.reader(f)
            count = 0
            head_row = next(reader)
            for row in reader:
                count += 1
                print count
                item = order_db.get_pay(int(row[2]))
                if item is None:
                    print('%s is null' % row[2])
                    continue
                if item.status != 2:
                    print('%s is failed' % row[2])
                    continue
                if item.total_fee != float(row[4]):
                    print('%s amount not match' % row[2])
                    continue
                ctime = datetime.strptime(row[0], "%Y-%m-%d %H:%M:%S")
                utime = datetime.strptime(row[1], "%Y-%m-%d %H:%M:%S")
                if ctime.day != 3 or utime.day != 3:
                    print('%s is time error' % row[2])
                    continue
        print '------done------'