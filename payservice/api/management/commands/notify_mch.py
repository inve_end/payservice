#! -*- coding:utf-8 -*-
import os
import json
import logging
from datetime import datetime

from async import async_job
from common.withdraw import db as trans_db
from common.withdraw import handler as trans_handler

from django.core.management.base import BaseCommand, CommandError


_LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):

    def handle(self, order_id, **options):
        success, withdraw_order = trans_handler.notify_mch(order_id)
        print success
