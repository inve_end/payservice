#! -*- coding:utf-8 -*-
import json
import logging
from datetime import datetime, timedelta
from django.core.management.base import BaseCommand

from async import async_job
from common.channel import admin_db as channel_db
from common.order.db import get_orders_by_channel
from common.order import admin_db as order_db
from common.order.model import PAY_STATUS
from common.channel.pay import e_times

_LOGGER = logging.getLogger(__name__)

# channel_type 284 237 235
class Command(BaseCommand):

    def handle(self,  **options):
        start = datetime.strptime("2018-3-19 7:30:0", "%Y-%m-%d %H:%M:%S")
        end = datetime.strptime("2018-3-19 8:0:0", "%Y-%m-%d %H:%M:%S")
        orders = get_orders_by_channel(284, start, end)
        print (str(start) + '  ' + str(end))
        print ("orders count : " + str(len(orders)))
        i = 0;
        for order in orders:
            e_times.query_charge(order, '1170')
            i = i + 1
            print i
