#! -*- coding:utf-8 -*-
import json
import logging

from django.core.management.base import BaseCommand

from common.withdraw.processor import start

_LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):


    def handle(self, **options):
        start()
