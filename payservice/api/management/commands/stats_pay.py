#! -*- coding:utf-8 -*-
import json
import logging

from django.core.management.base import BaseCommand

from common.order.model import *


_LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):

    def calc_range_ratio(self, mch_id):
        # 计算各个充值范围的转化率
        for k in [(1, 9), (10, 50), (51, 200), (201, 500), (501, 1000), (1001, 5000),
                  (5000, None)]:
            min_amount, max_amount = k[0], k[1]

    def handle(self, **options):
        pass
