#! -*- coding:utf-8 -*-
import json
import logging

from django.core.management.base import BaseCommand

from common.withdraw.processor import start, single_withdraw

_LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):


    def handle(self, cid, oid, **options):
        print cid, int(cid)
        print oid, int(oid)
        single_withdraw(int(oid), int(cid))
