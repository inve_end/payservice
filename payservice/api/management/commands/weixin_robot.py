# ! -*- coding:utf-8 -*-
from wxpy import *

def login_c():
    pass
# 初始化机器人，扫码登陆
bot = Bot(login_callback=login_c())


# 打印来自其他好友、群聊和公众号的消息
@bot.register()
def print_others(msg):
    print(msg)
    return u'亲  请您扫码关注代理的公众号进行充值，在公众号里面有专业的代理团队和代理人员为您诚信的服务哦 https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzU0Mjc3NTkwNg==&scene=124#wechat_redirect'


# 自动接受新的好友请求
@bot.register(msg_types=FRIENDS)
def auto_accept_friends(msg):
    # 接受好友请求
    new_friend = msg.card.accept()
    # 向新的好友发送消息
    new_friend.send(u'亲  请您扫码关注代理的公众号进行充值，在公众号里面有专业的代理团队和代理人员为您诚信的服务哦 https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzU0Mjc3NTkwNg==&scene=124#wechat_redirect')


# 进入 Python 命令行、让程序保持运行
embed()
