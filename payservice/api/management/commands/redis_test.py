
#! -*- coding:utf-8 -*-
from django.core.management.base import BaseCommand
from common.cache import redis_cache
from django.conf import settings


class Command(BaseCommand):

    def handle(self, **options):
        print 'start test redis'
        print ('redis_setting addr' + settings.REDIS_HOST)
        cache_id = redis_cache.save_html('111', '23123123redis')
        print cache_id
        print redis_cache.get_cache_html(cache_id)
        print 'end'
