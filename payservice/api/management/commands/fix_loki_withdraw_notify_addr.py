#! -*- coding:utf-8 -*-

from django.core.management.base import BaseCommand

from common.withdraw.model import WithdrawOrder




class Command(BaseCommand):

    def handle(self, **options):
        print 'start fix_loki_withdraw_notify'
        items = WithdrawOrder.query.filter(WithdrawOrder.mch_id == 6001015).filter(
            WithdrawOrder.notify_url == 'http://45.121.50.201/exchange/JUST').all()
        print len(items)
        for item in items:
            item.notify_url = 'http://45.121.50.201:9000/exchange/JUST'
            item.save()
        print 'end fix_loki_withdraw_notify'

