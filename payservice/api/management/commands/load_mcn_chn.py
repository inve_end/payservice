#! -*- coding:utf-8 -*-
import os
import json
import logging
from datetime import datetime

from common.mch import db as mch_db
from common.utils import tz

from django.core.management.base import BaseCommand, CommandError


_LOGGER = logging.getLogger(__name__)


MCH_CONF = {
    '6001000': {
        'service': {
            'wechat': {
                'chns': [1],
                'notify_prefix': 'http://219.135.56.195',
            },
            'alipay': {
                'chns': [2,3,4,5,6,7],
                'notify_prefix': 'http://219.135.56.195',
            }
        }
    }
}


class Command(BaseCommand):

    def handle(self, **options):
        for mch_id, conf in MCH_CONF.items():
            for service, s_conf in conf['service'].items():
                mch_db.create_mch_chn(mch_id, service, [str(x) for x in s_conf['chns']],
                    s_conf['notify_prefix'])
