#! -*- coding:utf-8 -*-
import time
import logging

from django.core.management.base import BaseCommand
from common.export.handler import process_export

_LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):


    def handle(self, export_id, **options):
        print 'start export : %s' % export_id
        s = int(time.time())
        process_export(int(export_id))
        e = int(time.time())
        print 'end export : %s, spend : %s s' % (export_id, e - s)
