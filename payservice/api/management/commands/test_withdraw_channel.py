#! -*- coding:utf-8 -*-
import json
import logging

from django.core.management.base import BaseCommand

from common.withdraw.processor import test_withdraw_channel

_LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):

    def handle(self, cid, **options):
        print cid, int(cid)
        print test_withdraw_channel(int(cid))
