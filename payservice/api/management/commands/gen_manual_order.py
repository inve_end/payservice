#! -*- coding:utf-8 -*-
import json
from django.core.management.base import BaseCommand
from common.order import db as order_db
from common.admin.model import Record
from common.admin.db import get_user

# morder.operator_id = operator.id
# morder.operator = operator.nickname
class cTmp(object):
    pass

class Command(BaseCommand):

    def handle(self, **kwargs):
        print '------begin------'
        items = Record.query.filter(Record.resource == 'make_order_success').all()
        for item in items:
            if item.resource_id:
                print item.resource_id
                operator = get_user(item.operator)
                if operator is None:
                    operator = cTmp()
                    operator.id = item.operator
                    operator.nickname = 'deleted'
                order_db.record_manual_order(item.resource_id, operator, json.loads(item.content)['reason'])
        print '------done------'