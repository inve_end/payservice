#! -*- coding:utf-8 -*-
import os
import json
import logging
from datetime import datetime

from common.channel import db as chn_db
from common.utils import tz

from django.core.management.base import BaseCommand, CommandError


_LOGGER = logging.getLogger(__name__)


ALL_CHANNELS = [{
    'name': u'openepay',
    'service': 'wechat',
    'channel_type': 29,
    'open_type': 'html',
    'info': {
        'app_id': '108510160818003',
        'sdk_version': {
            'include': [],
            'exclude': []
        }
    }
}]


class Command(BaseCommand):

    def handle(self, **options):
        for chn in ALL_CHANNELS:
            chn_db.create_channel(chn['name'], chn['service'],
                chn['channel_type'], chn['open_type'], chn['info'])
