#! -*- coding:utf-8 -*-
import os
import json
import logging
from datetime import datetime

from common.mch import db as mch_db
from common.utils import tz

from django.core.management.base import BaseCommand, CommandError


_LOGGER = logging.getLogger(__name__)


MCH_CONF = {
    'wechat': {
        'chns': [],
        'notify_prefix': 'http://219.135.56.195:8080',
    },
    'alipay': {
        'chns': [],
        'notify_prefix': 'http://219.135.56.195:8080',
    }
}


class Command(BaseCommand):

    def handle(self, name, password, **options):
        mch_account = mch_db.create_account(name, password)
        print 'created mch id {}, api key {}'.format(mch_account.id, mch_account.api_key)
        for service, s_conf in MCH_CONF.items():
            mch_db.create_mch_chn(mch_account.id, service, [str(x) for x in s_conf['chns']],
                s_conf['notify_prefix'])
