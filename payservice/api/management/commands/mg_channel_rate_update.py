#! -*- coding:utf-8 -*-
import logging
from django.core.management.base import BaseCommand
from pymongo import MongoClient
from django.conf import settings
from common.channel.admin_db import get_channel

_LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):
    # mg统计数据增加rate费率
    def handle(self, **options):
        print '---start---'
        mg = MongoClient(settings.MONGO_ADDR).paychannel_stat
        items = mg.channel_status.find()
        for item in items:
            if item.get('rate'):
                continue
            print item['_id']
            channel = get_channel(item['channel_id'])
            if channel:
                item['rate'] = channel.rate
            else:
                item['rate'] = 3
            mg.channel_status.update_one({'_id': item['_id']}, {'$set': item}, upsert=True)
        print '---end---'


