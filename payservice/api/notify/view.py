# -*- coding: utf-8 -*-
import json

from django.conf import settings
from django.http import HttpResponse
from django.template.response import TemplateResponse
from django.views.decorators.http import require_GET, require_POST, require_http_methods

from common.channel.pay import (mingtianyun, swiftpass, paytech, openepay,
                                youpay, trpay, iapppay, threepay, changfu,
                                xipay, ruyipay, zfpay, paytech_new, kspay,
                                thpay, mdpay, payfubao, dirpay, skillfully_pay,
                                yiaipay, kailianpay, duotpay, kdpay, xftpay, jcpay,
                                bopay, kvpay, yhxpay, yishangpay, boeingpay, fuyingbao,
                                okpay, our_alipay, aishangpay, zbtpay, yjhpay, e_times,
                                quick_money, jiuyingpay, huihepay, zeshengpay, cywpay,
                                guofupay, tonglepay, easypay, doudoupay, zeropay, e520pay,
                                zfbpay, xlbpay, huichaopay, weipaipay, mobaopay, yftpay,
                                bftpay, yifubaopay, juchengpay, huitianpay, unpay, weibaopay,
                                hbtpay, jinyangpay, lianyingpay, dirpay_new, tianjipay, qiaoshoupay,
                                pinduobaopay, yiantongpay, ruiyunpay, baishengpay, dirpay_union,
                                huanxunpay, hongchuangpay, haifupay, huiyinpay, ks2pay, qcmypay,
                                huiyintongpay, xinjiepay, ruibaopay, yutoupay, xunshoupay, suiszfpay,
                                v3pay, beifupay, xunpay, lipay, zhangxunpay, xun1pay, shanyipay,
                                anfupay, e_times_v2, dandanpay, tongdapay, sufupay, litaobopay,
                                newks2pay, weihubaopay, kairuipay, xinpay, ks3pay, xinkeyunpay,
                                wanlihuipay, duoduopay, zhifutongpay, lezhifupay, gopay, yihuipay,
                                guangtongpay, yizhipay, sc365pay, hanpay, xindanpay, mishangpay,
                                xunlianpay, boshipay, zfpfpay, zhipay, tengpay, xin1pay, jinruipay,
                                ks6pay, mishang_nativepay, zhinengyunpay, dopay, jiufutongpay, moshangpay,
                                yingtongpay, wufupay, dongfangpay, qlshpay, zhinengyunpay_v5, dopay_v1,
                                renrenpay, xinshunchangpay, shuntongpay, hub365pay, mishangpay_v3, mishangpay_v4, wopay,
                                spay, kpay, xinfutongpay, shenglipay, zhihuipay, lianruipay, xindaomipay,
                                yunbeipay, shuntingpay, hengfutongpay, shenglipay_v2, tongyupay, pay83,
                                meilianpay, shunfutongpay, chuangyingpay, zhongfutongpay, ks7pay, futurepay,
                                huifupay, skypay, sifangpay, suhuipay, yurunpay, zhipay_v2, makupay,
                                feiniubaopay, huifulapay, grouppay, kaixinguopay, xinshunfupay, hkerpay,
                                fuhuitongpay, xintaipay, cpay, yifupay, youshengpay, newpay, superstarpay,
                                wohuibaopay, jufpay, limafupay, haofupay, ehuipay, lefupay, huitongbaopay,
                                baijiepay, tongbaopay, kuailaifupay, moopay, manapay, nihoutaopay, yilianpay,
                                dajipay, zhiyuanpay, hengfengpay, cpayalipay2bank, dudupay, zhifubangpay, ffpay,
                                quansupay, gaotongpay, onepayquickpay, jintuopay, spayunionpay, yunrongpay, shengfupay,
                                wantongpay, yingxintongpay, ttpay, angelapay, wangshifupay, bajiepay, hydra, fengyunpay)
from common.channel.pay import (newdaomipay, mepay, onepay, jubaopay, yixinpay, huifutongpay, onepaycashpay, zhifupay,
                                lefucloudquickpay, anbaofupay, xinfupay, lefuv2pay, jbcpay, gbpays, miaofupay, jinyupay,
                                chuangpupay, coolpay, zhichengpay, changtongpay, shanfupay, lixianfupay, wandapay,
                                youlianpay, onegopay, wenfupay, huidingpay, hpay, peiqipay, paopaochengpay,
                                xinwirelesspay, yifutongpay, jubaopenpay, fubaotongpay, yfpay, baihepay, mfpay,
                                yifu2pay, lixianfu2pay, shikongpay, lekupay, epay, miaojiepay, amazepay, zhanshenpay,
                                paopaoyingpay, feiyupay, hengjiepay, yifubaopayv2, easypayv2, lelianmengpay,
                                kaisuobaopay, tenglongpay, dingshengpay, newchuangpupay, anxinpay, langlangpay,
                                yilaipay, sdppay, anpay, changshengpay, shengdapay, qixingpay, linghangpay, dafapay,
                                ddpay, zhuoyuepay, yibaopay, suifupay, un2pay, nppay, wanhepay, newlandpay, kukapay,
                                huayingpay, zhixinfupay, huifuyazhoupay, xiguapay, goldengragonpay, xundingpay,
                                newbaihepay, dezhunpay, xintongpay, majipay, miaomiaopay, qiantongyunpay,
                                xiyangyangpay, ccbypay, zhongchengpay, sijipay, jiefupay, shanpupay,anjipay, globalpay,
                                toppay, flogerpay, xinghuopay, huishengpay, shayupay, zhongfapay)
from common.channel.recharge_card_pay import group_pay as group_recharge_card_pay
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import exceptions as err
from common.utils import track_logging
from common.utils.api import get_client_ip

_LOGGER = track_logging.getLogger(__name__)


@require_GET
def common_return(request, pay_id):
    pay = order_db.get_pay(pay_id)
    if not pay or pay.status != PAY_STATUS.SUCC:
        trade_status = 0
    else:
        trade_status = 1
    return TemplateResponse(request, 'pay_status_new.html', {
        'return_url': settings.APP_OPEN_URL,
        'trade_status': trade_status
    })


@require_POST
def mingtianyun_notify(request):
    try:
        mingtianyun.check_notify_sign(request)
        return HttpResponse('{"success": "true"}', status=200)
    except err.ParamError as e:
        _LOGGER.warn('mingtianyun notify param error, %s', e)
        return HttpResponse('{"success": "notify param error"}', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('mingtianyun notify exception.(%s)' % e)
        return HttpResponse('{"success": "false"}', status=200)


@require_POST
def swiftpass_notify(request):
    try:
        swiftpass.check_notify_sign(request)
        # we must return 'success' after processing
        return HttpResponse('success', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('swiftpass notify exception.(%s)' % e)
        return HttpResponse('failure', status=400)


@require_POST
def paytech_notify(request):
    try:
        paytech.check_notify_sign(request)
        # we must return 'OK' after processing
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('paytech notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('paytech notify exception.(%s)' % e)
        return HttpResponse('FAIL', status=400)


@require_GET
def paytech_return(request):
    trade_status = int(request.GET.get('trade_status', 0))
    return TemplateResponse(request, 'pay_status_new.html', {'trade_status': trade_status})


@require_POST
def paytechnew_notify(request):
    try:
        paytech_new.check_notify_sign(request)
        # we must return 'OK' after processing
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('paytechnew notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('paytechnew notify exception.(%s)' % e)
        return HttpResponse('FAIL', status=400)


@require_GET
def paytechnew_return(request):
    trade_status = int(request.GET.get('trade_status', 0))
    return TemplateResponse(request, 'pay_status_new.html', {'trade_status': trade_status})


@require_POST
def openepay_notify(request):
    try:
        openepay.check_notify_sign(request)
        # we must return 'success' after processing
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('openepay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('openepay notify exception.(%s)' % e)
        return HttpResponse('failure', status=400)


@require_GET
def youpay_notify(request):
    try:
        youpay.check_notify_sign(request)
        # we must return 'OK' after processing
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('youpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('youpay notify exception.(%s)' % e)
        return HttpResponse('FAIL', status=400)


@require_GET
def youpay_return(request, pay_id):
    pay = order_db.get_pay(pay_id)
    if not pay or pay.status != PAY_STATUS.SUCC:
        trade_status = 0
    else:
        trade_status = 1
    return TemplateResponse(request, 'pay_status_new.html', {
        'return_url': settings.APP_OPEN_URL,
        'trade_status': trade_status
    })


@require_POST
def trpay_notify(request):
    try:
        trpay.check_notify_sign(request)
        # we must return 'ok' after processing
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('trpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('trpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def iapppay_notify(request):
    try:
        iapppay.check_notify_sign(request)
        # we must return 'OK' after processing
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('iapppay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('iapppay notify exception.(%s)' % e)
        return HttpResponse('FAIL', status=400)


@require_GET
def iapppay_return(request):
    pay_id = int(request.GET.get('pay_id', 0))
    pay = order_db.get_pay(pay_id)
    if not pay or pay.status != PAY_STATUS.SUCC:
        trade_status = 0
    else:
        trade_status = 1
    return TemplateResponse(request, 'pay_status_new.html', {
        'return_url': settings.APP_OPEN_URL,
        'trade_status': trade_status
    })


@require_GET
def threepay_notify(request):
    try:
        client_ip = get_client_ip(request)
        if client_ip not in ('160.19.51.80'):
            _LOGGER.warn('threepay sb notify from unspecified ip {}'.format(client_ip))
            return HttpResponse('OK', status=200)
        threepay.check_notify_sign(request)
        # we must return 'OK' after processing
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('threepay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('threeay notify exception.(%s)' % e)
        return HttpResponse('FAIL', status=400)


@require_GET
def threepay_return(request, pay_id):
    pay = order_db.get_pay(pay_id)
    if not pay or pay.status != PAY_STATUS.SUCC:
        trade_status = 0
    else:
        trade_status = 1
    return TemplateResponse(request, 'pay_status_new.html', {
        'return_url': settings.APP_OPEN_URL,
        'trade_status': trade_status
    })


@require_GET
def changfu_notify(request):
    try:
        changfu.check_notify_sign(request)
        # we must return 'ok' after processing
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('changfu notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('changfu notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def changfu_return(request, pay_id):
    pay = order_db.get_pay(pay_id)
    if not pay or pay.status != PAY_STATUS.SUCC:
        trade_status = 0
    else:
        trade_status = 1
    return TemplateResponse(request, 'pay_status_new.html', {
        'return_url': settings.APP_OPEN_URL,
        'trade_status': trade_status
    })


@require_GET
def xipay_notify(request):
    try:
        xipay.check_notify_sign(request)
        # we must return 'ok' after processing
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xipay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def xipay_return(request, pay_id):
    pay = order_db.get_pay(pay_id)
    if not pay or pay.status != PAY_STATUS.SUCC:
        trade_status = 0
    else:
        trade_status = 1
    return TemplateResponse(request, 'pay_status_new.html', {
        'return_url': '',
        'trade_status': trade_status
    })


@require_POST
def ruyipay_notify(request):
    try:
        ruyipay.check_notify_sign(request)
        # we must return 'stopnotify' after processing
        return HttpResponse('stopnotify', status=200)
    except err.ParamError as e:
        _LOGGER.warn('ruyipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('ruyipay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def ruyipay_return(request):
    pay_id = request.GET.get('merOrdId', 0)
    pay = order_db.get_pay(pay_id)
    if not pay or pay.status != PAY_STATUS.SUCC:
        trade_status = 0
    else:
        trade_status = 1
    return TemplateResponse(request, 'pay_status_new.html', {
        'return_url': settings.APP_OPEN_URL,
        'trade_status': trade_status
    })


@require_GET
def zfpay_notify(request):
    try:
        zfpay.check_notify_sign(request)
        # we must return 'OK' after processing
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zfpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zfpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def zfpay_return(request, pay_id):
    trade_status = request.GET.get('trade_status', 0)
    pay_type = request.GET.get('pay_type')
    total_fee = request.GET.get('total_fee')
    if trade_status == 'TRADE_SUCCESS' or (
            pay_type == 'pay_weixin_wap' and total_fee):
        trade_status = 1
    else:
        trade_status = 1
    return TemplateResponse(request, 'pay_status_new.html', {
        'return_url': settings.APP_OPEN_URL,
        'trade_status': trade_status
    })


@require_POST
def kspay_notify(request, app_id):
    try:
        kspay.check_notify_sign(request, app_id)
        # we must return 'success' after processing
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('kspay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('kspay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def thpay_notify(request, app_id):
    try:
        thpay.check_notify_sign(request, app_id)
        # we must return 'opstate=0' after processing
        return HttpResponse('opstate=0', status=200)
    except err.ParamError as e:
        _LOGGER.warn('thpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('thpay notify exception.(%s)' % e)
        return HttpResponse('opstate=-1', status=400)


@require_GET
def thpay_return(request, pay_id):
    pay = order_db.get_pay(pay_id)
    if not pay or pay.status != PAY_STATUS.SUCC:
        trade_status = 0
    else:
        trade_status = 1
    return TemplateResponse(request, 'pay_status_new.html', {
        'return_url': settings.APP_OPEN_URL,
        'trade_status': trade_status
    })


@require_GET
def mdpay_notify(request, app_id):
    try:
        mdpay.check_notify_sign(request, app_id)
        # we must return 'SUCCESS' after processing
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('madaipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('mdpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def mdpay_return(request, pay_id):
    pay = order_db.get_pay(pay_id)
    if not pay or pay.status != PAY_STATUS.SUCC:
        trade_status = 0
    else:
        trade_status = 1
    return TemplateResponse(request, 'pay_status_new.html', {
        'return_url': settings.APP_OPEN_URL,
        'trade_status': trade_status
    })


@require_GET
def payfubao_notify(request, app_id):
    try:
        payfubao.check_notify_sign(request, app_id)
        # we must return 'ok' after processing
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('payfubao notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('payfubao notify exception.(%s)' % e)
        return HttpResponse('ok', status=400)


@require_GET
def payfubao_return(request, pay_id):
    pay = order_db.get_pay(pay_id)
    if not pay or pay.status != PAY_STATUS.SUCC:
        trade_status = 0
    else:
        trade_status = 1
    return TemplateResponse(request, 'pay_status_new.html', {
        'return_url': settings.APP_OPEN_URL,
        'trade_status': trade_status
    })


@require_POST
def dirpay_notify(request, app_id):
    try:
        dirpay.check_notify_sign(request, app_id)
        # we must return 'OK' after processing
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('dirpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('dirpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def skillfully_notify(request, app_id):
    try:
        skillfully_pay.check_notify_sign(request, app_id)
        # we must return 'OK' after processing
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('skillfully notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('skillfully notify exception.(%s)' % e)
        return HttpResponse('FAILED', status=400)


@require_GET
def yiaipay_notify(request, app_id):
    try:
        yiaipay.check_notify_sign(request, app_id)
        return HttpResponse('opstate=0', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yiaipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yiaipay notify exception.(%s)' % e)
        return HttpResponse('opstate=1', status=400)


@require_POST
def kailianpay_notify(request, app_id):
    try:
        kailianpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('kailianpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('kailianpay notify exception.(%s)' % e)
        return HttpResponse('failed', status=400)


@require_GET
def duotpay_notify(request, app_id):
    try:
        duotpay.check_notify_sign(request, app_id)
        return HttpResponse('opstate=0', status=200)
    except err.ParamError as e:
        _LOGGER.warn('duotpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('duotpay notify exception.(%s)' % e)
        return HttpResponse('opstate=1', status=400)


@require_GET
def kdpay_notify(request, app_id):
    try:
        kdpay.check_notify_sign(request, app_id)
        return HttpResponse('errCode=0', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('kdpay notify exception.(%s)' % e)
        return HttpResponse('errCode=1', status=400)


@require_POST
def xftpay_notify(request, app_id):
    try:
        xftpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xftpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xftpay notify exception.(%s)' % e)
        return HttpResponse('failed', status=400)


@require_POST
def jcpay_notify(request, app_id):
    try:
        jcpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('jcpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('jcpay notify exception.(%s)' % e)
        return HttpResponse('failed', status=400)


@require_POST
def bopay_notify(request, app_id):
    try:
        bopay.check_notify_sign(request, app_id)
        return HttpResponse('result=SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('bopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('bopay notify exception.(%s)' % e)
        return HttpResponse('result=FAILED', status=400)


@require_GET
def kvpay_notify(request, app_id):
    try:
        kvpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('kvpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('kvpay notify exception.(%s)' % e)
        return HttpResponse('FAILED', status=400)


@require_GET
def yhxpay_notify(request, app_id):
    try:
        yhxpay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yhxpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yhxpay notify exception.(%s)' % e)
        return HttpResponse('FAILED', status=400)


@require_POST
def yishangpay_notify(request, app_id):
    try:
        yishangpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yishangpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yishangpay notify exception.(%s)' % e)
        return HttpResponse('FAILED', status=400)


@require_POST
def boeingpay_notify(request, app_id):
    try:
        boeingpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('boeingpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('boeingpay notify exception.(%s)' % e)
        return HttpResponse('FAILED', status=400)


@require_POST
def fuyingbao_notify(request, app_id):
    try:
        fuyingbao.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('fuyingbao notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('fuyingbao notify exception.(%s)' % e)
        return HttpResponse('FAILED', status=400)


@require_POST
def okpay_notify(request, app_id):
    try:
        okpay.check_notify_sign(request, app_id)
        return HttpResponse('Y', status=200)
    except err.ParamError as e:
        _LOGGER.warn('okpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('okpay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def our_alipay_notify(request):
    try:
        our_alipay.check_notify_sign(request)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('our_alipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('our_alipay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def aishangpay_notify(request, app_id):
    try:
        aishangpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('aishangpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('aishangpay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def zbtpay_notify(request, app_id):
    try:
        zbtpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zbtpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zbtpay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_GET
def yjhpay_notify(request, app_id):
    try:
        yjhpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yjhpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yjhpay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def e_times_notify(request, app_id):
    try:
        e_times.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('e_times notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('e_times notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def quick_money_notify(request, app_id):
    try:
        quick_money.check_notify_sign(request, app_id)
        return HttpResponse('{"resCode": "0000"}', status=200)
    except err.ParamError as e:
        _LOGGER.warn('quick_money notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('quick_money notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def jiuyingpay_notify(request, app_id):
    try:
        jiuyingpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('jiuyingpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('jiuyingpay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def huihepay_notify(request, app_id):
    try:
        huihepay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('huihepay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('huihepay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def zeshengpay_notify(request, app_id):
    try:
        zeshengpay.check_notify_sign(request, app_id)
        return HttpResponse('''{'code':'00'}''', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zeshengpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zeshengpay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def cywpay_notify(request, app_id):
    try:
        cywpay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('cywpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('cywpay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def guofupay_notify(request, app_id):
    try:
        guofupay.check_notify_sign(request, app_id)
        return HttpResponse('Y', status=200)
    except err.ParamError as e:
        _LOGGER.warn('guofupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('guofupay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def tonglepay_notify(request, app_id):
    try:
        tonglepay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('tonglepay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('tonglepay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_GET
def easypay_notify(request, app_id):
    try:
        easypay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('easypay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('easypay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def doudoupay_notify(request, app_id):
    try:
        doudoupay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCEED', status=200)
    except err.ParamError as e:
        _LOGGER.warn('doudoupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('doudoupay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def zeropay_notify(request, app_id):
    try:
        zeropay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zeropay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zeropay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def e520pay_notify(request, app_id):
    try:
        e520pay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('e520pay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('e520pay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def zfbpay_notify(request, app_id):
    try:
        zfbpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zfbpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zfbpay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def xlbpay_notify(request, app_id):
    try:
        xlbpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xlbpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xlbpay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def huichaopay_notify(request, app_id):
    try:
        huichaopay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('huichaopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('huichaopay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def weipaipay_notify(request, app_id):
    try:
        weipaipay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('weipaipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('weipaipay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def mobaopay_notify(request, app_id):
    try:
        mobaopay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('mobaopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('mobaopay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def yftpay_notify(request, app_id):
    try:
        yftpay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yftpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yftpay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def bftpay_notify(request, app_id):
    try:
        bftpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('bftpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('bftpay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def yifubaopay_notify(request, app_id):
    try:
        yifubaopay.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yifubao notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yifubao notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_GET
def juchengpay_notify(request, app_id):
    try:
        juchengpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('juchengpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('juchengpay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_GET
def huitianpay_notify(request, app_id):
    try:
        huitianpay.check_notify_sign(request, app_id)
        return HttpResponse('ErrCode=0', status=200)
    except err.ParamError as e:
        _LOGGER.warn('huitianpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('huitianpay notify exception.(%s)' % e)
        return HttpResponse('ProcessError', status=400)


@require_POST
def group_recharge_card_pay_notify(request, app_id):
    try:
        group_recharge_card_pay.check_notify_sign(request, app_id)
        return HttpResponse('{"status" : 0}', status=200)
    except err.ParamError as e:
        _LOGGER.warn('group_recharge_card_pay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('group_recharge_card_pay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def group_recharge_card_pay_create(request, app_id):
    try:
        group_recharge_card_pay.create_card_charge(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('group_recharge_card_pay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('group_recharge_card_pay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def unpay_notify(request, app_id):
    try:
        unpay.check_notify_sign(request, app_id)
        return HttpResponse('opstate=0', status=200)
    except err.ParamError as e:
        _LOGGER.warn('unpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('unpay notify exception.(%s)' % e)
        return HttpResponse('ProcessError', status=400)


@require_GET
def weibaopay_notify(request, app_id):
    try:
        weibaopay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('weibaopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('weibaopay notify exception.(%s)' % e)
        return HttpResponse('ProcessError', status=400)


@require_POST
def hbtpay_notify(request, app_id):
    try:
        hbtpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('hbtpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('hbtpay notify exception.(%s)' % e)
        return HttpResponse('ProcessError', status=400)


@require_GET
def jinyangpay_notify(request, app_id):
    try:
        jinyangpay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('jinyangpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('jinyangpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def lianyingpay_notify(request, app_id):
    try:
        lianyingpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('lianyingpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('lianyingpay notify exception.(%s)' % e)
        return HttpResponse('ProcessError', status=400)


@require_POST
def dirpay_new_notify(request, app_id):
    try:
        dirpay_new.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('dirpay_new notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('dirpay_new notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def tianjipay_notify(request, app_id):
    try:
        tianjipay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('tianjipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('tianjipay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def qiaoshoupay_notify(request, app_id):
    try:
        qiaoshoupay.check_notify_sign(request, app_id)
        return HttpResponse('Y', status=200)
    except err.ParamError as e:
        _LOGGER.warn('qiaoshoupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('qiaoshoupay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def pinduobaopay_notify(request, app_id):
    try:
        pinduobaopay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('pinduobaopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('pinduobaopay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def yiantongpay_notify(request, app_id):
    try:
        yiantongpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yiantongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yiantongpay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_GET
def ruiyunpay_notify(request, app_id):
    try:
        ruiyunpay.check_notify_sign(request, app_id)
        return HttpResponse('opstate=0', status=200)
    except err.ParamError as e:
        _LOGGER.warn('ruiyunpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('ruiyunpay notify exception.(%s)' % e)
        return HttpResponse('opstate=1', status=400)


@require_POST
def baishengpay_notify(request, app_id):
    try:
        baishengpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('baishengpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('baishengpay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def dirpay_union_notify(request, app_id):
    try:
        dirpay_union.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('dirpay_union notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('dirpay_union notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def huanxunpay_notify(request, app_id):
    try:
        huanxunpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('huanxunpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('huanxunpay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def hongchuangpay_notify(request, app_id):
    try:
        hongchuangpay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('hongchuangpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('hongchuangpay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def haifupay_notify(request, app_id):
    try:
        res = haifupay.check_notify_sign(request, app_id)
        return HttpResponse(res, status=200)
    except err.ParamError as e:
        _LOGGER.warn('haifupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('haifupay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_GET
def huiyinpay_notify(request, app_id):
    try:
        huiyinpay.check_notify_sign(request, app_id)
        return HttpResponse('''{'code':'success', 'msg':'success'}''', status=200)
    except err.ParamError as e:
        _LOGGER.warn('huiyinpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('huiyinpay notify exception.(%s)' % e)
        return HttpResponse('failed', status=400)


@require_POST
def ks2pay_notify(request, app_id):
    try:
        ks2pay.check_notify_sign(request, app_id)
        # we must return 'success' after processing
        return HttpResponse('''{"status" : 0}''', status=200)
    except err.ParamError as e:
        _LOGGER.warn('ks2pay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('ks2pay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def qcmypay_notify(request, app_id):
    try:
        qcmypay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('qcmypay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('qcmypay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def huiyintongpay_notify(request, app_id):
    try:
        huiyintongpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('huiyintongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('huiyintongpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def xinjiepay_notify(request, app_id):
    try:
        xinjiepay.check_notify_sign(request, app_id)
        return HttpResponse('opstate=0', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xinjiepay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xinjiepay notify exception.(%s)' % e)
        return HttpResponse('ProcessError', status=400)


@require_GET
def ruibaopay_notify(request, app_id):
    try:
        ruibaopay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('ruibaopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('ruibaopay notify exception.(%s)' % e)
        return HttpResponse('ProcessError', status=400)


@require_POST
def yutoupay_notify(request, app_id):
    try:
        yutoupay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yutoupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yutoupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def xunshoupay_notify(request, app_id):
    try:
        xunshoupay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xunshoupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xunshoupay notify exception.(%s)' % e)
        return HttpResponse('FAILED', status=400)


@require_GET
def suiszfpay_notify(request, app_id):
    try:
        suiszfpay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('suiszfpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('suiszfpay notify exception.(%s)' % e)
        return HttpResponse('FAILED', status=400)


@require_GET
def v3pay_notify(request, app_id):
    try:
        v3pay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('v3pay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('v3pay notify exception.(%s)' % e)
        return HttpResponse('FAILED', status=400)


@require_POST
def beifupay_notify(request, app_id):
    try:
        beifupay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('beifupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('beifupay notify exception.(%s)' % e)
        return HttpResponse('FAILED', status=400)


@require_GET
def xunpay_notify(request, app_id):
    try:
        xunpay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xunpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xunpay notify exception.(%s)' % e)
        return HttpResponse('FAILED', status=400)


@require_POST
def lipay_notify(request, app_id):
    try:
        lipay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('lipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('lipay notify exception.(%s)' % e)
        return HttpResponse('FAILED', status=400)


@require_POST
def zhangxunpay_notify(request, app_id):
    try:
        zhangxunpay.check_notify_sign(request, app_id)
        return HttpResponse('SC000000', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zhangxunpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zhangxunpay notify exception.(%s)' % e)
        return HttpResponse('FAILED', status=400)


@require_GET
def xun1pay_notify(request, app_id):
    try:
        xun1pay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xun1pay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xun1pay notify exception.(%s)' % e)
        return HttpResponse('FAILED', status=400)


@require_POST
def shanyipay_notify(request, app_id):
    try:
        shanyipay.check_notify_sign(request, app_id)
        return HttpResponse('0', status=200)
    except err.ParamError as e:
        _LOGGER.warn('shanyipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('shanyipay notify exception.(%s)' % e)
        return HttpResponse('FAILED', status=400)


@require_POST
def anfupay_notify(request, app_id):
    try:
        anfupay.check_notify_sign(request, app_id)
        return HttpResponse('0', status=200)
    except err.ParamError as e:
        _LOGGER.warn('anfupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('anfupay notify exception.(%s)' % e)
        return HttpResponse('FAILED', status=400)


@require_POST
def e_times_v2_notify(request, app_id):
    try:
        e_times_v2.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('e_times_v2 notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('e_times_v2 notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def dandanpay_notify(request, app_id):
    try:
        dandanpay.check_notify_sign(request, app_id)
        return HttpResponse('1', status=200)
    except err.ParamError as e:
        _LOGGER.warn('dandanpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('dandanpay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def tongdapay_notify(request, app_id):
    try:
        tongdapay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('tongdapay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('tongdapay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def sufupay_notify(request, app_id):
    try:
        sufupay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('sufupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('sufupay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def litaobopay_notify(request, app_id):
    try:
        litaobopay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('litaobopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('litaobopay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def newks2pay_notify(request, app_id):
    try:
        newks2pay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('newks2pay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('newks2pay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def weihubaopay_notify(request, app_id):
    try:
        weihubaopay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('weihubaopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('weihubaopay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def kairuipay_notify(request, app_id):
    try:
        kairuipay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('kairuipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('kairuipay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def xinpay_notify(request, app_id):
    try:
        xinpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xinpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xinpay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def ks3pay_notify(request, app_id):
    try:
        ks3pay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('ks3pay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('ks3pay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def xinkeyunpay_notify(request, app_id):
    try:
        xinkeyunpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xinkeyunpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xinkeyunpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def wanlihuipay_notify(request, app_id):
    try:
        wanlihuipay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('wanlihuipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('wanlihuipay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def duoduopay_notify(request, app_id):
    try:
        duoduopay.check_notify_sign(request, app_id)
        return HttpResponse('success|9999', status=200)
    except err.ParamError as e:
        _LOGGER.warn('duoduopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('duoduopay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def zhifutongpay_notify(request, app_id):
    try:
        zhifutongpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zhifutongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zhifutongpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def lezhifupay_notify(request, app_id):
    try:
        lezhifupay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('lezhifupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('lezhifupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def gopay_notify(request, app_id):
    try:
        gopay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('gopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('gopay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def yihuipay_notify(request, app_id):
    try:
        yihuipay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yihuipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yihuipay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def guangtongpay_notify(request, app_id):
    try:
        guangtongpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('guangtongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('guangtongpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def yizhipay_notify(request, app_id):
    try:
        yizhipay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yizhipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yizhipay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def sc365pay_notify(request, app_id):
    try:
        sc365pay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCEED', status=200)
    except err.ParamError as e:
        _LOGGER.warn('sc365pay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('sc365pay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def hanpay_notify(request, app_id):
    try:
        hanpay.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('hanpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('hanpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def xindanpay_notify(request, app_id):
    try:
        xindanpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xindanpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xindanpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def mishangpay_notify(request, app_id):
    try:
        mishangpay.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('mishangpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('mishangpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def xunlianpay_notify(request, app_id):
    try:
        xunlianpay.check_notify_sign(request, app_id)
        return HttpResponse('0', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xunlianpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xunlianpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def boshipay_notify(request, app_id):
    try:
        boshipay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('boshipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('boshipay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def zfpfpay_notify(request, app_id):
    try:
        zfpfpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zfpfpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zfpfpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def zhipay_notify(request, app_id):
    try:
        zhipay.check_notify_sign(request, app_id)
        return HttpResponse('0', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zhipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zhipay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def tengpay_notify(request, app_id):
    try:
        tengpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('tengpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('tengpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def xin1pay_notify(request, app_id):
    try:
        xin1pay.check_notify_sign(request, app_id)
        return HttpResponse('00', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xin1pay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xin1pay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def jinruipay_notify(request, app_id):
    try:
        jinruipay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('jinruipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('jinruipay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def ks6pay_notify(request, app_id):
    try:
        ks6pay.check_notify_sign(request, app_id)
        return HttpResponse('''{"status":0}''', status=200)
    except err.ParamError as e:
        _LOGGER.warn('ks6pay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('ks6pay notify exception.(%s)' % e)
        return HttpResponse('''{"status":1}''', status=400)


@require_POST
def mishang_nativepay_notify(request, app_id):
    try:
        mishang_nativepay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('mishang_nativepay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('mishang_nativepay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def zhinengyunpay_notify(request, app_id):
    try:
        zhinengyunpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zhinengyunpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zhinengyunpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def dopay_notify(request, app_id):
    try:
        dopay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('dopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('dopay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def jiufutongpay_notify(request, app_id):
    try:
        jiufutongpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('jiufutongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('jiufutongpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def moshangpay_notify(request, app_id):
    try:
        moshangpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('moshangpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('moshangpay notify exception.(%s)' % e)
        return HttpResponse('opstate=-1', status=400)


@require_POST
def yingtongpay_notify(request, app_id):
    try:
        yingtongpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yingtongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yingtongpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def wufupay_notify(request, app_id):
    try:
        wufupay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('wufupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('wufupay notify exception.(%s)' % e)
        return HttpResponse('FAILED', status=400)


@require_POST
def dongfangpay_notify(request, app_id):
    try:
        dongfangpay.check_notify_sign(request, app_id)
        return HttpResponse('00', status=200)
    except err.ParamError as e:
        _LOGGER.warn('dongfangpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('dongfangpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def qlshpay_notify(request, app_id):
    try:
        qlshpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('qlshpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('qlshpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def zhinengyunpay_v5_notify(request, app_id):
    try:
        zhinengyunpay_v5.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zhinengyunpay_v5 notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zhinengyunpay_v5 notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def dopay_v1_notify(request, app_id):
    try:
        dopay_v1.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('dopay_v1 notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('dopay_v1 notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def renrenpay_notify(request, app_id):
    try:
        renrenpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('renrenpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('renrenpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def xinshunchangpay_notify(request, app_id):
    try:
        xinshunchangpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xinshunchangpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xinshunchangpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def shuntongpay_notify(request, app_id):
    try:
        shuntongpay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('shuntongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('shuntongpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def hub365pay_notify(request, app_id):
    try:
        hub365pay.check_notify_sign(request, app_id)
        return HttpResponse('888888', status=200)
    except err.ParamError as e:
        _LOGGER.warn('hub365pay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('hub365pay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def mishangpay_v3_notify(request, app_id):
    try:
        mishangpay_v3.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('mishangpay_v3 notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('mishangpay_v3 notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def mishangpay_v4_notify(request, app_id):
    try:
        mishangpay_v4.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('mishangpay_v4 notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('mishangpay_v4 notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def wopay_notify(request, app_id):
    try:
        wopay.check_notify_sign(request, app_id)
        return HttpResponse('''{'result':'ok'}''', status=200)
    except err.ParamError as e:
        _LOGGER.warn('wopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('wopay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def spay_notify(request, app_id):
    try:
        spay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('spay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('spay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def kpay_notify(request, app_id):
    try:
        kpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('kpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('kpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def xinfutongpay_notify(request, app_id):
    try:
        xinfutongpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xinfutongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xinfutongpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def shenglipay_notify(request, app_id):
    try:
        shenglipay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('shenglipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('shenglipay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def lianruipay_notify(request, app_id):
    try:
        lianruipay.check_notify_sign(request, app_id)
        return HttpResponse('opstate=0', status=200)
    except err.ParamError as e:
        _LOGGER.warn('lianruipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('lianruipay notify exception.(%s)' % e)


@require_POST
def zhihuipay_notify(request, app_id):
    try:
        zhihuipay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zhihuipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zhihuipay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def xindaomipay_notify(request, app_id):
    try:
        xindaomipay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xindaomipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xindaomipay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def yunbeipay_notify(request, app_id):
    try:
        yunbeipay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yunbeipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yunbeipay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def shuntingpay_notify(request, app_id):
    try:
        shuntingpay.check_notify_sign(request, app_id)
        return HttpResponse('opstate=0', status=200)
    except err.ParamError as e:
        _LOGGER.warn('shuntingpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('shuntingpay notify exception.(%s)' % e)


@require_POST
def hengfutongpay_notify(request, app_id):
    try:
        hengfutongpay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('hengfutongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('hengfutongpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def shenglipay_v2_notify(request, app_id):
    try:
        shenglipay_v2.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('shenglipay_v2 notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('shenglipay_v2 notify exception.(%s)' % e)
        return HttpResponse('ERROR', status=400)


@require_POST
def tongyupay_notify(request, app_id):
    try:
        tongyupay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('tongyupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('tongyupay notify exception.(%s)' % e)
        return HttpResponse('ERROR', status=400)


@require_POST
def pay83_notify(request, app_id):
    try:
        pay83.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('pay83 notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('pay83 notify exception.(%s)' % e)
        return HttpResponse('ERROR', status=400)


@require_POST
def meilianpay_notify(request, app_id):
    try:
        meilianpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('meilianpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('meilianpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def shunfutongpay_notify(request, app_id):
    try:
        shunfutongpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('shunfutongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('shunfutongpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def chuangyingpay_notify(request, app_id):
    try:
        chuangyingpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('chuangyingpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('chuangyingpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def zhongfutongpay_notify(request, app_id):
    try:
        zhongfutongpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zhongfutongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zhongfutongpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def ks7pay_notify(request, app_id):
    try:
        ks7pay.check_notify_sign(request, app_id)
        return HttpResponse('''{"status":0}''', status=200)
    except err.ParamError as e:
        _LOGGER.warn('ks7pay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('ks7pay notify exception.(%s)' % e)
        return HttpResponse('''{"status":0}''', status=400)


@require_POST
def futurepay_notify(request, app_id):
    try:
        futurepay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('futurepay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('futurepay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def huifupay_notify(request, app_id):
    try:
        huifupay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('huifupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('huifupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def skypay_notify(request, app_id):
    try:
        skypay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('skypay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('skypay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def sifangpay_notify(request, app_id):
    try:
        sifangpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('sifangpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('sifangpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def suhuipay_notify(request, app_id):
    try:
        suhuipay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('suhuipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('suhuipay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def yurunpay_notify(request, app_id):
    try:
        yurunpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yurunpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yurunpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def zhipay_v2_notify(request, app_id):
    try:
        zhipay_v2.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zhipay_v2 notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zhipay_v2 notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def makupay_notify(request, app_id):
    try:
        makupay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('makupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('makupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def feiniubaopay_notify(request, app_id):
    try:
        feiniubaopay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('feiniubaopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('feiniubaopay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def huifulapay_notify(request, app_id):
    try:
        huifulapay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('huifulapay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('huifulapay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def grouppay_notify(request, app_id):
    try:
        grouppay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('grouppay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.RepeatError as e:
        _LOGGER.warn('grouppay success repeat pay, %s', e)
        return HttpResponse('success repeat pay', status=200)
    except err.RepeatNotifyError as e:
        _LOGGER.warn('grouppay success repeat pay, %s', e)
        return HttpResponse('success', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('grouppay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def kaixinguopay_notify(request, app_id):
    try:
        kaixinguopay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('kaixinguopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('kaixinguopay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def xinshunfupay_notify(request, app_id):
    try:
        xinshunfupay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xinshunfupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xinshunfupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def hkerpay_notify(request, app_id):
    try:
        hkerpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('hkerpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('hkerpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def fuhuitongpay_notify(request, app_id):
    try:
        fuhuitongpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('fuhuitongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('fuhuitongpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def xintaipay_notify(request, app_id):
    try:
        xintaipay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xintaipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xintaipay notify exception.(%s)' % e)
        return HttpResponse('', status=400)


@require_POST
def cpay_notify(request, app_id):
    try:
        cpay.check_notify_sign(request, app_id)
        return HttpResponse('{"msg":"success"}', status=200)
    except err.ParamError as e:
        _LOGGER.warn('cpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('cpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def yifupay_notify(request, app_id):
    try:
        yifupay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yifupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yifupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def youshengpay_notify(request, app_id):
    try:
        youshengpay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('youshengpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('youshengpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def newpay_notify(request, app_id):
    try:
        newpay.check_notify_sign(request, app_id)
        return HttpResponse('SC000000', status=200)
    except err.ParamError as e:
        _LOGGER.warn('newpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('newpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def superstarpay_notify(request, app_id):
    try:
        superstarpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('superstarpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('superstarpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def wohuibaopay_notify(request, app_id):
    try:
        wohuibaopay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('wohuibaopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('wohuibaopay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def jufpay_notify(request, app_id):
    try:
        jufpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('jufpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('jufpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def haofupay_notify(request, app_id):
    try:
        haofupay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('haofupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('haofupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def limafupay_notify(request, app_id):
    try:
        limafupay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('limafupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('limafupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def ehuipay_notify(request, app_id):
    try:
        ehuipay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('ehuipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('ehuipay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def lefupay_notify(request, app_id):
    try:
        lefupay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('lefupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('lefupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def huitongbaopay_notify(request, app_id):
    try:
        huitongbaopay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('huitongbaopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('huitongbaopay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def baijiepay_notify(request, app_id):
    try:
        baijiepay.check_notify_sign(request, app_id)
        return HttpResponse(json.dumps({"code": "00"}), status=200)
    except err.ParamError as e:
        _LOGGER.warn('baijiepay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('baijiepay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def kuailaifupay_notify(request, app_id):
    try:
        kuailaifupay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('kuailaifupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('kuailaifupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def tongbaopay_notify(request, app_id):
    try:
        tongbaopay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('tongbaopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('tongbaopay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def moopay_notify(request, app_id):
    try:
        moopay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('moopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('moopay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def manapay_notify(request, app_id):
    try:
        manapay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('manapay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('manapay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def nihoutaopay_notify(request, app_id):
    try:
        nihoutaopay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('nihoutaopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('nihoutaopay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def yilianpay_notify(request, app_id):
    try:
        yilianpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yilianpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yilianpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def yilianpay_return(request, pay_id):
    pay = order_db.get_pay(pay_id)
    if not pay or pay.status != PAY_STATUS.SUCC:
        trade_status = 0
    else:
        trade_status = 1
    return TemplateResponse(request, 'pay_status_new.html', {
        'return_url': settings.APP_OPEN_URL,
        'trade_status': trade_status
    })


@require_POST
def dajipay_notify(request, app_id):
    try:
        dajipay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('dajipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('dajipay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def zhiyuanpay_notify(request, app_id):
    try:
        zhiyuanpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zhiyuanpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zhiyuanpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def hengfengpay_notify(request, app_id):
    try:
        hengfengpay.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('hengfengpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('hengfengpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def cpayalipay2bank_notify(request, app_id):
    try:
        cpayalipay2bank.check_notify_sign(request, app_id)
        return HttpResponse('{"msg":"success"}', status=200)
    except err.ParamError as e:
        _LOGGER.warn('cpayalipay2bank notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('cpayalipay2bank notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def dudupay_notify(request, app_id):
    try:
        dudupay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('dudupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('dudupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def zhifubangpay_notify(request, app_id):
    try:
        zhifubangpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zhifubangpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zhifubangpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def ffpay_notify(request, app_id):
    try:
        ffpay.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('ffpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('ffpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def quansupay_notify(request, app_id):
    try:
        quansupay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('quansupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('quansupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def gaotongpay_notify(request, app_id):
    try:
        gaotongpay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('gaotongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('gaotongpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def onepayquickpay_notify(request, app_id):
    try:
        onepayquickpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('onepayquickpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('onepayquickpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def jintuopay_notify(request, app_id):
    try:
        jintuopay.check_notify_sign(request, app_id)
        # we must return 'OK' after processing
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('jintuopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('jintuopay notify exception.(%s)' % e)
        return HttpResponse('FAIL', status=400)


@require_GET
def jintuopay_return(request, pay_id):
    pay = order_db.get_pay(pay_id)
    if not pay or pay.status != PAY_STATUS.SUCC:
        trade_status = 0
    else:
        trade_status = 1
    return TemplateResponse(request, 'pay_status_new.html', {
        'return_url': settings.APP_OPEN_URL,
        'trade_status': trade_status
    })


@require_POST
def spayunionpay_notify(request, app_id):
    try:
        spayunionpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('spayunionpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('spayunionpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def yunrongpay_notify(request, app_id):
    try:
        yunrongpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yunrongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yunrongpay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_GET
def shengfupay_notify(request, app_id):
    try:
        shengfupay.check_notify_sign(request, app_id)
        return HttpResponse('opstate=0', status=200)
    except err.ParamError as e:
        _LOGGER.warn('shengfupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('shengfupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def wantongpay_notify(request, app_id):
    try:
        wantongpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('wantongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('wantongpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def yingxintongpay_notify(request, app_id):
    try:
        yingxintongpay.check_notify_sign(request, app_id)
        # we must return 'opstate=0' after processing
        return HttpResponse('opstate=0', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yingxintongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yingxintongpay notify exception.(%s)' % e)
        return HttpResponse('opstate=-1', status=400)


@require_POST
def ttpay_notify(request, app_id):
    try:
        ttpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('ttpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('ttpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def angelapay_notify(request, app_id):
    try:
        angelapay.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('angelapay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('angelapay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def wangshifupay_notify(request, app_id):
    try:
        wangshifupay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('wangshifupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('wangshifupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def bajiepay_notify(request, app_id):
    try:
        bajiepay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('bajiepay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('bajiepay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def hydra_notify(request, app_id):
    try:
        hydra.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('hydra notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('hydra notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def fengyunpay_notify(request, app_id):
    try:
        fengyunpay.check_notify_sign(request, app_id)
        return HttpResponse('true', status=200)
    except err.ParamError as e:
        _LOGGER.warn('fengyunpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('fengyunpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def newdaomipay_notify(request, app_id):
    try:
        newdaomipay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('newdaomipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('newdaomipay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def mepay_notify(request, app_id):
    try:
        mepay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('mepay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('mepay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def onepay_notify(request, app_id):
    try:
        onepay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('onepay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('onepay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def jubaopay_notify(request, app_id):
    try:
        jubaopay.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('jubaopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('jubaopay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def yixinpay_notify(request, app_id):
    try:
        yixinpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yixinpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yixinpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def huifutongpay_notify(request, app_id):
    try:
        huifutongpay.check_notify_sign(request, app_id)
        # we must return 'OK' after processing
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('huifutongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('huifutongpay notify exception.(%s)' % e)
        return HttpResponse('FAIL', status=400)


@require_POST
def onepaycashpay_notify(request, app_id):
    try:
        onepaycashpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('onepaycashpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('onepaycashpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def zhifupay_notify(request, app_id):
    try:
        zhifupay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zhifupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zhifupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def lefucloudquickpay_notify(request, app_id):
    try:
        lefucloudquickpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('lefucloudquickpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('lefucloudquickpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def anbaofupay_notify(request, app_id):
    try:
        anbaofupay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('anbaofupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('anbaofupay notify exception.(%s)' % e)
        return HttpResponse('FAILED', status=400)


@require_POST
def xinfupay_notify(request, app_id):
    try:
        xinfupay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xinfupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xinfupay notify exception.(%s)' % e)
        return HttpResponse('FAIL', status=400)


@require_POST
def lefuv2pay_notify(request, app_id):
    try:
        lefuv2pay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('lefuv2pay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('lefuv2pay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def jbcpay_notify(request, app_id):
    try:
        jbcpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('jbcpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('jbcpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def gbpays_notify(request, app_id):
    try:
        gbpays.check_notify_sign(request, app_id)
        return HttpResponse('1', status=200)
    except err.ParamError as e:
        _LOGGER.warn('gbpays notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('gbpays notify exception.(%s)' % e)
        return HttpResponse('FAIL', status=400)


@require_POST
def miaofupay_notify(request, app_id):
    try:
        miaofupay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('miaofupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('miaofupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def jinyupay_notify(request, app_id):
    try:
        jinyupay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.RepeatNotifyError as e:
        _LOGGER.warn('jinyupay repeat notify, %s', e)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('jinyupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('jinyupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def chuangpupay_notify(request, app_id):
    try:
        chuangpupay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('chuangpupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('chuangpupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def coolpay_notify(request, app_id):
    try:
        coolpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('coolpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('coolpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def zhichengpay_notify(request, app_id):
    try:
        zhichengpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zhichengpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zhichengpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def changtongpay_notify(request, app_id):
    try:
        changtongpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('changtongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('changtongpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def shanfupay_notify(request, app_id):
    try:
        shanfupay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('shanfupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('shanfupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def lixianfupay_notify(request, app_id):
    try:
        lixianfupay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('lixianfupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('lixianfupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def wandapay_notify(request, app_id):
    try:
        wandapay.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('wandapay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('wandapay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def youlianpay_notify(request, app_id):
    try:
        youlianpay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('youlianpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('youlianpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def onegopay_notify(request, app_id):
    try:
        onegopay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('onegopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('onegopay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def wenfupay_notify(request, app_id):
    try:
        wenfupay.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('wenfupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('wenfupay notify exception.(%s)' % e)
        return HttpResponse('FAIL', status=400)


@require_GET
def huidingpay_notify(request, app_id):
    try:
        huidingpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('huidingpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('huidingpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def hpay_notify(request, app_id):
    try:
        hpay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('hpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('hpay notify exception.(%s)' % e)
        return HttpResponse('FAIL', status=400)


@require_POST
def peiqipay_notify(request, app_id):
    try:
        peiqipay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('peiqipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('peiqipay notify exception.(%s)' % e)
        return HttpResponse('FAILED', status=400)


@require_POST
def paopaochengpay_notify(request, app_id):
    try:
        paopaochengpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('paopaochengpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('paopaochengpay notify exception.(%s)' % e)
        return HttpResponse('FAIL', status=400)


@require_POST
def xinwirelesspay_notify(request, app_id):
    try:
        xinwirelesspay.check_notify_sign(request, app_id)
        return HttpResponse('000000', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xinwirelesspay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xinwirelesspay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def yifutongpay_notify(request, app_id):
    try:
        yifutongpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yifutongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yifutongpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def jubaopenpay_notify(request, app_id):
    data = dict(request.POST.iteritems())
    result = {'company_order_num': str(data['company_order_num']),
              'mownecum_order_num': str(data['mownecum_order_num']),
              'status': '1'}
    try:
        jubaopenpay.check_notify_sign(request, app_id)
        return HttpResponse(str(result), status=200)
    except err.ParamError as e:
        _LOGGER.warn('jubaopenpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('jubaopenpay notify exception.(%s)' % e)
        result['status'] = '0'
        result['error_msg'] = ''
        return HttpResponse(str(result), status=400)


@require_POST
def fubaotongpay_notify(request, app_id):
    try:
        fubaotongpay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('fubaotongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('fubaotongpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def mfpay_notify(request, app_id):
    try:
        mfpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('mfpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('mfpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def yfpay_notify(request, app_id):
    try:
        yfpay.check_notify_sign(request, app_id)
        return HttpResponse('true', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yfpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yfpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def baihepay_notify(request, app_id):
    try:
        baihepay.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('baihepay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('baihepay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def yifu2pay_notify(request, app_id):
    try:
        yifu2pay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yifu2pay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yifu2pay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def lixianfu2pay_notify(request, app_id):
    try:
        lixianfu2pay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('lixianfu2pay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('lixianfu2pay notify exception.(%s)', e)
        return HttpResponse('fail', status=400)


@require_POST
def shikongpay_notify(request, app_id):
    try:
        shikongpay.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('shikongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('shikongpay notify exception.(%s)', e)
        return HttpResponse('fail', status=400)


@require_POST
def lekupay_notify(request, app_id):
    try:
        lekupay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('lekupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('lekupay notify exception.(%s)', e)
        return HttpResponse('fail', status=400)


@require_POST
def epay_notify(request, app_id):
    try:
        epay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('epay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('epay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def miaojiepay_notify(request, app_id):
    try:
        miaojiepay.check_notify_sign(request, app_id)
        # we must return 'OK' after processing
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('miaojiepay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('miaojiepay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def amazepay_notify(request, app_id):
    try:
        amazepay.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('amazepay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('amazepay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def zhanshenpay_notify(request, app_id):
    try:
        zhanshenpay.check_notify_sign(request, app_id)
        return HttpResponse('Success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zhanshenpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zhanshenpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def paopaoyingpay_notify(request, app_id):
    try:
        paopaoyingpay.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('paopaoyingpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('paopaoyingpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def feiyupay_notify(request, app_id):
    try:
        feiyupay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('feiyupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('feiyupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def hengjiepay_notify(request, app_id):
    try:
        hengjiepay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('hengjiepay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('hengjiepay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def yifubaopayv2_notify(request, app_id):
    try:
        yifubaopayv2.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yifubaopayv2 notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yifubaopayv2 notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def easypayv2_notify(request, app_id):
    try:
        easypayv2.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('easypayv2 notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('easypayv2 notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def lelianmengpay_notify(request, app_id):
    try:
        lelianmengpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('lelianmengpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('lelianmengpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def kaisuobaopay_notify(request, app_id):
    try:
        kaisuobaopay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('kaisuobaopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('kaisuobaopay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def tenglongpay_notify(request, app_id):
    try:
        tenglongpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('tenglongpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('tenglongpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def dingshengpay_notify(request, app_id):
    try:
        dingshengpay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('dingshengpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('dingshengpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def newchuangpupay_notify(request, app_id):
    try:
        newchuangpupay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('newchuangpupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('newchuangpupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def anxinpay_notify(request, app_id):
    try:
        anxinpay.check_notify_sign(request, app_id)
        return HttpResponse('opstate=0', status=200)
    except err.ParamError as e:
        _LOGGER.warn('anxinpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('anxinpay notify exception.(%s)' % e)
        return HttpResponse('ProcessError', status=400)


@require_POST
def changshengpay_notify(request, app_id):
    try:
        changshengpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('changshengpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('changshengpay notify exception.(%s)' % e)


def langlangpay_notify(request, app_id):
    try:
        langlangpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('langlangpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('langlangpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def yilaipay_notify(request, app_id):
    try:
        yilaipay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yilaipay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yilaipay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def sdppay_notify(request, app_id):
    try:
        sdppay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('sdppay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('sdppay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def anpay_notify(request, app_id):
    try:
        anpay.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('anpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('anpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def shengdapay_notify(request, app_id):
    try:
        shengdapay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('shengdapay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('shengdapay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def qixingpay_notify(request, app_id):
    try:
        qixingpay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('qixingpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('qixingpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def linghangpay_notify(request, app_id):
    try:
        linghangpay.check_notify_sign(request, app_id)
        return HttpResponse('Success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('linghangpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('linghangpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def dafapay_notify(request, app_id):
    try:
        dafapay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('dafapay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('dafapay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def ddpay_notify(request, app_id):
    try:
        ddpay.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('ddpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('ddpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def zhuoyuepay_notify(request, app_id):
    try:
        zhuoyuepay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zhuoyuepay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zhuoyuepay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def yibaopay_notify(request, app_id):
    try:
        yibaopay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('yibaopay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('yibaopay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def suifupay_notify(request, app_id):
    try:
        suifupay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('suifupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('suifupay notify exception.(%s)' % e)
        return HttpResponse('ERROR', status=400)


@require_GET
def un2pay_notify(request, app_id):
    try:
        un2pay.check_notify_sign(request, app_id)
        return HttpResponse('ok', status=200)
    except err.ParamError as e:
        _LOGGER.warn('un2pay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('un2pay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def nppay_notify(request, app_id):
    try:
        nppay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('nppay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('nppay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def wanhepay_notify(request, app_id):
    try:
        wanhepay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('wanhepay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('wanhepay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def newlandpay_notify(request, app_id):
    try:
        newlandpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('newlandpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('newlandpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def kukapay_notify(request, app_id):
    try:
        kukapay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('kukapay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('kukapay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_GET
def huayingpay_notify(request, app_id):
    try:
        huayingpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('huayingpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('huayingpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def zhixinfupay_notify(request, app_id):
    try:
        zhixinfupay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zhixinfupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zhixinfupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def huifuyazhoupay_notify(request, app_id):
    try:
        huifuyazhoupay.check_notify_sign(request, app_id)
        return HttpResponse('SUCCESS', status=200)
    except err.ParamError as e:
        _LOGGER.warn('huifuyazhoupay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('huifuyazhoupay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def xiguapay_notify(request, app_id):
    try:
        xiguapay.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xiguapay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xiguapay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def goldengragonpay_notify(request, app_id):
    try:
        goldengragonpay.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('goldengragonpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('goldengragonpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


def xundingpay_notify(request, app_id):
    try:
        xundingpay.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xundingpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xundingpay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def newbaihepay_notify(request, app_id):
    try:
        newbaihepay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('newbaihepay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('newbaihepay notify ip error, %s', e)
        return HttpResponse('newbaihepay notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('newbaihepay notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def dezhunpay_notify(request, app_id):
    try:
        dezhunpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('dezhunpay notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('notify ip error, %s', e)
        return HttpResponse('notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('dezhunpay notify exception.(%s)' % e)
        return HttpResponse('N', status=400)


@require_POST
def xintongpay_notify(request, app_id):
    try:
        xintongpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xintongpay_notify notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('xintongpay_notify notify ip error, %s', e)
        return HttpResponse('xintongpay_notify notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xintongpay_notify notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def majipay_notify(request, app_id):
    try:
        majipay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('majipay_notify notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('majipay_notify notify ip error, %s', e)
        return HttpResponse('majipay_notify notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('majipay_notify notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def miaomiaopay_notify(request, app_id):
    try:
        miaomiaopay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('miaomiaopay_notify notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('miaomiaopay_notify notify ip error, %s', e)
        return HttpResponse('miaomiaopay_notify notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('miaomiaopay_notify notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_http_methods(['GET', 'POST'])
def qiantongyunpay_notify(request, app_id):
    try:
        qiantongyunpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('qiantongyunpay_notify notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('qiantongyunpay_notify notify ip error, %s', e)
        return HttpResponse('qiantongyunpay_notify notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('qiantongyunpay_notify notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def xiyangyangpay_notify(request, app_id):
    try:
        xiyangyangpay.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.ParamError as e:
        _LOGGER.warn('xiyangyangpay_notify notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('xiyangyangpay_notify notify ip error, %s', e)
        return HttpResponse('xiyangyangpay_notify notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('xiyangyangpay_notify notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def ccbypay_notify(request, app_id):
    try:
        ccbypay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('ccbypay_notify notify param error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('ccbypay_notify notify ip error, %s', e)
        return HttpResponse('ccbypay_notify notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('ccbypay_notify notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def zhongchengpay_notify(request, app_id):
    try:
        zhongchengpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('zhongchengpay_notifyparam error, %s', e)
        return HttpResponse('notify param error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('zhongchengpay_notify ip error, %s', e)
        return HttpResponse('zhongchengpay_notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('zhongchengpay_notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def sijipay_notify(request, app_id):
    try:
        sijipay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('sijipay_notify param error, %s', e)
        return HttpResponse('notify param error', status=400)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('sijipay_notify ip error, %s', e)
        return HttpResponse('sijipay_notify ip not in white lists', status=200)
    except Exception as e:
        _LOGGER.exception('sijipay_notify exception.(%s)' % e)
        return HttpResponse('fail', status=400)


@require_POST
def jiefupay_notify(request, app_id):
    try:
        jiefupay.check_notify_sign(request, app_id)
        return HttpResponse('{"error_msg":"","status":"1"}', status=200)
    except err.SignError as e:
        _LOGGER.warn('jiefupay_notify sign error, %s', e)
        return HttpResponse('sign error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('jiefupay_notify notify ip error, %s', e)
        return HttpResponse('ip not in white lists', status=200)
    except err.NotResponsePayIdError as e:
        _LOGGER.warn('jiefupay_notify notify response is not pay_id, %s', e)
        return HttpResponse('response is not pay_id', status=200)
    except err.NotPayOrderError as e:
        _LOGGER.warn('jiefupay_notify notify list not pay_order, %s', e)
        return HttpResponse('this order is not found', status=200)
    except err.ProcessedPayOrderError as e:
        _LOGGER.warn('jiefupay_notify notify this order is processed, %s', e)
        return HttpResponse('this order is processed', status=200)
    except Exception as e:
        _LOGGER.exception('jiefupay_notify notify exception.(%s)' % e)
        return HttpResponse('other error', status=400)


@require_POST
def shanpupay_notify(request, app_id):
    try:
        shanpupay.check_notify_sign(request, app_id)
        return HttpResponse('respCode=000000', status=200)
    except err.SignError as e:
        _LOGGER.warn('shanpupay_notify sign error, %s', e)
        return HttpResponse('sign error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('shanpupay_notify notify ip error, %s', e)
        return HttpResponse('ip not in white lists', status=200)
    except err.NotResponsePayIdError as e:
        _LOGGER.warn('shanpupay_notify response is not pay_id, %s', e)
        return HttpResponse('response is not pay_id', status=200)
    except err.NotPayOrderError as e:
        _LOGGER.warn('shanpupay_notify list not pay_order, %s', e)
        return HttpResponse('this order is not found', status=200)
    except err.ProcessedPayOrderError as e:
        _LOGGER.warn('shanpupay_notify this order is processed, %s', e)
        return HttpResponse('this order is processed', status=200)
    except Exception as e:
        _LOGGER.exception('shanpupay_notify notify exception.(%s)' % e)
        return HttpResponse('other error', status=400)


@require_POST
def anjipay_notify(request, app_id):
    try:
        anjipay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.SignError as e:
        _LOGGER.warn('anjipay_notify sign error, %s', e)
        return HttpResponse('sign error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('anjipay_notify notify ip error, %s', e)
        return HttpResponse('ip not in white lists', status=200)
    except err.NotResponsePayIdError as e:
        _LOGGER.warn('anjipay_notify notify response is not pay_id, %s', e)
        return HttpResponse('response is not pay_id', status=200)
    except err.NotPayOrderError as e:
        _LOGGER.warn('anjipay_notify list not pay_order, %s', e)
        return HttpResponse('this order is not found', status=200)
    except err.ProcessedPayOrderError as e:
        _LOGGER.warn('anjipay_notify this order is processed, %s', e)
        return HttpResponse('this order is processed', status=200)
    except Exception as e:
        _LOGGER.exception('anjipay_notify notify exception.(%s)', e)
        return HttpResponse('other error', status=400)


@require_POST
def globalpay_notify(request, app_id):
    try:
        globalpay.check_notify_sign(request, app_id)
        return HttpResponse('{"code":200}', status=200)
    except err.SignError as e:
        _LOGGER.warn('globalpay_notify sign error, %s', e)
        return HttpResponse('sign error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('globalpay_notify notify ip error, %s', e)
        return HttpResponse('ip not in white lists', status=200)
    except err.NotResponsePayIdError as e:
        _LOGGER.warn('globalpay_notify notify response is not pay_id, %s', e)
        return HttpResponse('response is not pay_id', status=200)
    except err.NotPayOrderError as e:
        _LOGGER.warn('globalpay_notify list not pay_order, %s', e)
        return HttpResponse('this order is not found', status=200)
    except err.ProcessedPayOrderError as e:
        _LOGGER.warn('globalpay_notify this order is processed, %s', e)
        return HttpResponse('this order is processed', status=200)
    except Exception as e:
        _LOGGER.exception('globalpay_notify notify exception.(%s)', e)
        return HttpResponse('other error', status=400)


@require_POST
def toppay_notify(request, app_id):
    try:
        toppay.check_notify_sign(request, app_id)
        return HttpResponse('OK', status=200)
    except err.SignError as e:
        _LOGGER.warn('toppay_notify sign error, %s', e)
        return HttpResponse('sign error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('toppay_notify notify ip error, %s', e)
        return HttpResponse('ip not in white lists', status=200)
    except err.NotResponsePayIdError as e:
        _LOGGER.warn('toppay_notify notify response is not pay_id, %s', e)
        return HttpResponse('response is not pay_id', status=200)
    except err.NotPayOrderError as e:
        _LOGGER.warn('toppay_notify list not pay_order, %s', e)
        return HttpResponse('this order is not found', status=200)
    except err.ProcessedPayOrderError as e:
        _LOGGER.warn('toppay_notify this order is processed, %s', e)
        return HttpResponse('this order is processed', status=200)
    except Exception as e:
        _LOGGER.exception('toppay_notify notify exception.(%s)', e)
        return HttpResponse('other error', status=400)


@require_POST
def flogerpay_notify(request, app_id):
    try:
        flogerpay.check_notify_sign(request, app_id)
        return HttpResponse('{"status":0}', status=200)
    except err.SignError as e:
        _LOGGER.warn('flogerpay_notify sign error, %s', e)
        return HttpResponse('sign error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('flogerpay_notify notify ip error, %s', e)
        return HttpResponse('ip not in white lists', status=200)
    except err.NotResponsePayIdError as e:
        _LOGGER.warn('flogerpay_notify notify response is not pay_id, %s', e)
        return HttpResponse('response is not pay_id', status=200)
    except err.NotPayOrderError as e:
        _LOGGER.warn('flogerpay_notify list not pay_order, %s', e)
        return HttpResponse('this order is not found', status=200)
    except err.ProcessedPayOrderError as e:
        _LOGGER.warn('flogerpay_notify this order is processed, %s', e)
        return HttpResponse('this order is processed', status=200)
    except Exception as e:
        _LOGGER.exception('flogerpay_notify notify exception.(%s)', e)
        return HttpResponse('other error', status=400)


@require_POST
def xinghuopay_notify(request, app_id):
    try:
        xinghuopay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.SignError as e:
        _LOGGER.warn('xinghuopay_notify notify sign error, %s', e)
        return HttpResponse('sign error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('xinghuopay_notify notify ip error, %s', e)
        return HttpResponse('ip not in white lists', status=200)
    except err.NotResponsePayIdError as e:
        _LOGGER.warn('xinghuopay_notify notify response is not pay_id, %s', e)
        return HttpResponse('response is not pay_id', status=200)
    except err.NotPayOrderError as e:
        _LOGGER.warn('xinghuopay_notify notify list not pay_order, %s', e)
        return HttpResponse('this order is not found', status=200)
    except err.ProcessedPayOrderError as e:
        _LOGGER.warn('xinghuopay_notify notify this order is processed, %s', e)
        return HttpResponse('this order is processed', status=200)
    except Exception as e:
        _LOGGER.exception('xinghuopay_notify notify exception.(%s)', e)
        return HttpResponse('other error', status=400)


@require_POST
def huishengpay_notify(request, app_id):
    try:
        huishengpay.check_notify_sign(request, app_id)
        return HttpResponse('success', status=200)
    except err.SignError as e:
        _LOGGER.warn('huishengpay_notify notify sign error, %s', e)
        return HttpResponse('sign error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('huishengpay_notify notify ip error, %s', e)
        return HttpResponse('ip not in white lists', status=200)
    except err.NotResponsePayIdError as e:
        _LOGGER.warn('huishengpay_notify notify response is not pay_id, %s', e)
        return HttpResponse('response is not pay_id', status=200)
    except err.NotPayOrderError as e:
        _LOGGER.warn('huishengpay_notify notify list not pay_order, %s', e)
        return HttpResponse('this order is not found', status=200)
    except err.ProcessedPayOrderError as e:
        _LOGGER.warn('huishengpay_notify notify this order is processed, %s', e)
        return HttpResponse('this order is processed', status=200)
    except Exception as e:
        _LOGGER.exception('huishengpay_notify notify exception.(%s)', e)
        return HttpResponse('other error', status=400)


@require_POST
def shayupay_notify(request, app_id):
    try:
        shayupay.check_notify_sign(request, app_id)
        return HttpResponse("success", status=200)
    except err.SignError as e:
        _LOGGER.warn('shayupay_notify notify sign error, %s', e)
        return HttpResponse('sign error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('shayupay_notify notify ip error, %s', e)
        return HttpResponse('ip not in white lists', status=200)
    except err.NotResponsePayIdError as e:
        _LOGGER.warn('shayupay_notify notify response is not pay_id, %s', e)
        return HttpResponse('response is not pay_id', status=200)
    except err.NotPayOrderError as e:
        _LOGGER.warn('shayupay_notify notify list not pay_order, %s', e)
        return HttpResponse('this order is not found', status=200)
    except err.ProcessedPayOrderError as e:
        _LOGGER.warn('shayupay_notify notify this order is processed, %s', e)
        return HttpResponse('this order is processed', status=200)
    except Exception as e:
        _LOGGER.exception('shayupay_notify notify exception.(%s)', e)
        return HttpResponse('other error', status=400)


@require_GET
def zhongfapay_notify(request, app_id):
    try:
        zhongfapay.check_notify_sign(request, app_id)
        return HttpResponse("opstate=0", status=200)
    except err.SignError as e:
        _LOGGER.warn('zhongfapay_notify notify sign error, %s', e)
        return HttpResponse('sign error', status=200)
    except err.IpWhiteListsError as e:
        _LOGGER.warn('zhongfapay_notify notify ip error, %s', e)
        return HttpResponse('ip not in white lists', status=200)
    except err.NotResponsePayIdError as e:
        _LOGGER.warn('zhongfapay_notify notify response is not pay_id, %s', e)
        return HttpResponse('response is not pay_id', status=200)
    except err.NotPayOrderError as e:
        _LOGGER.warn('zhongfapay_notify notify list not pay_order, %s', e)
        return HttpResponse('this order is not found', status=200)
    except err.ProcessedPayOrderError as e:
        _LOGGER.warn('zhongfapay_notify notify this order is processed, %s', e)
        return HttpResponse('this order is processed and not pay success', status=200)
    except Exception as e:
        _LOGGER.exception('zhongfapay_notify notify exception.(%s)', e)
        return HttpResponse('other error', status=400)