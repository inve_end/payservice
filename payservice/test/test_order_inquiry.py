# -*- coding: utf-8 -*-
"""
Create Order Inquiry Sample
"""
import sys
import unittest

reload(sys)
sys.setdefaultencoding('utf-8')

import hashlib
import json
import logging
import random

import requests
import time
from datetime import datetime, timedelta, tzinfo

MCH_ID = '1'
API_KEY = '686bb462294e442a8410a54b7d1b4fa7'

#MCH_ID = '2'
#API_KEY = '883b8e200ef14f83a19d854ae68f7804'

#MCH_ID = '5'
#API_KEY = 'ce0a4678fbf54fed8a102d50887545a2'

# Local
ORDER_INQUIRY_URL = 'http://127.0.0.1:9003/pay/api/inquiry/create/'
# QUERY_URL = 'http://localhost:9003/pay/api/v2/inquiry/query/'

## Local Environment 50
#ORDER_INQUIRY_URL = 'http://103.230.240.50:9003/pay/api/inquiry/create/'
#QUERY_URL = 'http://103.230.240.50:9003/pay/api/inquiry/query/'
order_inquiry_no = 164210673215709005 + 17

def generate_sign(params, key):
        s = ''
        for k in sorted(params.keys()):
            s += '%s=%s&' % (k, params[k])
        s += 'key=%s' % key
        m = hashlib.md5()
        m.update(s.encode('utf8'))
        sign = m.hexdigest().upper()
        return sign

class CreateOrderInquiry(unittest.TestCase):
    def setUp(self):
        self.amount = 1.15 + 17
        self.API_KEY = '686bb462294e442a8410a54b7d1b4fa7'
        self.MCH_ID = '1'

    def create_order_inquiry(amount, order_inquiry_no, self):
        
       parameter_dict = {
            'order_inquiry_no': str(order_inquiry_no),
            'mch_id': '1',
            'out_trade_no': '1671952602196094976',
            'user_id': u'1095885',
            'username': u'李四',
            'amount': str(amount), 
            'sign': 'sdfsdf123',
            'transaction_time': '2019-04-22 16:14:00', 
            'receipt': '["http://opimg.yiwanshu.com/appeal/235d77cecc8046b7b9dacd013fc4a9c4.png?e=1558688219&token=IoVr0Ec_pPHuL0M6Fk0MhAPYQOgWzIeNVM8TBjIL:N-GKV0TpmG36s_5FrBi9uYPMMfA=", "http://opimg.yiwanshu.com/appeal/235d77cecc8046b7b9dacd013fc4a9c4.png?e=1558688219&token=IoVr0Ec_pPHuL0M6Fk0MhAPYQOgWzIeNVM8TBjIL:N-GKV0TpmG36s_5FrBi9uYPMMfA="]', 
            'notify_url': u'http://13.251.70.125:8888/admin/appeal/notify/'
        }
       #self.generate_sign(parameter_dict, self.API_KEY)
       parameter_dict['sign'] = 'FE6D336E59F90B07AEBEEA93E70B661F'
       res = requests.post(ORDER_INQUIRY_URL, data=parameter_dict, timeout=5).text
       return res

    def test_create_order_success(self):
        result = self.create_order_inquiry(self.amount, '1671953493038239423')
        self.assertEqual(result, self.create_order_inquiry(self.amount, '1671953493038989312'))
    
    def test_create_order_fail(self):
        result = self.create_order_inquiry(self.amount, '1671953493038239423')
        self.assertNotEqual(result, self.create_order_inquiry(123, 'sfdasdf'))

    def test_invalid_amount(self):
        result = self.create_order_inquiry(0, order_inquiry_no)
        self.assertEqual(result, self.create_order_inquiry(0, order_inquiry_no))

    def test_invalid_type(self):
        result = self.create_order_inquiry('zzsdf', order_inquiry_no)
        self.assertEqual(result, self.create_order_inquiry('asdf', order_inquiry_no))

    def test_data_error(self):
        result = self.create_order_inquiry('asdfasdfasdf', order_inquiry_no)
        r = json.loads(result)
        self.assertTrue(r['status'] == 4)
        
if __name__ == '__main__':
    unittest.main()