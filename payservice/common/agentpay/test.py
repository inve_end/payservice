# coding=utf-8


from common.channel.pay.mepay import handler
from common.agentpay.db import BankCard


def test_create_agent_pay():
    bankcard = BankCard('ABC', '6228480688266478678', u'许荣华', '13275084138', u'晋江安海支行')
    handler.create_agent_pay(1, 480, 200, bankcard)


def test_query_balance(mch_id='W20190118001'):
    handler.query_balance(mch_id)


def test_query_agent_pay():
    handler.query_agent_pay(1)
