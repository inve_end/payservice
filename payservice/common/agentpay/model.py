# -*- coding: utf-8 -*-
from common import orm
from common.utils.types import Enum

AGENT_PAY_STATUS = Enum({
    "CREATED": (0, u"创建"),
    "SUBMIT": (1, u"提交到渠道"),
    "SUCCESS": (2, u"成功"),
    "FAIL": (3, u"失败"),
    "CHECKED": (4, u"已核对"),
})


class AgentPayOrder(orm.Model):
    """
    代付订单
    """
    __tablename__ = "agent_pay_order"
    id = orm.Column(orm.BigInteger, primary_key=True)
    channel_id = orm.Column(orm.Integer)  # 支付通道id
    total_fee = orm.Column(orm.FLOAT)  # 交易金额
    order_no = orm.Column(orm.VARCHAR)  # 支付部门的订单号

    out_order_no = orm.Column(orm.VARCHAR)  # 渠道生成的订单号
    bank_code = orm.Column(orm.VARCHAR)  # 银行名称编
    card_no = orm.Column(orm.VARCHAR)  #
    account_name = orm.Column(orm.VARCHAR)  #
    phone_number = orm.Column(orm.VARCHAR)  #
    bank_name = orm.Column(orm.VARCHAR)  # 开户支行名称
    identity_card = orm.Column(orm.VARCHAR)  # 持卡人身份证号
    account_province = orm.Column(orm.VARCHAR)  # 银行卡归属省份
    account_city = orm.Column(orm.VARCHAR)  # 银行卡归属城市
    unionpay_no = orm.Column(orm.VARCHAR)  # 银联号
    card_num = orm.Column(orm.VARCHAR)  # 卡编号

    chn_type = orm.Column(orm.VARCHAR)  # 通道
    platform = orm.Column(orm.VARCHAR)  # 平台

    status = orm.Column(orm.Integer, default=AGENT_PAY_STATUS.CREATED)  # 交易状态
    extend = orm.Column(orm.TEXT)  # 附加信息

    notify_url = orm.Column(orm.VARCHAR)  # 回调url
    notified_at = orm.Column(orm.DATETIME)  # 回调时间
    notify_count = orm.Column(orm.Integer, default=0)  # 回调次数

    operator_id = orm.Column(orm.Integer)

    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
