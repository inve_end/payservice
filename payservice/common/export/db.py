# coding=utf-8

from common.export.model import ExportDb
from common.utils import track_logging
from common.utils.db import list_object, get, upsert, delete
from common.utils.decorator import sql_wrapper

_LOGGER = track_logging.getLogger(__name__)


@sql_wrapper
def get_export(id):
    return get(ExportDb, id)


@sql_wrapper
def upsert_export(info, id=None):
    return upsert(ExportDb, info, id)


@sql_wrapper
def list_export(query_dct):
    return list_object(query_dct, ExportDb)


@sql_wrapper
def delete_export(id):
    delete(ExportDb, id)
