# -*- coding: utf-8 -*-
from common import orm
from common.utils.types import Enum

EXPORT_STATUS = Enum({
    "START": (0L, u"开始"),
    "FAIL": (1L, u"失败"),
    "COMPLETE": (2L, u"完成"),
    "PROCESSING": (3L, u"处理中"),
})


class ExportDb(orm.Model):
    """
    异步导出记录
    """
    __tablename__ = "export"
    id = orm.Column(orm.Integer, primary_key=True, autoincrement=True)
    user_id = orm.Column(orm.Integer)
    condition = orm.Column(orm.VARCHAR)
    status = orm.Column(orm.Integer)
    url = orm.Column(orm.VARCHAR)
    desc = orm.Column(orm.VARCHAR)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
