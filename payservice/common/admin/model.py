# -*- coding: utf-8 -*-
from django.conf import settings

from common.utils.types import Enum
from common.utils.orm import ArmoryOrm


orm = ArmoryOrm()
orm.init_conf(settings.ADMIN_CONF)

FORBIDDEN_ROLE = 0
USER_NOT_DELETED = 0
USER_DELETED = 1
USER_DISABLE = 0
USER_ENABLE = 1

ROLE = Enum({
    "FORBIDDEN": (0, "forbidden"),
    "USER": (1, "user"),
    "SENIOR_USER": (2, "senior_user"),
    "MANAGER": (3, "manager"),
    "SENIOR_MANAGER": (4, "senior_manager"),
    "ADMIN": (5, "admin")
})

PERMISSION = Enum({
    "NONE": (0, "none"),
    "READ": (1, "read"),
    "WRITE": (2, "write"),
})

PERMISSION_MOD = Enum({
    "OPERATION": (1L, "运营相关"),
    "DATA": (2L, "数据统计"),
    "SETTLEMENT": (3L, "通道结算"),
    "USER": (4L, "用户登陆"),
    "SPECIAL": (5L, "特殊权限"),
    "CONSOLE": (9L, "控制台管理"),
    "ORDER_INQUIRY": (10L, "订单申诉")
})

ACTION = {
    'POST': 1,
    'PUT': 2,
    'PATCH': 2,
    'DELETE': 3
}

RESOURCE = {
    '^user/login/?$': 'login',
    '^mch/(?P<mch_id>\d+)/?$': 'single_mch',
    '^channel/(?P<chn_id>\d+)/?$': 'single_channel',
    '^withdraw/channel/(?P<chn_id>\d+)/?$': 'withdraw_channel',
    '^level/user/(?P<id>\d+)/?$': 'level_user',
    '^level/strategy/(?P<id>\d+)/?$': 'level_strategy',
    '^order/(?P<order_id>\d+)/make_success/?$': 'make_order_success',
}


ACTION_TO_PERM = {
    'GET': 1,
    'POST': 2,
    'PUT': 2,
    'PATCH': 2,
    'DELETE': 2
}


class User(orm.Model):
    __tablename__ = 'user'
    id = orm.Column(orm.BigInteger, primary_key=True)
    nickname = orm.Column(orm.VARCHAR)
    email = orm.Column(orm.VARCHAR)
    password = orm.Column(orm.VARCHAR)
    role = orm.Column(orm.SmallInteger)
    enabled = orm.Column(orm.SmallInteger, default=0)
    deleted = orm.Column(orm.SmallInteger, default=0)
    perm = orm.Column(orm.VARCHAR, default=0)
    use_safe_dog = orm.Column(orm.SmallInteger, default=1)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
    mch_ids = orm.Column(orm.VARCHAR)


class Permission(orm.Model):
    __tablename__ = 'permission'
    id = orm.Column(orm.BigInteger, primary_key=True)
    url = orm.Column(orm.VARCHAR)
    permission = orm.Column(orm.SmallInteger)
    permission_mod = orm.Column(orm.VARCHAR)
    min_role = orm.Column(orm.SmallInteger)
    user_perm = orm.Column(orm.VARCHAR) # 需要满足的用户权限
    desc = orm.Column(orm.VARCHAR)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)


class UserToken(orm.Model):
    __tablename__ = 'user_token'
    user_id = orm.Column(orm.BigInteger, primary_key=True)
    token = orm.Column(orm.VARCHAR, primary_key=True)
    deleted = orm.Column(orm.SmallInteger)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)


class Record(orm.Model):
    __tablename__ = 'record'
    id = orm.Column(orm.BigInteger, primary_key=True)
    resource = orm.Column(orm.VARCHAR)
    resource_id = orm.Column(orm.BigInteger)
    action = orm.Column(orm.SmallInteger)
    content = orm.Column(orm.Text)
    operator = orm.Column(orm.BigInteger)
    created_at = orm.Column(orm.DATETIME)


class UserChannel(orm.Model):
    __tablename__ = 'user_channel'
    id = orm.Column(orm.Integer, primary_key=True, autoincrement=True)
    user_id = orm.Column(orm.Integer)
    channel_id = orm.Column(orm.Integer)


class Role(orm.Model):
    __tablename__ = 'role'
    id = orm.Column(orm.BigInteger, primary_key=True)
    rolename = orm.Column(orm.VARCHAR)
    permissions = orm.Column(orm.VARCHAR)
    mch_ids = orm.Column(orm.VARCHAR)
    enabled = orm.Column(orm.SmallInteger)
    deleted = orm.Column(orm.SmallInteger)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
