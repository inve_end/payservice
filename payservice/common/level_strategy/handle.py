# coding=utf-8
from common.level_strategy.model import UserLevelStrategy, UserLevel


def check_user_level(info, level):
    if level.level == 0:
        return True
    if info['total_recharge'] <= 0:
        return False

    charge_success_rate_ok = True if not 'charge_success_rate' in info else info['charge_success_rate'] >= level.success_charge_rate

    if info['total_recharge'] >= level.total_recharge and info['total_bet'] >= level.total_bet \
        and info['account_day'] >= level.account_day \
        and info['total_bet'] / info['total_recharge'] >= level.bet_mult and \
        charge_success_rate_ok:
        return True
    return False


def clac_user_level(info, mch_id):
    strategy = UserLevelStrategy.query.filter(UserLevelStrategy.status == 1).filter(
        UserLevelStrategy.mch_id == int(mch_id)).first()
    if strategy is None:
        return -1,-1
    levels = UserLevel.query.filter(UserLevel.strategy_id == strategy.id).order_by(UserLevel.level.desc()).all()
    if levels:
        for l in levels:
            if check_user_level(info, l):
                return l.level, l.channel_level
    return -1,-1
