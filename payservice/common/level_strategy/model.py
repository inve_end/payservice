# -*- coding: utf-8 -*-
from common import orm


class MchUser(orm.Model):
    """
    商户用户信息
    """
    __tablename__ = "mch_user"
    id = orm.Column(orm.Integer, primary_key=True, autoincrement=True)
    mch_id = orm.Column(orm.Integer)
    user_id = orm.Column(orm.Integer)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
    level = orm.Column(orm.Integer)
    name = orm.Column(orm.VARCHAR)
    tel = orm.Column(orm.VARCHAR)
    account_day = orm.Column(orm.Integer)
    active_day = orm.Column(orm.Integer)
    count_recharge = orm.Column(orm.Integer)
    charge_success_count = orm.Column(orm.Integer)
    device_ip = orm.Column(orm.VARCHAR)
    total_bet = orm.Column(orm.FLOAT)
    total_recharge = orm.Column(orm.FLOAT)
    total_withdraw = orm.Column(orm.FLOAT)
    count_withdraw = orm.Column(orm.Integer)
    bankcard = orm.Column(orm.VARCHAR)
    charge_status = orm.Column(orm.Integer, default=1)
    withdraw_status = orm.Column(orm.Integer, default=1)


class UserLevelStrategy(orm.Model):
    """
    用户分级策略
    """
    __tablename__ = "user_level_strategy"
    id = orm.Column(orm.Integer, primary_key=True, autoincrement=True)
    title = orm.Column(orm.VARCHAR)
    mch_id = orm.Column(orm.Integer)
    status = orm.Column(orm.Integer)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)


class UserLevel(orm.Model):
    """
    用户级别描述
    """
    __tablename__ = "user_level"
    id = orm.Column(orm.Integer, primary_key=True, autoincrement=True)
    strategy_id = orm.Column(orm.Integer)
    level = orm.Column(orm.Integer)
    channel_level = orm.Column(orm.Integer)
    total_recharge = orm.Column(orm.FLOAT)
    total_bet = orm.Column(orm.FLOAT)
    success_charge_rate = orm.Column(orm.FLOAT)
    account_day = orm.Column(orm.Integer)
    active_day = orm.Column(orm.Integer)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
    bet_mult = orm.Column(orm.FLOAT)
