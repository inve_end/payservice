import logging
import time
from unittest import TestCase

from common.utils.metrics import Metric

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)


class TestMetric(TestCase):
    def setUp(self):
        log = logging.getLogger('metric')
        self.metric = Metric(log)

    def test_metric(self):
        self.metric.measure('begin create order')
        time.sleep(1)
        self.metric.measure('end create order')
        time.sleep(1)
        self.metric.measure('begin channel order')
        time.sleep(1)
        self.metric.measure('end channel order')
        time.sleep(1)
        self.metric.end()
