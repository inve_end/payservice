# -*- coding: utf-8 -*-
"""
Track Logging

contains unique event id in all out log in a full chain of request/response lifecycle.
"""
import random
import logging as sys_logging
import threading
import uuid

LOG_THREAD_VALUE = threading.local()


def getLogger(name=None):
    logger = sys_logging.getLogger(name)
    return TrackLogger(logger)


def clear_trace():
    try:
        del LOG_THREAD_VALUE.trace_id
    except AttributeError:
        pass


class TrackLogger(object):
    def __init__(self, logger):
        self.inner_logger = logger

    @staticmethod
    def _extend_msg(msg):
        try:
            trace_id = LOG_THREAD_VALUE.trace_id
        except AttributeError:
            trace_id = LOG_THREAD_VALUE.trace_id = uuid.uuid4()
        msg = 'trace_id: {} ,'.format(trace_id) + msg
        return msg

    def debug(self, msg, *args, **kwargs):
        self.inner_logger.debug(self._extend_msg(msg), *args, **kwargs)

    def info(self, msg, *args, **kwargs):
        self.inner_logger.info(self._extend_msg(msg), *args, **kwargs)

    def warning(self, msg, *args, **kwargs):
        self.inner_logger.warning(self._extend_msg(msg), *args, **kwargs)

    def warn(self, msg, *args, **kwargs):
        self.inner_logger.warn(self._extend_msg(msg), *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        if kwargs.get('spot_check'):
            spot_rate = kwargs.get('spot_check')
            del kwargs['spot_check']
            if random.random() > spot_rate:
                return
        self.inner_logger.error(self._extend_msg(msg), *args, **kwargs)

    def exception(self, msg, exc_info=True, *args, **kwargs):
        self.inner_logger.exception(self._extend_msg(msg), *args, exc_info=exc_info, **kwargs)

    def critical(self, msg, *args, **kwargs):
        self.inner_logger.critical(self._extend_msg(msg), *args, **kwargs)
