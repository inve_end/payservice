# -*- coding: utf-8 -*-
from common.utils.respcode import HttpCode, StatusCode


# Basic Exceptions
class Error(Exception):
    HTTPCODE = HttpCode.SERVER_ERROR
    STATUS = StatusCode.UNKNOWN_ERROR

    def __init__(self, msg='', httpcode=None, status=None):
        super(Error, self).__init__(msg)
        if httpcode:
            self.HTTPCODE = httpcode
        if status:
            self.STATUS = status


class ClientError(Error):
    HTTPCODE = HttpCode.BAD_REQUEST


class ServerError(Error):
    pass


class ThirdPartyError(Error):
    pass


class WechatError(ThirdPartyError):
    pass


class PingXXError(ThirdPartyError):
    STATUS = StatusCode.PINGXX_PLATFORM_ERROR


# General Exceptions


class ParamError(ClientError):
    STATUS = StatusCode.PARAM_REQUIRED


class MoneyNotMatchError(ClientError):
    STATUS = StatusCode.MONEY_NOT_MATCH


class NotChannelError(ClientError):
    STATUS = StatusCode.NOT_CHANNEL


class AppIdNotMatchError(ClientError):
    STATUS = StatusCode.APP_ID_NOT_MATCH


class MoneyMoreMaxError(ClientError):
    STATUS = StatusCode.MONEY_MORE_MAX


class RepeatNotifyError(ClientError):
    STATUS = StatusCode.REPEAT_NOTIFY


class IpWhiteListsError(ClientError):
    STATUS = StatusCode.IP_WHITE_LISTS_REQUIRED


class SignError(ClientError):
    STATUS = StatusCode.SIGN_ERROR


class NotPayOrderError(ClientError):
    STATUS = StatusCode.NOT_PAY_ORDER


class ProcessedPayOrderError(ClientError):
    STATUS = StatusCode.PROCESSED_PAY_ORDER


class NotResponsePayIdError(ClientError):
    STATUS = StatusCode.NOT_RESPONSE_PAY_ID


class KspayError(ClientError):
    STATUS = StatusCode.KSPAY_URL_ERROR
    HTTPCODE = HttpCode.SERVER_ERROR


class DataError(ClientError):
    STATUS = StatusCode.DATA_ERROR


class DbError(ServerError):
    STATUS = StatusCode.DB_ERROR


class CacheError(ServerError):
    STATUS = StatusCode.CACHE_ERROR


# Specific Exception
class ProtocolError(ClientError):
    HTTPCODE = HttpCode.FORBIDDEN
    STATUS = StatusCode.HTTPS_REQUIRED


class SmsPlatformError(ServerError):
    STATUS = StatusCode.SMS_PLATFORM_ERROR


class AuthenticateError(DataError):
    HTTPCODE = HttpCode.UNAUTHORIZED


class PermissionError(ClientError):
    STATUS = StatusCode.NOT_ALLOWED
    HTTPCODE = HttpCode.FORBIDDEN


class NotImplementedError(ServerError):
    HTTPCODE = HttpCode.NOT_IMPLEMENTED


class ResourceInsufficient(ClientError):
    HTTPCODE = HttpCode.FORBIDDEN
    STATUS = StatusCode.RESOURCE_INSUFFICIENT


class ResourceNotFound(ClientError):
    HTTPCODE = HttpCode.NOT_FOUND


class ResourceNotModified(ClientError):
    HTTPCODE = HttpCode.NOT_MODIFIED


class BalanceInsufficient(ClientError):
    STATUS = StatusCode.BALANCE_INSUFFICIENT


class TermExpired(ClientError):
    STATUS = StatusCode.TERM_EXPIRED


class ReachLimit(ClientError):
    STATUS = StatusCode.REACH_LIMIT


class RepeatError(ClientError):
    STATUS = StatusCode.REPEAT_ORDER_ERROR


class GCodeError(ClientError):
    STATUS = StatusCode.GCODE_ERROR
