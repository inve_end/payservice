import json
import decimal
from datetime import datetime
from bson.objectid import ObjectId

from django.http import HttpResponse


class EnhencedEncoder(json.JSONEncoder):

    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)
        elif isinstance(o, datetime):
            return o.isoformat(' ')
        elif isinstance(o, ObjectId):
            return str(o)
        return super(EnhencedEncoder, self).default(o)


class JsonResponse(HttpResponse):

    """
        JSON response, since django 1.7, it's included in django.
    """

    def __init__(self, content, content_type='application/json; charset=utf-8',
                 **kwargs):
        super(JsonResponse, self).__init__(
            content=json.dumps(
                content, cls=EnhencedEncoder, ensure_ascii=False),
            content_type=content_type,
            **kwargs
        )
