# -*- coding: utf-8 -*-
# 辅助计算的一些数学函数

import operator


def c(n, k):
    return reduce(operator.mul, range(n - k + 1, n + 1)) / reduce(
        operator.mul, range(1, k + 1))


def fac(n):
    return reduce(operator.mul, range(1, n + 1))


def a(n, k):
    return reduce(operator.mul, range(n - k + 1, n + 1))
