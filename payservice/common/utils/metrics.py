import time
import uuid


def current_milli_time():
    return int(round(time.time() * 1000))


class Metric:
    def __init__(self, log):
        self.trace_id = uuid.uuid4().hex
        self.start_ts = self.ts = current_milli_time()
        self.log = log

    def measure(self, msg):
        self.log.info('Metric: position at [{}] cross [{}] millis in [{}]'.format(msg, current_milli_time() - self.ts,
                                                                                  self.trace_id))
        self.ts = current_milli_time()

    def end(self):
        self.log.info('Metric: all cross [{}] millis in [{}]'.format(current_milli_time() - self.start_ts, self.trace_id))
        if current_milli_time() - self.start_ts > 3000:
            self.log.info('Metric: slow cross [{}] in [{}]'.format(current_milli_time() - self.start_ts, self.trace_id))
