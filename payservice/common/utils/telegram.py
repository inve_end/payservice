# -*- coding: utf-8 -*-
import json
import requests
from functools import partial

_SEND_TEXT_API_TELE = 'https://api.telegram.org/bot619271113:AAFoYg1zA-kUc-DK8OF3BauCxCWSbZoUgpI/sendMessage'
_SEND_TEXT_API_FIN_TELE = 'https://api.telegram.org/bot743402508:AAEmc54Z2oLZevyAjMGvLPlCs-ihdF7RboE/sendMessage'
# https://api.telegram.org/bot619271113:AAFoYg1zA-kUc-DK8OF3BauCxCWSbZoUgpI/getupdates
# 474594140 机器人id
# -270087999 justpay 产品运营组
NOTIFY_UIDS_TELE = [-282597926]


def send_text_msg_tele(chat_id, chat_text):
    chat_text = chat_text.replace('[', '-').replace(']', '-').replace('_', '-')
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    post_data = {
        "chat_id": chat_id,
        "text": chat_text,
        "parse_mode": "Markdown",
    }
    j = json.dumps(post_data, separators=(',', ':'))
    print j
    response = requests.post(_SEND_TEXT_API_TELE, data=j, headers=headers,
                             timeout=5)
    print response.text


def send_channel_text_msg_tele(chat_id, chat_text):
    chat_text = chat_text.replace('[', '-').replace(']', '-').replace('_', '-')
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    post_data = {
        "chat_id": chat_id,
        "text": chat_text,
        "parse_mode": "Markdown",
    }
    j = json.dumps(post_data, separators=(',', ':'))
    print j
    response = requests.post(_SEND_TEXT_API_FIN_TELE, data=j, headers=headers,
                             timeout=5)
    print response.text


if __name__ == "__main__":
    # d = {
    #     "text": "\u4e0b\u5206\u901a\u9053\uff1a**4**-**\u4e1c\u839e\u5e02\u73af\u6cfd\u7535\u5b50\u79d1\u6280\u6709\u9650\u516c\u53f8** \u62a5\u9519\nPAYEE_NOT_EXIST\n\u8ba2\u5355\u53f7\uff1a%s\n",
    #     "parse_mode": "Markdown", "chat_id": -220589678}
    d = {
        "text": "[\u652f\u4ed8\u544a\u8b66\n\u5546\u6237Witch\u652f\u4ed8\u901a\u9053\u5f02\u5e38\n\u901a\u9053\u5b9a\u989d205387f7269e6f640a2b89507bbf83157666d\u5fae\u6d3e\u652f\u4ed8-\u652f\u4ed8\u5b9d-witch(804-quotaalipay)\u8d85\u8fc753\u5206\u949f\u6ca1\u6709\u6210\u529f\u8ba2\u5355\uff0c\u5546\u6237\u53f76001000\n",
        "parse_mode": "Markdown", "chat_id": -1001331494801}
    send_text_msg_tele(474594140, d['text'])
    send_channel_text_msg_tele(-301835231, d['text'])
    send_channel_text_msg_tele(-1001331494801, d['text'])
    send_channel_text_msg_tele(-261523497, d['text'])

    print d['text'][197:210]
    print len(d['text'])
