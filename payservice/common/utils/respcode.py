# -*- coding: utf-8 -*-

from common.utils.types import Enum


class HttpCode(object):
    OK = 200
    CREATED = 201
    ACCEPTED = 202
    NO_CONTENT = 204
    PARTIAL_CONTENT = 206
    NOT_MODIFIED = 304
    BAD_REQUEST = 400
    UNAUTHORIZED = 401
    FORBIDDEN = 403
    NOT_FOUND = 404
    METHOD_NOT_ALLOWED = 405
    EXPECTATION_FAILED = 417
    SERVER_ERROR = 500
    NOT_IMPLEMENTED = 501


StatusCode = Enum({
    "OK": (0, ""),
    "UNKNOWN_ERROR": (1, u"未知错误"),
    "PARAM_REQUIRED": (2, u"缺少参数"),
    "HTTPS_REQUIRED": (3, u"需要使用HTTPS"),
    "DATA_ERROR": (4, u"数据错误"),
    "DB_ERROR": (5, u"数据库错误"),
    "CACHE_ERROR": (6, u"缓存错误"),
    "IP_WHITE_LISTS_REQUIRED": (7, u"需要加入白名单"),
    "REPEAT_NOTIFY": (8, u"重复通知"),
    # user related
    "INVALID_USER": (101, u"用户不存在"),
    "WRONG_PASSWORD": (102, u"密码错误"),
    "WRONG_AUTH_CODE": (103, u"验证码错误"),
    "DUPLICATE_ACCOUNT": (104, u"账户已存在"),
    "INVALID_TOKEN": (105, u"TOKEN失效"),
    "NOT_ALLOWED": (106, u"禁止访问"),
    "DUPLICATE_PHONE": (107, u"手机号已存在"),
    # third party component
    "SMS_PLATFORM_ERROR": (108, u"短信平台错误"),
    "PINGXX_PLATFORM_ERROR": (130, u"ping++ 平台错误"),

    "RESOURCE_INSUFFICIENT": (201, u"资源不足"),
    "BALANCE_INSUFFICIENT": (202, u"余额不足"),
    "TERM_EXPIRED": (203, u"期限已过"),
    "INVALID_LOTTERY": (204, u"彩种暂时不能购买"),
    "INVALID_BET": (205, u"玩法暂不能投注"),
    "SIGN_ERROR": (206, u"签名错误"),
    "NOT_PAY_ORDER": (207, u"该订单不存在"),
    "PROCESSED_PAY_ORDER": (208, u"订单已处理"),
    "NOT_RESPONSE_PAY_ID": (209, u"未获取到订单号"),
    "MONEY_NOT_MATCH": (210, u"回调支付金额与支付列表数据不匹配"),
    "NOT_CHANNEL": (211, u"无对应商户信息"),
    "APP_ID_NOT_MATCH": (212, u"回调商户号与该订单商户号不匹配"),
    "MONEY_MORE_MAX": (213, u"支付金额大于单笔最大限额"),

    "REACH_LIMIT": (301, u"达到限制"),

    "KSPAY_URL_ERROR": (401, u"闪付返回qr_alipay_com支付链接"),
    "REPEAT_ORDER_ERROR": (402, u"重复支付"),
    "GCODE_ERROR": (403, u"GOOGLE验证码错误"),
})

StatusCodeDict = StatusCode.to_dict()
