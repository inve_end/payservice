#! -*- coding:utf-8 -*-
import json
import requests

from common.utils.types import Enum


ADMIN = Enum({
    "ARNO": (8039825, u"arno"),
    "LORRY": (8040019, u"lorry"),
    "OWEN": (8039983, u"owen"),
    "MIKE": (8039163, u"mike"),
    "AMOS": (8040017, u"amos"),
    "LISP": (8001199, u"lisp"),
    "JAMES": (8039279, u"james"),
    "JOKER": (8036197, u"joker"),
    "KAEL": (8041455, u"kael"),
    "ALIN": (8040961, u"alin"),
})


_SEND_TEXT_API = 'https://api.potato.im:8443/8026170:CeywfHBsJqh1G1Gn6l07YXXc/sendTextMessage'


def send_text_msg(chat_type, chat_id, chat_text):
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    post_data = {
        "chat_type": chat_type,
        "chat_id": chat_id,
        "text": chat_text,
        "markdown": True,
    }
    response = requests.post(_SEND_TEXT_API, data=json.dumps(post_data,separators=(',',':')), headers=headers, timeout=5)
