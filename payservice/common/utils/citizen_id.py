# -*- coding: utf-8 -*-

"""验证身份证号码有效性
"""
chmap = {
    '0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8,
    '9': 9, 'x': 10, 'X': 10
}


def _ch_to_num(ch):
    return chmap[ch]


def _verify_list(l):
    sum = 0
    for ii, n in enumerate(l):
        i = 18 - ii
        weight = 2**(i - 1) % 11
        sum = (sum + n * weight) % 11

    return sum == 1


def verify_citizen_id(s):
    char_list = list(s)
    num_list = [_ch_to_num(ch) for ch in char_list]
    return _verify_list(num_list)
