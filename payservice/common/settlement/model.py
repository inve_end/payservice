# -*- coding: utf-8 -*-
from common import orm


class BankCard(orm.Model):
    """
    银行卡信息
    """
    __tablename__ = "bankcard"
    id = orm.Column(orm.Integer, primary_key=True, autoincrement=True)
    card_num = orm.Column(orm.VARCHAR)  # 卡编号
    account_holder = orm.Column(orm.VARCHAR)  # 开户名
    bank = orm.Column(orm.VARCHAR)  # 银行名称
    bank_province = orm.Column(orm.VARCHAR)  # 开户省
    bank_city = orm.Column(orm.VARCHAR)  # 开户市
    subbranch = orm.Column(orm.VARCHAR)  # 支行信息
    platform = orm.Column(orm.VARCHAR)  # 平台
    chn_type = orm.Column(orm.VARCHAR)  # 通道类型
    account_number = orm.Column(orm.VARCHAR)  # 银行卡号
    phone_num = orm.Column(orm.VARCHAR)  # 手机号
    status = orm.Column(orm.Integer)  # 0:禁用 1:可用
    description = orm.Column(orm.VARCHAR)  # 说明
    operator = orm.Column(orm.VARCHAR)  # 说明
    identity_card = orm.Column(orm.VARCHAR)  # 持卡人身份证号
    u_shield = orm.Column(orm.DATETIME)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)


class Settlement(orm.Model):
    """
    结算通道信息
    """
    __tablename__ = "settlement"
    id = orm.Column(orm.Integer, primary_key=True, autoincrement=True)
    name = orm.Column(orm.VARCHAR)
    mch_id = orm.Column(orm.Integer)
    chn_id = orm.Column(orm.Integer)
    status = orm.Column(orm.Integer)  # 0:不监控 1:监控
    rate = orm.Column(orm.FLOAT)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
    balance = orm.Column(orm.FLOAT)
    today_income = orm.Column(orm.FLOAT)
    today_withdraw = orm.Column(orm.FLOAT)
    warning_amount = orm.Column(orm.FLOAT)
    chn_ids = orm.Column(orm.VARCHAR)


class SettlementOrder(orm.Model):
    """
    通道结算订单
    """
    __tablename__ = "settlement_order"
    id = orm.Column(orm.Integer, primary_key=True, autoincrement=True)
    mch_id = orm.Column(orm.Integer)
    des = orm.Column(orm.VARCHAR)
    total_fee = orm.Column(orm.FLOAT)
    status = orm.Column(orm.Integer)  # 0:不可用 1:可用
    notify_url = orm.Column(orm.VARCHAR)
    sign = orm.Column(orm.VARCHAR)
    third_id = orm.Column(orm.VARCHAR)
    bankcard_id = orm.Column(orm.Integer)
    account_number = orm.Column(orm.VARCHAR)
    account_holder = orm.Column(orm.VARCHAR)
    payed_at = orm.Column(orm.DATETIME)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
    settlement_id = orm.Column(orm.Integer)
    channel_id = orm.Column(orm.Integer)
    operator = orm.Column(orm.VARCHAR)
    operator_id = orm.Column(orm.Integer)
