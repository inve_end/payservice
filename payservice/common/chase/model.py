# -*- coding: utf-8 -*-
from common import orm
from common.utils.types import Enum


CHASE_TYPE = Enum({
    "PLUS": (0L, "加分"),
    "MINUS": (1L, "减分")
})

CHASE_ORIGIN_TYPE = Enum({
    "RECHARGE": (0L, "充值订单"),
    "WITHDRAW": (1L, "提现订单"),
})

CHASE_ORDER_STATUS = Enum({
    "CREATED": (0L, "已提交"),
    "FAIL": (1L, "补单失败"),
    "SUCC": (2L, "补单成功"),
})


class ChaseOrder(orm.Model):
    __tablename__ = 'chase_order'
    id = orm.Column(orm.BigInteger, primary_key=True, autoincrement=False)
    mch_id = orm.Column(orm.Integer)  # 产品DWC, 6001011
    service = orm.Column(orm.VARCHAR)  # 支付类型
    channel_id = orm.Column(orm.Integer)  # 通道id
    user_id = orm.Column(orm.VARCHAR)
    amount = orm.Column(orm.Integer)  # 充值金额
    status = orm.Column(orm.SmallInteger, default=0)  # 追分状态, 默认0: 已提交, 1: 已处理
    chase_type = orm.Column(orm.SmallInteger, default=0)  # 类型：加分/减分
    chase_origin_type = orm.Column(orm.SmallInteger, default=0)  # 追分类型：充值/提现
    chase_origin_id = orm.Column(orm.VARCHAR)  # 关连订单号
    out_trade_no = orm.Column(orm.VARCHAR)  # 商户订单号
    operator = orm.Column(orm.VARCHAR)  # 操作者
    operator_id = orm.Column(orm.Integer)  # 操作者id
    desc = orm.Column(orm.TEXT)  # 描述
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
