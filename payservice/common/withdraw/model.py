# -*- coding: utf-8 -*-
from common import orm
from common.utils.types import Enum


WITHDRAW_STATUS = Enum({
    "READY": (0L, u"未兑换"),
    "FAIL": (1L, u"兑换失败"),
    "SUCC": (2L, u"兑换成功"),
})


NOTIFY_STATUS = Enum({
    "READY": (0L, "ready to notify"),
    "FAIL": (1L, "notify failed"),
    "SUCC": (2L, "notify successed"),
})


class WithdrawChannel(orm.Model):
    """
    兑换通道配置
    """
    __tablename__ = "withdraw_channel"
    id = orm.Column(orm.Integer, primary_key=True, autoincrement=True)
    name = orm.Column(orm.VARCHAR)           # 通道名称
    mch_id = orm.Column(orm.Integer)         # 商户编号
    app_id = orm.Column(orm.Integer)         # 官方app_id
    payer_name = orm.Column(orm.VARCHAR)     # 付款方姓名
    private_key = orm.Column(orm.TEXT)       # 通道app私钥
    public_key = orm.Column(orm.TEXT)        # 官方(alipay)公钥
    status = orm.Column(orm.Integer)         # 0:不可用 1:可用
    weight = orm.Column(orm.Integer)         # 权重，影响分配比例
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
    balance = orm.Column(orm.FLOAT)          # 通道余额


class WithdrawOrder(orm.Model):
    """
    兑换订单
    """
    __tablename__ = "withdraw_order"
    id = orm.Column(orm.BigInteger, primary_key=True, autoincrement=False)
    mch_id = orm.Column(orm.Integer)
    service = orm.Column(orm.VARCHAR)        # 服务名称alipay/union
    channel_id = orm.Column(orm.Integer)     # 通道id
    out_trade_no = orm.Column(orm.VARCHAR)   # 商户订单号
    total_fee = orm.Column(orm.FLOAT)        # 交易金额
    payee_no = orm.Column(orm.VARCHAR)       # 支付宝帐号
    payee_real_name = orm.Column(orm.VARCHAR)# 支付宝姓名
    status = orm.Column(orm.Integer)         # 交易状态
    notify_url = orm.Column(orm.VARCHAR)     # 通知url
    mch_create_ip = orm.Column(orm.VARCHAR)  # 发起支付的机器ip
    sign = orm.Column(orm.VARCHAR)           # 签名
    extra = orm.Column(orm.TEXT)             # 附加信息
    third_id = orm.Column(orm.VARCHAR)       # 上游支付交易号
    withdraw_at = orm.Column(orm.DATETIME)   # 兑换完成时间
    notified_at = orm.Column(orm.DATETIME)   # 回调时间
    notify_status = orm.Column(orm.Integer)  # 回调状态
    notify_count = orm.Column(orm.Integer, default=0)   # 回调次数
    extend = orm.Column(orm.TEXT)            # 交易完成详情
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
    user_id = orm.Column(orm.BigInteger)
