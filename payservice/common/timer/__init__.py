# -*- coding: utf-8 -*-
import json
import logging
from common.utils.types import Enum
from common.cache import redis_cache
from common.utils import id_generator
from common.utils.exceptions import DataError
from future.utils import raise_with_traceback

_LOGGER = logging.getLogger(__name__)

TIMER_EVENT_TYPE = Enum({
    "MCH_NOTIFY": (1L, "notify mch in time"),  # for pay 上分
    "WITHDRAW_NOTIFY": (2L, "notify mch for withdraw in time"),  # for withdraw 下分
})


class TimerEvent(object):
    """Timer event wrapper
    """

    def __init__(self, event_type, event_value, timestamp):
        self.event_type = event_type
        # must be a json dict, like {'id': xxxx, 'msg': xxxx}
        self.event_value = event_value
        self.timestamp = timestamp

    @classmethod
    def submit(cls, event_type, event_msg, timestamp):
        # construct event_value dict
        event_value = {}
        uuid = id_generator.generate_uuid()
        event_value.update({'id': uuid})
        event_value.update({'msg': event_msg})
        try:
            cache_value = json.dumps(event_value)
            redis_cache.submit_timer_event(event_type, cache_value, timestamp)
        except Exception as e:
            _LOGGER.error('timer event submit error.(%s)' % e)
            raise_with_traceback(DataError(e))


class EventHandler(object):
    """
    Abstract class
    """

    def process(self, event):
        raise NotImplementedError(
            'EventHandler class is supposed to an abstract class')
