# -*- coding: utf-8 -*-
from common import orm


class OrderInquiry(orm.Model):
    """
    订单申诉
    """
    __tablename__ = "order_inquiry"
    id = orm.Column(orm.Integer, primary_key=True, autoincrement=True)
    order_inquiry_no = orm.Column(orm.VARCHAR)
    mch_id = orm.Column(orm.VARCHAR)
    out_trade_no = orm.Column(orm.VARCHAR)
    service = orm.Column(orm.VARCHAR)
    user_id = orm.Column(orm.Integer)
    username = orm.Column(orm.VARCHAR)
    amount = orm.Column(orm.FLOAT)
    transaction_time = orm.Column(orm.DATETIME)
    processing_time = orm.Column(orm.DATETIME)
    notify_url = orm.Column(orm.VARCHAR)
    mch_create_ip = orm.Column(orm.VARCHAR)
    sign = orm.Column(orm.VARCHAR)
    status = orm.Column(orm.Integer)
    receipt = orm.Column(orm.TEXT)
    reason_failure_id = orm.Column(orm.Integer)
    reason_failure_desc = orm.Column(orm.VARCHAR)
    operator = orm.Column(orm.VARCHAR)
    created_at = orm.Column(orm.DATETIME)
    confirmed_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
    notified_at = orm.Column(orm.DATETIME)
    notify_status = orm.Column(orm.Integer)
    notify_count = orm.Column(orm.Integer)
    remark = orm.Column(orm.TEXT)
    inquiry_desc = orm.Column(orm.VARCHAR)
