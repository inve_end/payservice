# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests

from async import async_job
from common.channel.pay import check_channel_order
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

# 新顺付
APP_CONF = {
    'tg-001': {  # zs 20-5000
        'API_KEY': 'c1d1d84c0ec5e6e3',
        'gateway': 'http://www.shunfu68.com:8089/midpay',
        'query_gateway': 'http://www.shunfu68.com:8089/depositstatus',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _get_agent_no(mch_id):
    return APP_CONF[mch_id]['agent_no']


def _gen_sign(s):
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest()
    return sign


# 版本+商户名+类型 +付款人+商户秘钥 组合的md5加密
def generate_sign(parameter, key):
    s = '{}{}{}{}{}'.format(parameter['version'], parameter['MerchaantNo'],
                            parameter['type'], parameter['payer'], key)
    _LOGGER.info('xinshunfupay sign str : %s', s)
    return _gen_sign(s)


# 支付訂單號+充值金額+備註+商户秘钥组合的md5加密
def generate_notify_sign(parameter, key):
    s = '{}{}{}{}'.format(parameter['depositNumber'], parameter['amount'],
                          parameter['note'], key)
    _LOGGER.info('xinshunfupay notify sign str : %s', s)
    return _gen_sign(s)


# depositNumber+MerchaantNo+payer+商户秘钥组 合的md5加密
def generate_query_sign(parameter, key):
    s = '{}{}{}{}'.format(parameter['depositNumber'], parameter['MerchaantNo'],
                          parameter['payer'], key)
    _LOGGER.info('xinshunfupay query sign str : %s', s)
    return _gen_sign(s)


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_notify_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("xinshunfupay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def _get_pay_type(service):
    if service == 'alipay':
        return 'alapi'
    return 'alapi'


def _get_pay_channel(service):
    if service == 'alipay':
        return 'ALIPAY'
    return 'ALIPAY'


def create_charge(pay, pay_amount, info):
    service = info.get('service')
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('version', '1.0'),
        ('MerchaantNo', app_id),
        ('type', _get_pay_type(service)),
        ('amount', str(int(pay_amount))),
        ('payer', str(pay.id)),
        ('mobile', '1'),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("xinshunfupay create: %s", json.dumps(parameter_dict))
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_gateway(app_id), data=parameter_dict, headers=headers, timeout=5, verify=False)
    _LOGGER.info("xinshunfupay create rsp : %s, jp_id: %s", response.text, str(pay.id))
    return {'charge_info': json.loads(response.text)['data.url']}


def check_notify_sign(request, app_id):
    api_key = _get_api_key(app_id)
    data = dict(request.POST.iteritems())
    _LOGGER.info("xinshunfupay notify data: %s", data)
    verify_notify_sign(data, api_key)
    pay_id = data['userRemark']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('xinshunfupay event does not contain pay ID')

    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = 'success'
    trade_no = data['billNo']
    total_fee = float(data['amount'])
    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    check_channel_order(pay_id, total_fee, app_id)

    if trade_status == 'success' and total_fee > 0.0:
        _LOGGER.info('xinshunfupay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    """ 查询订单 """
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('MerchaantNo', app_id),
        ('payer', str(pay_order.id)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info('xinshunfupay query data, %s', parameter_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_query_gateway(app_id), data=parameter_dict, headers=headers, timeout=3, verify=False)
    _LOGGER.info('xinshunfupay query rsp, %s', response.text)
    if response.status_code == 200:
        data = json.loads(response.text)
        mch_id = pay_order.mch_id
        trade_status = str(data['data.state'])
        total_fee = float(data['data.amount'])
        trade_no = data['data.depositNumber']

        extend = {
            'trade_status': trade_status,
            'total_fee': total_fee,
            'trade_no': trade_no,
        }

        if trade_status == '':
            check_channel_order(pay_id, total_fee, app_id)

            _LOGGER.info('xinshunfupay query order success, mch_id:%s pay_id:%s' % (mch_id, pay_order.id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)

            async_job.notify_mch(pay_order.id)
