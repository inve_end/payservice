# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict
from datetime import datetime

import requests
from django.conf import settings

from async import async_job
from common.channel.pay import check_channel_order
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '152018120500207': {  # 汇通宝支付  alipay-h5  10-5000  2.5%  dwc
        'API_KEY': 'bc2e076569285fd5df1bd60dc0db0fe0',
        'gateway': 'http://www.52luna.com/trade/api/unifiedOrder',
        'query_gateway': 'http://www.52luna.com/trade/api/queryPay',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _gen_sign(s):
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().lower()
    return sign


def generate_sign(parameter, key):
    s = ''
    for k in sorted(parameter.keys()):
        if k != 'signType' and parameter[k]:
            s += '%s=%s&' % (k, parameter[k])
    s = s[:len(s) - 1]
    s += '%s' % key
    _LOGGER.info("huitongbaopay sign str: %s", s)
    return _gen_sign(s)


def verify_notify_sign(params, key):
    _LOGGER.info("huitongbaopay verify_notify params is: %s", params)
    sign = params['sign']
    params.pop('sign')
    params.pop('signType')
    calculated_sign = generate_sign(params, key)
    if sign.lower() != calculated_sign.lower():
        _LOGGER.info("huitongbaopay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def verify_query_response_sign(params, key):
    _LOGGER.info("huitongbaopay query_response_sign params is: %s", params)
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_sign(params, key)
    if sign.lower() != calculated_sign.lower():
        _LOGGER.info("huitongbaopay query_response_sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _get_pay_type(service):
    if service == 'alipay':
        return 'H5'
    return 'H5'


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def create_charge(pay, pay_amount, info):
    service = info.get('service')
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('service', 'unifiedOrder'),
        ('signType', 'MD5'),
        ('inputCharset', 'UTF-8'),
        ('sysMerchNo', app_id),
        ('outOrderNo', str(pay.id)),
        ('orderTime', datetime.now().strftime('%Y%m%d%H%M%S')),
        ('orderAmt', '%.2f' % pay_amount),
        ('orderTitle', 'charge'),
        ('selectFinaCode', 'ALIPAY'),
        ('tranAttr', _get_pay_type(service)),
        ('openId', ''),
        ('backUrl', '{}/pay/api/{}/huitongbaopay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('frontUrl', '{}/pay/api/{}/huitongbaopay/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, pay.id)),
        ('clientIp', _get_device_ip(info)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("huitongbaopay create: %s", json.dumps(parameter_dict))
    headers = {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}
    response = requests.post(_get_gateway(app_id), data=parameter_dict, headers=headers, timeout=5)
    _LOGGER.info('huitongbaopay create response, %s', response.text)
    return {'charge_info': json.loads(response.text)['jumpUrl']}


# SUCCESS
def check_notify_sign(request, app_id):
    api_key = _get_api_key(app_id)
    data = dict(request.POST.iteritems())
    _LOGGER.info("huitongbaopay notify data: %s", data)
    verify_notify_sign(data, api_key)
    pay_id = data['outOrderNo']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('huitongbaopay event does not contain pay ID')
    pay = order_db.get_pay(str(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(data['tranResult'])
    total_fee = float(data['tranAmt'])
    trade_no = data['outOrderNo']
    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }
    check_channel_order(pay_id, total_fee, app_id)
    if trade_status == 'SUCCESS' and total_fee > 0.0:
        _LOGGER.info('huitongbaopay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('service', 'queryPay'),
        ('signType', 'MD5'),
        ('inputCharset', 'UTF-8'),
        ('sysMerchNo', app_id),
        ('outOrderNo', pay_id),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info('huitongbaopay query data, %s', parameter_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'}
    response = requests.post(_get_query_gateway(app_id), data=parameter_dict, headers=headers, timeout=5)
    _LOGGER.info('huitongbaopay query rsp, %s', response.text)
    if response.status_code == 200:
        data = json.loads(response.text)
        _LOGGER.info("huitongbaopay query response body: %s", data)
        verify_query_response_sign(data, api_key)
        mch_id = pay_order.mch_id
        trade_status = str(data['orderStatus'])
        total_fee = float(data['tranAmt'])
        trade_no = data['outOrderNo']
        extend = {
            'trade_status': trade_status,
            'total_fee': total_fee,
            'trade_no': trade_no,
        }
        if trade_status == '02':
            check_channel_order(pay_id, total_fee, app_id)

            _LOGGER.info('huitongbaopay query order success, mch_id:%s pay_id:%s' % (mch_id, pay_order.id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)
            async_job.notify_mch(pay_order.id)
