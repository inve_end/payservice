# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.channel.pay import check_channel_order
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

# 开心果
APP_CONF = {
    '248114312487505920': {  # dwc 支付宝（1-5000)
        'API_KEY': 'f3974cb6b97eededa1d421876242f1cd',
        'gateway': 'http://pay.bailidaa.cn/4pay-api/services/mainScan/pay',
        'query_gateway': 'http://pay.bailidaa.cn/4pay-api/services/order/orderQuery',
    },
    '251014935449047040': {  # witch 支付宝（1-5000)
        'API_KEY': 'b3a25d3937fe7e15ea0533e248ab94cb',
        'gateway': 'http://pay.bailidaa.cn/4pay-api/services/mainScan/pay',
        'query_gateway': 'http://pay.bailidaa.cn/4pay-api/services/order/orderQuery',
    },
    '251014650370592768': {  # zs 支付宝（1-5000)
        'API_KEY': '65f3d83d6df62b09f163c04c99cfdd55',
        'gateway': 'http://pay.bailidaa.cn/4pay-api/services/mainScan/pay',
        'query_gateway': 'http://pay.bailidaa.cn/4pay-api/services/order/orderQuery',
    },
    '251013577308246016': {  # loki 支付宝（1-5000)
        'API_KEY': '47fb7376cb966f443dfb5351a6368134',
        'gateway': 'http://pay.bailidaa.cn/4pay-api/services/mainScan/pay',
        'query_gateway': 'http://pay.bailidaa.cn/4pay-api/services/order/orderQuery',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _gen_sign(s):
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().upper()
    return sign


def generate_sign(parameter, key):
    s = ''
    for k in sorted(parameter.keys()):
        if parameter[k]:
            s += '%s=%s&' % (k, parameter[k])
    s += str(key)
    _LOGGER.info("kaixinguopay sign str: %s", s)
    return _gen_sign(s)


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_sign(params, key)
    if sign.upper() != calculated_sign.upper():
        _LOGGER.info("kaixinguopay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def _get_pay_type(service):
    if service == 'alipay':
        return '1002'
    return '1002'


def create_charge(pay, pay_amount, info):
    service = info.get('service')
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('merchant_id', app_id),
        ('biz_code', _get_pay_type(service)),
        ('order_id', str(pay.id)),
        ('order_amt', int(pay_amount * 100)),
        ('bg_url', '{}/pay/api/{}/kaixinguopay/{}/'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('return_url',
         '{}/pay/api/{}/kaixinguopay/{}/'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, str(pay.id))),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("kaixinguopay create: %s", json.dumps(parameter_dict))
    headers = {'Content-Type': 'application/json;charset=utf-8'}
    response = requests.post(_get_gateway(app_id), data=json.dumps(parameter_dict), headers=headers, timeout=5)
    _LOGGER.info("kaixinguopay create: response: %s", response.text)
    return {'charge_info': json.loads(response.text)['pay_url']}


def check_notify_sign(request, app_id):
    api_key = _get_api_key(app_id)
    data = dict(request.POST.iteritems())
    _LOGGER.info("kaixinguopay notify data: %s", data)
    verify_notify_sign(data, api_key)
    pay_id = data['order_id']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('kaixinguopay event does not contain pay ID')

    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = data['state']
    trade_no = data['up_order_id']
    total_fee = float(data['order_amt']) / 100.0
    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    check_channel_order(pay_id, total_fee, app_id)

    # 交易实际状态，0：成功，1：失败，2：处理中
    if trade_status == '0' and total_fee > 0.0:
        _LOGGER.info('kaixinguopay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    """ 查询订单 """
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('merchant_id', app_id),
        ('order_id', str(pay_order.id)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info('kaixinguopay query data, %s', json.dumps(parameter_dict))
    headers = {'Content-Type': 'application/json;charset=utf-8'}
    response = requests.post(_get_query_gateway(app_id), data=json.dumps(parameter_dict), headers=headers, timeout=3)
    _LOGGER.info('kaixinguopay query rsp, %s', response.text)
    if response.status_code == 200:
        data = json.loads(response.text)
        mch_id = pay_order.mch_id
        trade_status = str(data['state'])
        total_fee = float(data['order_amt']) / 100.0
        trade_no = data['up_order_id']

        extend = {
            'trade_status': trade_status,
            'total_fee': total_fee,
            'trade_no': trade_no,
        }

        if trade_status == '0':
            check_channel_order(pay_id, total_fee, app_id)

            _LOGGER.info('kaixinguopay query order success, mch_id:%s pay_id:%s' % (mch_id, pay_order.id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)

            async_job.notify_mch(pay_order.id)
