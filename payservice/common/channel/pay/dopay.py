# -*- coding: utf-8 -*-
import hashlib
import json
import time
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.cache import redis_cache
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    'ee00ab562967525c9d9b5233': {  # 自有支付方式测试 个人支付宝 https://www.paysapi.com/docpay
        'API_KEY': 'b168ce1bdd9cb499997f9ea9dbe81055',
        'gateway': 'https://pay.bbbapi.com/',
        'query_gateway': 'https://api.bbbapi.com/get_order_staus_by_id',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _gen_sign(s):
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest()
    return sign


# goodsname + istype + notify_url + orderid + orderuid + price + return_url + token + uid
def generate_sign(d, key):
    s = d['goodsname'] + d['istype'] + d['notify_url'] + d['orderid'] + d['orderuid'] + d['price'] + d[
        'return_url'] + key + d['uid']
    _LOGGER.info("dopay sign str: %s", s)
    return _gen_sign(s)


# orderid + orderuid + paysapi_id + price + realprice + token
def generate_notify_sign(d, key):
    s = d['orderid'] + d['orderuid'] + d['paysapi_id'] + d['price'] + d['realprice'] + key
    _LOGGER.info("dopay notify sign str: %s", s)
    return _gen_sign(s)


# uid + orderid + r + token
def generate_query_sign(d, key):
    s = d['uid'] + d['orderid'] + d['r'] + key
    _LOGGER.info("dopay query sign str: %s", s)
    return _gen_sign(s)


def verify_notify_sign(params, key):
    sign = params['key']
    params.pop('key')
    calculated_sign = generate_notify_sign(params, key)
    if sign.upper() != calculated_sign.upper():
        _LOGGER.info("dopay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


# 必填。1：支付宝；2：微信支付
def _get_pay_type(service):
    if service == 'wxpay':
        payType = '2'
    else:
        payType = '1'
    return payType


def _build_form(params, gateway):
    html = u"<head><title>loading...</title></head><form id='submit' name='submit' action='" + gateway + "' method='post'>"
    for k, v in params.items():
        html += "<input type='hidden' name='%s' value='%s'/>" % (k, v)
    html += "</form>"
    html += "<script>doc" \
            "ument.forms['submit'].submit();</script>"
    return html


def create_charge(pay, pay_amount, info):
    service = info.get('service')
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('uid', app_id),
        ('price', '%.2f' % pay_amount),
        ('istype', _get_pay_type(service)),
        ('notify_url', '{}/pay/api/{}/dopay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('return_url', '{}/pay/api/{}/dopay/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, str(pay.id))),
        ('orderid', str(pay.id)),
        ('orderuid', str(pay.user_id)),
        ('goodsname', 'charge'),
    ))
    parameter_dict['key'] = generate_sign(parameter_dict, api_key)
    html_text = _build_form(parameter_dict, _get_gateway(app_id))
    cache_id = redis_cache.save_html(pay.id, html_text)
    url = settings.PAY_CACHE_URL + cache_id
    _LOGGER.info("dopay create: %s, %s", parameter_dict, url)
    return {'charge_info': url}


# success
def check_notify_sign(request, app_id):
    api_key = _get_api_key(app_id)
    _LOGGER.info("dopay notify body: %s", request.body)
    data = dict(request.POST.iteritems())
    _LOGGER.info("dopay notify data: %s", data)
    verify_notify_sign(data, api_key)
    pay_id = data['orderid']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('dopay event does not contain pay ID')

    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = 'success'
    trade_no = data['paysapi_id']
    total_fee = float(data['price'])  # realprice
    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }
    from common.channel.pay import check_channel_order
    check_channel_order(pay_id, total_fee, app_id)
    if trade_status == 'success':
        _LOGGER.info('dopay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('uid', app_id),
        ('orderid', str(pay_order.id)),
        ('r', time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())),
    ))
    parameter_dict['key'] = generate_query_sign(parameter_dict, api_key)
    _LOGGER.info('dopay query data, %s', parameter_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_query_gateway(app_id), data=parameter_dict, headers=headers, timeout=3, verify=False)
    _LOGGER.info('dopay query rsp, %s', response.text)
    if response.status_code == 200:
        data = json.loads(response.text)['data']
        mch_id = pay_order.mch_id
        trade_status = str(data['status'])
        total_fee = float(pay_order.total_fee)
        trade_no = ''

        extend = {
            'trade_status': trade_status,
            'total_fee': total_fee,
            'trade_no': trade_no,
        }

        # 订单状态：0-等待支付；1-付款成功；2-付款未完成
        if trade_status == '1':
            _LOGGER.info('dopay query order success, mch_id:%s pay_id:%s' % (mch_id, pay_order.id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)

            async_job.notify_mch(pay_order.id)
