# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.cache import redis_cache
from common.channel.pay import check_channel_order
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError
from common.utils.tz import local_now
from common.utils.ip_address import check_valid_ip_address

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '10272': {  # easypay支付 支付宝个码  费率2.1% 100-10000 dwc
        'API_KEY': 'm2oh3dsxsiq7hsgqrvo5bd72r1ffb21p',
        'gateway': 'http://api.yitabc.com/Pay_Index.html',
        'query_gateway': 'http://api.yitabc.com/Pay_Trade_query.html',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _get_pay_type(service):
    if service == 'alipay':
        return '903'
    elif service == '':
        return ''
    else:
        return '903'


def generate_create_sign(d, key):
    '''  生成下单签名 '''
    s = 'pay_amount={}&pay_applydate={}&pay_bankcode={}&pay_callbackurl={}&pay_memberid={}&pay_notifyurl={' \
        '}&pay_orderid={}&key={}'.format(
        d['pay_amount'], d['pay_applydate'],
        d['pay_bankcode'], d['pay_callbackurl'], d['pay_memberid'], d['pay_notifyurl'], d['pay_orderid'], key)
    return _gen_sign(s)


def _gen_sign(s):
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    return m.hexdigest().upper()


def generate_notify_sign(parameter, key):
    '''  生成下单签名 '''
    s = ''
    for k in sorted(parameter.keys()):
        if parameter[k] and k not in ['sign', 'attach']:
            s += '%s=%s&' % (k, parameter[k])
    s += 'key=%s' % key
    return _gen_sign(s)


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_notify_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("easypayv2 sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def generate_query_sign(parameter, key):
    '''  生成下单签名 '''
    s = ''
    for k in parameter.keys():
        if parameter[k] != '' and parameter[k] is not None:
            s += '%s=%s&' % (k, parameter[k])
    s += 'key=%s' % key
    return _gen_sign(s)


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_notify_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("easypayv2 sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _build_form(params, gateway):
    html = u"<head><title>loading...</title></head><form id='submit' name='submit' action='" + gateway + "' method='post'>"
    for k, v in params.items():
        html += "<input type='hidden' name='%s' value='%s'/>" % (k, v)
    html += "</form>"
    html += "<script>doc" \
            "ument.forms['submit'].submit();</script>"
    return html


def create_charge(pay, pay_amount, info):
    ''' 创建订单 '''
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    service = info.get('service')
    parameter_dict = OrderedDict((
        ('pay_memberid', app_id),
        ('pay_orderid', str(pay.id)),
        ('pay_amount', str(pay_amount)),
        ('pay_productname', 'charge'),
        ('pay_applydate', local_now().strftime("%Y%m%d%H%M%S")),
        ('pay_bankcode', _get_pay_type(service)),
        (
            'pay_notifyurl',
            '{}/pay/api/{}/easypayv2/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('pay_callbackurl',
         '{}/pay/api/{}/easypayv2/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, pay.id)),
    ))
    parameter_dict['pay_md5sign'] = generate_create_sign(parameter_dict, api_key)
    _LOGGER.info("easypayv2 create data: %s, order_id is: %s", json.dumps(parameter_dict),
                 parameter_dict['pay_orderid'])
    # pay_returntype 缺省值为1;    1页面直接跳转 2 json格式返回
    # 使用 pay_returntype = 2时，就可以用下面这段代码
    # headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    # response = requests.post(_get_gateway(app_id), data=parameter_dict, headers=headers, timeout=3)
    # _LOGGER.info('easypayv2 create response: %s:', response.text)
    # return {'charge_info': json.loads(response.text)['data']['pay_url']}
    html_text = _build_form(parameter_dict, _get_gateway(app_id))
    cache_id = redis_cache.save_html(pay.id, html_text)
    _LOGGER.info('easypayv2 url: %s', settings.PAY_CACHE_URL + cache_id)
    return {'charge_info': settings.PAY_CACHE_URL + cache_id}


# SUCCESS
def check_notify_sign(request, app_id):
    _LOGGER.info("easypayv2 notify body: %s", request.body)
    data = dict(request.POST.iteritems())
    api_key = _get_api_key(app_id)
    _LOGGER.info("easypayv2 notify data: %s", data)
    verify_notify_sign(data, api_key)
    pay_id = data['orderid']
    check_valid_ip_address(str(request.META['REMOTE_ADDR']), pay_id)
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s", data)
        raise ParamError('easypayv2 event does not contain pay ID')

    pay = order_db.get_pay(pay_id)
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(data['returncode'])
    trade_no = str(data['transaction_id'])
    total_fee = float(data['amount'])

    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }
    check_channel_order(pay_id, total_fee, app_id)
    if trade_status == '00':
        _LOGGER.info('easypayv2 check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('pay_memberid', app_id),
        ('pay_orderid', str(pay_id)),
    ))
    parameter_dict['pay_md5sign'] = generate_query_sign(parameter_dict, api_key)
    _LOGGER.info("easypayv2 query data: %s, order_id is: %s", json.dumps(parameter_dict), parameter_dict['pay_orderid'])
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_query_gateway(app_id), data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info("easypayv2 query rsp data: %s", response.text)
    if response.status_code == 200:
        data = json.loads(response.text)
        trade_status = str(data['trade_state'])
        trade_no = str(data['transaction_id'])
        total_fee = float(data['amount'])
        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }
        if trade_status == 'SUCCESS':
            _LOGGER.info('easypayv2 query order success, user_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)
            async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('easypayv2 data error, status_code: %s', response.status_code)
