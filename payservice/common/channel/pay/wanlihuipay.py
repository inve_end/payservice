# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.cache import redis_cache
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError
from common.utils.tz import local_now

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '1234567890': {
        'API_KEY': 'd437f6675a9a445593759bcf53429fb8',
        'gateway': 'http://pay.kcgame.pw/pay/gateway',
        'query_gateway': 'http://pay.kcgame.pw/query/gateway',
    },
    '8888866602': {
        'API_KEY': '21f85b48c4b44540ad4368c7d3cc5bd0',
        'gateway': 'http://pay.kcgame.pw/pay/gateway',
        'query_gateway': 'http://pay.kcgame.pw/query/gateway',
    },
    '8894116888': {  # dwc 中辰 支付宝 １００－５０００  QQ 10 ~1000   京东5000  快捷5000
        'API_KEY': '7b99470e245840d486db598f6f8248ac',
        'gateway': 'http://pay.iiapx.pw/pay/gateway',
        'query_gateway': 'http://pay.iiapx.pw/query/gateway',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


# 00	B2C网银
# 04	支付宝H5
# 05	微信H5
# 06	支付宝扫码
# 07	微信扫码
# 08	QQ钱包扫码
# 09	QQH5
# 11	快捷支付
# 21	银联扫码
# 13 京东扫码
# 16 京东H5
def _get_pay_type(service):
    if service == 'alipay':
        return '04'
    elif service == 'wxpay':
        return '05'
    elif service == 'qq':
        return '09'
    elif service == 'jd':
        return '16'
    else:
        return '11'


def generate_sign(parameter, key):
    '''  生成下单签名 '''
    s = ''
    for k in sorted(parameter.keys()):
        if parameter[k] != '' and parameter[k] != None:
            s += '%s=%s&' % (k, parameter[k])
    s += 'key=%s' % key
    _LOGGER.info("wanlihuipay sign str : %s", s)
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().upper()
    return sign


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("wanlihuipay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _build_form(params, gateway):
    html = u"<head><title>loading...</title></head><form id='submit' name='submit' action='" + gateway + "' method='post'>"
    for k, v in params.items():
        html += "<input type='hidden' name='%s' value='%s'/>" % (k, v)
    html += "</form>"
    html += "<script>doc" \
            "ument.forms['submit'].submit();</script>"
    return html


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def create_charge(pay, pay_amount, info):
    ''' 创建订单 '''
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    service = info.get('service')
    parameter_dict = OrderedDict((
        ('version', 'V2.0'),
        ('merchantId', app_id),
        ('goodsName', 'charge'),
        ('busType', _get_pay_type(service)),
        ('orderId', str(pay.id)),
        ('orderTime', local_now().strftime('%Y%m%d%H%M%S')),
        ('orderAmt', '%.2f' % pay_amount),
        ('notifyUrl', '{}/pay/api/{}/wanlihuipay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('signType', 'MD5'),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    html_text = _build_form(parameter_dict, _get_gateway(app_id))
    cache_id = redis_cache.save_html(pay.id, html_text)
    url = settings.PAY_CACHE_URL + cache_id
    _LOGGER.info("wanlihuipay create  data: %s, url: %s", parameter_dict, _get_gateway(app_id))
    return {'charge_info': url}


# SUCCESS
def check_notify_sign(request, app_id):
    data = dict(request.POST.iteritems())
    api_key = _get_api_key(app_id)
    _LOGGER.info("wanlihuipay notify data1: %s", data)
    verify_notify_sign(data, api_key)
    pay_id = data['orderId']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('wanlihuipay event does not contain pay ID')

    pay = order_db.get_pay(pay_id)
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(data['status'])
    trade_no = data['transId']
    total_fee = float(data['orderAmt'])

    extend = {
        'trade_status': 'success',
        'trade_no': trade_no,
        'total_fee': total_fee
    }
    from common.channel.pay import check_channel_order
    check_channel_order(pay_id, total_fee, app_id)

    if trade_status == 'SUCCESS':
        _LOGGER.info('wanlihuipay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    # prepare param
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('version', 'V2.0'),
        ('merchantId', app_id),
        ('busType', '01'),
        ('orderId', str(pay_id)),
        ('signType', 'MD5'),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("wanlihuipay query  data: %s", parameter_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_query_gateway(app_id), data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info("wanlihuipay query  rsp data: %s", response.text)
    if response.status_code == 200:
        data = json.loads(response.text)
        trade_status = str(data['transStatus'])
        trade_no = data['transId']
        total_fee = float(data['orderAmt'])

        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }

        if trade_status == '1':
            _LOGGER.info('wanlihuipay query order success, user_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)
            async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('wanlihuipay data error, status_code: %s', response.status_code)
