# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests

from async import async_job
from common.channel.pay import check_channel_order
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '20190275': {  # 支付宝红包H5/扫码 2.6% 10-10000 witch
        'API_KEY': 'c065e813381577ef175716e399ed23d9',
        'gateway': 'http://www.ppcheng.net/api/default/index.html',
        'query_gateway': 'http://www.ppcheng.net/api/default/query.html',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def generate_sign(parameter, key):
    s = ''
    for k in sorted(parameter.keys()):
        if parameter[k]:
            s += '%s=%s&' % (k, parameter[k])
    s = '{}'.format(key) + s[:-1]
    _LOGGER.info("paopaochengpay sign str : %s", s)
    return encrypt_string(s)


def encrypt_string(hash_string):
    sha_signature = hashlib.sha256(hash_string.encode()).hexdigest()
    return sha_signature


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_sign(params, key)
    if sign.upper() != calculated_sign.upper():
        _LOGGER.info("paopaochengpay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _get_pay_type(service):
    if service == 'alipay_scan':
        # return '1'
        return 'aliscan'
    elif service == 'alipay_h5':
        # return '2'
        return 'aliwap'
    # elif service == 'alipay2bank':
    #     return '3'
    return 'aliscan'


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def create_charge(pay, pay_amount, info):
    service = info.get('service')
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('merchant_code', app_id),
        ('app_order', str(pay.id)),
        ('amount', str(pay_amount)),
        ('user_ip', _get_device_ip(info)),
        ('type', _get_pay_type(service)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("paopaochengpay create: %s", json.dumps(parameter_dict))
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_gateway(app_id), data=parameter_dict, headers=headers, timeout=5)
    _LOGGER.info("paopaochengpay create charge data: %s %s", response.status_code, response.text)
    return {'charge_info': json.loads(response.text)['message']}


def check_notify_sign(request, app_id):
    api_key = _get_api_key(app_id)
    data = dict(request.POST.iteritems())
    _LOGGER.info("paopaochengpay notify data: %s, order_id is: %s", data, data['app_order'])
    verify_notify_sign(data, api_key)
    pay_id = data['app_order']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s", data)
        raise ParamError('paopaochengpay event does not contain pay ID')
    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)
    mch_id = pay.mch_id
    trade_status = ''
    trade_no = data['trade_no']
    total_fee = float(data['pay_amount'])
    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }
    check_channel_order(pay_id, total_fee, app_id)
    if data['app_order'] and total_fee > 0.0:
        _LOGGER.info('paopaochengpay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    """ 查询订单 """
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('merchant_code', app_id),
        ('app_order', str(pay_order.id)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info('paopaochengpay query data: %s, order_id is: %s', json.dumps(parameter_dict),
                 parameter_dict['app_order'])
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_query_gateway(app_id), data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info('paopaochengpay query rsp, %s', response.text)
    if response.status_code == 200:
        data = json.loads(response.text)['message']
        mch_id = pay_order.mch_id
        trade_status = str(data['status'])
        total_fee = float(data['pay_amount'])
        trade_no = data['trade_no']

        extend = {
            'trade_status': trade_status,
            'total_fee': total_fee,
            'trade_no': trade_no,
        }
        # 1支付成功，0待支付
        if trade_status == '1':
            check_channel_order(pay_id, total_fee, app_id)
            _LOGGER.info('paopaochengpay query order success, mch_id:%s pay_id:%s' % (mch_id, pay_order.id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)

            async_job.notify_mch(pay_order.id)
