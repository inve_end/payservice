# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.cache import redis_cache
from common.channel.pay import check_channel_order
from common.order.db import get_pay, add_pay_success
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '1010': {
        'API_KEY': '0GJQo9yY9m98o0NQ099RnP89484nKryO',
        'gateway': 'http://dj.kcpay.ltd/xxky-pay-web/other/open.do',
        'query_gateway': 'http://dj.kcpay.ltd/Pay_Trade_query.html',
    },
    '1036': {
        'API_KEY': '1Ad8kh0t2505660jTY08hJ606t08c1ay',
        'gateway': 'http://dj.kcpay.ltd/xxky-pay-web/other/open.do',
        'query_gateway': 'http://dj.kcpay.ltd/Pay_Trade_query.html',
    },
    '11257': {  # dwc 支付宝3.1%（10----10万）
        'API_KEY': '373EDEA6A58045F9BB250F5F63B14D20',
        # 'gateway': 'http://dj.kcpay.ltd/xxky-pay-web/alipay/open.do',
        'gateway': 'http://dj.kcpay.ltd/xxky-pay-web/other/open.do',
        'query_gateway': 'http://dj.kcpay.ltd/Pay_Trade_query.html',
    },
    '180778724': {
        'API_KEY': 'bftwj8333zvacul6ft79kb3z18engpu5',
        'gateway': 'http://dj.kcpay.ltd/xxky-pay-web/other/open.do',
        # 'query_gateway': 'http://dj.kcpay.ltd/xxky-pay-web/order/pwd.do',
        'query_gateway': 'http://dj.kcpay.ltd/Pay_Trade_query.html',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def generate_sign(s):
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().lower()
    return sign


def generate_query_sign(s):
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().upper()
    return sign


def gen_order_sign(d, key):
    s = 'pid={}&money={}&out_trade_no={}&return_url={}&notify_url={}&{}'.format(
        d['pid'], d['money'], d['out_trade_no'], d['return_url'], d['notify_url'], key
    )
    return generate_sign(s)


def gen_notify_sign(d, key):
    s = 'pid={}&trade_status={}&trade_no={}&out_trade_no={}&money={}&{}'.format(
        d['pid'], d['trade_status'], d['trade_no'], d['out_trade_no'], d['money'], key
    )
    return generate_sign(s)


def gen_query_sign(d, key):
    s = 'pay_memberid={}&pay_orderid={}&key={}'.format(
        d['pay_memberid'], d['pay_orderid'], key
    )
    return generate_query_sign(s)


def gen_verify_query_sign(d, key):
    s = ''
    for k in sorted(d.keys()):
        s += '%s=%s&' % (k, d[k])
    s += 'key=%s' % key
    return generate_query_sign(s)


def verify_notify_sign(params, key):
    calculated_sign = gen_notify_sign(params, key)
    if params['sign'] != calculated_sign:
        _LOGGER.info("xinkeyunpay sign: %s, calculated sign: %s", params['sign'], calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def verify_query_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = gen_verify_query_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("xinkeyunpay sign: %s, calculated sign: %s", params['sign'], calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _build_form(params, gateway):
    html = u"<head><title>loading...</title></head><form id='submit' name='submit' action='" + gateway + "' method='post'>"
    for k, v in params.items():
        html += "<input type='hidden' name='%s' value='%s'/>" % (k, v)
    html += "</form>"
    html += "<script>doc" \
            "ument.forms['submit'].submit();</script>"
    return html


def _get_pay_type(service):
    if service == 'alipay':
        return '1'
    elif service == 'wechat':
        return '2'
    elif service == 'union_pay':
        return '3'
    return '1'


def create_charge(pay, pay_amount, info):
    app_id = info['app_id']
    service = info.get('service')
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('pid', app_id),
        ('type', _get_pay_type(service)),
        ('out_trade_no', str(pay.id)),
        ('notify_url', '{}/pay/api/{}/xinkeyunpay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('return_url', '{}/pay/api/{}/xinkeyunpay/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, pay.id)),
        ('name', 'charge'),
        ('money', '%.2f' % pay_amount),
    ))
    parameter_dict['sign'] = gen_order_sign(parameter_dict, api_key)
    _LOGGER.info("xinkeyunpay create date: %s, order_id is: %s", json.dumps(parameter_dict),
                 parameter_dict['out_trade_no'])
    html_text = _build_form(parameter_dict, _get_gateway(app_id))
    cache_id = redis_cache.save_html(pay.id, html_text)
    _LOGGER.info('xinkeyunpay url: %s', settings.PAY_CACHE_URL + cache_id)
    return {'charge_info': settings.PAY_CACHE_URL + cache_id}


def check_notify_sign(request, app_id):
    data = dict(request.POST.iteritems())
    _LOGGER.info("xinkeyunpay notify data: %s", data)
    verify_notify_sign(data, _get_api_key(app_id))
    pay_id = data['out_trade_no']
    if not pay_id:
        _LOGGER.error("fatal error, pay object not exists, data: %s" % data)
        raise ParamError('xinkeyunpay event does not contain valid pay ID')

    pay = get_pay(pay_id)
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = data['trade_status']
    trade_no = data['trade_no']
    total_fee = float(data['money'])

    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    check_channel_order(pay_id, total_fee, app_id)

    if trade_status == 'TRADE_SUCCESS':
        _LOGGER.info('xinkeyunpay check order success, user_id:%s pay_id:%s' % (mch_id, pay_id))
        add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('pay_memberid', app_id),
        ('pay_orderid', str(pay_order.id)),
    ))
    parameter_dict['pay_md5sign'] = gen_query_sign(parameter_dict, api_key)
    _LOGGER.info('xinkeyunpay query data %s, order_id is: %s', json.dumps(parameter_dict),
                 parameter_dict['pay_orderid'])
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_query_gateway(app_id), data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info('xinkeyunpay query rsp, %s', response.text)
    data = json.loads(response.text)
    verify_query_sign(data, _get_api_key(app_id))
    if response.status_code == 200:
        trade_status = str(data['trade_state'])  # 订单已完成, 订单未完成
        total_fee = float(data['amount'])
        trade_no = str(data['transaction_id'])
        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }
        # NOTPAY-未支付 SUCCESS 已支付
        if trade_status == 'SUCCESS':
            check_channel_order(pay_id, total_fee, app_id)
            _LOGGER.info('xinkeyunpay query order success, mch_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            res = add_pay_success(pay_order.mch_id, pay_id, total_fee, trade_no, extend)
            if res:
                async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('xinkeyunpay data error, status_code: %s', response.status_code)
