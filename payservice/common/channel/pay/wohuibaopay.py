# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.cache import redis_cache
from common.channel.pay import check_channel_order
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '600111': {  # dwc 付宝2.5%（100-5000）D0结算   # 商户号
        'API_KEY': 'd65b5b554a3a44aebf04b5f0b0ebb5c0',  # 密钥
        'gateway': 'http://pay.wooopay.com/chargebank.aspx',  # 下单路径
        # 'query_gateway': 'http://pay.wooopay.com/chargebank.aspx/query'
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _gen_sign(s):
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest()
    return sign


def generate_sign(parameter, key):
    s = ''
    key_list = ['parter', 'type', 'value', 'orderid', 'callbackurl', 'opstate', 'ovalue']
    new_dict = OrderedDict((k, parameter.get(k)) for k in key_list)
    for k in new_dict:
        if new_dict[k]:
            s += '%s=%s&' % (k, new_dict[k])
    s += 'key=%s' % key
    _LOGGER.info("wohuibao sign str: %s", s)
    return _gen_sign(s)


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def verify_notify_sign(params, key):
    sign = params.pop('sign')
    calculated_sign = generate_sign(params, key)
    if sign.upper() != calculated_sign.upper():
        _LOGGER.info("wohuibao notify sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _get_pay_type(service):
    if service == 'alipay':
        return '1006'
    return '1006'


def _build_form(params, gateway):
    html = u"<head><title>loading...</title></head><form id='submit' name='submit' action='" + gateway + "' method='post'>"
    for k, v in params.items():
        html += "<input type='hidden' name='%s' value='%s'/>" % (k, v)
    html += "</form>"
    html += "<script>doc" \
            "ument.forms['submit'].submit();</script>"
    return html


def create_charge(pay, pay_amount, info):
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    service = info['service']
    parameter_dict = OrderedDict((
        ('parter', app_id),
        ('type', _get_pay_type(service)),
        ('value', int(pay_amount * 100)),
        ('orderid', str(pay.id)),
        ('callbackurl', '{}/pay/api/{}/wohuibaopay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('payerIp', _get_device_ip(info)),
        ('attach', 'charge'),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    url = _get_gateway(app_id)
    # headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    _LOGGER.info("wohuibao create: url: %s, %s", url, json.dumps(parameter_dict))
    # response = requests.post(url, data=parameter_dict, headers=headers, timeout=5)
    # _LOGGER.info("wohuibao create: response: %s", response.text)
    # data = json.loads(response.text)
    html_text = _build_form(parameter_dict, _get_gateway(app_id))
    cache_id = redis_cache.save_html(pay.id, html_text)
    return {'charge_info': settings.PAY_CACHE_URL + cache_id}


def check_notify_sign(request, app_id):
    data = dict(request.POST.iteritems())
    api_key = _get_api_key(app_id)
    _LOGGER.info("wohuibao notify data: %s", data)
    verify_notify_sign(data, api_key)
    pay_id = data['orderid']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('wohuibao event does not contain pay ID')

    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id  # 商户编号
    trade_status = str(data['opstate'])
    total_fee = float(data['ovalue']) / 100.0
    trade_no = data['sysorderid']
    trade_time = data['systime']
    trade_attach = data['attach']
    trade_msg = data['msg']
    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee,
        'trade_time': trade_time,
        'trade_attach': trade_attach,
        'trade_msg': trade_msg
    }

    check_channel_order(pay_id, total_fee, app_id)

    # 支付状态：00:未交易 01:成 功 02:失败 06:未支付.
    if trade_status == '01' and total_fee > 0.0:
        _LOGGER.info('wohuibao check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    """ 查询订单 """
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('orderid', str(pay.id)),
        ('opstate', ''),
        ('ovalue', ''),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    _LOGGER.info("wohuibao create: url: %s, %s", _get_query_gateway(app_id), json.dumps(parameter_dict))
    response = requests.post(_get_query_gateway(app_id), data=parameter_dict, headers=headers, timeout=5)
    _LOGGER.info("wohuibao create: response: %s", response.text)
    if response.status_code == 200:
        data = json.loads(response.text)

        verify_notify_sign(data, api_key)

        mch_id = pay_order.mch_id
        trade_status = str(data['opstate'])
        total_fee = float(data['ovalue']) / 100.0
        trade_no = str(data['orderid'])
        extend = {
            'trade_status': trade_status,
            'total_fee': total_fee,
            'trade_no': trade_no,
        }

        # 00:未交易 01:成 功 02:失败 06:未支付.
        if trade_status == '01' and total_fee > 0.0:
            check_channel_order(pay_id, total_fee, app_id)

            _LOGGER.info('wohuibao query order success, mch_id:%s pay_id:%s' % (mch_id, pay_order.id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)

            async_job.notify_mch(pay_order.id)
