# -*- coding: utf-8 -*-
import hashlib
import json

import requests
from django.conf import settings

from async import async_job
from common.channel.pay import check_channel_order
from common.order.db import get_pay, add_pay_success
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '10155': {  # loki 支付宝3%（200-5000）D0结算
        'API_KEY': '99aa493f25dd49f991f979e324fa24d2',
        'gateway': 'http://pay.ulinepay.com/bank/',
        'query_gateway': 'http://pay.ulinepay.com/search.ashx?parter={}&orderid={}&sign={}',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def gen_order_str(parameter):
    return 'parter={}&type={}&value={}&orderid={}&callbackurl={}'.format(
        parameter['parter'], parameter['type'], parameter['value'],
        parameter['orderid'], parameter['callbackurl'])


def generate_order_sign(parameter, key):
    s = gen_order_str(parameter) + key
    m = hashlib.md5()
    m.update(s.encode('GB2312'))
    sign = m.hexdigest()
    return sign


def generate_query_sign(parameter, key):
    s = 'orderid={}&parter={}{}'.format(parameter['orderid'], parameter['parter'], key)
    m = hashlib.md5()
    m.update(s.encode('GB2312'))
    sign = m.hexdigest()
    return sign


def verify_notify_sign(params, key):
    sign = params.pop('sign')
    s = 'orderid={}&opstate={}&ovalue={}{}'.format(params['orderid'], params['opstate'], params['ovalue'], key)
    m = hashlib.md5()
    m.update(s.encode('GB2312'))
    calculated_sign = m.hexdigest()
    if sign != calculated_sign:
        _LOGGER.info("youlianpay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def verify_query_rsp_sign(params, key):
    sign = params.pop('sign')
    s = 'orderid={}&opstate={}&ovalue={}{}'.format(params['orderid'], params['opstate'], params['ovalue'], key)
    m = hashlib.md5()
    m.update(s.encode('GB2312'))
    calculated_sign = m.hexdigest()
    if sign != calculated_sign:
        _LOGGER.info("youlianpay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _get_pay_type(service):
    if service == 'alipay_scan':
        return '1003'
    return '1003'


def create_charge(pay, pay_amount, info):
    app_id = info['app_id']
    key = _get_api_key(app_id)
    service = info.get('service')
    p_dict = {
        'parter': app_id,
        'type': _get_pay_type(service),
        'value': '%.2f' % pay_amount,
        'orderid': str(pay.id),
        'callbackurl': '{}/pay/api/{}/youlianpay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id),
        'hrefbackurl': '{}/pay/api/{}/youlianpay/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, str(pay.id))
    }
    sign = generate_order_sign(p_dict, key)
    url = _get_gateway(app_id) + '?' + gen_order_str(p_dict) + '&sign=' + sign
    _LOGGER.info("youlianpay data url: %s", url)
    return {'charge_info': url}


# ok
def check_notify_sign(request, app_id):
    key = _get_api_key(app_id)
    data = dict(request.GET.iteritems())
    _LOGGER.info("youlianpay notify data: %s, order_id is: %s", json.dumps(data), data['orderid'])
    verify_notify_sign(data, key)
    pay_id = data['orderid']
    if not pay_id:
        _LOGGER.error("fatal error, pay object not exists, data: %s" % data)
        raise ParamError('youlianpay event does not contain valid pay ID')

    pay = get_pay(pay_id)
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = int(data['opstate'])
    trade_no = str(data['sysorderid'])
    total_fee = float(data['ovalue'])

    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }
    check_channel_order(pay_id, total_fee, app_id)
    # 0 成功， -1 ：请求参数无效 -2：签名错误
    if trade_status == 0:
        _LOGGER.info('youlianpay check order success, user_id:%s pay_id:%s' % (mch_id, pay_id))
        add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def _get_str(s, start, end):
    p1 = s.index(start)
    p2 = s.index(end, p1 + len(start))
    if p2 > p1:
        return s[p1 + len(start): p2]
    else:
        return ''


def _get_sign(s, start):
    p1 = s.index(start)
    return s[p1 + len(start):]


def query_charge(pay_order, app_id):
    pay_id = pay_order.id
    key = _get_api_key(app_id)
    p_dict = {
        'parter': app_id,
        'orderid': str(pay_id),
    }
    p_dict['sign'] = generate_query_sign(p_dict, key)
    url = _get_query_gateway(app_id).format(p_dict['parter'], p_dict['orderid'], p_dict['sign'])
    _LOGGER.info(u'youlianpay create query url : %s', url)
    response = requests.get(url)
    _LOGGER.info(u'youlianpay create query rsp: %s', response.text)
    data = {'orderid': str(_get_str(response.text, 'orderid=', '&')),
            'opstate': int(_get_str(response.text, 'opstate=', '&')),
            'ovalue': _get_str(response.text, 'ovalue=', '&'),
            'sign': str(_get_sign(response.text, 'sign='))}
    verify_query_rsp_sign(data, key)
    if response.status_code == 200:
        trade_status = int(_get_str(response.text, 'opstate=', '&'))
        total_fee = float(_get_str(response.text, 'ovalue=', '&'))
        trade_no = pay_order.third_id
        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }

        # 3：请求参数无效 2：签名错误 1：商户订单号无效 0：支付成功 其他：用户还未完成支付或者支付失败
        if trade_status == 0:
            _LOGGER.info('youlianpay query order success, mch_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            add_pay_success(pay_order.mch_id, pay_id, total_fee, trade_no, extend)
            async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('youlianpay data error, status_code: %s', response.status_code)
