# -*- coding: utf-8 -*-
import hashlib
import json
import time
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.cache import redis_cache
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

_GATEWAY = 'http://paypaul.385mall.top/onlinepay/h5PayApi'
# _GATEWAY = 'http://test.18mall.top:9988/onlinepay/h5PayApi'
_QUERY_GATEWAY = 'http://paypaul.385mall.top/onlinepay/orderQuery'

APP_CONF = {
    '999941000620': {
        'API_KEY': '8AE58B99DEF7E83B3DF5888BFB150777',
    },
    '999941000643': {
        'API_KEY': '904C92BC2AEA4CDB5300DF48D2A881B7',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


# 支付类型微信30，支付宝22 ，QQ钱包31
def _get_pay_type(service):
    if service == 'alipay':
        return '22'
    elif service == 'wxpay':
        return '30'
    elif service == 'qq':
        return '31'
    else:
        return '30'


def generate_sign(parameter, key):
    '''  生成下单签名 '''
    s = ''
    for k in sorted(parameter.keys()):
        if parameter[k] != '' and parameter[k] != None:
            s += '%s=%s&' % (k, parameter[k])
    s += 'key=%s' % key
    _LOGGER.info("bftpay sign str : %s", s)
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().upper()
    return sign


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    params.pop('random')
    calculated_sign = generate_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("bftpay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _build_form(params):
    html = u"<head><title>loading...</title></head><form id='submit' name='submit' action='" + _GATEWAY + "' method='post'>"
    for k, v in params.items():
        html += "<input type='hidden' name='%s' value='%s'/>" % (k, v)
    html += "</form>"
    html += "<script>doc" \
            "ument.forms['submit'].submit();</script>"
    return html


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def create_charge(pay, pay_amount, info):
    ''' 创建订单 '''
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    service = info.get('service')
    parameter_dict = OrderedDict((
        ('payType', _get_pay_type(service)),
        ('merchantId', app_id),
        ('orderId', str(pay.id)),
        ('amount', str(pay_amount)),
        ('mode', 'T0'),
        ('notifyUrl', '{}/pay/api/{}/bftpay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('returnUrl', '{}/pay/api/{}/bftpay/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, app_id)),
        ('requestIp', _get_device_ip(info).replace('.', '_')),
        ('orderTime', time.strftime("%Y%m%d%H%M%S", time.localtime())),
        ('goodsName', 'charge'),
        ('goodsNum', '1'),
        ('goodsDesc', 'charge'),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("bftpay create  data: %s, url: %s", parameter_dict, _GATEWAY)
    html_text = _build_form(parameter_dict)
    cache_id = redis_cache.save_html(pay.id, html_text)
    return {'charge_info': settings.PAY_CACHE_URL + cache_id}


# success
def check_notify_sign(request, app_id):
    data1 = dict(request.POST.iteritems())
    api_key = _get_api_key(app_id)
    _LOGGER.info("bftpay notify data1: %s", data1)
    _LOGGER.info("bftpay notify body: %s", request.body)
    data = json.loads(request.body)
    verify_notify_sign(data, api_key)
    pay_id = data['orderId']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('bftpay event does not contain pay ID')

    pay = order_db.get_pay(pay_id)
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(data['status'])
    trade_no = pay_id
    total_fee = float(data['amount'])

    extend = {
        'trade_status': 'success',
        'trade_no': trade_no,
        'total_fee': total_fee
    }
    from common.channel.pay import check_channel_order
    check_channel_order(pay_id, total_fee, app_id)
    # 1 下单成功
    # 2 下单失败
    # 3 下单中
    # 4 支付成功
    # 5 支付失败
    # 6 状态不详
    # 7 代付中
    # 8 代付失败
    # 9 交易退款
    if trade_status == '4':
        _LOGGER.info('bftpay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    # prepare param
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('transCode', '002'),
        ('merchantId', app_id),
        ('orderId', str(pay_id)),
        ('amount', str(pay_order.total_fee)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("bftpay query  data: %s", parameter_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_QUERY_GATEWAY, data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info("bftpay query  rsp data: %s", response.text)
    if response.status_code == 200:
        data = json.loads(response.text)
        trade_status = str(data['status'])
        trade_no = pay_id
        total_fee = float(pay_order.total_fee)

        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }

        if trade_status == '4':
            _LOGGER.info('bftpay query order success, user_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)
            async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('bftpay data error, status_code: %s', response.status_code)
