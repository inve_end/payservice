# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.cache import redis_cache
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

_GATEWAY = 'http://api.smpay2016.com/Bank'
_QUERY_GATEWAY = 'http://api.smpay2016.com/search.aspx?parter={}&orderid={}&sign={}'

APP_CONF = {
    '1912': {  # 安心付 witch 微信 300-3000 100倍数 3.3%
        'API_KEY': '2957cc4bbde140b6a19041314ce8f736',
    },
    '1913': {  # 安心付 dwc 支付宝 50-10000 2.1%
        'API_KEY': 'b1dff383b0dc4b8093e14d57d48753a4',
    },
    '1920': {  # 安心付 dwc 云闪付 50-5000 1.3%
        'API_KEY': '6e28235ea0e74e309700359b660f9c4c',
    },
    # 云闪付 50-5000 1.3%
    # 支付宝 50-10000 2.1%
    # 微信 300-3000 100倍数 3.3%
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


# 编号 类型:tyid 参数
# 102网上银行/兼容 手机界面
# 1020手机版 网银/标准 H5 界 面
# 99微信 二维码
# 98支付宝 二维码
# 101银联扫码
# 990微信 H5-直接打开微信客户端付款
# 980支付宝 H5-直接打开 支付宝客户端付款
# 100QQ 钱包


# 1006 支付宝 ALAPPh5-直接打开支付宝客户端付款
# 1007 微信 WCAPPH5-直接打开微信客户端付款
# 1005 手机版 网银 支付 WAP
# 992 支付宝 二维码 ALQR
# 991 微信 二维码 WCQR
# 1021 银联 二维码 UNQR
# 993 QQ 钱包 QQwallet
def _get_pay_type(service):
    if service == 'alipay_scan':
        return '98', '992'
    elif service == 'wechat_scan':
        return '99', '991'
    elif service == 'alipay_h5':
        return '980', '1006'
    elif service == 'wechat_h5':
        return '990', '1007'
    elif service == 'cloud_flash':
        return '101', '1021'
    else:
        return '98', '992'


def _gen_sign(s):
    m = hashlib.md5()
    m.update(s.encode('GB2312'))
    sign = m.hexdigest().lower()
    return sign


def generate_sign(d, key):
    '''  生成下单签名 '''
    s = 'parter={}&type={}&value={}&orderid={}&tyid={}&callbackurl={}{}'.format(
        d['parter'], d['type'], d['value'], d['orderid'], d['tyid'], d['callbackurl'], key
    )
    return _gen_sign(s)


def generate_notify_sign(d, key):
    '''  生成下单签名 '''
    s = 'orderid={}&opstate={}&ovalue={}{}'.format(
        d['orderid'], d['opstate'], d['ovalue'], key
    )
    return _gen_sign(s)


def generate_query_sign(d, key):
    '''  生成下单签名 '''
    s = 'orderid={}&parter={}{}'.format(
        d['orderid'], d['parter'], key
    )
    return _gen_sign(s)


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_notify_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("anxinpay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _build_form(params):
    html = u"<head><title>loading...</title></head><form id='submit' name='submit' action='" + _GATEWAY + "' method='get'>"
    for k, v in params.items():
        html += "<input type='hidden' name='%s' value='%s'/>" % (k, v)
    html += "</form>"
    html += "<script>doc" \
            "ument.forms['submit'].submit();</script>"
    return html


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def _get_str(s, start, end):
    p1 = s.index(start)
    p2 = s.index(end, p1 + len(start))
    if p2 > p1:
        return s[p1 + len(start): p2]
    else:
        return ''


def create_charge(pay, pay_amount, info):
    ''' 创建订单 '''
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    service = info.get('service')
    tyid, type = _get_pay_type(service)
    parameter_dict = OrderedDict((
        ('parter', app_id),
        ('type', type),
        ('value', '%.2f' % pay_amount),
        ('orderid', str(pay.id)),
        ('tyid', tyid),
        ('callbackurl', '{}/pay/api/{}/anxinpay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('hrefbackurl', '{}/pay/api/{}/anxinpay/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, app_id)),
        ('payerIp', _get_device_ip(info)),
        ('agent', ''),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("anxinpay create  data: %s, url: %s", json.dumps(parameter_dict), _GATEWAY)
    html_text = _build_form(parameter_dict)
    cache_id = redis_cache.save_html(pay.id, html_text)
    return {'charge_info': settings.PAY_CACHE_URL + cache_id}


# opstate=0
def check_notify_sign(request, app_id):
    data = dict(request.GET.iteritems())
    api_key = _get_api_key(app_id)
    _LOGGER.info("anxinpay notify data1: %s", data)
    verify_notify_sign(data, api_key)
    pay_id = data['orderid']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('anxinpay event does not contain pay ID')

    pay = order_db.get_pay(pay_id)
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(data['opstate'])
    trade_no = data['sysorderid']
    total_fee = float(data['ovalue'])

    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    from common.channel.pay import check_channel_order
    check_channel_order(pay_id, total_fee, app_id)

    # 0：处理成功。 -1：请求参数无效。 -2：签名错误 。只有opstate = 0才说明付款成功。其他状态无论什么内容，都不是成功
    if trade_status == '0':
        _LOGGER.info('anxinpay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    # prepare param
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('parter', app_id),
        ('orderid', str(pay_id)),
    ))
    parameter_dict['sign'] = generate_query_sign(parameter_dict, api_key)
    _LOGGER.info("anxinpay query data: %s", json.dumps(parameter_dict))
    response = requests.get(
        _QUERY_GATEWAY.format(parameter_dict['parter'], parameter_dict['orderid'], parameter_dict['sign']), timeout=3)
    _LOGGER.info("anxinpay query rsp data: %s", response.text)
    if response.status_code == 200:
        rsp = str(response.text)
        data = {}
        for s in rsp.split('&'):
            key_value = s.split('=')
            data[key_value[0]] = key_value[1]
        trade_status = str(data['opstate'])
        trade_no = pay_order.third_id
        total_fee = float(data['ovalue'])

        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }

        if trade_status == '0':
            _LOGGER.info('anxinpay query order success, user_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)
            async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('anxinpay data error, status_code: %s', response.status_code)
