# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

from django.conf import settings

from async import async_job
from common.cache import redis_cache
from common.channel.pay import check_channel_order
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '20790': {  # 嘟嘟支付  alipay-h5 50-5000 2.7% DWC
        'API_KEY': 'f33385a14038d047383a875a0f67b6f2 ',
        'gateway': 'http://168tui.cn/pay',
    },
    '20877': {  # 嘟嘟支付  alipay-h5 100-10000 2.7% zs
        'API_KEY': '2ed8d65b3feeaa436e25a2dcebe97c43',
        'gateway': 'http://168tui.cn/pay',
    },
    '20878': {  # 嘟嘟支付  alipay-h5 100-10000 2.7% witch
        'API_KEY': 'a56141fd2e1cd383fe74c0d618d11511',
        'gateway': 'http://168tui.cn/pay',
    },
    '20931': {  # 嘟嘟支付  alipay-h5 100-5000 3% tt
        'API_KEY': '4c8751a59f2f23487de0e8313c927903',
        'gateway': 'http://168tui.cn/pay',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _gen_sign(s):
    if isinstance(s, str):
        input_string = s.decode('ascii', 'ignore').encode('ascii')
    m = hashlib.md5()
    m.update(input_string.encode('utf8'))
    sign = m.hexdigest().lower()
    return sign


def generate_sign(d, key):
    s = 'merchant_id={}&order_id={}&amount={}&sign={}'.format(d['merchant_id'], d['order_id'],
                                                              d['amount'], key)
    return _gen_sign(s)


def generate_notify_sign(d, key):
    s = 'merchant_id={}&order_id={}&amount={}&sign={}'.format(
        d['merchant_id'], d['order_id'],
        d['amount'], key)
    return _gen_sign(s)


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_notify_sign(params, key)
    if sign.upper() != calculated_sign.upper():
        _LOGGER.info("dudupay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _get_pay_type(service):
    if service == 'alipay':
        return 5
    return 5


def _build_form(params, gateway):
    html = u"<head><title>loading...</title></head><form id='submit' name='submit' action='" + gateway + "' method='get'>"
    for k, v in params.items():
        html += "<input type='hidden' name='%s' value='%s'/>" % (k, v)
    html += "</form>"
    html += "<script>doc" \
            "ument.forms['submit'].submit();</script>"
    return html


def create_charge(pay, pay_amount, info):
    service = info.get('service')
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('merchant_id', int(app_id)),
        ('order_id', str(pay.id)),
        ('amount', str(int(pay_amount))),
        ('notify_url', '{}/pay/api/{}/dudupay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('return_url', '{}/pay/api/{}/dudupay/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, pay.id)),
        ('pay_method', _get_pay_type(service)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("dudupay create: %s", json.dumps(parameter_dict))
    _LOGGER.info("dudupay order_id is: %s", parameter_dict['order_id'])
    html_text = _build_form(parameter_dict, _get_gateway(app_id))
    cache_id = redis_cache.save_html(pay.id, html_text)
    return {'charge_info': settings.PAY_CACHE_URL + cache_id}


# SUCCESS
def check_notify_sign(request, app_id):
    api_key = _get_api_key(app_id)
    data = json.loads(request.body)
    _LOGGER.info("dudupay notify data: %s", data)
    _LOGGER.info("dudupay notify order_id: %s", data['order_id'])
    verify_notify_sign(data, api_key)
    pay_id = data['order_id']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s", data)
        raise ParamError('dudupay event does not contain pay ID')

    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(data['isPaid'])
    trade_no = data['order_id']
    total_fee = float(data['amount'])
    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }
    check_channel_order(pay_id, total_fee, app_id)
    if trade_status == '1' and total_fee > 0.0:
        _LOGGER.info('dudupay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    pass
