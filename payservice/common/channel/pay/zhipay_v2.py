# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.channel.pay import check_channel_order
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '60008': {  # zs 支付宝2.8%（10---5000）
        'API_KEY': '62CCCED7D0134C2B86368EBB343B366A',
        'gateway': 'http://39.98.44.153/pay/unifiedorder',
        'query_gateway': 'http://39.98.44.153/pay/orderquery',
    },
    '60009': {  # dwc 支付宝2.8%（10---5000）
        'API_KEY': 'D5D8DFAAE22E4A5091605C8FCDBA123D',
        'gateway': 'http://39.98.44.153/pay/unifiedorder',
        'query_gateway': 'http://39.98.44.153/pay/orderquery',
    },
    '60010': {  # witch 支付宝2.8%（10---5000）
        'API_KEY': 'D505A84CD84B4CCE90E1460CB309DAD1',
        'gateway': 'http://39.98.44.153/pay/unifiedorder',
        'query_gateway': 'http://39.98.44.153/pay/orderquery',
    },
    '60011': {  # loki 支付宝2.8%（10---5000）
        'API_KEY': 'B93A303F6F6E4CBCB00BCF4068946805',
        'gateway': 'http://39.98.44.153/pay/unifiedorder',
        'query_gateway': 'http://39.98.44.153/pay/orderquery',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _gen_sign(s):
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().upper()
    return sign


def generate_sign(parameter, key):
    s = ''
    for k in sorted(parameter.keys()):
        if parameter[k]:
            s += '%s=%s&' % (k, parameter[k])
    s += 'key=%s' % key
    _LOGGER.info('zhipay_v2 sign str : %s', s[:-1])
    return _gen_sign(s)


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_sign(params, key)
    if sign.upper() != calculated_sign.upper():
        _LOGGER.info("zhipay_v2 sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def _get_pay_type(service):
    if service == 'alipay':
        return 'H5'
    return 'H5'


def _get_pay_channel(service):
    if service == 'alipay':
        return 'ALIPAY'
    return 'ALIPAY'


def create_charge(pay, pay_amount, info):
    service = info.get('service')
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('merId', app_id),
        ('outTradeNo', str(pay.id)),
        ('nonceStr', str(pay.id)),
        ('body', 'charge'),
        ('notifyUrl', '{}/pay/api/{}/zhipay_v2/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('callBackUrl', '{}/pay/api/{}/zhipay_v2/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, str(pay.id))),
        ('totalFee', str(int(pay_amount * 100))),
        ('payType', _get_pay_type(service)),
        ('payChannel', _get_pay_channel(service)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("zhipay_v2 create: %s", json.dumps(parameter_dict))
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_gateway(app_id), data=parameter_dict, headers=headers, timeout=5, verify=False)
    _LOGGER.info("zhipay_v2 create rsp : %s, jp_id: %s", response.text, str(pay.id))
    return {'charge_info': json.loads(response.text)['data']['payUrl']}


def check_notify_sign(request, app_id):
    api_key = _get_api_key(app_id)
    data = dict(request.POST.iteritems())
    _LOGGER.info("zhipay_v2 notify data: %s", data)
    verify_notify_sign(data, api_key)
    pay_id = data['outTradeNo']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('zhipay_v2 event does not contain pay ID')

    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(data['status'])
    trade_no = data['orderNo']
    total_fee = float(data['totalFee']) / 100.0
    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    check_channel_order(pay_id, total_fee, app_id)

    if trade_status == 'paid' and total_fee > 0.0:
        _LOGGER.info('zhipay_v2 check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    """ 查询订单 """
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('merId', app_id),
        ('outTradeNo', str(pay_order.id)),
        ('nonceStr', str(pay_order.id)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info('zhipay_v2 query data, %s', parameter_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_query_gateway(app_id), data=parameter_dict, headers=headers, timeout=3, verify=False)
    _LOGGER.info('zhipay_v2 query rsp, %s', response.text)
    if response.status_code == 200:
        data = json.loads(response.text)
        mch_id = pay_order.mch_id
        trade_status = str(data['data']['status'])
        total_fee = float(data['data']['totalFee']) / 100.0
        trade_no = data['data']['orderNo']

        extend = {
            'trade_status': trade_status,
            'total_fee': total_fee,
            'trade_no': trade_no,
        }

        if trade_status == 'paid':
            check_channel_order(pay_id, total_fee, app_id)

            _LOGGER.info('zhipay_v2 query order success, mch_id:%s pay_id:%s' % (mch_id, pay_order.id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)

            async_job.notify_mch(pay_order.id)
