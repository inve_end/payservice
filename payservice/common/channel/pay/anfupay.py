# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

_GATEWAY = 'http://api.abf.money/gateway/pay'
_QUERY_GATEWAY = 'http://api.abf.money/gateway/query'

APP_CONF = {
    '180429': {  # witch 支付宝
        'API_KEY': '1de9c948-799f-4fd0-973e-3e445ed38498',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


# wxpay 为微信, alipay 为支付宝
def _get_pay_type(service):
    if service == 'wxpay':
        return 'wxpay'
    else:
        return 'alipay'


def generate_sign(d, key):
    '''  生成下单签名 '''
    s = d['pay_type'] + d['order_code'] + d['business_code'] + key
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest()
    return sign


def verify_notify_sign(params, key, app_id):
    sign = params['md5']
    params['business_code'] = app_id
    params['pay_type'] = 'alipay'
    calculated_sign = generate_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("anfupay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def create_charge(pay, pay_amount, info):
    ''' 创建订单 '''
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    service = info.get('service')
    parameter_dict = OrderedDict((
        ('business_token', api_key),
        ('business_code', app_id),
        ('order_code', str(pay.id)),
        ('total_fee', str(int(pay_amount * 100))),
        ('pay_type', _get_pay_type(service)),
        ('notify_url', '{}/pay/api/{}/anfupay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('return_url', '{}/pay/api/{}/anfupay/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, pay.id)),
    ))

    parameter_dict['md5'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("anfupay create  data: %s", parameter_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_GATEWAY, data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info("anfupay create rsp data: %s %s", response.status_code, response.text)
    return {'charge_info': json.loads(response.text)['data']['code_url']}


# ok
def check_notify_sign(request, app_id):
    data = dict(request.POST.iteritems())
    api_key = _get_api_key(app_id)
    _LOGGER.info("anfupay notify data1: %s", data)
    _LOGGER.info("anfupay notify body: %s", request.body)
    data = json.loads(request.body)
    verify_notify_sign(data, api_key)
    pay_id = data['order_code']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('anfupay event does not contain pay ID')

    pay = order_db.get_pay(pay_id)
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(data['status'])
    trade_no = ''
    total_fee = float(pay.total_fee)

    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    from common.channel.pay import check_channel_order
    check_channel_order(pay_id, total_fee, app_id)
    if trade_status == 'SUCCESS':
        _LOGGER.info('anfupay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    # prepare param
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('business_token', api_key),
        ('business_code', app_id),
        ('order_code', str(pay_id)),
        ('pay_type', 'alipay'),
    ))
    parameter_dict['md5'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("anfupay query  data: %s", parameter_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_QUERY_GATEWAY, data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info("anfupay query  rsp data: %s", response.text)
    if response.status_code == 200:
        data = json.loads(response.text)['data']
        trade_status = str(data['status'])
        trade_no = ''
        total_fee = float(pay_order.total_fee)

        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }

        # 0.支付中1.成功, 2.失败，3.冻结
        if trade_status == 'SUCCESS':
            _LOGGER.info('anfupay query order success, user_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)
            async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('anfupay data error, status_code: %s', response.status_code)
