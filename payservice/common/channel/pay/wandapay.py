# -*- coding: utf-8 -*-
import hashlib
import json
import random
import string
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.cache import redis_cache
from common.channel.pay import check_channel_order
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '2792': {  # 万达支付 支付宝红包 2.7% 200-10000 dwc
        'API_KEY': '502c50c6b424ef73ab8d4b631edbbbe6',
        'gateway': 'https://pay.xmwlhd.com/pay',
        'query_gateway': 'https://pay.xmwlhd.com/api/getStatusByOrderId',
    },
    '2793': {  # 万达支付 支付宝红包 2.7% 200-10000 witch
        'API_KEY': 'edfd0394aafb6abd37f35c07fa05443a',
        'gateway': 'https://pay.xmwlhd.com/pay',
        'query_gateway': 'https://pay.xmwlhd.com/api/getStatusByOrderId',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _gen_sign(s):
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest()
    return sign


def generate_sign(d, key):
    s = d['goodsname'] + '+' + d['istype'] + '+' + d['notify_url'] + '+' + d['orderid'] + '+' + d['orderuid'] + '+' + d[
        'price'] + '+' + d[
            'return_url'] + '+' + key + '+' + d['uid']
    return _gen_sign(s)


def generate_notify_sign(d, key):
    s = d['orderid'] + '+' + d['orderuid'] + '+' + d['platform_trade_no'] + '+' + d['price'] + '+' + d[
        'realprice'] + '+' + key
    return _gen_sign(s)


def generate_query_sign(d, key):
    s = d['uid'] + '+' + d['orderid'] + '+' + d['r'] + '+' + key
    return _gen_sign(s)


def verify_notify_sign(params, key):
    sign = params['key']
    params.pop('key')
    calculated_sign = generate_notify_sign(params, key)
    if sign.upper() != calculated_sign.upper():
        _LOGGER.info("wandapay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


# 必填。1：支付宝；2：微信支付
def _get_pay_type(service):
    if service == 'wxpay':
        return '2'
    elif service == 'alipay':
        return '1'
    return '1'


def _build_form(params, gateway):
    html = u"<head><title>loading...</title></head><form id='submit' name='submit' action='" + gateway + "' method='post'>"
    for k, v in params.items():
        html += "<input type='hidden' name='%s' value='%s'/>" % (k, v)
    html += "</form>"
    html += "<script>doc" \
            "ument.forms['submit'].submit();</script>"
    return html


def create_charge(pay, pay_amount, info):
    service = info.get('service')
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('uid', app_id),
        ('price', '%.2f' % pay_amount),
        ('istype', _get_pay_type(service)),
        ('notify_url', '{}/pay/api/{}/wandapay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('return_url', '{}/pay/api/{}/wandapay/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, str(pay.id))),
        ('orderid', str(pay.id)),
        ('orderuid', str(pay.user_id)),
        ('goodsname', 'charge'),
    ))
    parameter_dict['key'] = generate_sign(parameter_dict, api_key)
    html_text = _build_form(parameter_dict, _get_gateway(app_id))
    cache_id = redis_cache.save_html(pay.id, html_text)
    url = settings.PAY_CACHE_URL + cache_id
    _LOGGER.info("wandapay create: %s, order_id is:%s, %s", json.dumps(parameter_dict), parameter_dict['orderid'], url)
    return {'charge_info': url}


# success
def check_notify_sign(request, app_id):
    api_key = _get_api_key(app_id)
    data = dict(request.POST.iteritems())
    _LOGGER.info("wandapay notify data: %s, order_id is: %s", data, data['orderid'])
    verify_notify_sign(data, api_key)
    pay_id = data['orderid']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s", data)
        raise ParamError('wandapay event does not contain pay ID')

    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = ''
    trade_no = data['platform_trade_no']
    total_fee = float(data['price'])  # realprice
    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    check_channel_order(pay_id, total_fee, app_id)
    # 根据我们的付款成功回调通知是否送到，来判断交易是否成功。
    if trade_no:
        _LOGGER.info('wandapay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('uid', app_id),
        ('orderid', str(pay_order.id)),
        ('r', ''.join(random.sample(string.ascii_letters + string.digits, 8))),
    ))
    parameter_dict['key'] = generate_query_sign(parameter_dict, api_key)
    _LOGGER.info('wandapay query data, %s', json.dumps(parameter_dict))
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_query_gateway(app_id), data=parameter_dict, headers=headers, timeout=3, verify=False)
    _LOGGER.info('wandapay query rsp, %s', response.text)
    if response.status_code == 200:
        data = json.loads(response.text)['data']
        mch_id = pay_order.mch_id
        trade_status = str(data['status'])
        total_fee = float(data['realprice'])
        trade_no = pay_order.third_id

        extend = {
            'trade_status': trade_status,
            'total_fee': total_fee,
            'trade_no': trade_no,
        }

        # 订单状态：0-等待支付；1-付款成功；2-已关闭
        if trade_status == '1':
            _LOGGER.info('wandapay query order success, mch_id:%s pay_id:%s' % (mch_id, pay_order.id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)

            async_job.notify_mch(pay_order.id)
