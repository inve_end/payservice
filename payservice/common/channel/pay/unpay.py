# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.cache import redis_cache
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

_GATEWAY = 'http://center.qpay888.com/Bank'
# _GATEWAY = ' http://center.qpay888.com/test2.aspx'
_QUERY_GATEWAY = 'http://center.qpay888.com/search.aspx'

APP_CONF = {
    '2143': {
        'API_KEY': 'e9d1a1c9e78e424b96ebdf0c8b7a5d10',
    },
    '1720': {
        'API_KEY': '96a7df2e6ec54fc89dbeacbca94fe1ad',
    },
    '2155': {  # 万豪 快捷
        'API_KEY': '25249409bcd347008e5a1f02847b7b06',
    },
    '2151': {  # 电玩城  支付宝和银联快捷
        'API_KEY': 'de4eb7debbcd4c0ca8c1ad7683d4584c',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


# tyid=0	可以一键接入所有产品	type=0	可以一键接入所有产品
# tyid=102	网银支付OB/兼容手机	/	每有银行编码需要逐个接入，具体看本页底部，可以直联银行
# tyid=1020	手机快捷网银WAP-H5	type=1005	手机快捷网银，网银WAP支付，纯正H5,只要卡号和手机号密码就可付款。推荐

# tyid=201	QQ钱包－QQH5	type=1009	QQ钱包H5－自动跳转唤醒QQ客户端付款

# tyid=202	京东钱包－JDQR	type=1010	京东钱包JDQR,扫码支付
# tyid=990	微信H5－WCH5	type=1007	微信WCAPPH5－自动跳转唤醒微信客户端付款

# tyid=980	支付宝H5－ALH5	type=1006	支付宝ALAPPh5－自动跳转唤醒支付宝客户端付款
# tyid=99	微信扫码WCQR	type=991	微信二维码, 扫码支付
# tyid=200	微信公众号WCMP	type=1008	微信公众号二维码, 扫码支付
# tyid=98	支付宝扫码ALQR	type=992	支付宝二维码, 扫码支付
# tyid=101	银联扫码UNQR	type=1021	银联二维码, 扫码支付
# tyid=100	QQ钱包扫码QQWallet	type=993	QQ钱包二维码, 扫码支付
# tyid=?	点卡支付CARDPAY	type=？	每个点卡编码需要逐个接入，查询本页底部具体编码，可以直联点卡品牌
def _get_pay_type(service):
    if service == 'alipay':
        return '980', '1006'
    elif service == 'wxpay':
        return '990', '1007'
    elif service == 'qq':
        # return '201', '1009'
        return '100', '993'
    elif service == 'quick':
        return '1020', '1005'
    else:
        return '201', '1009'


def generate_sign(d, key):
    '''  生成下单签名 '''
    s = 'parter={}&type={}&value={}&orderid={}&tyid={}&callbackurl={}{}'.format(
        d['parter'], d['type'], d['value'], d['orderid'], d['tyid'], d['callbackurl'], key
    )
    _LOGGER.info("unpay sign str : %s", s)
    m = hashlib.md5()
    m.update(s.encode('GB2312'))
    sign = m.hexdigest().lower()
    return sign


def generate_notify_sign(d, key):
    '''  生成下单签名 '''
    s = 'orderid={}&opstate={}&ovalue={}{}'.format(
        d['orderid'], d['opstate'], d['ovalue'], key
    )
    _LOGGER.info("unpay sign str : %s", s)
    m = hashlib.md5()
    m.update(s.encode('GB2312'))
    sign = m.hexdigest().lower()
    return sign


def generate_query_sign(d, key):
    '''  生成下单签名 '''
    s = 'orderid={}&parter={}{}'.format(
        d['orderid'], d['parter'], key
    )
    _LOGGER.info("unpay sign str : %s", s)
    m = hashlib.md5()
    m.update(s.encode('GB2312'))
    sign = m.hexdigest().lower()
    return sign


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_notify_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("unpay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _build_form(params):
    html = u"<head><title>loading...</title></head><form id='submit' name='submit' action='" + _GATEWAY + "' method='get'>"
    for k, v in params.items():
        html += "<input type='hidden' name='%s' value='%s'/>" % (k, v)
    html += "</form>"
    html += "<script>doc" \
            "ument.forms['submit'].submit();</script>"
    return html


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def create_charge(pay, pay_amount, info):
    ''' 创建订单 '''
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    service = info.get('service')
    tyid, type = _get_pay_type(service)
    parameter_dict = OrderedDict((
        ('parter', app_id),
        ('type', type),
        ('value', '%.2f' % pay_amount),
        ('orderid', str(pay.id)),
        ('tyid', tyid),
        ('callbackurl', '{}/pay/api/{}/unpay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('hrefbackurl', '{}/pay/api/{}/unpay/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, app_id)),
        ('payerIp', _get_device_ip(info)),
        ('agent', ''),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("unpay create  data: %s, url: %s", parameter_dict, _GATEWAY)
    html_text = _build_form(parameter_dict)
    cache_id = redis_cache.save_html(pay.id, html_text)
    return {'charge_info': settings.PAY_CACHE_URL + cache_id}


# opstate=0
def check_notify_sign(request, app_id):
    data = dict(request.GET.iteritems())
    api_key = _get_api_key(app_id)
    _LOGGER.info("unpay notify data1: %s", data)
    verify_notify_sign(data, api_key)
    pay_id = data['orderid']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('unpay event does not contain pay ID')

    pay = order_db.get_pay(pay_id)
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(data['opstate'])
    trade_no = data['sysorderid']
    total_fee = float(data['ovalue'])

    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    from common.channel.pay import check_channel_order
    check_channel_order(pay_id, total_fee, app_id)

    # 0：处理成功。 -1：请求参数无效。 -2：签名错误 。只有opstate = 0才说明付款成功。其他状态无论什么内容，都不是成功
    if trade_status == '0':
        _LOGGER.info('unpay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    # prepare param
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('parter', app_id),
        ('orderid', str(pay_id)),
    ))
    parameter_dict['sign'] = generate_query_sign(parameter_dict, api_key)
    _LOGGER.info("unpay query  data: %s", parameter_dict)
    response = requests.get(_QUERY_GATEWAY, data=parameter_dict, timeout=3)
    _LOGGER.info("unpay query  rsp data: %s", response.text)
    if response.status_code == 200:
        data = json.loads(response.text)
        trade_status = str(data['opstate'])
        trade_no = ''
        total_fee = float(data['ovalue'])

        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }

        if trade_status == '0':
            _LOGGER.info('unpay query order success, user_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)
            async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('unpay data error, status_code: %s', response.status_code)
