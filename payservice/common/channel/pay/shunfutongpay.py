# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.channel.pay import check_channel_order
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '10103': {  # 支付宝2.6%（10--3000）D0结算 zs
        'API_KEY': 'ne7zi26QSB0Wb8TYFeqQKRWXKZtx95k2',
        'gateway': 'http://pay.u3vpat.cn/service/saveOrder',
        'query_gateway': 'http://pay.u3vpat.cn/service/QueryOrder',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _get_pay_type(service):
    if service == 'alipay':
        return '1001'


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def generate_sign(parameter, key):
    s = ''
    for k in sorted(parameter.keys()):
        s += '%s=%s&' % (k, parameter[k])
    s = s + 'key=' + str(key)
    _LOGGER.info('shunfutongpay sign : %s', s)
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest()
    return sign


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("shunfutongpay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def create_charge(pay, pay_amount, info):
    """ 创建订单 """
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    service = info.get('service')
    parameter_dict = OrderedDict((
        ('merid', app_id),
        ('trade_no', str(pay.id)),
        ('cip', _get_device_ip(info)),
        ('money', str(int(pay_amount * 100))),
        ('subject', 'charge'),
        ('type', _get_pay_type(service)),
        ('notify_url', '{}/pay/api/{}/shunfutongpay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('return_url',
         '{}/pay/api/{}/shunfutongpay/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, str(pay.id)))
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("shunfutongpay create  data: %s", json.dumps(parameter_dict))
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_gateway(app_id), data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info("shunfutongpay create  response: %s", response.text)
    return {'charge_info': json.loads(response.text)['data']}


# SUCCESS
def check_notify_sign(request, app_id):
    resp = json.loads(request.body)
    api_key = _get_api_key(app_id)
    _LOGGER.info("shunfutongpay notify data: %s", resp)
    verify_notify_sign(resp['data'], api_key)
    pay_id = resp['data']['trade_no']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % resp)
        raise ParamError('shunfutongpay event does not contain pay ID')

    pay = order_db.get_pay(pay_id)
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('shunfutongpay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(resp['status'])
    trade_no = ''
    total_fee = float(resp['data']['money']) / 100.0

    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    check_channel_order(pay_id, total_fee, app_id)

    if trade_status == '10000' and int(total_fee) > 0:
        _LOGGER.info('shunfutongpay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    """ 查询订单 """
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('merid', app_id),
        ('trade_no', str(pay_id)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("shunfutongpay query  data: %s", parameter_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_query_gateway(app_id), data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info("shunfutongpay query  rsp data: %s", response.text)
    if response.status_code == 200:
        resp = json.loads(response.text)
        trade_status = str(resp['status'])
        trade_no = ''
        total_fee = float(resp['data']['money']) / 100.0

        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }

        if trade_status == '10000':
            check_channel_order(pay_id, total_fee, app_id)

            _LOGGER.info('shunfutongpay query order success, user_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)
            async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('shunfutongpay data error, status_code: %s', response.status_code)
