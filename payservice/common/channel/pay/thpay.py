# -*- coding: utf-8 -*-
import hashlib
import math
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

_GATEWAY = 'http://api.tonghepay.com/chargebank.aspx?parter={}&type={}&value={}&orderid={}&callbackurl={}&refbackurl={}&sign={}'
_QUERY_GATEWAY = 'http://api.tonghepay.com/search.aspx?orderid={}&parter={}&sign={}'

APP_CONF = {
    '7330': {
        'API_KEY': '05751b18a0f64ad28c0e4e6d013f65f2'
    },
    '7333': {
        'API_KEY': 'f8ad820940ee41daa04d57df7e4cb3e8'
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def generate_sign(parameter, key):
    '''  生成下单签名 '''
    s = 'parter={}&type={}&value={}&orderid={}&callbackurl={}{}'.format(
        parameter['parter'], parameter['type'], parameter['value'],
        parameter['orderid'], parameter['callbackurl'], key)
    m = hashlib.md5()
    m.update(s.encode('gb2312'))
    sign = m.hexdigest()
    _LOGGER.info(u'thpay origin string: %s, sign:%s', s, sign)
    return sign


def generate_query_sign(parameter, key):
    '''  生成查询签名 '''
    s = 'orderid={}&parter={}{}'.format(
        parameter['orderid'], parameter['parter'], key)
    m = hashlib.md5()
    m.update(s.encode('gb2312'))
    sign = m.hexdigest()
    _LOGGER.info(u'thpay origin string: %s, sign:%s', s, sign)
    return sign


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    s = 'orderid={}&opstate={}&ovalue={}{}'.format(params['orderid'], params['opstate'],
                                                   params['ovalue'], key)
    m = hashlib.md5()
    m.update(s.encode('gb2312'))
    calculated_sign = m.hexdigest()
    if sign != calculated_sign:
        _LOGGER.info("thpay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def create_charge(pay, pay_amount, info):
    ''' 创建订单 '''
    charge_resp = {}
    service = info['service']  # wechat or alipay
    pay_type = '2098' if service == 'alipay' else '2099'
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    p_dict = OrderedDict((
        ('parter', app_id),
        ('type', pay_type),
        ('value', '%.2f' % math.ceil(pay_amount)),
        ('orderid', str(pay.id)),
        ('callbackurl', '{}/pay/api/{}/thpay/{}'.format(
            settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('hrefbackurl', '{}/pay/api/{}/thpay/{}/'.format(
            settings.NOTIFY_PREFIX, settings.RETURN_PATH, pay.id)),
    ))
    p_dict['sign'] = generate_sign(p_dict, api_key)
    gate_url = _GATEWAY.format(p_dict['parter'], p_dict['type'], p_dict['value'],
                               p_dict['orderid'], p_dict['callbackurl'], p_dict['hrefbackurl'], p_dict['sign'])
    _LOGGER.info('thpay brefore data: %s', gate_url)
    charge_resp.update({
        'charge_info': gate_url,
    })
    return charge_resp


def check_notify_sign(request, app_id):
    data = dict(request.GET.iteritems())
    api_key = _get_api_key(app_id)
    _LOGGER.info("thpay notify data: %s", data)
    verify_notify_sign(data, api_key)
    pay_id = data['orderid']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('thpay event does not contain pay ID')

    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = data['opstate']
    trade_no = data.get('sysorderid')
    total_fee = float(data['ovalue'])

    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    from common.channel.pay import check_channel_order
    check_channel_order(pay_id, total_fee, app_id)

    if trade_status == '0':
        _LOGGER.info('thpay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    api_key = _get_api_key(app_id)
    p_dict = OrderedDict((
        ('orderid', pay_order.id),
        ('parter', app_id),
    ))
    p_dict['sign'] = generate_query_sign(p_dict, api_key)
    gate_url = _QUERY_GATEWAY.format(p_dict['orderid'], p_dict['parter'], p_dict['sign'])
    response = requests.get(gate_url)
    if response.status_code == 200:
        # response text is a pure html text
        _LOGGER.info('thpay query, %s', response.text)
        data = dict()
        for entry in response.text.split('&'):
            k, v = entry.split('=')
            data[k] = v
        verify_notify_sign(data, api_key)
        mch_id = pay_order.mch_id
        trade_status = data['opstate']
        total_fee = float(data['ovalue'])
        trade_no = pay_order.id

        extend = {
            'trade_status': trade_status,
            'total_fee': total_fee
        }

        if trade_status == '0' and total_fee > 0:
            _LOGGER.info('thpay query order success, mch_id:%s pay_id:%s' % (mch_id, pay_order.id))
            res = order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                           total_fee, trade_no, extend)
            if res:
                # async notify
                async_job.notify_mch(pay_order.id)
