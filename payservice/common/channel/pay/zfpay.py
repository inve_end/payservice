# -*- coding: utf-8 -*-
import hashlib
import json
import random
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    'B1005': {  # dwc
        'API_KEY': 'FMLKO2RXUVIB2QT2CJW9R7Z5U',
        'gateway': 'http://pay.cypkm.com/ZFPayApi.aspx',
        'query_gateway': 'http://pay.cypkm.com/OrderSelectInterface.aspx',
        'qq': '1755248464'
    },
    'B1010': {
        'API_KEY': '1WLQ8NM4XAYC6O25AAULSIFN1',
        'gateway': 'http://pay.cypkm.com/ZFPayApi.aspx',
        'query_gateway': 'http://pay.cypkm.com/OrderSelectInterface.aspx',
        'qq': ''
    },
    'B1003': {  # witch
        'API_KEY': 'CACCH2QAGXUFYHK58PJNPBLD0',
        'gateway': 'http://pay.cypkm.com/ZFPayApi.aspx',
        'query_gateway': 'http://pay.cypkm.com/OrderSelectInterface.aspx',
        'qq': '3290936612'
    },
    'B1009': {
        'API_KEY': 'DHA1GQ6AVLQR16MP16DIBI1TX',
        'gateway': 'http://pay.cypkm.com/ZFPayApi.aspx',
        'query_gateway': 'http://pay.cypkm.com/OrderSelectInterface.aspx',
        'qq': ''
    },
    'B1001': {
        'API_KEY': 'L8M52X2F96Y02R7UJ58O8VHCC',
        'gateway': 'http://pay.80332.net/ZFPayApi.aspx',
        'query_gateway': 'http://pay.80332.net/OrderSelectInterface.aspx',
        'qq': ''
    },
    'D0107': {  # witch
        'API_KEY': '9ATI46jF3ycVOuLUZrKr9VMyn',
        'gateway': 'http://pay.cypkm.com/ZFPayApi.aspx',
        'query_gateway': 'http://pay.cypkm.com/OrderSelectInterface.aspx',
        'qq': ''
    },
    'D0106': {  # dwc 替换 B1005
        'API_KEY': 'I9pGWEADeDexdN6AFgTekGpjl',
        'gateway': 'http://pay.80332.net/ZFPayApi.aspx',
        'query_gateway': 'http://pay.80332.net/OrderSelectInterface.aspx',
        'qq': '1755248464'
    },
    'B1037': {  # loki
        'API_KEY': 'Mc8cIKTXGpfMoqoV2RVuS8I8e',
        'gateway': 'http://pay.cypkm.com/ZFPayApi.aspx',
        'query_gateway': 'http://pay.cypkm.com/OrderSelectInterface.aspx',
        'qq': ''
    },
    'B1038': {  # loki
        'API_KEY': '1RpWEiRPtQZEVuXM1RITQiyNW',
        'gateway': 'http://pay.cypkm.com/ZFPayApi.aspx',
        'query_gateway': 'http://pay.cypkm.com/OrderSelectInterface.aspx',
        'qq': '1755248464'
    },
    'D0154': {  # loki 支付宝3.5%（10---3000）D0结算
        'API_KEY': 'N4KabIYpMKkVfSSyDA7X9PWZ8',
        'gateway': 'http://pay.cypkm.com/ZFPayApi.aspx',
        'query_gateway': 'http://pay.cypkm.com/OrderSelectInterface.aspx',
        'qq': '1755248464'
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_connection_qq(mch_id):
    return APP_CONF[mch_id]['qq']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def generate_sign(parameter, key):
    s = ''
    for k in sorted(parameter.keys()):
        s += '%s=%s&' % (k, parameter[k])
    s += 'key=%s' % key
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().upper()
    return sign


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("zfpay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _fix_pay_amount(pay, pay_amount):
    extend = json.loads(pay.extend or '{}')
    if pay_amount >= 10 and int(pay_amount) == pay_amount:
        discount = random.randint(1, 10)
        pay_amount = pay_amount - float(discount) / 100
        extend.update({'discount': str(float(discount) / 100)})
        order_db.fill_extend(pay.id, extend)
    return pay_amount


# 微信扫码：pay_weixin_scan
# 支付宝扫码：pay_alipay_scan
# qq扫码：pay_qqpay_scan
# 微信wap：pay_weixin_wap
# 支付宝wap：pay_alipay_wap
# qqWAP：pay_qqpay_wap
# 网银：pay_note_wap
# 银联快捷：pay_ylpay_wap
# 银联扫码：pay_ylpay_scan
def _get_pay_type(service):
    if service == 'wechat':
        pay_type = 'pay_weixin_wap'
    elif service == 'qq':
        pay_type = 'pay_qqpay_wap'
    elif service == 'quick':
        pay_type = 'pay_ylpay_wap'
    else:
        pay_type = 'pay_alipay_wap'
    return pay_type


def create_charge(pay, pay_amount, info):
    ''' 创建订单 '''
    charge_resp = {}
    service = info['service']  # wechat or alipay
    app_id = info['app_id']
    api_key = _get_api_key(app_id)

    # pay_amount = _fix_pay_amount(pay, pay_amount)
    parameter_dict = OrderedDict((
        ('subject', u'QQ:%s' % _get_connection_qq(app_id)),
        ('total_fee', int(pay_amount * 100)),
        ('pay_type', _get_pay_type(service)),
        ('mchNo', app_id),
        ('body', 'charge'),
        ('version', '2.0'),
        ('showurl', '{}/pay/api/{}/zfpay/{}/'.format(
            settings.NOTIFY_PREFIX, settings.RETURN_PATH, pay.id)),
        ('mchorderid', str(pay.id)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)

    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    parameter_dict['client_ip'] = user_info.get('device_ip') or '127.0.0.1'
    parameter_dict['callback_url'] = '{}/pay/api/{}/zfpay/'.format(
        settings.NOTIFY_PREFIX, settings.NOTIFY_PATH)
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    json_data = json.dumps(parameter_dict, separators=(',', ':'))
    _LOGGER.info('zfpay create charge params: %s', json_data)
    response = requests.post(_get_gateway(app_id), data=json_data, headers=headers, timeout=15)
    _LOGGER.info('zfpay create charge rsp: %s, %s', response.text, response.url)
    if response.status_code == 200:
        resp = json.loads(response.text)
        pay_url = resp.get('code_url') or resp.get('code_img_url')
        charge_resp.update({
            'charge_info': pay_url,
        })
    else:
        _LOGGER.warn('zfpay data error, status_code: %s', response.status_code)
    return charge_resp


def check_notify_sign(request):
    data = dict(request.GET.iteritems())
    _LOGGER.info("zfpay notify data: %s", data)
    app_id = data['mchno']
    api_key = _get_api_key(app_id)
    verify_notify_sign(data, api_key)
    pay_id = data['mchorderid']
    if not pay_id:
        _LOGGER.error("fatal error, zfpay out_trade_no not exists, data: %s" % data)
        raise ParamError('zfpay event does not contain pay ID')

    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_no = data['pdorderid']
    total_fee = float(data['total_fee']) / 100

    if total_fee != float(pay.total_fee):
        _LOGGER.warn('pay %s price invalid %s != %s', pay_id, total_fee, pay.total_fee)

    discount = float(json.loads(pay.extend or '{}').get('discount', 0))
    if discount:
        assert (total_fee / discount) >= 20
    extend = {
        'discount': discount,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    from common.channel.pay import check_channel_order
    check_channel_order(pay_id, total_fee, app_id)

    _LOGGER.info('zfpay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
    res = order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
    # async notify
    async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    # prepare param
    pay_id = pay_order.id
    charge_resp = {}
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('mchNo', app_id),
        ('mchorderid', str(pay_id)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    response = requests.post(_get_query_gateway(app_id), data=json.dumps(parameter_dict, separators=(',', ':')),
                             headers=headers,
                             timeout=3)
    if response.status_code == 200:
        # response text is a pure html text
        data = json.loads(response.text)
        print data
        # verify_notify_sign(data, api_key)
        trade_no = 0
        trade_status = int(data['status'])
        total_fee = float(data['total_fee']) / 100

        discount = float(json.loads(pay_order.extend or '{}').get('discount', 0))
        if discount:
            assert (total_fee / discount) >= 20
        extend = {
            'discount': discount,
            'trade_status': trade_status,
            'total_fee': total_fee
        }

        if trade_status == 1:
            _LOGGER.info('zfpay query order success, mch_id:%s pay_id:%s',
                         pay_order.mch_id, pay_order.id)
            res = order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                           total_fee, trade_no, extend)
            if res:
                # async notify
                async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('zfpay data error, status_code: %s', response.status_code)
    return charge_resp
