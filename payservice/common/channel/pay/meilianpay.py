# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.channel.pay import check_channel_order
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '100000108': {  # 微信 2.9%（1-3000) dwc
        'API_KEY': 'efc0487257f383a88ec6c6e2b4c37c7e',
        'gateway': 'http://pay.buyustar.com/pay/api/api.php',
        'query_gateway': 'http://pay.buyustar.com/pay/api/query.php',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _get_pay_type(service):
    if service == 'wxpay':
        return 'wxwap'


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def _build_query_string(params):
    s = ''
    for k in params.keys():
        s += '%s=%s&' % (k, params[k])
    return s[:-1]


def generate_sign(parameter, key):
    s = ''
    for k in sorted(parameter.keys()):
        s += '%s=%s&' % (k, parameter[k])
    s = s + 'key=' + str(key)
    _LOGGER.info('meilianpay sign : %s', s)
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().lower()
    return sign


def generate_notify_sign(parameter, key):
    parameter.pop('respMsg')
    s = ''
    for k in sorted(parameter.keys()):
        s += '%s=%s&' % (k, parameter[k])
    s = s + 'key=' + str(key)
    _LOGGER.info('meilianpay sign : %s', s)
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().lower()
    return sign


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_notify_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("meilianpay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def create_charge(pay, pay_amount, info):
    """ 创建订单 """
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    service = info.get('service')
    parameter_dict = OrderedDict((
        ('channel', app_id),
        ('callback', '{}/pay/api/{}/meilianpay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('orderid', str(pay.id)),
        ('txnAmt', str(int(pay_amount))),
        ('paytype', _get_pay_type(service)),
        ('ip', _get_device_ip(info)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("meilianpay create  data: %s", json.dumps(parameter_dict))
    url = _get_gateway(app_id) + '?' + _build_query_string(parameter_dict)
    _LOGGER.info(u'meilianpay query: %s', url)
    response = requests.get(url, timeout=3)
    _LOGGER.info("meilianpay create  response: %s", response.text)
    return {'charge_info': json.loads(response.text)['codeUrl']}


# SUCCESS
def check_notify_sign(request, app_id):
    data = dict(request.POST.iteritems())
    api_key = _get_api_key(app_id)
    _LOGGER.info("meilianpay notify data: %s", data)
    verify_notify_sign(data, api_key)
    pay_id = data['merOrderId']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('meilianpay event does not contain pay ID')

    pay = order_db.get_pay(pay_id)
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('meilianpay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(data['respCode'])
    trade_no = str(data['orgMerOrderId'])
    total_fee = float(data['txnAmt']) / 100.0  # 回调已分为单位

    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    check_channel_order(pay_id, total_fee, app_id)

    if trade_status in ['0000', '1001'] and int(total_fee) > 0:
        _LOGGER.info('meilianpay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    """ 查询订单 """
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('channel', app_id),
        ('orderid', str(pay_id)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("meilianpay query  data: %s", parameter_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_query_gateway(app_id), data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info("meilianpay query  rsp data: %s", response.text)
    if response.status_code == 200:
        data = json.loads(response.text)
        trade_status = str(data['resultCode'])
        trade_no = ''
        total_fee = float(data['txnAmt'])

        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }

        if trade_status == '0000':
            check_channel_order(pay_id, total_fee, app_id)

            _LOGGER.info('meilianpay query order success, user_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)
            async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('meilianpay data error, status_code: %s', response.status_code)
