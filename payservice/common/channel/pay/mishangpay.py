# -*- coding: utf-8 -*-
import hashlib
import hmac
import json
import time
import urllib
import uuid
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    'dc9a8b39-36a4-458a-9b96-171396abbe7d': {  # dwc 支付宝 定额
        'API_KEY': 'fb3434967ff48565ab066b007c2412b9',
        'gateway': 'http://zohosalesiq.info/api/v2/payment?amount={}&channel={}&data={}&methods={}&request={}&return={}&time={}&signature={}&user={}',
        'query_gateway': 'https://zohosalesiq.info/api/v2/payment/{}',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _gen_sha256_sign(key, s):
    return hmac.new(key, s.encode('utf8'), hashlib.sha256).hexdigest()


def generate_sign(parameter, key):
    s = ''
    for k in sorted(parameter.keys()):
        if k != 'payee':
            s += k + '=' + str(parameter[k])
        else:
            s += k + '=' + parameter[k]
    return _gen_sha256_sign(key, s)


def verify_notify_sign(params, key):
    sign = params.pop('signature')
    calculated_sign = generate_sign(params, key)
    if sign.upper() != calculated_sign.upper():
        _LOGGER.info("mishangpay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


# 商户或客户可接受的支付方式。目前支持的支付方式和取值为
# 银行转账 = 1、支付宝 = 2、微信 = 4。
# 商户仅可指定一种可以接受的支付方式，不可为同一个订单同时指定多种支付方式。
def _get_pay_type(service):
    if service == 'wxpay':
        payType = '4'
    elif service == 'alipay':
        payType = '2'
    else:
        payType = '1'
    return payType


def create_charge(pay, pay_amount, info):
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    service = info.get('service')
    request_uuid = uuid.uuid1()
    parameter_dict = OrderedDict((
        ('amount', str(int(pay_amount))),
        ('channel', app_id),
        ('data', str(pay.id)),
        ('methods', _get_pay_type(service)),
        ('request', request_uuid),
        ('return', '{}/pay/api/{}/mishangpay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('time', str(int(time.time()))),
        # ('user', str(pay.user_id)),
    ))
    parameter_dict['signature'] = generate_sign(parameter_dict, api_key)
    parameter_dict['return'] = urllib.quote(parameter_dict['return'])
    parameter_dict['user'] = str(pay.user_id)
    url = _get_gateway(app_id).format(parameter_dict['amount'], parameter_dict['channel'], parameter_dict['data'],
                                      parameter_dict['methods'], parameter_dict['request'], parameter_dict['return'],
                                      parameter_dict['time'], parameter_dict['signature'], parameter_dict['user'])
    _LOGGER.info("mishangpay create: url: %s", url)
    response = requests.get(url)
    _LOGGER.info("mishangpay create: response: %s", response.text)
    j = json.loads(response.text)
    order_db.fill_third_id(pay.id, j['id'])
    return {'charge_info': j['extra'].replace('HTTPS://QR.ALIPAY.COM/', 'https://qr.alipay.com/')}


# 已经测试成功 无法查询
def create_web_charge(pay, pay_amount, info):
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    service = info.get('service')
    request_uuid = uuid.uuid1()
    parameter_dict = OrderedDict((
        ('amount', str(int(pay_amount))),
        ('channel', app_id),
        ('data', str(pay.id)),
        ('methods', _get_pay_type(service)),
        ('request', request_uuid),
        ('return', '{}/pay/api/{}/mishangpay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('time', str(int(time.time()))),
    ))
    parameter_dict['signature'] = generate_sign(parameter_dict, api_key)
    parameter_dict['return'] = urllib.quote(parameter_dict['return'])
    url = _get_gateway(app_id).format(parameter_dict['amount'], parameter_dict['channel'], parameter_dict['data'],
                                      parameter_dict['methods'], parameter_dict['request'], parameter_dict['return'],
                                      parameter_dict['time'], parameter_dict['signature'])
    _LOGGER.info("mishangpay create: url: %s", url)
    return {'charge_info': url}


# OK
def check_notify_sign(request, app_id):
    data = dict(request.POST.iteritems())
    api_key = _get_api_key(app_id)
    _LOGGER.info("mishangpay notify data: %s", data)
    verify_notify_sign(data, api_key)
    pay_id = data['data']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('mishangpay event does not contain pay ID')

    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(data['status'])
    trade_no = data['id']
    total_fee = float(data['amount'])
    discount = float(json.loads(pay.extend or '{}').get('discount', 0))
    extend = {
        'discount': discount,
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    from common.channel.pay import check_channel_order
    check_channel_order(pay_id, total_fee, app_id)
    if trade_status == '4':
        _LOGGER.info('mishangpay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    pay_id = pay_order.id
    url = _get_query_gateway(app_id).format(pay_order.third_id)
    _LOGGER.info(u'mishangpay query: %s', url)
    response = requests.get(url, verify=False)
    _LOGGER.info(u'mishangpay query rsp: %s', response.text)
    data = json.loads(response.text)
    if response.status_code == 200:
        trade_status = str(data['status'])  # 0 订单未成功 1订单成功
        total_fee = float(data['amount'])
        trade_no = data['id']
        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }

        if trade_status == '4':
            _LOGGER.info('mishangpay query order success, mch_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            order_db.add_pay_success(pay_order.mch_id, pay_id, total_fee, trade_no, extend)
            async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('mishangpay data error, status_code: %s', response.status_code)


# 9,19,29,49,69,99,149,199,299,499,60,100,150,300,500,1000,1500,2000,2500,3000。"]}
if __name__ == '__main__':
    d = {u'status': u'4', u'account': u'15617929572',
         u'return': u'http://219.135.56.195:9003/pay/api/third/notify/mishangpay/dc9a8b39-36a4-458a-9b96-171396abbe7d',
         u'extra': u'HTTPS://QR.ALIPAY.COM/FKX07592YYWRB1DBH4H360?t=1528477345894', u'amount': u'9',
         u'signature': u'd9eb6d73519afa644a10a666ae3b6537e804e1a6c2d8a22d0535dd1c882b20be',
         u'request': u'bae1b6e6-84ed-11e8-b725-50e549e0237f', u'updated': u'1531301788', u'expiry': u'300',
         u'payee': u'\u9f50\u540c\u9601', u'extra2': u'', u'time': u'1531301763',
         u'id': u'0a1de059-fa81-4ede-8be1-a89aee7390a5', u'data': u'1644222703164456960', u'method': u'2'}
    sign = d.pop('signature')
    calculated_sign = generate_sign(d, 'fb3434967ff48565ab066b007c2412b9')
    if calculated_sign == sign:
        print('ok')
    else:
        print('fail')
