# -*- coding: utf-8 -*-
import hashlib
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.cache import redis_cache
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '10058': {  # dwc alipay
        'API_KEY': 'A8881EBE9AB37B2DA2F23BC73D4E036C',
        'gateway': 'http://pay.brpay.cn/interface/chargebank.aspx',
        'query_gateway': 'http://pay.brpay.cn/pay/Query.aspx',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


# 支付宝扫码	ALIPAY
# 财付通余额	TENPAY
# 微信扫码支付	WEIXIN
# 微信H5支付	WXWAP
# 微信公众号支付	WXAPP
# 支付宝H5支付	ALIWAP
# QQ钱包扫码支付	QQCODE
# QQ钱包H5支付	QQWAP
# QQ钱包公众号支付	QQAPP
# 京东扫码支付	JINGDONG
# 京东H5支付	JDWAP
# 银联扫码支付	VISA
def _get_pay_type(service):
    if service == 'alipay':
        return 'ALIWAP'
    elif service == 'wxpay':
        return 'WXWAP'
    elif service == 'qq':
        return 'QQWAP'
    elif service == 'quick':
        return 'VISA'
    elif service == 'jd':
        return 'JDWAP'
    else:
        return 'ALIWAP'


def generate_sign(d, key):
    '''  生成下单签名 '''
    s = 'parter={}&type={}&orderid={}&callbackurl={}{}'.format(
        d['parter'], d['type'], d['orderid'], d['callbackurl'], key
    )
    _LOGGER.info("ruibaopay sign str : %s", s)
    m = hashlib.md5()
    m.update(s.encode('GB2312'))
    sign = m.hexdigest().lower()
    return sign


def generate_notify_sign(d, key):
    '''  生成下单签名 '''
    s = 'orderid={}&restate={}&ovalue={}{}'.format(
        d['orderid'], d['restate'], d['ovalue'], key
    )
    _LOGGER.info("ruibaopay sign str : %s", s)
    m = hashlib.md5()
    m.update(s.encode('GB2312'))
    sign = m.hexdigest().lower()
    return sign


def generate_query_sign(d, key):
    '''  生成下单签名 '''
    s = 'parter={}&type={}&orderid={}&key={}'.format(
        d['parter'], d['type'], d['orderid'], key
    )
    _LOGGER.info("ruibaopay query sign str : %s", s)
    m = hashlib.md5()
    m.update(s.encode('GB2312'))
    sign = m.hexdigest().lower()
    return sign


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_notify_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("ruibaopay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _build_form(params, gateway):
    html = u"<head><title>loading...</title></head><form id='submit' name='submit' action='" + gateway + "' method='get'>"
    for k, v in params.items():
        html += "<input type='hidden' name='%s' value='%s'/>" % (k, v)
    html += "</form>"
    html += "<script>doc" \
            "ument.forms['submit'].submit();</script>"
    return html


def create_charge(pay, pay_amount, info):
    ''' 创建订单 '''
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    service = info.get('service')
    parameter_dict = OrderedDict((
        ('orderid', str(pay.id)),
        ('value', '%.2f' % pay_amount),
        ('parter', app_id),
        ('type', _get_pay_type(service)),
        ('callbackurl', '{}/pay/api/{}/ruibaopay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('hrefbackurl', '{}/pay/api/{}/ruibaopay/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, pay.id)),
        ('attach', 'charge'),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("ruibaopay create  data: %s, url: %s", parameter_dict, _get_gateway(app_id))
    html_text = _build_form(parameter_dict, _get_gateway(app_id))
    cache_id = redis_cache.save_html(pay.id, html_text)
    return {'charge_info': settings.PAY_CACHE_URL + cache_id}


# ok
def check_notify_sign(request, app_id):
    data = dict(request.GET.iteritems())
    api_key = _get_api_key(app_id)
    _LOGGER.info("ruibaopay notify data1: %s", data)
    verify_notify_sign(data, api_key)
    pay_id = data['orderid']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('ruibaopay event does not contain pay ID')

    pay = order_db.get_pay(pay_id)
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(data['restate'])
    trade_no = ''
    total_fee = float(data['ovalue'])

    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    from common.channel.pay import check_channel_order
    check_channel_order(pay_id, total_fee, app_id)
    # 0：处理成功。
    if trade_status == '0':
        _LOGGER.info('ruibaopay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def _get_str(s, start, end):
    p1 = s.index(start)
    p2 = s.index(end, p1 + len(start))
    if p2 > p1:
        return s[p1 + len(start): p2]
    else:
        return ''


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    # prepare param
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('orderid', str(pay_id)),
        ('type', '1'),
        ('parter', app_id),
    ))
    parameter_dict['sign'] = generate_query_sign(parameter_dict, api_key)
    _LOGGER.info("ruibaopay query  data: %s", parameter_dict)
    response = requests.get(_get_query_gateway(app_id), data=parameter_dict, timeout=3)
    _LOGGER.info("ruibaopay query  rsp data: %s", response.text)
    if response.status_code == 200:
        trade_status = _get_str(response.text, 'ordstate=', '&')
        trade_no = _get_str(response.text, 'sysorderid=', '&')
        total_fee = float(_get_str(response.text, 'payamt=', '&'))

        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }

        # 0订单处理中， 1支付成功， 2失败
        if trade_status == '1':
            _LOGGER.info('ruibaopay query order success, user_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)
            async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('ruibaopay data error, status_code: %s', response.status_code)
