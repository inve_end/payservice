# -*- coding: utf-8 -*-
import hashlib
import time
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.cache import redis_cache
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    'mx_qiaoshou': {  # 接Loki的支付宝和QQ钱包，优先级较高，我已拉你进群，群名侨首贸易
        'API_KEY': 'wruuNGTO6A9dehgeiOg7a7FuPVxDLKEC5To7w2WC5OtcoFc6j6UuHhGlqZwqKPc4olrkpMleMjYFmEv57bfmWiRdlnN1d3XIUyXxpGowNwcKa6TaRRk3tapCrAiMwNmm',
        'userid': 'CAT201803161011520006',
        'deskey': 'eZS2NWDur3jtAgAJvD5OBT5f5iwYMLJo',
        'query_gateway': 'http://zs.qilijiakeji.com:18000/GW/gw.inter',
        'ali_gateway': 'http://zs.qilijiakeji.com:18000/GW/PayH5Ali.do',
        'qq_gateway': 'http://zs.qilijiakeji.com:18000/GW/payqq.do',
        'quick_gateway': 'http://zs.qilijiakeji.com:18000/GW/fast.do',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_qq_gateway(mch_id):
    return APP_CONF[mch_id]['qq_gateway']


def _get_ali_gateway(mch_id):
    return APP_CONF[mch_id]['ali_gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _get_userid(mch_id):
    return APP_CONF[mch_id]['userid']


def verify_notify_sign(params, key):
    sign = params['hmac']
    params.pop('hmac')
    calculated_sign = generate_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("qiaoshoupay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def check_notify_sign(request, mer_id):
    key = _get_api_key(mer_id)
    data = dict(request.POST.iteritems())
    _LOGGER.info("qiaoshoupay notify data: %s", data)
    verify_notify_sign(data, key)
    pay_id = data['apporderid']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('qiaoshoupay event does not contain pay ID')

    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(data['status'])
    trade_no = data['tradesno']
    total_fee = float(data['realamount'])

    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee,
    }

    from common.channel.pay import check_channel_order
    check_channel_order(pay_id, total_fee, mer_id)
    if trade_status == '0':  # 状态，0-成功，1-失败，3-处理中
        _LOGGER.info('qiaoshoupay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def _gen_sign(s):
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().lower()
    return sign


def _gen_sign_str(parameter):
    s = ''
    for k in sorted(parameter.keys()):
        s += '%s=%s&' % (k, parameter[k])
    return s[:len(s) - 1]


def generate_sign(parameter, key):
    s = _gen_sign_str(parameter)
    return _gen_sign(s + key)


def _build_form(params, gateway):
    html = u"<head><title>loading...</title></head><form id='submit' name='submit' action='" + gateway + "' method='post'>"
    for k, v in params.items():
        html += "<input type='hidden' name='%s' value='%s'/>" % (k, v)
    html += "</form>"
    html += "<script>document.forms['submit'].submit();</script>"
    return html


def create_charge(pay, pay_amount, info):
    service = info.get('service')
    if service == 'qq':
        url = create_qq1_charge(pay, pay_amount, info)
    else:
        url = create_ali_charge(pay, pay_amount, info)
    return {'charge_info': url}


def create_qq_charge(pay, pay_amount, info):
    ''' 创建订单 '''
    mer_id = info['app_id']
    key = _get_api_key(mer_id)
    parameter_dict = OrderedDict((
        ("cmd", "PAYH5"),
        ("version", "2.0"),
        ("appid", mer_id),
        ("ordertime", time.strftime("%Y%m%d%H%M%S", time.localtime())),
        ("userid", _get_userid(mer_id)),
        ('apporderid', str(pay.id)),
        ('orderbody', 'charge'),
        ("amount", str(pay_amount)),
        ("biztype", 'qq'),
        ('pageyurl', '{}/pay/api/{}/qiaoshoupay/{}/'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, pay.id)),
        ('notifyurl', '{}/pay/api/{}/qiaoshoupay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, mer_id)),
    ))
    s = _gen_sign_str(parameter_dict)
    parameter_dict['hmac'] = _gen_sign(s + key)
    _LOGGER.info("qiaoshoupay create charge data: %s", parameter_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_qq_gateway(mer_id), data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info("qiaoshoupay create rsp data: %s %s", response.status_code, response.text)
    cache_id = redis_cache.save_html(pay.id, response.text)
    return settings.PAY_CACHE_URL + cache_id


def create_qq1_charge(pay, pay_amount, info):
    ''' 创建订单 '''
    mer_id = info['app_id']
    key = _get_api_key(mer_id)
    parameter_dict = OrderedDict((
        ("cmd", "PAYMOBILEQQ"),
        ("version", "2.0"),
        ("appid", mer_id),
        ("ordertime", time.strftime("%Y%m%d%H%M%S", time.localtime())),
        ("userid", _get_userid(mer_id)),
        ('apporderid', str(pay.id)),
        ('tradetype', '1'),
        ('ordertitle', 'charge'),
        ('orderbody', 'charge'),
        ("amount", str(pay_amount)),
        ("deviceinfo", 'AND_WAP'),
        ("mchappid", settings.NOTIFY_PREFIX),
        ("mchappname", settings.NOTIFY_PREFIX),
        ('pageyurl', '{}/pay/api/{}/qiaoshoupay/{}/'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, pay.id)),
        ('notifyurl', '{}/pay/api/{}/qiaoshoupay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, mer_id)),
    ))
    s = _gen_sign_str(parameter_dict)
    parameter_dict['hmac'] = _gen_sign(s + key)
    _LOGGER.info("qiaoshoupay create charge data: %s", parameter_dict)
    cache_id = redis_cache.save_html(pay.id, _build_form(parameter_dict, _get_qq_gateway(mer_id)))
    return settings.PAY_CACHE_URL + cache_id


def create_ali_charge(pay, pay_amount, info):
    ''' 创建订单 '''
    mer_id = info['app_id']
    key = _get_api_key(mer_id)
    parameter_dict = OrderedDict((
        ("cmd", "PAYH5ALIPAY"),
        ("version", "2.0"),
        ("appid", mer_id),
        ("ordertime", time.strftime("%Y%m%d%H%M%S", time.localtime())),
        ("userid", _get_userid(mer_id)),
        ('apporderid', str(pay.id)),
        ('orderbody', 'charge'),
        ('orderdesc', 'charge'),
        ("amount", str(pay_amount)),
        ('notifyurl', '{}/pay/api/{}/qiaoshoupay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, mer_id)),
        (
            'front_skip_url',
            '{}/pay/api/{}/qiaoshoupay/{}/'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, pay.id)),
    ))
    s = _gen_sign_str(parameter_dict)
    parameter_dict['hmac'] = _gen_sign(s + key)
    _LOGGER.info("qiaoshoupay create charge data: %s", parameter_dict)
    cache_id = redis_cache.save_html(pay.id, _build_form(parameter_dict, _get_ali_gateway(mer_id)))
    return settings.PAY_CACHE_URL + cache_id


def _get_str(s, start, end):
    p1 = s.index(start)
    p2 = s.index(end, p1 + len(start))
    if p2 > p1:
        return s[p1 + len(start): p2]
    else:
        return ''


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ("cmd", "QUERYORDERINFO"),
        ("version", "2.0"),
        ("appsno", "1"),
        ("appid", app_id),
        ("userid", _get_userid(app_id)),
        ('apporderid', str(pay_id)),
    ))
    s = _gen_sign_str(parameter_dict)
    parameter_dict['hmac'] = _gen_sign(s + api_key)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_query_gateway(app_id), data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info('qiaoshoupay query response, status_code: %s, %s: ', response.status_code, response.text)
    if response.status_code == 200:

        trade_status = _get_str(response.text, 'errcode=', '&')
        trade_no = ''
        total_fee = float(_get_str(response.text, 'amount=', '&'))

        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }

        # 0:成功1：失败，3：处理中等待通知或查询
        if trade_status == '0':
            _LOGGER.info('qiaoshoupay query order success, pay_id:%s' % pay_id)
            res = order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                           total_fee, trade_no, extend)
            if res:
                # async notify
                async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('qiaoshoupay data error, status_code: %s', response.status_code)
