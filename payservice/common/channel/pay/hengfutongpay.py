# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.channel.pay import check_channel_order
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '8668895': {  # dbl 支付宝 3% 20---4999 D0结算
        'API_KEY': '1465f867a8fa42e0d6459177ec5ffa9b',
        'gateway': 'http://www.jiaquan360.com/api/Payh5/pay_h5/',
        'query_gateway': 'http://www.jiaquan360.com/api/Payh5/order_search/',
    },
    '8228969': {  # zs 支付宝2.7%（20---5000）D0结算
        'API_KEY': 'ac3dddc100e8fb44f99df520b9d1827b',
        'gateway': 'http://www.jiaquan360.com/api/Payh5/pay_h5/',
        'query_gateway': 'http://www.jiaquan360.com/api/Payh5/order_search/',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _gen_sign(s):
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest()
    return sign


# MD5(merchantId+"pay"+totalAmount+corp_flow_no+商户秘钥code)
def generate_sign(d, key):
    s = d['merchantId'] + 'pay' + d['totalAmount'] + d['corp_flow_no'] + key
    _LOGGER.info("hengfutongpay sign str: %s", s)
    return _gen_sign(s)


# MD5(merchantId + "pay" + corp_flow_no + 商户秘钥)
def generate_notify_sign(d, key):
    s = d['merchantId'] + 'pay' + d['corp_flow_no'] + key
    _LOGGER.info("hengfutongpay notify sign str: %s", s)
    return _gen_sign(s)


# MD5(merchantId+corp_flow_no+商户秘钥code)
def generate_query_sign(d, key):
    s = d['merchantId'] + d['corp_flow_no'] + key
    _LOGGER.info("hengfutongpay query sign str: %s", s)
    return _gen_sign(s)


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_notify_sign(params, key)
    if sign.lower() != calculated_sign.lower():
        _LOGGER.info("hengfutongpay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def _get_pay_type(service):
    if service == 'alipay':
        payType = '2'
    elif service == 'wxpay':
        payType = '1'
    return payType


def _build_pay_string(params):
    s = 'merchantId={}&type={}&totalAmount={}&sign={}&corp_flow_no={}&notify_url={}&return_url={}'.format(
        params['merchantId'], params['type'],
        params['totalAmount'], params['sign'],
        params['corp_flow_no'], params['notify_url'],
        params['return_url'])
    return s


def _build_query_string(params):
    s = 'merchantId={}&sign={}&corp_flow_no={}'.format(
        params['merchantId'], params['sign'],
        params['corp_flow_no'])
    return s


def create_charge(pay, pay_amount, info):
    service = info.get('service')
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('merchantId', app_id),
        ('corp_flow_no', str(pay.id)),
        ('totalAmount', str(int(pay_amount))),
        ('notify_url', '{}/pay/api/{}/hengfutongpay/{}/'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('return_url', '{}/pay/api/{}/hengfutongpay/{}/'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, app_id)),
        ('desc', 'charge'),
        ('type', _get_pay_type(service)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    data = json.dumps(parameter_dict)
    _LOGGER.info("hengfutongpay create: %s", parameter_dict)
    url = _get_gateway(app_id) + '?' + _build_pay_string(parameter_dict)
    _LOGGER.info("hengfutongpay create: url: %s, %s", url, data)
    return {'charge_info': url}


# 00
def check_notify_sign(request, app_id):
    api_key = _get_api_key(app_id)
    data = dict(request.POST.iteritems())
    _LOGGER.info("hengfutongpay notify data: %s", data)
    verify_notify_sign(data, api_key)
    pay_id = data['corp_flow_no']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('hengfutongpay event does not contain pay ID')

    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(data['code'])
    trade_no = ''
    total_fee = float(data['totalAmount'])
    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    check_channel_order(pay_id, total_fee, app_id)

    if trade_status == '00':
        _LOGGER.info('hengfutongpay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    """ 查询订单 """
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('merchantId', app_id),
        ('corp_flow_no', str(pay_order.id)),
    ))
    parameter_dict['sign'] = generate_query_sign(parameter_dict, api_key)
    _LOGGER.info('hengfutongpay query data, %s', parameter_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    url = _get_query_gateway(app_id) + '?' + _build_query_string(parameter_dict)
    response = requests.get(url)
    _LOGGER.info('hengfutongpay query rsp, %s', response.text)
    if response.status_code == 200:
        data = json.loads(response.text)
        mch_id = pay_order.mch_id
        trade_status = str(data['Result'])
        total_fee = float(data['totalAmount'])
        trade_no = ''

        extend = {
            'trade_status': trade_status,
            'total_fee': total_fee,
        }

        if trade_status == 'true':
            _LOGGER.info('hengfutongpay query order success, mch_id:%s pay_id:%s' % (mch_id, pay_order.id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)

            async_job.notify_mch(pay_order.id)
