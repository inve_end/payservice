# -*- coding: utf-8 -*-
import hashlib
import json
import time
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.cache import redis_cache
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

_GATEWAY = 'http://zf.huoyunlv.com/apisubmit'
_QUERY_GATEWAY = 'http://zf.huoyunlv.com/apiorderquery'
#  witch
APP_CONF = {
    '11005': {
        'API_KEY': '2b74bf086fbf44d22675a48132214b0daee6f966',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


# 支付宝wap	alipaywap	正常
# 网银	bank	正常
# 微信wap	wxh5	关闭
# qqwap	qqwallet	关闭
# qq	qqrcode	关闭
# WAP收银台	jbywap	关闭
def _get_pay_type(service):
    if service == 'alipay':
        return 'alipaywap'
    elif service == 'wxpay':
        return 'wxh5'
    elif service == 'qq':
        return 'qqwallet'
    elif service == 'quick':
        return 'kuaijie'
    else:
        return 'alipaywap'


def generate_sign(d, key):
    '''  生成下单签名 '''
    s = 'version={}&customerid={}&total_fee={}&sdorderno={}&notifyurl={}&returnurl={}&{}'.format(
        d['version'], d['customerid'], d['total_fee'], d['sdorderno'], d['notifyurl'], d['returnurl'], key
    )
    _LOGGER.info("zfbpay create  sign str: %s", s)
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().lower()
    return sign


def generate_notify_sign(d, key):
    '''  生成下单签名 '''
    s = 'customerid={}&status={}&sdpayno={}&sdorderno={}&total_fee={}&paytype={}&{}'.format(
        d['customerid'], d['status'], d['sdpayno'], d['sdorderno'], d['total_fee'], d['paytype'], key
    )
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().lower()
    return sign


def generate_query_sign(d, key):
    '''  生成下单签名 '''
    s = 'customerid={}&sdorderno={}&reqtime={}&{}'.format(
        d['customerid'], d['sdorderno'], d['reqtime'], key
    )
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().lower()
    return sign


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_notify_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("zfbpay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _build_form(params):
    html = u"<head><title>loading...</title></head><form id='submit' name='submit' action='" + _GATEWAY + "' method='post'>"
    for k, v in params.items():
        html += "<input type='hidden' name='%s' value='%s'/>" % (k, v)
    html += "</form>"
    html += "<script>doc" \
            "ument.forms['submit'].submit();</script>"
    return html


def create_charge(pay, pay_amount, info):
    ''' 创建订单 '''
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    service = info.get('service')
    parameter_dict = OrderedDict((
        ('version', '1.0'),
        ('customerid', app_id),
        ('sdorderno', str(pay.id)),
        ('total_fee', '%.2f' % pay_amount),
        ('paytype', _get_pay_type(service)),
        ('notifyurl', '{}/pay/api/{}/zfbpay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('returnurl', '{}/pay/api/{}/zfbpay/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, pay.id)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("zfbpay create  data: %s", parameter_dict)
    html = _build_form(parameter_dict)
    cache_id = redis_cache.save_html(pay.id, html)
    return {'charge_info': settings.PAY_CACHE_URL + cache_id}


# success
def check_notify_sign(request, app_id):
    data = dict(request.POST.iteritems())
    api_key = _get_api_key(app_id)
    _LOGGER.info("zfbpay notify data: %s", data)
    verify_notify_sign(data, api_key)
    pay_id = data['sdorderno']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('zfbpay event does not contain pay ID')

    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(data['status'])
    trade_no = data['sdpayno']
    total_fee = float(data['total_fee'])

    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    from common.channel.pay import check_channel_order
    check_channel_order(pay_id, total_fee, app_id)

    # 1成功0失败
    if trade_status == '1':
        _LOGGER.info('zfbpay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    # prepare param
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('customerid', app_id),
        ('sdorderno', str(pay_id)),
        ('reqtime', time.strftime("%Y%m%d%H%M%S", time.localtime())),
    ))
    parameter_dict['sign'] = generate_query_sign(parameter_dict, api_key)
    _LOGGER.info("zfbpay query  data: %s", parameter_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_QUERY_GATEWAY, data=parameter_dict, headers=headers,timeout=3)
    _LOGGER.info("zfbpay query  rsp data: %s", response.text)
    if response.status_code == 200:
        data = json.loads(response.text)
        trade_status = str(data['status'])
        trade_no = data['sdpayno']
        total_fee = float(data['total_fee'])

        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }

        # 1成功0失败
        if trade_status == '1':
            _LOGGER.info('zfbpay query order success, user_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            res = order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                           total_fee, trade_no, extend)
            if res:
                # async notify
                async_job.notify_mch(pay_order.id)
            print("order process successfully. %s" % pay_order.id)
    else:
        _LOGGER.warn('zfbpay data error, status_code: %s', response.status_code)
