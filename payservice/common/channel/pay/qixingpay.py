# -*- coding: utf-8 -*-
import hashlib
import json
import random
import string
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.channel.pay import check_channel_order
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError
from common.utils.ip_address import check_valid_ip_address
from common.utils.tz import local_now

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    'M08201905011332140XG088': {
        # 七星支付 银联快捷 1.4% 10-5000 witch
        # 七星支付 微信个人 2.7% 50-10000 witch
        # 七星支付 支付宝原生 3.7% 10-10000 witch
        # 七星支付 支付宝个人 2.2% 50-10000 witch
        'API_KEY': '8aefb87d92b02e1080d0bb2630bf081c',
        'gateway': 'https://openapi.7pay.shysrj.com/gateway/pay',
        'query_gateway': 'https://openapi.7pay.shysrj.com/gateway/query',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _gen_sign(s):
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().upper()
    return sign


def generate_sign(parameter, key):
    s = ''
    for k in sorted(parameter.keys()):
        if parameter[k] != '':
            s += '%s' % parameter[k]
    s += '%s' % key
    return _gen_sign(s)


def verify_sign(params, key):
    sign = params['platSign']
    params.pop('platSign')
    calculated_sign = generate_sign(params, key)
    if sign.lower() != calculated_sign.lower():
        _LOGGER.info("qixingpay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _get_pay_type(service):
    if service == 'alipay_scan':
        return 'ALIPAY_PC'
    elif service == 'alipay_h5':
        return 'ALIPAY_MOBILE'
    elif service == 'wechat_scan':
        return 'WECHAT_PC'
    elif service == 'wechat_h5':
        return 'WECHAT_MOBILE'
    elif service == 'unionpay':
        return 'UNION'
    return 'ALIPAY_MOBILE'


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def _get_random_string():
    return ''.join(random.sample(string.ascii_letters + string.digits, 11))


def create_charge(pay, pay_amount, info):
    service = info.get('service')
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('merchantCode', app_id),
        ('method', _get_pay_type(service)),
        ('signType', 'MD5'),
        ('dateTime', local_now().strftime("%Y%m%d%H%M%S")),
        ('orderNum', str(pay.id)),
        ('payMoney', str(int(pay_amount * 100))),
        ('productName', 'qxPay'),
        ('notifyUrl', '{}/pay/api/{}/qixingpay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('spbillCreateIp', _get_device_ip(info)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("qixingpay create: %s, order_id is: %s", json.dumps(parameter_dict), str(pay.id))
    headers = {"Content-type": "application/x-www-form-urlencoded"}
    response = requests.post(_get_gateway(app_id), data=parameter_dict, headers=headers, timeout=5)
    _LOGGER.info('qixingpay create rsp: %s', response.text)
    return {'charge_info': json.loads(response.text)['payUrl']}


# SUCCESS
def check_notify_sign(request, app_id):
    api_key = _get_api_key(app_id)
    _LOGGER.info("qixingpay notify body is: %s", request.body)
    data = dict(request.POST.iteritems())
    _LOGGER.info("qixingpay notify data is: %s", data)
    verify_sign(data, api_key)
    pay_id = data['orderNum']
    check_valid_ip_address(str(request.META['REMOTE_ADDR']), pay_id)
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('qixingpay event does not contain pay ID')

    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(data['status'])
    total_fee = float(data['amount']) / 100.0
    trade_no = str(data['platOrderNum'])
    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }
    check_channel_order(pay_id, total_fee, app_id)
    # 支付状态('SUCCESS'-成功 'FAIL'-失败 ')
    if trade_status == 'SUCCESS' and total_fee > 0.0:
        _LOGGER.info('qixingpay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('merchantCode', app_id),
        ('method', 'ORDER_QUERY'),
        ('signType', 'MD5'),
        ('dateTime', local_now().strftime("%Y%m%d%H%M%S")),
        ('orderNum', str(pay_order.id)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info('qixingpay query data: %s, order_id is: %s', json.dumps(parameter_dict), str(pay_order.id))
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_query_gateway(app_id), data=parameter_dict, headers=headers, timeout=5)
    _LOGGER.info('qixingpay query rsp, %s', response.text)
    if response.status_code == 200:
        data = json.loads(response.text)
        verify_sign(data, api_key)
        mch_id = pay_order.mch_id
        trade_status = str(data['status'])
        total_fee = float(data['amount']) / 100.0
        trade_no = str(data['platOrderNum'])
        extend = {
            'trade_status': trade_status,
            'total_fee': total_fee,
            'trade_no': trade_no,
        }
        # 支付状态('SUCCESS'-成功 'FAIL'-失败 ')
        if trade_status == 'SUCCESS' and total_fee > 0.0:
            pay_id = str(pay_order.id)
            check_channel_order(pay_id, total_fee, app_id)
            _LOGGER.info('qixingpay query order success, mch_id:%s pay_id:%s' % (mch_id, pay_order.id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)
            async_job.notify_mch(pay_order.id)
