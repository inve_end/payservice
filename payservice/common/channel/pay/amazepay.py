# -*- coding: utf-8 -*-
import ast
import hashlib
import json
import random
import string
import time
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.agentpay.base import AbstractHandler
from common.agentpay.db import get_agent_pay_order
from common.cache import redis_cache
from common.channel import admin_db as channel_admin_db
from common.channel.pay import check_channel_order
from common.order.db import get_pay, add_pay_success
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError
from common.utils.types import Enum
from common.utils.ip_address import check_valid_ip_address

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '80001195': {  # amaze支付 10—3000 2.5% qq钱包wap dwc
        'API_KEY': '726664e85560e23e49bb2e707544b108',
        'gateway': 'http://47.75.88.84:7569/Gateway.ashx',
        'query_gateway': 'http://47.75.88.84:7569/GatewayQuery.ashx',
        'draw_gateway': 'http://47.75.88.84:6568/OtherPay.ashx',
        'draw_query_gateway': 'http://47.75.88.84:6568/OtherPayQuery.ashx',
        'balance_query_gateway': 'http://47.75.88.84:6568/Other.ashx',
    },
    '80327369': {
        'API_KEY': 'bf5a0fc2dc70ab118f4927a45547156d',
        'gateway': 'http://api.cheqianpay.com/Gateway.ashx',
        'query_gateway': 'http://api.cheqianpay.com/GatewayQuery.ashx',
        'draw_gateway': 'http://payment.cheqianpay.com/OtherPay.ashx',
        'draw_query_gateway': 'http://payment.cheqianpay.com/OtherPayQuery.ashx',
        'balance_query_gateway': 'http://payment.cheqianpay.com/Other.ashx',
    },
}

NOTIFY_PAY_STATUS = Enum({
    "WAIT": (0, u"等待"),
    "SUCCESS": (1, u"成功"),
    "FAIL": (2, u"失败"),
})


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _get_draw_gateway(mch_id):
    return APP_CONF[mch_id]['draw_gateway']


def _get_draw_query_gateway(mch_id):
    return APP_CONF[mch_id]['draw_query_gateway']


def _get_balance_gateway(mch_id):
    return APP_CONF[mch_id]['balance_query_gateway']


def _gen_sign(s):
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().lower()
    return sign


def generate_sign(parameter, key):
    s = ''
    sorted_keys = sorted(parameter, key=str.lower)
    for k in sorted_keys:
        if parameter[k]:
            s += '%s=%s&' % (k, parameter[k])
    s = (s[:-1] + '|' + '%s' % key).lower()
    _LOGGER.info("amazepay agentpay sign string: %s", s)
    return _gen_sign(s)


def _gen_sign_gbk(s):
    m = hashlib.md5()
    m.update(s.encode('GBK'))
    sign = m.hexdigest().lower()
    return sign


def generate_sign_gbk(parameter, key):
    s = ''
    sorted_keys = sorted(parameter, key=str.lower)
    for k in sorted_keys:
        if parameter[k]:
            s += '%s=%s&' % (k, parameter[k])
    s = (s[:-1] + '|' + '%s' % key).lower()
    _LOGGER.info("amazepay agentpay sign string: %s", s)
    return _gen_sign_gbk(s)


def generate_verify_sign(parameter, key):
    s = ''
    for k in sorted(parameter.keys()):
        if parameter[k]:
            s += '%s=%s&' % (k, parameter[k])
    s = (s[:-1] + '|' + '%s' % key).lower()
    return _gen_sign(s)


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_verify_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("amazepay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _build_form(params, gateway):
    html = u"<head><title>loading...</title></head><form id='submit' name='submit' action='" + gateway + "' method='post'>"
    for k, v in params.items():
        html += "<input type='hidden' name='%s' value='%s'/>" % (k, v)
    html += "</form>"
    html += "<script>doc" \
            "ument.forms['submit'].submit();</script>"
    return html


# wechat.native	微信扫码
# wechat.wap	微信 Wap
# alipay.native	支付宝扫码
# alipay.wap	支付宝Wap
# qq.native	QQ扫码
# qq.wap	QQWap

def _get_pay_type(service):
    if service == 'qq_wap':
        return 'qq.wap'
    elif service == 'qq_scan':
        return 'qq.native'
    elif service == 'wechat_wap':
        return 'wechat.wap'
    elif service == 'wechat_scan':
        return 'wechat.native'
    elif service == 'alipay_wap':
        return 'alipay.wap'
    elif service == 'alipay_scan':
        return 'alipay.native'
    elif service == 'unionpay':
        return '1002'
    return 'alipay.wap'


def create_charge(pay, pay_amount, info):
    app_id = info['app_id']
    service = info.get('service')
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('MerchantId', app_id),
        ('MerOrderNo', str(pay.id)),
        ('OrderMoney', str(int(pay_amount * 100))),
        ('BankCode', _get_pay_type(service)),
        ('RandomStr', ''.join(random.sample(string.ascii_letters + string.digits, 16))),
        ('NotifyUrl', '{}/pay/api/{}/amazepay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('ReturnUrl', '{}/pay/api/{}/amazepay/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, pay.id)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("amazepay create date: %s, order_id is: %s", json.dumps(parameter_dict),
                 parameter_dict['MerOrderNo'])
    html_text = _build_form(parameter_dict, _get_gateway(app_id))
    cache_id = redis_cache.save_html(pay.id, html_text)
    _LOGGER.info('amazepay url: %s', settings.PAY_CACHE_URL + cache_id)
    return {'charge_info': settings.PAY_CACHE_URL + cache_id}


def check_notify_sign(request, app_id):
    _LOGGER.info("amazepay notify body: %s", request.body)
    data = dict(request.GET.iteritems())
    _LOGGER.info("amazepay notify data: %s, order_id is: %s", data, data['merorderno'])
    verify_notify_sign(data, _get_api_key(app_id))
    pay_id = data['merorderno']
    check_valid_ip_address(str(request.META['REMOTE_ADDR']), pay_id)
    if not pay_id:
        _LOGGER.error("fatal error, pay object not exists, data: %s" % data)
        raise ParamError('amazepay event does not contain valid pay ID')

    pay = get_pay(pay_id)
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = data['orderstate']
    trade_no = data['sysorderno']
    total_fee = float(data['ordermoney']) / 100.0

    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    check_channel_order(pay_id, total_fee, app_id)

    if trade_status == '1':
        _LOGGER.info('amazepay check order success, user_id:%s pay_id:%s' % (mch_id, pay_id))
        add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('MerchantId', app_id),
        ('MerOrderNo', str(pay_order.id)),
    ))
    parameter_dict['Sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info('amazepay query data %s, order_id is: %s', json.dumps(parameter_dict),
                 parameter_dict['MerOrderNo'])
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_query_gateway(app_id), data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info('amazepay query rsp, %s', response.text)
    data = json.loads(response.text)
    if response.status_code == 200:
        trade_status = str(data['Code'])  # 订单已完成, 订单未完成
        total_fee = float(data['OrderMoney']) / 100.0
        trade_no = str(data['SysOrderNo'])
        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }
        # NOTPAY-未支付 SUCCESS 已支付
        if trade_status == '50':
            check_channel_order(pay_id, total_fee, app_id)
            _LOGGER.info('amazepay query order success, mch_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            res = add_pay_success(pay_order.mch_id, pay_id, total_fee, trade_no, extend)
            if res:
                async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('amazepay data error, status_code: %s', response.status_code)


def _get_bank_type(bank_type):
    if bank_type == 'ICBC':
        return '1002'
    elif bank_type == 'CCB':
        return '1003'
    elif bank_type == 'PNB':
        return '1010'
    elif bank_type == 'BOC':
        return '1052'
    elif bank_type == 'ABC':
        return '1005'
    elif bank_type == 'BCM':
        return '1020'
    elif bank_type == 'CMB':
        return '1001'
    elif bank_type == 'CNCB':
        return '1021'
    elif bank_type == 'CEB':
        return '1022'
    elif bank_type == 'HXB':
        return '1025'
    elif bank_type == 'SPDB':
        return '1004'
    elif bank_type == 'CIB':
        return '1009'
    elif bank_type == 'CMBC':
        return '1006'
    elif bank_type == 'CGB':
        return '1027'
    elif bank_type == 'PSBC':
        return '1028'
    else:
        return 'THIRD_NOT_SUPPORT'


def _get_bank_name(bank_type):
    if bank_type == 'ICBC':
        return '工商银行'
    elif bank_type == 'CCB':
        return '建设银行'
    elif bank_type == 'PNB':
        return '平安银行'
    elif bank_type == 'BOC':
        return '中国银行'
    elif bank_type == 'ABC':
        return '农业银行'
    elif bank_type == 'BCM':
        return '交通银行'
    elif bank_type == 'CMB':
        return '招商银行'
    elif bank_type == 'CNCB':
        return '中信银行'
    elif bank_type == 'CEB':
        return '光大银行'
    elif bank_type == 'HXB':
        return '华夏银行'
    elif bank_type == 'SPDB':
        return '浦发银行'
    elif bank_type == 'CIB':
        return '兴业银行'
    elif bank_type == 'CMBC':
        return '民生银行'
    elif bank_type == 'CGB':
        return '广发银行'
    elif bank_type == 'PSBC':
        return '邮政银行'
    else:
        return 'THIRD_NOT_SUPPORT'


class AmazePayHandler(AbstractHandler):
    def submit_order(self, order_no):
        """
        创建代付请求
        :return:
        """
        agent_pay = get_agent_pay_order(order_no)
        chn = channel_admin_db.get_channel(int(agent_pay.channel_id))

        chn_info = json.loads(chn.info)
        app_id = chn_info['app_id']
        api_key = _get_api_key(app_id)
        params = {
            'MerchantId': app_id,
            'MerOrderNo': agent_pay.order_no,
            'OrderMoney': int(agent_pay.total_fee * 100),
            'BankCode': _get_bank_type(agent_pay.bank_code),
            'Timestamp': int(time.time()),
            'Province': agent_pay.account_province.encode('utf-8'),
            'City': agent_pay.account_city.encode('utf-8'),
            'CardNo': agent_pay.card_no,
            'CardName': agent_pay.account_name.encode('utf-8'),
        }
        params['sign'] = generate_sign_gbk(params, api_key)
        _LOGGER.info('amazepay draw data: %s', json.dumps(params))
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        response = requests.post(_get_draw_gateway(app_id), data=params, timeout=3, headers=headers, verify=False)
        _LOGGER.info("amazepay draw rsp data: %s %s", response.status_code, response.text)
        data = json.loads(response.text)
        if data['Code'] == '1':
            return params['MerOrderNo'], '', '提交成功->' + str(response.text)
        else:
            return params['MerOrderNo'], '', '提交失败->' + str(response.text)

    def query_agent_pay(self, order_no):
        """
        查询代付请求
        :return:
        """
        agent_pay = get_agent_pay_order(order_no)
        chn = channel_admin_db.get_channel(int(agent_pay.channel_id))
        chn_info = json.loads(chn.info)
        app_id = chn_info['app_id']
        api_key = _get_api_key(app_id)
        params = {
            'MerchantId': app_id,
            'MerOrderNo': agent_pay.order_no,
        }
        params['Sign'] = generate_sign_gbk(params, api_key)
        _LOGGER.info('amazepay draw query data: %s', json.dumps(params))
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        response = requests.post(_get_draw_query_gateway(app_id), data=params, timeout=3, headers=headers, verify=False)
        _LOGGER.info("amazepay draw query rsp data: %s %s", response.status_code, response.text)
        if response.status_code != 200:
            raise ParamError('amazepay query_agent_pay error')
        return str(response.text)

    def query_balance(self, info):
        info = ast.literal_eval(info.encode("utf-8"))
        app_id = info['app_id']
        api_key = _get_api_key(app_id)
        params = {
            'MerchantId': app_id,
        }
        params['Sign'] = generate_sign_gbk(params, api_key)
        _LOGGER.info('amazepay balance query data: %s', json.dumps(params))
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        response = requests.post(_get_balance_gateway(app_id), data=params, timeout=3, headers=headers, verify=False)
        _LOGGER.info("amazepay balance query rsp data: %s %s", response.status_code, response.text)
        if response.status_code != 200:
            raise ParamError('amazepay query_agent_pay error')
        data = json.loads(response.text)
        return float(data['Money']) / 100.0


handler = AmazePayHandler()
