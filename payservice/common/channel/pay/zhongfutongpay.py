# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.cache import redis_cache
from common.channel.pay import check_channel_order
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError
from common.utils.tz import local_now

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '3369220181019': {  # 支付宝2.4%(10---5000) D0结算 zs
        'API_KEY': 'Q6VPun2ZxNcBhjM',
        'MARK_KEY': '5XPCq',
        'gateway': 'http://47.52.44.224:8080/YBT/YBTPAY',
        'query_gateway': 'http://47.52.44.224:8080/YBT/YBTQUERY',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _get_merMark(mch_id):
    return APP_CONF[mch_id]['MARK_KEY']


def _get_pay_type(service):
    if service == 'alipay':
        return 'aliH5'


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def _build_form(params, gateway):
    html = u"<head><title>loading...</title></head><form id='submit' name='submit' action='" + gateway + "' method='get'>"
    for k, v in params.items():
        html += "<input type='hidden' name='%s' value='%s'/>" % (k, v)
    html += "</form>"
    html += "<script>doc" \
            "ument.forms['submit'].submit();</script>"
    return html


def _gen_sign(s):
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().upper()
    return sign


def generate_sign(parameter, key):
    s = ''
    for k in parameter.keys():
        s += '%s=%s&' % (k, parameter[k])
    s += 'key={}'.format(key)
    _LOGGER.info('zhongfutongpay sign : %s', s)
    return _gen_sign(s)


def generate_notify_sign(parameter, key):
    s = 'merchantNum={}&orderNum={}&amount={}&nonce_str={}&orderStatus={}&key={}'.format(
        parameter['merchantNum'], parameter['orderNum'],
        parameter['amount'], parameter['nonce_str'],
        parameter['orderStatus'], key
    )
    _LOGGER.info('zhongfutongpay notify sign : %s', s)
    return _gen_sign(s)


def generate_query_sign(parameter, key):
    s = 'merchantNum={}&orderNum={}&amount={}&nonce_str={}&key={}'.format(
        parameter['merchantNum'], parameter['orderNum'],
        parameter['amount'], parameter['nonce_str'], key
    )
    _LOGGER.info('zhongfutongpay query sign : %s', s)
    return _gen_sign(s)


def generate_notify_query_sign(parameter, key):
    s = 'merchantNum={}&orderNum={}&amount={}&orderStatus={}&nonce_str={}&key={}'.format(
        parameter['merchantNum'], parameter['orderNum'],
        parameter['amount'], parameter['orderStatus'],
        parameter['nonce_str'], key
    )
    _LOGGER.info('zhongfutongpay notify query sign : %s', s)
    return _gen_sign(s)


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    params.pop('remark')
    calculated_sign = generate_notify_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("zhongfutongpay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def verify_notify_query_sign(params, key):
    sign = params['sign']
    calculated_sign = generate_notify_query_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("zhongfutongpay query sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def create_charge(pay, pay_amount, info):
    """ 创建订单 """
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    service = info.get('service')
    parameter_dict = OrderedDict((
        ('version', 'V1.0'),
        ('merchantNum', app_id),
        ('nonce_str', str(pay.id)),
        ('merMark', _get_merMark(app_id)),
        ('client_ip', _get_device_ip(info)),
        ('payType', _get_pay_type(service)),
        ('orderNum', _get_merMark(app_id) + str(pay.id)),
        ('amount', str(int(pay_amount * 100))),
        ('body', 'charge'),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    parameter_dict['notifyUrl'] = '{}/pay/api/{}/zhongfutongpay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH,
                                                                           app_id)
    parameter_dict['orderTime'] = local_now().strftime('%Y-%m-%d %H:%M:%S')
    parameter_dict['signType'] = 'MD5'
    _LOGGER.info("zhongfutongpay create  data: %s, url: %s", parameter_dict, _get_gateway(app_id))
    html_text = _build_form(parameter_dict, _get_gateway(app_id))
    cache_id = redis_cache.save_html(pay.id, html_text)
    return {'charge_info': settings.PAY_CACHE_URL + cache_id}


# SUCCESS
def check_notify_sign(request, app_id):
    api_key = _get_api_key(app_id)
    resp = dict(request.POST.iteritems())
    _LOGGER.info("zhongfutongpay notify data: %s", resp)
    verify_notify_sign(resp, api_key)
    pay_id = resp['orderNum']
    pay_id = pay_id.replace(_get_merMark(app_id), '')
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % resp)
        raise ParamError('zhongfutongpay event does not contain pay ID')

    pay = order_db.get_pay(pay_id)
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('zhongfutongpay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(resp['orderStatus'])
    trade_no = ''
    total_fee = float(resp['amount']) / 100.0

    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    check_channel_order(pay_id, total_fee, app_id)

    if trade_status == 'SUCCESS' and int(total_fee) > 0:
        _LOGGER.info('zhongfutongpay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    """ 查询订单 """
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('merchantNum', app_id),
        ('orderNum', _get_merMark(app_id) + str(pay_id)),
        ('amount', str(float(pay_order.total_fee) * 100)),
        ('nonce_str', str(pay_id)),
    ))
    parameter_dict['sign'] = generate_query_sign(parameter_dict, api_key)
    _LOGGER.info("zhongfutongpay query  data: %s", parameter_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_query_gateway(app_id), data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info("zhongfutongpay query  rsp data: %s", response.text)
    if response.status_code == 200:
        resp = json.loads(response.text)

        if resp['resp_Code'] != 'S':
            _LOGGER.warn('zhongfutongpay query err, data: %s', response.text)
            raise ParamError(u'channel query api error')

        verify_notify_query_sign(resp, api_key)

        trade_status = str(resp['orderStatus'])
        trade_no = ''
        total_fee = float(resp['amount']) / 100.0

        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }

        if trade_status == 'SUCCESS':
            check_channel_order(pay_id, total_fee, app_id)

            _LOGGER.info('zhongfutongpay query order success, user_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)
            async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('zhongfutongpay data error, status_code: %s', response.status_code)
