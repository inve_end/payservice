# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.order.db import get_pay, add_pay_success
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '2018109': {  # dwc 支付宝，微信（4.5%，0---3000）。qq1.6%，0---5000.京东1.7%，0---3000.快捷1.4%，0---10000.
        'API_KEY': 'KMTqOJmTMiPYPKcNzuCJxSihDNzSAtUD',
        'gateway': 'http://www.njops.cn/Pay/',
    },
    '2018126': {  # witch 支付宝3.5%（1---3000）d0结算。
        'API_KEY': 'YrzMQevpCotHUUfRuPhZqYqeYIByOEHc',
        'gateway': 'http://www.njops.cn/Pay/',
    },
    '2018107': {  # dwc 支付宝3.3%（1-----5000）D0结算
        'API_KEY': 'npbgCWlQTLEmcgIVvcMOQFfWifwJHLNI',
        'gateway': 'http://www.jubaopaypay.com/Pay',
    },
    '2018120': {  # ks 支付宝：2.6%（100---1w）d0结算。
        'API_KEY': 'myYyFQDmuPRmTLXjjHpwXiCUEJagqsJV',
        'gateway': 'http://www.jubaopaypay.com/Pay',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def generate_order_sign(d, key):
    s = d['fxid'] + d['fxddh'] + d['fxfee'] + d['fxnotifyurl'] + key
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest()
    return sign


# md5($fxstatus . $fxid . $fxddh . $fxfee . $fxkey); //验证签名
def generate_notify_sign(d, key):
    s = d['fxstatus'] + d['fxid'] + d['fxddh'] + d['fxfee'] + key
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest()
    return sign


# md5($data["fxid"] . $data["fxddh"] . $data["fxaction"] . $fxkey);
def generate_query_sign(d, key):
    s = d['fxid'] + d['fxddh'] + d['fxaction'] + key
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest()
    return sign


def verify_notify_sign(params, key):
    sign = params['fxsign']
    calculated_sign = generate_notify_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("lipay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


# 请求类型
# 【微信wap：wxwap】【微信公众号：wxgzh】【微信扫码：wxsm】
# 【支付宝wap：zfbwap】【支付宝扫码：zfbsm】
# 【QQWAP：qqwap】【QQ扫码：qqsm】
# 【银联扫码：ylsm】【银联WAP：ylwap】
# 【京东扫码：jdsm】【京东WAP：jdwap】
# 【苏宁扫码：snsm】【苏宁wap：snwap】
# 【百度扫码：bdsm】【百度wap：bdwap】
# 【美团扫码：mtsm】【美团wap：mtwap】
# 【网银：bank】【网银快捷：ylkj】
# 【QQ二维码（跳转）：qqewm】【银联二维码（跳转）：ylewm】
def _get_pay_type(service):
    if service == 'wxpay':
        payType = 'wxwap'
    elif service == 'alipay':
        payType = 'zfbwap'
    elif service == 'alipay1':
        payType = 'zfbewm1'
    elif service == 'qq':
        payType = 'qqwap'
    elif service == 'quickwap':
        payType = 'ylwap'
    elif service == 'quick':
        payType = 'ylkj'
    elif service == 'meituan':
        payType = 'mtwap'
    elif service == 'baidu':
        payType = 'bdwap'
    elif service == 'suning':
        payType = 'snwap'
    else:
        payType = 'jdwap'
    return payType


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def create_charge(pay, pay_amount, info):
    mch_id = info['app_id']
    service = info.get('service')
    key = _get_api_key(mch_id)
    parameter_dict = OrderedDict((
        ('fxid', mch_id),
        ('fxddh', str(pay.id)),
        ('fxdesc', 'charge'),
        ('fxfee', str(pay_amount)),
        ('fxnotifyurl', '{}/pay/api/{}/lipay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, mch_id)),
        ('fxbackurl', '{}/pay/api/{}/lipay/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, pay.id)),
        ('fxpay', _get_pay_type(service)),
        ('fxip', _get_device_ip(info)),
    ))
    parameter_dict['fxsign'] = generate_order_sign(parameter_dict, key)
    _LOGGER.info("lipay create data: %s", parameter_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_gateway(mch_id), data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info("lipay create rsp: %s, %s", response.text, response.url)
    j = json.loads(response.text)
    url = j['payurl'].replace('\/', '/')
    _LOGGER.info("lipay create url: %s", url)
    return {'charge_info': url}


# success
def check_notify_sign(request, app_id):
    key = _get_api_key(app_id)
    data = dict(request.POST.iteritems())
    _LOGGER.info("lipay notify data: %s", data)
    verify_notify_sign(data, key)
    pay_id = data['fxddh']
    if not pay_id:
        _LOGGER.error("fatal error, pay object not exists, data: %s" % data)
        raise ParamError('lipay event does not contain valid pay ID')

    pay = get_pay(pay_id)
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = int(data['fxstatus'])
    trade_no = data['fxorder']
    total_fee = float(data['fxfee'])

    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }
    from common.channel.pay import check_channel_order
    check_channel_order(pay_id, total_fee, app_id)
    # 1为成功，6为退款，其他为失败
    if trade_status == 1:
        _LOGGER.info('lipay check order success, user_id:%s pay_id:%s' % (mch_id, pay_id))
        add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    pay_id = pay_order.id
    key = _get_api_key(app_id)
    p_dict = {
        'fxid': app_id,
        'fxddh': str(pay_id),
        'fxaction': 'orderquery',
    }
    p_dict['fxsign'] = generate_query_sign(p_dict, key)
    _LOGGER.info(u'lipay query data: %s', p_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_gateway(app_id), data=p_dict, headers=headers, timeout=3)
    _LOGGER.info(u'lipay query: %s', response.text)
    data = json.loads(response.text)
    if response.status_code == 200:
        trade_status = int(data['fxstatus'])
        total_fee = float(data['fxfee'])
        trade_no = data['fxorder']
        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }

        if trade_status == 1:
            _LOGGER.info('lipay query order success, mch_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            add_pay_success(pay_order.mch_id, pay_id, total_fee, trade_no, extend)
            async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('lipay data error, status_code: %s', response.status_code)
