# -*- coding: utf-8 -*-
import hashlib
import json
import time
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.cache import redis_cache
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

_GATEWAY = 'http://106.14.206.106'
_QUERY_GATEWAY = 'http://106.14.206.106/index.php/Query'
# 成都摩宝只付接的是loki,支付方式：qq钱包，微信h5，支付宝h5 单笔限额都是，（5----1500）可以对接
APP_CONF = {
    '309864': {
        'API_KEY': 'RaJeolySNnZ6d5mWHk2T9haCofmdubvA',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


# 3:wap网银（快捷）
# 4:微信扫码
# 5:支付宝扫码
# 6:微信WAP支付
# 7:支付宝WAP支付
# 8:微信公众号支付
# 9:QQ钱包扫码
# 10:QQ钱包WAP支付
# 11.银联二维码
def _get_pay_type(service):
    if service == 'alipay':
        return '7'
    elif service == 'wxpay':
        return '6'
    elif service == 'qq':
        return '10'
    else:
        return '3'  # wap网银


def generate_sign(parameter, key):
    '''  生成下单签名 '''
    s = ''
    for k in sorted(parameter.keys()):
        if parameter[k] != '' and parameter[k] != None:
            s += '%s=%s&' % (k, parameter[k])
    s += 'key=%s' % key
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().upper()
    return sign


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("mobaopay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _build_form(params):
    html = u"<head><title>loading...</title></head><form id='submit' name='submit' action='" + _GATEWAY + "' method='post'>"
    for k, v in params.items():
        html += "<input type='hidden' name='%s' value='%s'/>" % (k, v)
    html += "</form>"
    html += "<script>doc" \
            "ument.forms['submit'].submit();</script>"
    return html


def create_charge(pay, pay_amount, info):
    ''' 创建订单 '''
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    service = info.get('service')
    order_id = '1' + str(pay.id)
    parameter_dict = OrderedDict((
        ('partner', app_id),
        ('pay_input_charset', '1'),
        ('pay_service_type', _get_pay_type(service)),
        ('pay_date', str(int(time.time()))),
        ('pay_order_id', order_id),
        ('pay_product_name', 'charge'),
        ('pay_amount', str(int(pay_amount * 100))),
        ('pay_notify_url', '{}/pay/api/{}/mobaopay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('pay_return_url', '{}/pay/api/{}/mobaopay/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, app_id)),
    ))
    parameter_dict['pay_sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("mobaopay create  data: %s", parameter_dict)
    html_text = _build_form(parameter_dict)
    cache_id = redis_cache.save_html(pay.id, html_text)
    return {'charge_info': settings.PAY_CACHE_URL + cache_id}


# ok
def check_notify_sign(request, app_id):
    data = dict(request.POST.iteritems())
    api_key = _get_api_key(app_id)
    _LOGGER.info("mobaopay notify data: %s", data)
    _LOGGER.info("mobaopay notify body: %s", request.body)
    verify_notify_sign(data, api_key)
    pay_id = data['pay_order_id']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('mobaopay event does not contain pay ID')

    t = str(pay_id)[1:]
    pay_id = int(t)
    pay = order_db.get_pay(pay_id)
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = 'success'
    trade_no = data['pay_order_id']
    total_fee = float(data['pay_amount']) / 100.0

    extend = {
        'trade_status': 'success',
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    from common.channel.pay import check_channel_order
    check_channel_order(pay_id, total_fee, app_id)
    if trade_status == 'success':
        _LOGGER.info('mobaopay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    # prepare param
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('partner', app_id),
        ('version', '1'),
        ('sign_type', '1'),
        ('query_date', int(time.time())),
        ('order_id', '1' + str(pay_id)),
    ))
    parameter_dict['signature'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("mobaopay query  data: %s", parameter_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_QUERY_GATEWAY, data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info("mobaopay query  rsp data: %s", response.text)
    if response.status_code == 200:
        data = json.loads(response.text)['reData']
        trade_status = str(data['r_status'])
        trade_no = data['r_order_id']
        total_fee = float(data['r_amout'])

        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }

        # 0 为失败
        # 1 为成功
        if trade_status == '1':
            _LOGGER.info('mobaopay query order success, user_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)
            async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('mobaopay data error, status_code: %s', response.status_code)
