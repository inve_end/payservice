# -*- coding: utf-8 -*-
import hashlib
import hmac
import json
from binascii import b2a_hex
from collections import OrderedDict

import requests
from Crypto.Cipher import AES as _AES

from async import async_job
from common.channel.pay import check_channel_order
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError
from common.utils.tz import local_now

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    'CD0002120': {  # zs 支付宝2.2%（100-5000）
        'gateway': 'http://open.cpay.life/api/alipay/tran',
        'query_gateway': 'http://open.cpay.life/api/alipay_order/detail',
        'pub_key': "5bf36957ac7da40baaafdbde",
        'pri_key': "c4f2e5c57f979076bc57b50790125096",
    },
    'CD0002398': {  # loki 支付宝2.2%（100-5000）
        'gateway': 'http://open.cpay.life/api/alipay/tran',
        'query_gateway': 'http://open.cpay.life/api/alipay_order/detail',
        'pub_key': "5c1da556ac7da438eff6a00c",
        'pri_key': "fcceb7fea4ef3d89f0772d489d6aea05",
    },
    'CD0002441': {  # witch 支付宝2.2%（100-5000）
        'gateway': 'http://open.cpay.life/api/alipay/tran',
        'query_gateway': 'http://open.cpay.life/api/alipay_order/detail',
        'pub_key': "5c1ee5a1ac7da45f6394748f",
        'pri_key': "b43af2cb588b775f5e4858c16e06782d",
    },
    'CD0002442': {  # dwc 支付宝2.2%（100-5000）
        'gateway': 'http://open.cpay.life/api/alipay/tran',
        'query_gateway': 'http://open.cpay.life/api/alipay_order/detail',
        'pub_key': "5c1ee5feac7da45f63947492",
        'pri_key': "0f3b4f4ced9cf8be4a79b1c8f697806c",
    },
}


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _get_cpay_pub_key(mch_id):
    return APP_CONF[mch_id]['pub_key']


def _get_cpay_pri_key(mch_id):
    return APP_CONF[mch_id]['pri_key']


def _gen_sign(s):
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().lower()
    return sign


def encode(key, text):
    if not isinstance(text, str) and not isinstance(text, unicode):
        text = json.dumps(text)
    cryptor = _AES.new(key, _AES.MODE_ECB, b'3eddc41dd41239c8')
    length = 16
    count = len(text)
    if count < length:
        add = (length - count)
        # \0 backspace
        text = text + ('\0' * add)
    elif count > length and count % length != 0:
        add = (length - (count % length))
        text = text + ('\0' * add)
    ciphertext = cryptor.encrypt(text)

    return b2a_hex(ciphertext)


def generate_sign(secret_key, parameter_dict):
    """
    加密
    :param secret_key:
    :param parameter_dict:
    :return:
    """
    mapping = {}
    for k, v in parameter_dict.items():
        mapping[k.lower()] = v

    keys = [k.lower() for k in mapping.keys()]
    keys.sort()
    s = "&".join(["%s=%s" % (k, mapping[k]) for k in keys])
    s = encode(secret_key, s)
    s = hmac.HMAC(secret_key.encode("u8"), s).hexdigest()
    return s


def verify_notify_sign(params, key, token):
    calculated_sign = generate_sign(key, params)
    if token != calculated_sign:
        raise ParamError('sign not pass, data: %s' % params)


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def _get_pay_type(service):
    if service == 'alipay':
        return 'alipay'
    return 'alipay'


def create_charge(pay, pay_amount, info):
    service = info.get('service')
    app_id = info['app_id']
    parameter_dict = OrderedDict((
        ('psd_order_id', str(pay.id)),
        ('psd_order_time', local_now().strftime('%Y%m%d%H%M%S')),
        ('order_amount', str(int(pay_amount) * 100)),
        ('order_source', '2'),  # 1 PC 2 手机
        ('pay_type', _get_pay_type(service)),
    ))
    token = generate_sign(_get_cpay_pri_key(app_id), parameter_dict)
    _LOGGER.info("cpay create: %s", json.dumps(parameter_dict))
    headers = {'Content-Type': 'application/x-www-form-urlencoded', 'appkey': _get_cpay_pub_key(app_id), 'token': token}
    _LOGGER.info('cpay header: %s', headers)
    response = requests.post(_get_gateway(app_id), data=parameter_dict, headers=headers, timeout=5, verify=False)
    _LOGGER.info("cpay create rsp : %s", response.text)
    data = json.loads(response.text)
    if data['data'].get('cpay_order_id', None):
        order_db.fill_third_id(pay.id, data['data']['cpay_order_id'])
    return {'charge_info': data['data']['alipay']['qrcode']}


def check_notify_sign(request, app_id):
    pri_key = _get_cpay_pri_key(app_id)
    _LOGGER.info("cpay notify data: %s", request.body)
    data = dict(request.POST.iteritems())
    token = request.META.get('HTTP_TOKEN')
    verify_notify_sign(data, pri_key, token)
    pay_id = data['psd_order_id']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('cpay event does not contain pay ID')

    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = 'success'
    trade_no = pay.third_id
    total_fee = float(data['order_amount']) / 100.0
    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    check_channel_order(pay_id, total_fee, app_id)

    if trade_status == 'success' and total_fee > 0.0:
        _LOGGER.info('cpay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    """ 查询订单 """
    pay_id = pay_order.id
    parameter_dict = OrderedDict((
        ('psd_order_id', str(pay_order.id)),
    ))
    token = generate_sign(_get_cpay_pri_key(app_id), parameter_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded', 'appkey': _get_cpay_pub_key(app_id), 'token': token}
    response = requests.post(_get_query_gateway(app_id), data=parameter_dict, headers=headers, timeout=5)
    _LOGGER.info('cpay query rsp, %s', response.text)
    if response.status_code == 200:
        data = json.loads(response.text)
        mch_id = pay_order.mch_id
        trade_status = str(data['data']['state'])
        total_fee = float(data['data']['order_amount']) / 100.0
        trade_no = data['data']['cpay_order_id']

        extend = {
            'trade_status': trade_status,
            'total_fee': total_fee,
            'trade_no': trade_no,
        }

        if trade_status == '3':
            check_channel_order(pay_id, total_fee, app_id)

            _LOGGER.info('cpay query order success, mch_id:%s pay_id:%s' % (mch_id, pay_order.id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)

            async_job.notify_mch(pay_order.id)
