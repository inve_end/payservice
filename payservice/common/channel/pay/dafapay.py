# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.order.db import get_pay, add_pay_success
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError
from common.utils.ip_address import check_valid_ip_address

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '2019168': {
        # 大发支付 支付宝原生H5 3.5% 10-50000 dwc
        # 大发支付 微信原生扫码 4% 10-50000 dwc
        'API_KEY': 'whmqqoDEnsTCXYEuMFEWzMTEVbYxfrnF',
        'gateway': 'http://gateway.395838.com/Pay',
        'query_gateway': 'http://gateway.395838.com/Pay',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def generate_order_sign(d, key):
    s = d['fxid'] + d['fxddh'] + d['fxfee'] + d['fxnotifyurl'] + key
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest()
    return sign


def generate_notify_sign(d, key):
    s = d['fxstatus'] + d['fxid'] + d['fxddh'] + d['fxfee'] + key
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest()
    return sign


def generate_query_sign(d, key):
    s = d['fxid'] + d['fxddh'] + d['fxaction'] + key
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest()
    return sign


def verify_notify_sign(params, key):
    sign = params['fxsign']
    calculated_sign = generate_notify_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("dafapay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


# 请求类型 【支付宝H5：alipayh5】【支付宝扫码：alipaysm】【微信扫码-TZ：wechat】
def _get_pay_type(service):
    if service == 'alipay_h5':
        return 'alipayh5'
    elif service == 'alipay_scan':
        return 'alipaysm'
    elif service == 'wechat_scan':
        return 'wechat'
    return 'zfbewm'


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def create_charge(pay, pay_amount, info):
    mch_id = info['app_id']
    service = info.get('service')
    key = _get_api_key(mch_id)
    parameter_dict = OrderedDict((
        ('fxid', mch_id),
        ('fxddh', str(pay.id)),
        ('fxdesc', 'charge'),
        ('fxfee', str(pay_amount)),
        ('fxnotifyurl', '{}/pay/api/{}/dafapay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, mch_id)),
        ('fxbackurl', '{}/pay/api/{}/dafapay/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, pay.id)),
        ('fxpay', _get_pay_type(service)),
        ('fxip', _get_device_ip(info)),
    ))
    parameter_dict['fxsign'] = generate_order_sign(parameter_dict, key)
    _LOGGER.info("dafapay create data: %s", json.dumps(parameter_dict))
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_gateway(mch_id), data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info("dafapay create rsp: %s, %s", response.text, response.url)
    j = json.loads(response.text)
    url = j['payurl'].replace('\/', '/')
    _LOGGER.info("dafapay create url: %s, order_id: %s", url, dict(parameter_dict)['fxddh'])
    return {'charge_info': url}


def check_notify_sign(request, app_id):
    key = _get_api_key(app_id)
    data = dict(request.POST.iteritems())
    _LOGGER.info("dafapay notify data: %s", data)
    verify_notify_sign(data, key)
    pay_id = data['fxddh']
    check_valid_ip_address(str(request.META['REMOTE_ADDR']), pay_id)
    if not pay_id:
        _LOGGER.error("fatal error, pay object not exists, data: %s" % data)
        raise ParamError('dafapay event does not contain valid pay ID')

    pay = get_pay(pay_id)
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = int(data['fxstatus'])
    trade_no = data['fxorder']
    total_fee = float(data['fxfee'])

    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }
    from common.channel.pay import check_channel_order
    check_channel_order(pay_id, total_fee, app_id)
    if trade_status == 1:
        _LOGGER.info('dafapay check order success, user_id:%s pay_id:%s' % (mch_id, pay_id))
        add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    pay_id = pay_order.id
    key = _get_api_key(app_id)
    p_dict = {
        'fxid': app_id,
        'fxddh': str(pay_id),
        'fxaction': 'orderquery',
    }
    p_dict['fxsign'] = generate_query_sign(p_dict, key)
    _LOGGER.info(u'dafapay query data: %s', p_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_gateway(app_id), data=p_dict, headers=headers, timeout=3)
    _LOGGER.info(u'dafapay query: %s', response.text)
    data = json.loads(response.text)
    if response.status_code == 200:
        trade_status = int(data['fxstatus'])
        total_fee = float(data['fxfee'])
        trade_no = data['fxorder']
        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }

        if trade_status == 1:
            _LOGGER.info('dafapay query order success, mch_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            add_pay_success(pay_order.mch_id, pay_id, total_fee, trade_no, extend)
            async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('dafapay data error, status_code: %s', response.status_code)
