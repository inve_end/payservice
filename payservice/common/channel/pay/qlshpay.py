# -*- coding: utf-8 -*-
import hashlib
import json
import time
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.cache import redis_cache
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '33396133': {  # 测试账户
        'API_KEY': '879adf264bf0fc038d427a413ede6b03',
        'gateway': 'http://47.52.72.144:18181/channel/Common/mail_interface',
        'query_gateway': 'http://47.52.72.144:18181/channel/Common/query_pay',
    },
    '30616262': {  #
        'API_KEY': 'f19ab1eec188db5bfb80f15ef59ab114',
        'gateway': 'http://47.52.72.144:18181/channel/Common/mail_interface',
        'query_gateway': 'http://47.52.72.144:18181/channel/Common/query_pay',
    },
    '32393062': {  #
        'API_KEY': '6b6c4bb78a9279f875470b220f6e7460',
        'gateway': 'http://47.52.241.164:10101/channel/Common/mail_interface',
        'query_gateway': 'http://47.52.241.164:10101/channel/Common/query_pay',
    },
    '39333664': {  #
        'API_KEY': 'a8dad26d0b8f1dc35a8b1ee3c37a02dd',
        'gateway': 'http://www.yifupaygate.com/guest/channel/Common/mail_interface',
        'query_gateway': 'http://www.yifupaygate.com/guest/channel/Common/query_pay',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _gen_sign(s):
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().upper()
    return sign


def generate_sign(parameter, key):
    s = ''
    for k in sorted(parameter.keys()):
        if k != 'sign' and parameter[k] != '':
            s += '%s=%s&' % (k, parameter[k])
    s += 'key=%s' % key
    _LOGGER.info("qlshpay sign str: %s", s)
    return _gen_sign(s)


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    params.pop('messages')
    calculated_sign = generate_sign(params, key)
    if sign.upper() != calculated_sign.upper():
        _LOGGER.info("qlshpay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _get_pay_type(service):
    if service == 'wxpay':
        payType = 'wechat_wap'
    elif service == 'alipay':
        payType = 'alipay'
        # payType = 'alipay_wap'
    else:
        payType = 'alipay'
    return payType


def _build_form(params, gateway):
    html = u"<head><title>loading...</title></head><form id='submit' name='submit' action='" + gateway + "' method='post'>"
    for k, v in params.items():
        html += "<input type='hidden' name='%s' value='%s'/>" % (k, v)
    html += "</form>"
    html += "<script>doc" \
            "ument.forms['submit'].submit();</script>"
    return html


def create_charge(pay, pay_amount, info):
    service = info.get('service')
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('return_type', 'html'),
        ('api_code', app_id),
        ('is_type', _get_pay_type(service)),
        ('price', '%.2f' % pay_amount),
        ('order_id', str(pay.id)),
        ('time', int(time.time())),
        ('mark', 'charge'),
        ('return_url', '{}/pay/api/{}/qlshpay/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, str(pay.id))),
        ('notify_url', '{}/pay/api/{}/qlshpay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("qlshpay create: %s", json.dumps(parameter_dict))
    html_text = _build_form(parameter_dict, _get_gateway(app_id))
    cache_id = redis_cache.save_html(pay.id, html_text)
    return {'charge_info': settings.PAY_CACHE_URL + cache_id}


# SUCCESS
def check_notify_sign(request, app_id):
    api_key = _get_api_key(app_id)
    _LOGGER.info("qlshpay notify body: %s", request.body)
    data = json.loads(request.body)
    verify_notify_sign(data, api_key)
    pay_id = data['order_id']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('qlshpay event does not contain pay ID')

    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(data['code'])
    trade_no = data['paysapi_id']
    total_fee = float(data['real_price'])
    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }
    from common.channel.pay import check_channel_order
    check_channel_order(pay_id, total_fee, app_id)
    # 0 未处理 1 交易成功 2 支付失败 3 关闭交易 4 支付超时
    if trade_status == '1':
        _LOGGER.info('qlshpay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('api_code', app_id),
        ('is_type', _get_pay_type(pay_order.service)),
        ('order_id', str(pay_order.id)),
        ('price', '%.2f' % float(pay_order.total_fee)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info('qlshpay query data, %s', parameter_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_query_gateway(app_id), data=parameter_dict, headers=headers, timeout=5)
    _LOGGER.info('qlshpay query rsp, %s', response.text)
    if response.status_code == 200:
        data = json.loads(response.text)
        mch_id = pay_order.mch_id
        trade_status = str(data['code'])
        total_fee = float(data['price'])
        trade_no = data['paysapi_id']

        extend = {
            'trade_status': trade_status,
            'total_fee': total_fee,
            'trade_no': trade_no,
        }

        if trade_status == '1':
            _LOGGER.info('qlshpay query order success, mch_id:%s pay_id:%s' % (mch_id, pay_order.id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)

            async_job.notify_mch(pay_order.id)
