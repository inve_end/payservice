# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.cache import redis_cache
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '1707': {  # dwc 对接的是qqwap和支付宝wap单笔限额是0---3000
        'API_KEY': '597b6b3bede3436594bf8ebb78b7c469',
        'gateway': 'http://api.njxej.com/bank',
        'query_gateway': 'http://api.njxej.com/search.ashx',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


# 1003	支付宝扫码
# 1004	微信扫码
# 1009	QQ钱包扫码
# 1008	手机QQ钱包
# 1007	手机微信
# 1006	手机支付宝
# 1005	手机网银
def _get_pay_type(service):
    if service == 'alipay':
        return '1006'
    elif service == 'wxpay':
        return '1007'
    elif service == 'qq':
        return '1008'
    elif service == 'quick':
        return '1005'
    else:
        return '1008'


def generate_sign(d, key):
    '''  生成下单签名 '''
    s = 'parter={}&type={}&value={}&orderid={}&callbackurl={}{}'.format(
        d['parter'], d['type'], d['value'], d['orderid'], d['callbackurl'], key
    )
    _LOGGER.info("xinjiepay sign str : %s", s)
    m = hashlib.md5()
    m.update(s.encode('GB2312'))
    sign = m.hexdigest().lower()
    return sign


def generate_notify_sign(d, key):
    '''  生成下单签名 '''
    s = 'orderid={}&opstate={}&ovalue={}{}'.format(
        d['orderid'], d['opstate'], d['ovalue'], key
    )
    _LOGGER.info("xinjiepay sign str : %s", s)
    m = hashlib.md5()
    m.update(s.encode('GB2312'))
    sign = m.hexdigest().lower()
    return sign


def generate_query_sign(d, key):
    '''  生成下单签名 '''
    s = 'orderid={}&parter={}{}'.format(
        d['orderid'], d['parter'], key
    )
    _LOGGER.info("xinjiepay sign str : %s", s)
    m = hashlib.md5()
    m.update(s.encode('GB2312'))
    sign = m.hexdigest().lower()
    return sign


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_notify_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("xinjiepay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _build_form(params, gateway):
    html = u"<head><title>loading...</title></head><form id='submit' name='submit' action='" + gateway + "' method='get'>"
    for k, v in params.items():
        html += "<input type='hidden' name='%s' value='%s'/>" % (k, v)
    html += "</form>"
    html += "<script>doc" \
            "ument.forms['submit'].submit();</script>"
    return html


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def create_charge(pay, pay_amount, info):
    ''' 创建订单 '''
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    service = info.get('service')
    parameter_dict = OrderedDict((
        ('parter', app_id),
        ('type', _get_pay_type(service)),
        ('value', '%.2f' % pay_amount),
        ('orderid', str(pay.id)),
        ('callbackurl', '{}/pay/api/{}/xinjiepay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        # ('hrefbackurl', '{}/pay/api/{}/xinjiepay/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, pay.id)),
        ('payerIp', _get_device_ip(info)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("xinjiepay create  data: %s, url: %s", parameter_dict, _get_gateway(app_id))
    html_text = _build_form(parameter_dict, _get_gateway(app_id))
    cache_id = redis_cache.save_html(pay.id, html_text)
    return {'charge_info': settings.PAY_CACHE_URL + cache_id}


# opstate=0
def check_notify_sign(request, app_id):
    data = dict(request.GET.iteritems())
    api_key = _get_api_key(app_id)
    _LOGGER.info("xinjiepay notify data1: %s", data)
    verify_notify_sign(data, api_key)
    pay_id = data['orderid']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('xinjiepay event does not contain pay ID')

    pay = order_db.get_pay(pay_id)
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(data['opstate'])
    trade_no = data['sysorderid']
    total_fee = float(data['ovalue'])

    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    from common.channel.pay import check_channel_order
    check_channel_order(pay_id, total_fee, app_id)
    # 0：处理成功。 -1：请求参数无效。 -2：签名错误 。只有opstate = 0才说明付款成功。其他状态无论什么内容，都不是成功
    if trade_status == '0':
        _LOGGER.info('xinjiepay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    # prepare param
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('parter', app_id),
        ('orderid', str(pay_id)),
    ))
    parameter_dict['sign'] = generate_query_sign(parameter_dict, api_key)
    _LOGGER.info("xinjiepay query  data: %s", parameter_dict)
    response = requests.get(_get_query_gateway(app_id), data=parameter_dict, timeout=3)
    _LOGGER.info("xinjiepay query  rsp data: %s", response.text)
    if response.status_code == 200:
        data = json.loads(response.text)
        trade_status = str(data['opstate'])
        trade_no = ''
        total_fee = float(data['ovalue'])

        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }

        if trade_status == '0':
            _LOGGER.info('xinjiepay query order success, user_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)
            async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('xinjiepay data error, status_code: %s', response.status_code)
