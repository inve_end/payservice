# -*- coding: utf-8 -*-
import hashlib
import json
import time
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.cache import redis_cache
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

_GATEWAY = 'http://spayment.zsagepay.com/wap/createOrder.do'
_QUERY_GATEWAY = 'http://payment.zsagepay.com/ebank/queryOrder.do'
# 汇合是接loki
APP_CONF = {
    '1000002363': {
        'API_KEY': 'b4054985-8976-49de-9b2d-ac43709eb4cb',
    },
    '1000002395': {  # dwc
        'API_KEY': '93875eb9-2a1b-4727-a0f4-85c5ec4285f4',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


# 00:微信
# 01：支付宝
# 02：QQ 的wap
def _get_pay_type(service):
    if service == 'alipay':
        return '01'
    elif service == 'qq':
        return '02'
    else:
        return '00'


def generate_sign(parameter, key):
    '''  生成下单签名 '''
    s = ''
    for k in sorted(parameter.keys()):
        if parameter[k] != '' and parameter[k] != None:
            s += '%s=%s&' % (k, parameter[k])
    s += 'KEY=%s' % key
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().upper()
    return sign


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("zeshengpay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _build_form(params):
    html = u"<head><title>loading...</title></head><form id='submit' name='submit' action='" + _GATEWAY + "' method='post'>"
    for k, v in params.items():
        html += "<input type='hidden' name='%s' value='%s'/>" % (k, v)
    html += "</form>"
    html += "<script>doc" \
            "ument.forms['submit'].submit();</script>"
    return html


def create_charge(pay, pay_amount, info):
    ''' 创建订单 '''
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    service = info.get('service')
    parameter_dict = OrderedDict((
        ('merchantCode', app_id),
        ('outOrderId', str(pay.id)),
        ('totalAmount', str(int(pay_amount * 100))),
        ('merchantOrderTime', time.strftime("%Y%m%d%H%M%S")),
        ('notifyUrl', '{}/pay/api/{}/zeshengpay/{}'.format(
            settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('randomStr', '231132123'),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    parameter_dict['payWay'] = _get_pay_type(service)
    _LOGGER.info("zeshengpay create  data: %s", parameter_dict)
    html = _build_form(parameter_dict)
    cache_id = redis_cache.save_html(pay.id, html)
    return {'charge_info': settings.PAY_CACHE_URL + cache_id}


# {'code':'00'}
def check_notify_sign(request, app_id):
    data = dict(request.POST.iteritems())
    api_key = _get_api_key(app_id)
    _LOGGER.info("zeshengpay notify data: %s", data)
    _LOGGER.info("zeshengpay notify body: %s", request.body)
    verify_notify_sign(data, api_key)
    pay_id = data['outOrderId']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('zeshengpay event does not contain pay ID')

    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = 'SUC'
    trade_no = data['instructCode']
    total_fee = float(data['totalAmount']) / 100.0

    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    from common.channel.pay import check_channel_order
    check_channel_order(pay_id, total_fee, app_id)

    if trade_status == 'SUC':
        _LOGGER.info('zeshengpay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    # prepare param
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('merchantCode', app_id),
        ('outOrderId', str(pay_id)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("zeshengpay query  data: %s", parameter_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_QUERY_GATEWAY, data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info("zeshengpay query  rsp data: %s", response.text)
    if response.status_code == 200:
        # response text is a pure html text
        data = json.loads(response.text)['data']
        trade_status = str(data['replyCode'])
        trade_no = data['instructCode']
        total_fee = float(data['amount']) / 100.0

        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }

        # 00 账单支付成功，其他失败
        if trade_status == '00':
            _LOGGER.info('zeshengpay query order success, user_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            res = order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                           total_fee, trade_no, extend)
            if res:
                # async notify
                async_job.notify_mch(pay_order.id)
            print("order process successfully. %s" % pay_order.id)
    else:
        _LOGGER.warn('zeshengpay data error, status_code: %s', response.status_code)
