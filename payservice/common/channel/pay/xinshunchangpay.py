# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.channel.pay import check_channel_order
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

# 新顺畅
APP_CONF = {
    '8e8c37a0': {  # dwc 10~5000 支付宝
        'API_KEY': 'c2b8ac1d849154f65477d2450068ffab',
        'gateway': 'http://www.9555o.com/topay/wgzfbsmzf/pay',
        'query_gateway': 'http://api.hxx13581983709.top/topay/wgzfbsmzf/checkOrder',
    },
    '1aacff17': {  # loki 10~5000 支付宝
        'API_KEY': '5d541518d7de8018cfa3a3d8ec202817',
        'gateway': 'http://www.9555o.com/topay/wgzfbsmzf/pay',
        'query_gateway': 'http://api.hxx13581983709.top/topay/wgzfbsmzf/checkOrder',
    },
    '6c2d8714': {  # loki 10~5000 支付宝
        'API_KEY': '321846a27975fbf0e1e13440c2fc897a',
        'gateway': 'http://www.9555o.com/topay/wgzfbsmzf/pay',
        'query_gateway': 'http://api.hxx13581983709.top/topay/wgzfbsmzf/checkOrder',
    },
    'hdy3dd': {  # zs 100~5000 支付宝
        'API_KEY': '37caa44ed1c2cb6e8e0fdf38e7a8b487',
        'gateway': 'http://www.9555o.com/topay/wgzfbsmzf/pay',
        'query_gateway': 'http://api.hxx13581983709.top/topay/wgzfbsmzf/checkOrder',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _gen_sign(s):
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().lower()
    return sign


def generate_sign(parameter, key):
    s = ''
    for k in sorted(parameter.keys()):
        if k != 'sign' and parameter[k] != '':
            s += '%s=%s&' % (k, parameter[k])
    s += 'signkey=%s' % key
    _LOGGER.info("xinshunchangpay sign str: %s", s)
    return _gen_sign(s)


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_sign(params, key)
    if sign.upper() != calculated_sign.upper():
        _LOGGER.info("xinshunchangpay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def create_charge(pay, pay_amount, info):
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('appid', app_id),
        ('price', str(int(pay_amount * 100))),
        ('type', '0'),
        ('goodsname', 'charge'),
        ('down_trade_no', str(pay.id)),
        ('backurl', '{}/pay/api/{}/xinshunchangpay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("xinshunchangpay create: %s", json.dumps(parameter_dict))
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_gateway(app_id), data=parameter_dict, headers=headers, timeout=5)
    _LOGGER.info("xinshunchangpay create rsp data: %s", response.text)
    return {
        'charge_info': json.loads(response.text)['data']['qrcode'].replace('\/', '/').replace('HTTPS://QR.ALIPAY.COM/',
                                                                                              'https://qr.alipay.com/')}


# SUCCESS
def check_notify_sign(request, app_id):
    api_key = _get_api_key(app_id)
    data = dict(request.POST.iteritems())
    _LOGGER.info("xinshunchangpay notify data: %s", data)
    verify_notify_sign(data, api_key)
    pay_id = data['orderNumber']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('xinshunchangpay event does not contain pay ID')

    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = 'success'
    trade_no = ''
    total_fee = float(data['price']) / 100.0
    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    check_channel_order(pay_id, total_fee, app_id)

    # 0已支付，1未支付，其他失败
    if trade_status == 'success' and total_fee > 0.0:
        _LOGGER.info('xinshunchangpay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    api_key = _get_api_key(app_id)
    pay_id = pay_order.id
    parameter_dict = OrderedDict((
        ('appid', app_id),
        ('down_trade_no', str(pay_order.id)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info('xinshunchangpay query data, %s', parameter_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_query_gateway(app_id), data=parameter_dict, headers=headers, timeout=5)
    _LOGGER.info('xinshunchangpay query rsp, %s', response.text)
    if response.status_code == 200:
        data = json.loads(response.text)['data']
        mch_id = pay_order.mch_id
        trade_status = str(data['flag'])
        total_fee = float(data['total_fee'])
        trade_no = data['out_trade_no']

        extend = {
            'trade_status': trade_status,
            'total_fee': total_fee,
            'trade_no': trade_no,
        }

        # 1 未支付 2 已支付
        if trade_status == '2':
            check_channel_order(pay_id, total_fee, app_id)

            _LOGGER.info('xinshunchangpay query order success, mch_id:%s pay_id:%s' % (mch_id, pay_order.id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)

            async_job.notify_mch(pay_order.id)
