# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.channel.pay import check_channel_order
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils import tz
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '0c7588dc316b499bb1fc933a8675507f': {
        #  创普支付 支付宝WAP 3.2% 200-10000 dwc
        'API_KEY': 'f96e3ba91c5141cfb4dd95117cb8717e',
        'gateway': 'http://120.78.180.18/gateway/cnpPay/initPay',
        'query_gateway': 'http://120.78.180.18/gateway/query/singleOrder',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _gen_sign(s):
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().upper()
    return sign


def generate_sign(parameter, key):
    s = ''
    for k in sorted(parameter.keys()):
        if k != 'sign' and parameter[k] != '':
            s += '%s=%s&' % (k, parameter[k])
    s += 'paySecret=%s' % key
    return _gen_sign(s)


def verify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_sign(params, key)
    if sign.upper() != calculated_sign.upper():
        _LOGGER.info("chuangpupay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _get_pay_type(service):
    if service == 'alipay_scan':
        return '20000301'
    elif service == 'wechat_scan':
        return '10000101'
    elif service == 'alipay_wap':
        return '20000203'
    return '20000301'


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '219.135.56.195'


def create_charge(pay, pay_amount, info):
    service = info.get('service')
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('payKey', app_id),
        ('orderPrice', '%.2f' % pay_amount),
        ('outTradeNo', str(pay.id)),
        ('productType', _get_pay_type(service)),
        ('orderTime', tz.local_now().strftime('%Y%m%d%H%M%S')),
        ('productName', 'CPP'),
        ('orderIp', _get_device_ip(service)),
        ('returnUrl', '{}/pay/api/{}/chuangpupay/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, str(pay.id))),
        ('notifyUrl', '{}/pay/api/{}/chuangpupay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("chuangpupay create: %s, order_id is: %s", json.dumps(parameter_dict), parameter_dict['outTradeNo'])
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_gateway(app_id), data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info("chuangpupay create rsp data: %s %s", response.status_code, response.text)
    data = json.loads(response.text)
    verify_sign(data, api_key)
    return {'charge_info': data['payMessage']}


# D0商户异步通道
def check_notify_sign(request, app_id):
    api_key = _get_api_key(app_id)
    data = dict(request.GET.iteritems())
    _LOGGER.info("chuangpupay notify data: %s, order_id is: %s", data, data['outTradeNo'])
    verify_sign(data, api_key)
    pay_id = data['outTradeNo']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s", data)
        raise ParamError('chuangpupay event does not contain pay ID')
    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)
    mch_id = pay.mch_id
    trade_status = str(data['tradeStatus'])
    trade_no = data['trxNo']
    total_fee = float(data['orderPrice'])
    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }
    check_channel_order(pay_id, total_fee, app_id)
    # 订单状态: SUCCESS 交易成功 ,FINISH 交易完成 ,FAILED 交易失败 ,WAITING_PAYMENT 等待支付
    if trade_status == 'SUCCESS':
        _LOGGER.info('chuangpupay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    # prepare param
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('payKey', app_id),
        ('outTradeNo', str(pay_id)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("chuangpupay query data: %s, order_id: %s", json.dumps(parameter_dict), parameter_dict['outTradeNo'])
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_query_gateway(app_id), data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info("chuangpupay query rsp data: %s", response.text)
    verify_sign(json.loads(response.text), api_key)
    if response.status_code == 200:
        data = json.loads(response.text)
        trade_status = str(data['orderStatus'])
        trade_no = str(data['orderPrice'])
        total_fee = float(data['orderPrice'])
        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }
        # 订单状态: SUCCESS 交易成功 ,FINISH 交易完成 ,FAILED 交易失败 ,WAITING_PAYMENT 等待支付
        if trade_status == 'SUCCESS':
            _LOGGER.info('chuangpupay query order success, user_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)
            async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('chuangpupay data error, status_code: %s', response.status_code)
