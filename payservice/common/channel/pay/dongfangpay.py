# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '2118': {  # dwc 支付宝3.5% 支付宝2.8%（1---3000）Do结算
        'API_KEY': '42ac1d04b1247251d8b984e904760640',
        'gateway': 'http://www.7ypay.com/api/Payh5/pay_h5/?merchantId={}&type={}&totalAmount={}&sign={}&corp_flow_no={}&notify_url={}&return_url={}&desc={}',
        # 'query_gateway': 'http://www.7ypay.com/api/Pay/order_search',
        'query_gateway': 'http://www.7ypay.com/api/Payh5/order_s',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _gen_sign(s):
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest()
    return sign


# MD5(merchantId+"pay"+totalAmount+corp_flow_no+商户秘钥code)
def generate_sign(d, key):
    s = d['merchantId'] + 'pay' + d['totalAmount'] + d['corp_flow_no'] + key
    _LOGGER.info("dongfangpay sign str: %s", s)
    return _gen_sign(s)


# merchantId+"pay" +corp_flow_no+商户秘钥
def generate_notify_sign(d, key):
    s = d['merchantId'] + 'pay' + d['corp_flow_no'] + key
    _LOGGER.info("dongfangpay notify sign str: %s", s)
    return _gen_sign(s)


# merchantId + corp_flow_no+商户秘钥
def generate_query_sign(d, key):
    s = d['merchantId'] + d['corp_flow_no'] + key
    _LOGGER.info("dongfangpay query sign str: %s", s)
    return _gen_sign(s)


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('sign')
    calculated_sign = generate_notify_sign(params, key)
    if sign.upper() != calculated_sign.upper():
        _LOGGER.info("dongfangpay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


# 2:支付宝
def _get_pay_type(service):
    return '2'


def create_charge(pay, pay_amount, info):
    service = info.get('service')
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    d = OrderedDict((
        ('merchantId', app_id),
        ('totalAmount', '%.2f' % pay_amount),
        ('desc', 'charge'),
        ('corp_flow_no', str(pay.id)),
        ('type', _get_pay_type(service)),
        ('notify_url', '{}/pay/api/{}/dongfangpay/{}/'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('return_url',
         '{}/pay/api/{}/dongfangpay/{}/'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, str(pay.id))),
    ))
    d['sign'] = generate_sign(d, api_key)
    url = _get_gateway(app_id).format(d['merchantId'], d['type'], d['totalAmount'], d['sign'], d['corp_flow_no'],
                                      d['notify_url'], d['return_url'], d['desc'])
    _LOGGER.info("dongfangpay create: %s", url)
    return {'charge_info': url}


# 00
def check_notify_sign(request, app_id):
    _LOGGER.info("dongfangpay notify body: %s", request.body)
    api_key = _get_api_key(app_id)
    data = json.loads(request.body)
    verify_notify_sign(data, api_key)
    pay_id = data['corp_flow_no']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('dongfangpay event does not contain pay ID')

    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(data['code'])
    trade_no = ''
    total_fee = float(data['totalAmount'])
    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }
    from common.channel.pay import check_channel_order
    check_channel_order(pay_id, total_fee, app_id)
    if trade_status == '00':
        _LOGGER.info('dongfangpay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('merchantId', app_id),
        ('corp_flow_no', str(pay_order.id)),
    ))
    parameter_dict['sign'] = generate_query_sign(parameter_dict, api_key)
    _LOGGER.info('dongfangpay query data, %s', parameter_dict)
    headers = {'Content-Type': 'application/json;charset=utf-8'}
    response = requests.post(_get_query_gateway(app_id), data=json.dumps(parameter_dict), headers=headers, timeout=5)
    _LOGGER.info('dongfangpay query rsp, %s', response.text)
    if response.status_code == 200:
        data = json.loads(response.text.replace(u'\ufeff', ''))
        mch_id = pay_order.mch_id
        trade_status = str(data['Code'])
        total_fee = float(data['totalAmount'])
        trade_no = ''

        extend = {
            'trade_status': trade_status,
            'total_fee': total_fee,
            'trade_no': trade_no,
        }

        if trade_status == '00':
            _LOGGER.info('dongfangpay query order success, mch_id:%s pay_id:%s' % (mch_id, pay_order.id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)

            async_job.notify_mch(pay_order.id)
