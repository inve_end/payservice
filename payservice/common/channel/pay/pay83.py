# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict
from urllib import unquote

import requests
from django.conf import settings

from async import async_job
from common.channel.pay import check_channel_order
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    'C100583': {  # 支付宝2.6%（50---5000）D0结算 dwc
        'API_KEY': 'd23da4cd2c0a605b2ee838b2466979a6',
        'gateway': 'http://83pay.com/gateway/interface/pay',
        'query_gateway': 'http://83pay.com/gateway/query',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def _get_query_gateway(mch_id):
    return APP_CONF[mch_id]['query_gateway']


def _get_pay_type(service):
    if service == 'alipay':
        return 'ALIPAYJSAPI'


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def generate_sign(parameter, key):
    s = ''
    for k in sorted(parameter.keys()):
        s += '%s=%s&' % (k, parameter[k])
    s = s[:-1] + str(key)
    _LOGGER.info('pay83 sign : %s', s)
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().upper()
    return sign


def verify_notify_sign(params, key):
    sign = params['sign']
    params.pop('returnParam')
    params.pop('sign')
    calculated_sign = generate_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("pay83 sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def create_charge(pay, pay_amount, info):
    """ 创建订单 """
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    service = info.get('service')
    parameter_dict = OrderedDict((
        ('apiCode', 'YL-PAY'),
        ('inputCharset', 'UTF-8'),
        ('signType', 'MD5'),
        ('partner', app_id),
        ('outOrderId', str(pay.id)),
        ('amount', str(int(pay_amount))),
        ('payType', _get_pay_type(service)),
        ('notifyUrl', '{}/pay/api/{}/pay83/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("pay83 create  data: %s", json.dumps(parameter_dict))
    headers = {'Content-Type': 'application/json;charset=utf-8'}
    response = requests.post(_get_gateway(app_id), data=json.dumps(parameter_dict), headers=headers, timeout=3)
    _LOGGER.info("pay83 create  response: %s", response.text)
    return {'charge_info': json.loads(response.text)['qrCodeUrl']}


# SUCCESS
def check_notify_sign(request, app_id):
    data = json.loads(request.body)
    api_key = _get_api_key(app_id)
    _LOGGER.info("pay83 notify data: %s", data)
    verify_notify_sign(data, api_key)
    pay_id = data['outOrderId']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('pay83 event does not contain pay ID')

    pay = order_db.get_pay(pay_id)
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay83 %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = str(data['responseCode'])
    trade_no = str(data['orderCode'])
    total_fee = pay.total_fee

    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    check_channel_order(pay_id, total_fee, app_id)

    if trade_status == '000000' and int(total_fee) > 0:
        _LOGGER.info('pay83 check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    """ 查询订单 """
    pay_id = pay_order.id
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('queryCode', 'YL-QUERY'),
        ('inputCharset', 'UTF-8'),
        ('partner', str(app_id)),
        ('outOrderId', str(pay_id)),
        ('signType', 'MD5'),
    ))
    parameter_dict['sign'] = generate_sign(parameter_dict, api_key)
    _LOGGER.info("pay83 query  data: %s", parameter_dict)
    headers = {'Content-Type': 'application/json;charset=utf-8'}
    response = requests.post(_get_query_gateway(app_id), data=json.dumps(parameter_dict), headers=headers, timeout=3)
    _LOGGER.info("pay83 query  rsp data: %s", unquote(response.text))
    if response.status_code == 200:
        data = json.loads(unquote(response.text))
        trade_status = str(data['responseCode'])
        trade_no = str(data['orderCode'])
        total_fee = float(data['amount'])

        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }

        if trade_status == '0000':
            check_channel_order(pay_id, total_fee, app_id)

            _LOGGER.info('pay83 query order success, user_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                     total_fee, trade_no, extend)
            async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('pay83 data error, status_code: %s', response.status_code)
