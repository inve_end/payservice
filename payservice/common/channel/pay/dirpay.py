# -*- coding: utf-8 -*-
import hashlib
import json
import random
import urllib
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.order import db as order_db
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError
from common.utils.tz import local_now, utc_to_local

_LOGGER = track_logging.getLogger(__name__)

_GATEWAY = 'http://pay.drippayment.com/pay/wapPay.json'

APP_CONF = {
    '0000137': {
        'API_KEY': 'BB8C99ECFB9FD2AB',
    },
    '0000199': {
        'API_KEY': '373ACD54072F0C7D',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def generate_sign(parameter, key):
    s = ''
    for k in sorted(parameter.keys()):
        s += '%s=%s&' % (k, parameter[k])
    s += 'key=%s' % key
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().upper()
    return sign


def verify_notify_sign(params, key):
    sign = params['signData']
    params.pop('signData')
    calculated_sign = generate_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("dirpay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _fix_pay_amount(pay, pay_amount):
    extend = json.loads(pay.extend or '{}')
    if pay_amount >= 10 and int(pay_amount) == pay_amount:
        discount = random.randint(1, 10)
        pay_amount = pay_amount - float(discount) / 100
        extend.update({'discount': str(float(discount) / 100)})
        order_db.fill_extend(pay.id, extend)
    return pay_amount


def create_charge(pay, pay_amount, info):
    ''' 创建订单 '''
    charge_resp = {}
    app_id = info['app_id']
    api_key = _get_api_key(app_id)
    pay_amount = _fix_pay_amount(pay, pay_amount)
    parameter_dict = OrderedDict((
        ('versionId', '001'),
        ('businessType', '1800'),
        ('transChanlName', '0008'),
        ('merId', app_id),
        ('orderId', str(pay.id)[2:]),
        ('transDate', local_now().strftime('%Y%m%d%H%M%S')),
        ('transAmount', str(pay_amount)),
        ('backNotifyUrl', '{}/pay/api/{}/dirpay/{}'.format(
            settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, app_id)),
        ('backurl', '{}/pay/api/{}/dirpay/{}/'.format(
            settings.NOTIFY_PREFIX, settings.RETURN_PATH, pay.id)),
        ('orderDesc', urllib.quote(u'客服联系QQ 或者微信'.encode('gbk'))),
    ))
    parameter_dict['signData'] = generate_sign(parameter_dict, api_key)
    parameter_dict['signType'] = 'MD5'
    parameter_dict['ReqSource'] = 'AND_WAP'
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_GATEWAY, data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info("dirpay create charge data: %s %s",
                 response.status_code, response.text)
    res_obj = json.loads(response.text)
    third_id = res_obj.get('ksPayOrderId')
    order_db.fill_third_id(pay.id, third_id)
    charge_resp.update({
        'charge_info': res_obj['payUrl'].decode('gbk'),
    })
    return charge_resp


def check_notify_sign(request, app_id):
    data = dict(request.POST.iteritems())
    api_key = _get_api_key(app_id)
    _LOGGER.info("dirpay notify data: %s", data)
    verify_notify_sign(data, api_key)
    pay_id = '16' + data['orderId']
    if not pay_id:
        _LOGGER.error("fatal error, out_trade_no not exists, data: %s" % data)
        raise ParamError('dirpay event does not contain pay ID')

    pay = order_db.get_pay(int(pay_id))
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = data['refcode']
    trade_no = data['ksPayOrderId']
    total_fee = float(data['transAmount'])

    discount = float(json.loads(pay.extend or '{}').get('discount', 0))
    extend = {
        'discount': discount,
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }

    from common.channel.pay import check_channel_order
    check_channel_order(pay_id, total_fee, app_id)

    if trade_status == '00':
        _LOGGER.info('dirpay check order success, mch_id:%s pay_id:%s' % (mch_id, pay_id))
        order_db.add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    ''' 查询订单 '''
    # prepare param
    pay_id = pay_order.id
    order_id = str(pay_id)[2:]
    api_key = _get_api_key(app_id)
    parameter_dict = OrderedDict((
        ('versionId', '001'),
        ('businessType', '1801'),
        ('merId', app_id),
        ('transChanlName', '0008'),
        ('orderId', str(order_id)),
        ('transDate', utc_to_local(pay_order.updated_at).strftime('%Y%m%d%H%M%S')),
        ('ksPayOrderId', pay_order.third_id),
    ))
    parameter_dict['signData'] = generate_sign(parameter_dict, api_key)
    parameter_dict['signType'] = 'MD5'
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_GATEWAY, data=parameter_dict, headers=headers, timeout=3)
    if response.status_code == 200:
        # response text is a pure html text
        data = json.loads(response.text)
        print data
        trade_status = data['refcode']
        trade_no = data['ksPayOrderId']
        total_fee = float(data['transAmount'])

        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }

        if trade_status == '00':
            _LOGGER.info('dirpay query order success, user_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            res = order_db.add_pay_success(pay_order.mch_id, pay_order.id,
                                           total_fee, trade_no, extend)
            if res:
                # async notify
                async_job.notify_mch(pay_order.id)
            print("order process successfully. %s" % pay_order.id)
    else:
        _LOGGER.warn('dirpay data error, status_code: %s', response.status_code)


if __name__ == '__main__':
    s = 'businessType=1800&ksPayOrderId=00001997248526426133160611807232&orderDesc=%BF%CD%B7%FE%C1%AA%CF%B5QQ+%BB%F2%D5%DF%CE%A2%D0%C5&orderId=26133160611807232&refMsg=%BD%BB%D2%D7%B3%C9%B9%A6&refcode=00&transAmount=1.0&transChanlName=0008&transDate=2017-12-28 17:48:52&key=373ACD54072F0C7D'
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest().upper()
    print sign
