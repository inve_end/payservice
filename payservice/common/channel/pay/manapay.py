# -*- coding: utf-8 -*-
import hashlib
import json
from collections import OrderedDict

import requests
from django.conf import settings

from async import async_job
from common.order.db import get_pay, add_pay_success
from common.order.model import PAY_STATUS
from common.utils import track_logging
from common.utils.exceptions import ParamError

_LOGGER = track_logging.getLogger(__name__)

APP_CONF = {
    '2018126': {  # mana支付 支付宝 2.8% 100-10000 dwc
        'API_KEY': 'OirEcLChzcxmQPoKeIKpuYhOhdoGDguH',
        'gateway': 'http://www.linmana.com/Pay',
        'query_gateway': 'http://www.linmana.com/Pay',
    },
    '2019116': {  # mana支付 支付宝 2.8% 100-10000 witch
        'API_KEY': 'LXSVZlQBmkylLbJDbpmtXkwkBMbSoDAv',
        'gateway': 'http://www.linmana.com/Pay',
        'query_gateway': 'http://www.linmana.com/Pay',
    },
}


def _get_api_key(mch_id):
    return APP_CONF[mch_id]['API_KEY']


def _get_gateway(mch_id):
    return APP_CONF[mch_id]['gateway']


def generate_order_sign(d, key):
    s = d['fxid'] + d['fxddh'] + d['fxfee'] + d['fxnotifyurl'] + key
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest()
    return sign


def generate_notify_sign(d, key):
    s = d['fxstatus'] + d['fxid'] + d['fxddh'] + d['fxfee'] + key
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest()
    return sign


def generate_query_sign(d, key):
    s = d['fxid'] + d['fxddh'] + d['fxaction'] + key
    m = hashlib.md5()
    m.update(s.encode('utf8'))
    sign = m.hexdigest()
    return sign


def verify_notify_sign(params, key):
    sign = params['fxsign']
    calculated_sign = generate_notify_sign(params, key)
    if sign != calculated_sign:
        _LOGGER.info("manapay sign: %s, calculated sign: %s", sign, calculated_sign)
        raise ParamError('sign not pass, data: %s' % params)


def _get_pay_type(service):
    if service == 'alipay':
        return 'zfbewm'
        # return 'zfbewmhhh'
    return 'zfbewm'


def _get_device_ip(info):
    try:
        extra = json.loads(info['extra'])
    except:
        extra = {}
    user_info = extra.get('user_info', {})
    return user_info.get('device_ip') or '127.0.0.1'


def create_charge(pay, pay_amount, info):
    mch_id = info['app_id']
    service = info.get('service')
    key = _get_api_key(mch_id)
    parameter_dict = OrderedDict((
        ('fxid', mch_id),
        ('fxddh', str(pay.id)),
        ('fxdesc', 'charge'),
        ('fxfee', str(pay_amount)),
        ('fxnotifyurl', '{}/pay/api/{}/manapay/{}'.format(settings.NOTIFY_PREFIX, settings.NOTIFY_PATH, mch_id)),
        ('fxbackurl', '{}/pay/api/{}/manapay/{}'.format(settings.NOTIFY_PREFIX, settings.RETURN_PATH, pay.id)),
        ('fxpay', _get_pay_type(service)),
        ('fxip', _get_device_ip(info)),
    ))
    parameter_dict['fxsign'] = generate_order_sign(parameter_dict, key)
    _LOGGER.info("manapay create data: %s", json.dumps(parameter_dict))
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_gateway(mch_id), data=parameter_dict, headers=headers, timeout=3)
    _LOGGER.info("manapay create rsp: %s, %s", response.text, response.url)
    j = json.loads(response.text)
    url = j['payurl'].replace('\/', '/')
    _LOGGER.info("manapay create url: %s, order_id: %s", url, dict(parameter_dict)['fxddh'])
    return {'charge_info': url}


def check_notify_sign(request, app_id):
    key = _get_api_key(app_id)
    data = dict(request.POST.iteritems())
    _LOGGER.info("manapay notify data: %s", data)
    verify_notify_sign(data, key)
    pay_id = data['fxddh']
    if not pay_id:
        _LOGGER.error("fatal error, pay object not exists, data: %s" % data)
        raise ParamError('manapay event does not contain valid pay ID')

    pay = get_pay(pay_id)
    if not pay:
        raise ParamError('pay_id: %s invalid' % pay_id)
    if pay.status != PAY_STATUS.READY:
        raise ParamError('pay %s has been processed' % pay_id)

    mch_id = pay.mch_id
    trade_status = int(data['fxstatus'])
    trade_no = data['fxorder']
    total_fee = float(data['fxfee'])

    extend = {
        'trade_status': trade_status,
        'trade_no': trade_no,
        'total_fee': total_fee
    }
    from common.channel.pay import check_channel_order
    check_channel_order(pay_id, total_fee, app_id)
    if trade_status == 1:
        _LOGGER.info('manapay check order success, user_id:%s pay_id:%s' % (mch_id, pay_id))
        add_pay_success(mch_id, pay_id, total_fee, trade_no, extend)
        # async notify
        async_job.notify_mch(pay_id)


def query_charge(pay_order, app_id):
    pay_id = pay_order.id
    key = _get_api_key(app_id)
    p_dict = {
        'fxid': app_id,
        'fxddh': str(pay_id),
        'fxaction': 'orderquery',
    }
    p_dict['fxsign'] = generate_query_sign(p_dict, key)
    _LOGGER.info(u'manapay query data: %s', p_dict)
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    response = requests.post(_get_gateway(app_id), data=p_dict, headers=headers, timeout=3)
    _LOGGER.info(u'manapay query: %s', response.text)
    data = json.loads(response.text)
    if response.status_code == 200:
        trade_status = int(data['fxstatus'])
        total_fee = float(data['fxfee'])
        trade_no = data['fxorder']
        extend = {
            'trade_status': trade_status,
            'trade_no': trade_no,
            'total_fee': total_fee
        }

        if trade_status == 1:
            _LOGGER.info('manapay query order success, mch_id:%s pay_id:%s' % (pay_order.mch_id, pay_id))
            add_pay_success(pay_order.mch_id, pay_id, total_fee, trade_no, extend)
            async_job.notify_mch(pay_order.id)
    else:
        _LOGGER.warn('manapay data error, status_code: %s', response.status_code)
