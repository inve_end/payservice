# -*- coding: utf-8 -*-
import time
import logging

from common.cache import ProxyAgent, prefix_key, union_client
from common.utils import id_generator
from common.utils.tz import now_ts, left_seconds_today, to_ts
from common.utils.decorator import cache_wrapper

_LOGGER = logging.getLogger(__name__)
_LOCK_TIMEOUT = 10
_ACCOUNT_EXPIRE = 3600
_TREND_COUNT = 1200  # 最近1200期的趋势图数据


@cache_wrapper
def submit_timer_event(event_type, cache_value, timestamp):
    """
    used for db
    """
    key = prefix_key('timerzset:%s' % event_type)
    return ProxyAgent().zadd(key, timestamp, cache_value)


@cache_wrapper
def range_expired_events(event_type, max_time):
    """
    used for db
    """
    key = prefix_key('timerzset:%s' % event_type)
    return ProxyAgent().zrangebyscore(key, 0, max_time)


@cache_wrapper
def remove_expired_event(event_type, event_value):
    """
    used for db
    """
    key = prefix_key('timerzset:%s' % event_type)
    return ProxyAgent().zrem(key, event_value)


@cache_wrapper
def timer_event_processed(event_id):
    """
    used for db
    """
    key = prefix_key('timerlock:%s' % event_id)
    return not ProxyAgent().setnx(key, int(time.time()))


@cache_wrapper
def save_html(pay_id, html_text):
    cache_id = id_generator.generate_uuid('{}{}'.format(pay_id, int(time.time())))
    key = prefix_key('chargehtml:%s' % cache_id)
    union_client.setex(key, 300, html_text)
    return cache_id


@cache_wrapper
def get_cache_html(cache_id):
    key = prefix_key('chargehtml:%s' % cache_id)
    return union_client.get(key)


@cache_wrapper
def save_last_order_ts(mch_id, user_id):
    key = prefix_key('lastorder:%s:%s' % (mch_id, user_id))
    return ProxyAgent().setex(key, 3600, int(time.time()))


@cache_wrapper
def get_last_order_ts(mch_id, user_id):
    key = prefix_key('lastorder:%s:%s' % (mch_id, user_id))
    return int(ProxyAgent().get(key) or 0)


@cache_wrapper
def get_chn_amount(chn_id):
    daily_key = prefix_key('dailychn:%s:stats' % (chn_id))
    hourly_key = prefix_key('hourlychn:%s:stats' % (chn_id))
    minutely_key = prefix_key('minutelychn:%s:stats' % (chn_id))
    daily_stats = ProxyAgent().hgetall(daily_key) or {}
    hourly_stats = ProxyAgent().hgetall(hourly_key) or {}
    minutely_stats = ProxyAgent().hgetall(minutely_key) or {}
    return daily_stats, hourly_stats, minutely_stats


@cache_wrapper
def add_chn_amount(chn_id, amount):
    # todo 过期逻辑有bug，可能导致redis中的键不过期
    amount = float(amount)
    daily_key = prefix_key('dailychn:%s:stats' % (chn_id))
    ttl = left_seconds_today()
    ProxyAgent().hincrby(daily_key, 'amount', int(amount))
    ProxyAgent().hincrby(daily_key, 'count', 1)
    ProxyAgent().expire(daily_key, ttl)
    hourly_key = prefix_key('hourlychn:%s:stats' % (chn_id))
    exists = ProxyAgent().exists(hourly_key)
    ProxyAgent().hincrby(hourly_key, 'amount', int(amount))
    ProxyAgent().hincrby(hourly_key, 'count', 1)
    if not exists:
        ProxyAgent().expire(hourly_key, 3600)
    else:
        if ProxyAgent().ttl(hourly_key) == -1:
            # ProxyAgent().expire(hourly_key, 3600)
            ProxyAgent().expire(hourly_key, 60)
    minutely_key = prefix_key('minutelychn:%s:stats' % (chn_id))
    exists = ProxyAgent().exists(minutely_key)
    ProxyAgent().hincrby(minutely_key, 'amount', int(amount))
    ProxyAgent().hincrby(minutely_key, 'count', 1)
    if not exists:
        ProxyAgent().expire(minutely_key, 60)
    else:
        if ProxyAgent().ttl(minutely_key) == -1:
            ProxyAgent().expire(minutely_key, 60)


@cache_wrapper
def set_last_monitor_id(pay_id):
    key = prefix_key('last_monitor_id')
    union_client.set(key, pay_id)


@cache_wrapper
def get_last_monitor_id():
    key = prefix_key('last_monitor_id')
    return long(union_client.get(key) or 0)


@cache_wrapper
def set_chn_last_pay(chn_id, payed_at):
    key = prefix_key('chn_last_pay')
    union_client.hset(key, chn_id, to_ts(payed_at))


@cache_wrapper
def get_chn_last_pay(chn_id):
    key = prefix_key('chn_last_pay')
    return int(union_client.hget(key, chn_id) or 0)


@cache_wrapper
def get_withdraw_status(payer_no):
    """
    used for db
    """
    key = prefix_key('withdrawstatus:{}'.format(payer_no))
    return ProxyAgent().get(key)


@cache_wrapper
def set_withdraw_status(payer_no, status):
    """
    used for db
    """
    key = prefix_key('withdrawstatus:{}'.format(payer_no))
    return ProxyAgent().set(key, status)


@cache_wrapper
def add_user_pay_count(mch_id, user_id):
    key = prefix_key('recharge_stats:{}:{}'.format(mch_id, user_id))
    ProxyAgent().hincrby(key, 'count', 1)
    ProxyAgent().expire(key, 360)


@cache_wrapper
def get_user_pay_count(mch_id=None, user_id=None):
    if mch_id and user_id:
        key = prefix_key('recharge_stats:{}:{}'.format(mch_id, user_id))
        return ProxyAgent().hget(key, 'count')
    dct = {}
    scan_key = prefix_key('recharge_stats:*')
    keys = ProxyAgent().keys(scan_key)
    for key in keys:
        dct.update({
            key.replace('pay:recharge_stats:', ''): ProxyAgent().hget(key, 'count')
        })
    return dct


@cache_wrapper
def remove_user_pay_count(mch_id, user_id):
    key = prefix_key('recharge_stats:{}:{}'.format(mch_id, user_id))
    return ProxyAgent().hdel(key, 'count')


@cache_wrapper
def set_recharge_risk(mch_id, user_id, ttl):
    key = prefix_key('recharge_risk:{}:{}'.format(mch_id, user_id))
    return ProxyAgent().setex(key, ttl, int(time.time()))


@cache_wrapper
def get_recharge_risk(mch_id, user_id):
    if not mch_id or not user_id:
        return False
    key = prefix_key('recharge_risk:{}:{}'.format(mch_id, user_id))
    return int(ProxyAgent().get(key) or 0)


@cache_wrapper
def add_minutes_chn_count(chn_id):
    minutely_key = prefix_key('minutes:%s:stats' % chn_id)
    exists = ProxyAgent().exists(minutely_key)
    ProxyAgent().hincrby(minutely_key, 'count', 1)
    if not exists:
        ProxyAgent().expire(minutely_key, 60)
    else:
        if ProxyAgent().ttl(minutely_key) == -1:
            ProxyAgent().expire(minutely_key, 60)


@cache_wrapper
def get_minutes_chn_count(chn_id):
    minutely_key = prefix_key('minutes:%s:stats' % chn_id)
    minutely_stats = ProxyAgent().hgetall(minutely_key) or {}
    return minutely_stats


@cache_wrapper
def set_recharge_daily(mch_id, user_id):
    key = prefix_key('daily_recharge::{}:{}'.format(mch_id, user_id))
    exists = ProxyAgent().exists(key)
    ProxyAgent().hincrby(key, 'count', 1)
    if not exists:
        ProxyAgent().expire(key, 86400)


@cache_wrapper
def get_recharge_daily(mch_id, user_id):
    key = prefix_key('daily_recharge::{}:{}'.format(mch_id, user_id))
    count = ProxyAgent().hgetall(key) or {}
    return count


@cache_wrapper
def set_recharge_record(mch_id, user_id, ttl):
    key = prefix_key('recharge_record:{}:{}'.format(mch_id, user_id))
    return ProxyAgent().setex(key, ttl, int(time.time()))


@cache_wrapper
def get_recharge_record(mch_id, user_id):
    key = prefix_key('recharge_record:{}:{}'.format(mch_id, user_id))
    return int(ProxyAgent().get(key) or 0)
