# -*- coding: utf-8 -*-
from common import orm
from common.utils.types import Enum

MCH_TYPE = Enum({
    "WITCH": (6001000, u"万壕"),
    "KS": (6001011, u"凯萨"),
    "LOKI": (6001015, u"幸运"),
    "ZS": (6001017, u"战神"),
    "DBL": (6001019, u"大波罗"),
    "RM": (6001020, u"RM"),
    "SP": (6001021, u"SP"),
    "TT": (6001022, u"泰坦"),
})


class MchAccount(orm.Model):
    """
    商户账号
    """
    __tablename__ = "mch_account"
    id = orm.Column(orm.Integer, primary_key=True, autoincrement=True)
    name = orm.Column(orm.VARCHAR)           # 商户名称
    password = orm.Column(orm.VARCHAR)       # 商户密码，MD5+salt 
    api_key = orm.Column(orm.VARCHAR)        # 商户秘钥
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)


class MchChannel(orm.Model):
    """
    商户通道配置
    """
    __tablename__ = "mch_channel"
    id = orm.Column(orm.Integer, primary_key=True, autoincrement=True)
    mch_id = orm.Column(orm.Integer)         # 商户id
    service_name = orm.Column(orm.VARCHAR)   # 支付名称(兼容兑换)
    chns = orm.Column(orm.VARCHAR)           # chn列表，逗号分隔
    notify_prefix = orm.Column(orm.VARCHAR)  # 回调host 
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
