# -*- coding: utf-8 -*-
from common import orm
from common.utils.types import Enum


STRATEGY_STATUS = Enum({
    "STOP": (0L, u"未启用"),
    "START": (1L, u"已启用"),
})


class RiskStrategy(orm.Model):
    """
    风控策略
    """
    __tablename__ = "risk_strategy"
    id = orm.Column(orm.Integer, primary_key=True, autoincrement=True)
    title = orm.Column(orm.VARCHAR)
    mch_id = orm.Column(orm.Integer)
    chns = orm.Column(orm.TEXT)
    content = orm.Column(orm.TEXT) # 策略描述，json描述
    status = orm.Column(orm.Integer)         # 状态
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
