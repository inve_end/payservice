# -*- coding: utf-8 -*-
from common.strategy.model import *
from common.utils import track_logging
from common.utils.decorator import sql_wrapper

_LOGGER = track_logging.getLogger(__name__)


@sql_wrapper
def get_enable_strategy(mch_id, chn):
    query = RiskStrategy.query.filter(RiskStrategy.mch_id == mch_id).filter(
        RiskStrategy.status == STRATEGY_STATUS.START)
    if chn:
        s = '%,' + chn + ',%'
        q1 = query.filter(RiskStrategy.chns.like(s)).first()
        if q1:
            return q1
    query = query.filter(RiskStrategy.chns.in_((None, '')))
    return query.first()
