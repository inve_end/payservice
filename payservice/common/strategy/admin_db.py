# coding=utf-8

from common.strategy.model import RiskStrategy
from common.utils import track_logging
from common.utils.db import list_object, get, upsert, delete
from common.utils.decorator import sql_wrapper

_LOGGER = track_logging.getLogger(__name__)


@sql_wrapper
def get_strategy(id):
    return get(RiskStrategy, id)


@sql_wrapper
def upsert_strategy(info, id=None):
    return upsert(RiskStrategy, info, id)


@sql_wrapper
def list_strategy(query_dct):
    return list_object(query_dct, RiskStrategy)


@sql_wrapper
def delete_strategy(id):
    delete(RiskStrategy, id)
