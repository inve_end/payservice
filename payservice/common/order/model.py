# -*- coding: utf-8 -*-
from common import orm
from common.utils.types import Enum


PAY_STATUS = Enum({
    "READY": (0L, u"未支付"),
    "FAIL": (1L, u"支付失败"),
    "SUCC": (2L, u"支付成功"),
})


NOTIFY_STATUS = Enum({
    "READY": (0L, "ready to notify"),
    "FAIL": (1L, "notify failed"),
    "SUCC": (2L, "notify successed"),
})


class PayOrder(orm.Model):
    """
    支付订单
    """
    __tablename__ = "pay_order"
    id = orm.Column(orm.BigInteger, primary_key=True, autoincrement=False)
    mch_id = orm.Column(orm.Integer)
    service = orm.Column(orm.VARCHAR)        # 支付服务名称WECHAT/ALIPAY
    sdk_version = orm.Column(orm.VARCHAR)        # 支付版本号
    channel_id = orm.Column(orm.Integer)     # 支付通道id
    channel_type = orm.Column(orm.Integer)   # 支付通道类型-->pay_type
    out_trade_no = orm.Column(orm.VARCHAR)# 商户订单号
    body = orm.Column(orm.VARCHAR)           # 商品描述
    application_amount = orm.Column(orm.FLOAT)
    total_fee = orm.Column(orm.FLOAT)      # 交易金额
    order_inquiry_status = orm.Column(orm.Integer)  # 申诉状态
    status = orm.Column(orm.Integer)         # 交易状态
    notify_url = orm.Column(orm.VARCHAR)     # 通知url
    mch_create_ip = orm.Column(orm.VARCHAR)  # 发起支付的机器ip
    sign = orm.Column(orm.VARCHAR)           # 签名
    extra = orm.Column(orm.TEXT)             # 附加信息
    extend = orm.Column(orm.TEXT)            # 交易完成详情
    third_id = orm.Column(orm.VARCHAR)       # 上游支付交易号
    payed_at = orm.Column(orm.DATETIME)      # 支付完成时间
    notified_at = orm.Column(orm.DATETIME)   # 回调时间
    notify_status = orm.Column(orm.Integer)  # 回调状态
    notify_count = orm.Column(orm.Integer, default=0)   # 回调次数
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
    user_id = orm.Column(orm.BigInteger)


class ManualOrder(orm.Model):
    """
    补单订单
    """
    __tablename__ = "manual_order"
    id = orm.Column(orm.BigInteger, primary_key=True, autoincrement=False)
    mch_id = orm.Column(orm.Integer)
    service = orm.Column(orm.VARCHAR)        # 支付服务名称WECHAT/ALIPAY
    channel_id = orm.Column(orm.Integer)     # 支付通道id
    out_trade_no = orm.Column(orm.VARCHAR)# 商户订单号
    total_fee = orm.Column(orm.FLOAT)      # 交易金额
    status = orm.Column(orm.Integer)         # 交易状态
    payed_at = orm.Column(orm.DATETIME)      # 支付完成时间
    notified_at = orm.Column(orm.DATETIME)   # 回调时间
    notify_status = orm.Column(orm.Integer)  # 回调状态
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
    user_id = orm.Column(orm.BigInteger)
    operator_id = orm.Column(orm.Integer)
    operator = orm.Column(orm.VARCHAR)
    reason = orm.Column(orm.VARCHAR)
