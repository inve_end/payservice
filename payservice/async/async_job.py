# -*- coding: utf-8 -*-
from __future__ import absolute_import

import logging

import common.timer.handler
from async.celery import app
from common.export.handler import process_export, process_export_reconciliation
from common.order import db as order_db

_LOGGER = logging.getLogger(__name__)


@app.task(name='common.notify_mch')
def notify_mch(pay_id):
    _LOGGER.info('to process async job notify_mch, pay[%s]', pay_id)
    pay = order_db.get_pay(pay_id)
    common.timer.handler.submit_notify_event(pay)
    # success, pay = mch_handler.notify_mch(pay)
    # if not success:
    #     # start timer to notify again
    #     common.timer.handler.submit_notify_event(pay)


@app.task(name='common.export_data')
def export_data(export_id):
    _LOGGER.info('to process async job export_data, id[%s]', export_id)
    process_export(export_id)


@app.task(name='common.export_data_reconciliation')
def export_data_reconciliation(export_id):
    _LOGGER.info('to process async job export_data_reconciliation, id[%s]', export_id)
    process_export_reconciliation(export_id)
