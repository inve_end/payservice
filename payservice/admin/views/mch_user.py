# -*- coding: utf-8 -*-

import json

from django.utils.decorators import method_decorator
from django.utils.encoding import smart_unicode
from django.views.generic import TemplateView

from common.admin import db as user_db
from common.level_strategy import db as level_db
from common.utils import track_logging
from common.utils.api import token_required
from common.utils.decorator import response_wrapper
from common.utils.tz import utc_to_local_str
from common.utils.api import get_location

_LOGGER = track_logging.getLogger(__name__)


class MchUserView(TemplateView):
    def get(self, req):
        query_dct = req.GET.dict()
        cond = user_db.get_mchids_filter_by_user(req.user_id)
        if cond and not query_dct.get('mch_id', ''):
            query_dct['mch_id'] = cond
        _LOGGER.info('MchUserView get mchs: %s, ', query_dct)

        items, total_count = level_db.list_mch_user(query_dct)

        resp_items = []
        for item in items:
            data = item.as_dict()
            data['created_at'] = utc_to_local_str(data['created_at'])
            data['updated_at'] = utc_to_local_str(data['updated_at'])
            if data['device_ip']:
                data['location'] = get_location(data['device_ip'])
            else:
                data['location'] = ''
            resp_items.append(data)

        return {'list': resp_items, 'page': query_dct.get('$page', 1),
                'size': len(resp_items), 'total_count': total_count}

    def post(self, req):
        param_dct = json.loads(req.body)
        level_db.upsert_mch_user(param_dct)
        return {}

    def put(self, req):
        return self.post(req)

    @method_decorator(response_wrapper)
    @method_decorator(token_required)
    def dispatch(self, *args, **kwargs):
        return super(MchUserView, self).dispatch(*args, **kwargs)


class SingleMchUserView(TemplateView):
    def get(self, req, id):
        mch_user = level_db.get_mch_user(id=int(id))
        data = mch_user.as_dict()
        data['created_at'] = utc_to_local_str(data['created_at'])
        data['updated_at'] = utc_to_local_str(data['updated_at'])
        return data

    def post(self, req, id):
        return self.put(req, id)

    def put(self, req, id):
        query_dct = json.loads(smart_unicode(req.body))
        level_db.upsert_mch_user(query_dct, int(id))
        return {}

    def patch(self, req, id):
        query_dct = json.loads(smart_unicode(req.body))
        level_db.upsert_mch_user(query_dct, int(id))
        return {}

    def delete(self, req, id):
        level_db.delete_mch_user(int(id))
        _LOGGER.info('delete channel: %s, user: %s', id, req.user_id)
        return {}

    @method_decorator(response_wrapper)
    @method_decorator(token_required)
    def dispatch(self, *args, **kwargs):
        return super(SingleMchUserView, self).dispatch(*args, **kwargs)
