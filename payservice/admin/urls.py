# coding=utf-8
from django.conf.urls import patterns, url

from admin.views import chase as chase_view
from admin.views.agentpay import AgentPayOrderView, checked_pay_agent, card_num_list
from admin.views.bankcard import BankCardView, SingleBankCardView
from admin.views.channel import ChannelView, SingleChannelView, set_test_channel, get_channel_balance
from admin.views.mch import MchView, SingleMchView, get_mch_detail, modify_mch
from admin.views.mch_user import MchUserView, SingleMchUserView
from admin.views.order import PayOrderView, SinglePayOrderView, RepeatOrderView
from admin.views.order import handle_repeat_order
from admin.views.order_inquiry import OrderInquiryView, SingleOrderInquiryView
from admin.views.settlement import SettlementView, SingleSettlementView
from admin.views.settlement_order import SettlementOrderView, SingleSettlementOrderView, settlementOrderCreate
from admin.views.stat import get_channel_daily_point_pie, get_channel_hourly_point_pie, get_channel_daily_point, get_single_channel_daily_point, \
    get_mch_hourly_point, get_single_channel_hourly_point, get_mg_channel_day_detail
from admin.views.stat import get_channel_statistic_list, get_reconciliation_statement
from admin.views.strategy import StrategyView, SingleStrategyView
from admin.views.user import (UserView, SingleUserView, PermissionView,
                              SinglePermissionView, login, logout, create_role, get_role_list,
                              list_perm_mod, list_perm_tree, update_role_info, delete_user,
                              get_user_list, update_user_info, get_user_info, get_role,
                              delete_role, list_perm, list_records)
from admin.views.user_level import UserLevelView, SingleUserLevelView
from admin.views.user_level_strategy import UserLevelStrategyView, SingleUserLevelStrategyView, UserLevelStrategyCreate
from admin.views.withdraw import (WithdrawOrderView, SingleWithdrawOrderView,
                                  WithdrawChannelView, SingleWithdrawChannelView)

urlpatterns = patterns(
    '',
    # user
    url(r'^user/login/?$', login),
    url(r'^user/logout/?$', logout),
    url(r'^user/?$', UserView.as_view()),
    url(r'^user/(?P<user_id>\d+)/?$', SingleUserView.as_view()),
    url(r'^permission/?$', PermissionView.as_view()),
    url(r'^permission/(?P<perm_id>\d+)/?$', SinglePermissionView.as_view()),
    url(r'^record/?$', 'admin.views.user.list_records'),
    # mch
    url(r'^mch/?$', MchView.as_view()),
    url(r'^mch/(?P<mch_id>\d+)/?$', SingleMchView.as_view()),
    url(r'^mch/get_mch_detail/(?P<mch_id>\d+)/?$', get_mch_detail),
    url(r'^mch/modify_mch/(?P<mch_id>\d+)/?$', modify_mch),
    # channel
    url(r'^channel/?$', ChannelView.as_view()),
    url(r'^channel/(?P<chn_id>\d+)/?$', SingleChannelView.as_view()),

    # 统计
    url(r'^stat/?$', get_channel_statistic_list),

    # reconciliation
    url(r'^reconciliation/?$', get_reconciliation_statement),

    # 通道数据获取
    url(r'^stat/get_channel_daily_points/?$', get_channel_daily_point),
    url(r'^stat/get_single_channel_daily_point/?$', get_single_channel_daily_point),
    url(r'^stat/get_mch_hourly_point/?$', get_mch_hourly_point),
    url(r'^stat/get_single_channel_hourly_point/?$', get_single_channel_hourly_point),
    url(r'^stat/get_mg_channel_detail/?$', get_mg_channel_day_detail),
    url(r'^stat/get_channel_daily_points_pie/?$', get_channel_daily_point_pie),
    url(r'^stat/get_channel_hourly_points_pie/?$', get_channel_hourly_point_pie),

    # strategy
    url(r'^strategy/?$', StrategyView.as_view()),
    url(r'^strategy/(?P<strategy_id>\d+)/?$', SingleStrategyView.as_view()),
    # order
    url(r'^order/pay/?$', PayOrderView.as_view()),
    url(r'^order/pay/(?P<order_id>\d+)/?$', SinglePayOrderView.as_view()),
    url(r'^order/fresh/(?P<order_id>\d+)/?$', 'admin.views.order.fresh'),
    url(r'^order/make_success/(?P<order_id>\d+)/?$', 'admin.views.order.make_success'),  # 未支付订单 上分
    url(r'^get_export_status/?$', 'admin.views.order.get_export_status'),  # 查询订单导出状态
    url(r'^get_manual_orders/?$', 'admin.views.order.get_manual_orders'),  # 查询补单订单
    url(r'^get_repeat_orders/?$', RepeatOrderView.as_view()),  # 查询重复订单(支付宝扫描)
    url(r'^handle_repeat_order/?$', handle_repeat_order),  # 处理重复支付订单

    # order inquiry
    url(r'^order/inquiry/?$', OrderInquiryView.as_view()),
    url(r'^order/inquiry/(?P<order_inquiry_id>\d+)/?$', SingleOrderInquiryView.as_view()),

    # withdraw channel
    url(r'^withdraw/channel/?$', WithdrawChannelView.as_view()),
    url(r'^withdraw/channel/(?P<chn_id>\d+)/?$', SingleWithdrawChannelView.as_view()),
    url(r'^withdraw/channel/charge/(?P<chn_id>\d+)/?$', 'admin.views.withdraw.channel_charge'),
    # withdraw order
    url(r'^withdraw/order/?$', WithdrawOrderView.as_view()),
    url(r'^withdraw/order/(?P<order_id>\d+)/?$', SingleWithdrawOrderView.as_view()),
    url(r'^withdraw/order/fresh/(?P<order_id>\d+)/?$', 'admin.views.withdraw.fresh'),
    # for chn info
    url(r"^chns/?$", "admin.views.channel.get_chns"),

    # 商户用户分级
    url(r'^level/user/?$', MchUserView.as_view()),
    url(r'^level/user/(?P<id>\d+)/?$', SingleMchUserView.as_view()),
    url(r'^level/strategy/?$', UserLevelStrategyView.as_view()),
    url(r'^level/strategy/(?P<id>\d+)/?$', SingleUserLevelStrategyView.as_view()),
    url(r'^level/strategy_level/?$', UserLevelView.as_view()),
    url(r'^level/strategy_level/(?P<id>\d+)/?$', SingleUserLevelView.as_view()),
    url(r'^level/strategy/create/?$', UserLevelStrategyCreate),

    # 通道代付结算
    url(r'^settlement/bankcard/?$', BankCardView.as_view()),
    url(r'^settlement/bankcard/(?P<id>\d+)/?$', SingleBankCardView.as_view()),
    url(r'^settlement/channel/?$', SettlementView.as_view()),
    url(r'^settlement/channel/(?P<id>\d+)/?$', SingleSettlementView.as_view()),
    url(r'^settlement/order/?$', SettlementOrderView.as_view()),
    url(r'^settlement/order/(?P<id>\d+)/?$', SingleSettlementOrderView.as_view()),
    url(r'^settlement/createSettle/?$', settlementOrderCreate),

    # user management
    url(r'^manage/get_user_list/?$', get_user_list),  # 获取用户列表
    url(r'^manage/update_user_info/(?P<user_id>\d+)/?$', update_user_info),  # 修改用户信息[他人]
    url(r'^manage/get_user_info/(?P<user_id>\d+)/?$', get_user_info),  # 获取用户信息[他人]
    url(r'^manage/delete_user/(?P<user_id>\d+)/?$', delete_user),  # 删除用户
    url(r'^manage/get_role_list/?$', get_role_list),  # 获取角色列表
    url(r'^manage/update_role_info/(?P<role_id>\d+)/?$', update_role_info),  # 更新角色
    url(r'^manage/create_role/?$', create_role),  # 创建角色
    url(r'^manage/get_role/(?P<role_id>\d+)/?$', get_role),  # 获取角色详情
    url(r'^manage/delete_role/(?P<role_id>\d+)/?$', delete_role),  # 删除角色
    url(r'^manage/get_perm_list/?$', list_perm),  # 获取权限列表
    url(r'^manage/get_perm_mod/?$', list_perm_mod),  # 获取权限大模块列表
    url(r'^manage/get_perm_tree/?$', list_perm_tree),  # 获取权限列表[树结构]
    url(r'^manage/record/?$', list_records),  # 查看操作记录

    url(r'^chase_order/?$', chase_view.create_chase_order),  # 创建追分订
    url(r'^chase_order/list/?$', chase_view.get_order_list),  # 获取追分订单列表
    url(r'^chase_order/review/(?P<order_id>\d+)/?$', chase_view.review_order),  # 审核订单

    url(r'^set_test_channel/?$', set_test_channel),  # 设置测试通道

    url(r'^get_channel_balance/?$', get_channel_balance),  # 查询商户余额

    # 代付
    url(r'^channel/agentpay/?$', AgentPayOrderView.as_view()),
    url(r'^channel/agentpay/checked/?$', checked_pay_agent),
    url(r'^channel/agentpay/card_num/?$', card_num_list),
    url(r'^channel/fresh/(?P<order_id>\d+)/?$', 'admin.views.agentpay.fresh'),
)
