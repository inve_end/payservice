# coding=utf-8
DEBUG = False

CORS_ORIGIN_ALLOW_ALL = True

MYSQL_CONF = {
    'db': 'mysql://bigbang:n0ra@wang@103.230.243.167:3306/pay?charset=utf8',
    'DEBUG': DEBUG
}

SLAVE_CONF = MYSQL_CONF

ADMIN_CONF = {
    'db': 'mysql://bigbang:n0ra@wang@103.230.243.167:3306/pay_admin_new?charset=utf8',
    'DEBUG': DEBUG
}

ENABLE_CODIS = False

REDIS_HOST = '127.0.0.1'
REDIS_PORT = 6379

# 本机ip
NOTIFY_PREFIX = 'http://pay.xyz115.com:9999'

PAY_CACHE_URL = 'http://pay.xyz115.com:9999/pay/api/cache/'

APP_OPEN_URL = 'cp://open/home?index=0'

MONGO_ADDR = '103.230.243.167:27017'

GEOLITE_CITY_DB = '/home/ubuntu/af-env/GeoLite2-City.mmdb'
