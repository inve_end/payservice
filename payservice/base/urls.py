from django.conf.urls import patterns, include, url
from api.notify import view as notify


urlpatterns = patterns(
    '',
    url(r'^pay/api/', include('api.urls')),
    url(r'^pay/admin/', include('admin.urls')),
    url(r'notify/?$', notify.our_alipay_notify),
)
