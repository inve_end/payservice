# coding=utf-8
DEBUG = False

CORS_ORIGIN_ALLOW_ALL = True

MYSQL_CONF = {
    'db': 'mysql://root:P@55word@127.0.0.1:3306/pay?charset=utf8',
    'DEBUG': DEBUG
}

SLAVE_CONF = MYSQL_CONF

ADMIN_CONF = {
    'db': 'mysql://root:P@55word@127.0.0.1:3306/pay_admin?charset=utf8',
    'DEBUG': DEBUG
}

ENABLE_CODIS = False

REDIS_HOST = '127.0.0.1'
REDIS_PORT = 6379

# 本机ip
NOTIFY_PREFIX = 'http://219.135.56.195:9003'

PAY_CACHE_URL = 'http://195.testxyz115.com:9003/pay/api/cache/'

GEOLITE_CITY_DB = '/home/ubuntu/af-env/GeoLite2-City.mmdb'

IS_PRODUCTION_ENV = False
