# coding=utf-8
DEBUG = False

CORS_ORIGIN_ALLOW_ALL = True

MYSQL_CONF = {
    'db': 'mysql://bigbang:n0ra@wang@103.230.243.167:3306/pay?charset=utf8',
    'DEBUG': DEBUG
}

SLAVE_CONF = MYSQL_CONF

ADMIN_CONF = {
    'db': 'mysql://bigbang:n0ra@wang@103.230.243.167:3306/pay_admin?charset=utf8',
    'DEBUG': DEBUG
}

ENABLE_CODIS = False

REDIS_HOST = '127.0.0.1'
REDIS_PORT = 6379

# 本机ip
NOTIFY_PREFIX = 'http://183.60.109.220:9003'

PAY_CACHE_URL = 'http://183.60.109.220:9003/pay/api/cache/'

RETURN_PATH = 'return_url'

APP_OPEN_URL = 'xingyuncp://open/home?index=0'

MONGO_ADDR = '103.230.243.167:27017'
