DEBUG = False

CORS_ORIGIN_ALLOW_ALL = True

MYSQL_CONF = {
    'db': 'mysql://lucky:P@55word@192.168.20.65:3306/pay?charset=utf8',
    'DEBUG': False
}

SLAVE_CONF = MYSQL_CONF

ADMIN_CONF = {
    'db': 'mysql://lucky:P@55word@192.168.20.65:3306/pay_admin?charset=utf8',
    'DEBUG': False
}

ENABLE_CODIS = False

REDIS_HOST = '192.168.20.65'
REDIS_PORT = 6379
